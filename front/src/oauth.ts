export const OAuthSettings = {
    appId: 'ccc3d401-b2ad-4f79-9198-9776e7170192',
    scopes: [
      "user.read",
      "calendars.read",
      "Files.ReadWrite.All", 
      "Sites.ReadWrite.All"
    ]
};