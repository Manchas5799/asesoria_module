-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 24-10-2019 a las 17:57:25
-- Versión del servidor: 10.2.27-MariaDB-cll-lve
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `asttrid_slisi`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `Antes` ()  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT(" ");
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `Asistencia` (IN `id` VARCHAR(150), IN `mes` VARCHAR(150))  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("select DATE_FORMAT(entrada,'%a %d %h:%i %p')as entrada, DATE_FORMAT(salida,'%a %d %h:%i %p') as salida from asistencias where users_id = ",id," and MONTH(entrada) = ",mes);
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `Band` (IN `id` VARCHAR(150))  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("SELECT id,salida FROM asistencias where users_id = ",id," ORDER BY id DESC LIMIT 0, 1");
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `Buscar` (IN `tabla` VARCHAR(150))  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("SELECT * FROM ",tabla);
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `Buscarcredito` (IN `dni` VARCHAR(150))  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("SELECT id, DATE_FORMAT(fecha,'%d %b %Y %h:%i %p') as fecha ,total , usuario FROM ventas where estado='Credito' and dni=",dni," ORDER BY fecha DESC");
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `Buscarpago` (IN `id` VARCHAR(150))  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("SELECT DATE_FORMAT(fecha,'%d %b %Y %h:%i %p') as fecha ,monto , usuario FROM pagos where credito_id=",id," ORDER BY fecha DESC");
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `BuscarProducto` ()  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("SELECT p.id, p.codigo, p.nombre,p.marca,p.color,p.caracteristica,p.stock,p.stock_minimo as smin,p.precioVenta, t.nombre as tipo FROM productos as p,tipoproductos as t where p.tipoproducto_id = t.id");
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `Carrito` (IN `codigo` VARCHAR(150))  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("SELECT * FROM productos where codigo='", codigo,"'");
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `detalle` (IN `dni` INT(150), IN `documento` VARCHAR(150), IN `cliente` VARCHAR(250), IN `direccion` TEXT, IN `total` DECIMAL(12,2), IN `usuario` VARCHAR(150), IN `estado` VARCHAR(150), IN `deudo` VARCHAR(2), IN `serie` VARCHAR(20))  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("insert into ventas values(null,",dni,",'",documento,"','",cliente,"','",direccion,"',",total,",NOW(),'",usuario,"','",estado,"','",deudo,"','",serie,"')");
	PREPARE stmt FROM @q;
	EXECUTE stmt;
    SELECT LAST_INSERT_ID();
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `DetalleProducto` (IN `id` VARCHAR(150))  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("SELECT p.codigo, p.nombre,p.marca,p.color,p.caracteristica,p.stock,p.stock_minimo as smin,p.precioVenta,p.precioMayor,p.precioCompra, t.nombre as tipo ,pr.nombre as proveedor FROM productos as p,tipoproductos as t, proveedors as pr where (p.tipoproducto_id = t.id and p.proveedors_id = pr.id) and p.id=", id);
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `detalleVen` (IN `id` VARCHAR(150))  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("SELECT d.cantidad,CONCAT(p.nombre, ' ', p.marca)as producto,p.precioVenta,(d.cantidad * p.precioVenta )as subtotal  FROM detalleventas as d, productos as p where p.id = d.producto_id  and d.venta_id=", id);
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `Entrada` (IN `id` VARCHAR(150))  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("insert into asistencias values(null,NOW(),null,",id,")");
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `getSerie` (IN `doc` VARCHAR(3))  NO SQL
BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("select count(*) as nr from ventas as v where v.documento =",doc);
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `hventas` (IN `tabla` VARCHAR(150), IN `fecha` DATE, IN `condicion` VARCHAR(150))  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("SELECT *FROM ",tabla," where (date(fecha) LIKE '",fecha,"') and estado='",condicion,"'");
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `InsertRegistro` (IN `accion` VARCHAR(150), IN `descripcion` VARCHAR(150), IN `producto_id` INT(5), IN `cantidad` INT(8))  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("insert into registros values(null,'",accion,"','",descripcion,"',",producto_id,",",cantidad,",NOW())");
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `pagoCredito` (IN `monto` DECIMAL(12,2), IN `usuario` VARCHAR(150), IN `credito_id` VARCHAR(150))  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("insert into pagos values(null,NOW(),",monto,",'",usuario,"',",credito_id,")");
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `reportBarras` (IN `inicio` DATE, IN `fin` DATE)  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("SELECT DATE_FORMAT(fecha,'%W %d') as fecha , SUM(total) as total FROM ventas where (date(fecha) >='",inicio,"')  AND (date(fecha) <= '",fin,"') group by day(fecha);");
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `reporteBasico` (IN `estado` VARCHAR(10), IN `fecha1` VARCHAR(80), IN `fecha2` VARCHAR(80))  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("select SUM(total) as ganancia ,count(id) as despachos from ventas where estado = '", estado ,"' and  fecha BETWEEN '", fecha1 ,"' AND '",fecha2,"'");
    PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `reporteRegistro` (IN `mes` INT(3), IN `ano` INT(4))  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("select * from registros where month(fecha)=",mes," and year(fecha)=",ano);
    PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

CREATE DEFINER=`asttrid`@`localhost` PROCEDURE `Salida` (IN `id` VARCHAR(150))  BEGIN
	DECLARE q VARCHAR(255);
	SET @q=CONCAT("UPDATE asistencias SET salida= NOW() WHERE id =",id);
	PREPARE stmt FROM @q;
	EXECUTE stmt;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencias`
--

CREATE TABLE `asistencias` (
  `id` int(11) NOT NULL,
  `entrada` datetime DEFAULT NULL,
  `salida` datetime DEFAULT NULL,
  `users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asistencias`
--

INSERT INTO `asistencias` (`id`, `entrada`, `salida`, `users_id`) VALUES
(19, '2019-08-11 05:08:47', '2019-08-11 05:08:47', 7),
(20, '2019-08-11 05:09:28', '2019-08-11 07:35:15', 7),
(21, '2019-08-11 07:37:21', '2019-08-11 07:39:54', 7),
(22, '2019-08-11 07:40:41', '2019-08-21 10:56:04', 7),
(23, '2019-08-17 09:49:43', '2019-08-17 09:49:43', 8),
(24, '2019-08-21 10:58:57', '2019-08-21 10:58:57', 9),
(25, '2019-08-21 11:03:36', '2019-08-21 11:03:36', 10),
(26, '2019-08-21 12:39:06', '2019-08-21 13:21:14', 9),
(27, '2019-08-21 19:25:33', '2019-08-21 19:25:38', 9),
(28, '2019-08-21 19:25:36', '2019-08-22 13:34:47', 9),
(29, '2019-08-22 16:39:17', '2019-08-23 19:45:09', 9),
(30, '2019-08-27 11:33:01', '2019-08-27 19:06:36', 9),
(31, '2019-08-28 13:13:26', NULL, 7),
(32, '2019-08-28 13:13:28', '2019-09-03 11:09:54', 7),
(33, '2019-08-28 14:37:14', NULL, 9),
(34, '2019-08-28 14:37:16', '2019-08-28 19:16:37', 9),
(35, '2019-08-29 11:11:38', NULL, 9),
(36, '2019-08-29 11:11:38', '2019-08-29 13:28:55', 9),
(37, '2019-08-29 14:59:34', '2019-08-29 19:23:56', 9),
(38, '2019-08-30 09:48:04', NULL, 9),
(39, '2019-08-30 09:48:06', '2019-08-30 13:20:57', 9),
(40, '2019-08-30 14:20:30', NULL, 9),
(41, '2019-08-30 14:20:34', '2019-08-30 20:00:41', 9),
(42, '2019-09-02 10:40:54', '2019-09-02 13:23:08', 9),
(43, '2019-09-02 17:09:15', '2019-09-02 17:09:20', 9),
(44, '2019-09-02 19:45:43', NULL, 9),
(45, '2019-09-02 19:45:44', NULL, 9),
(46, '2019-09-02 19:45:45', '2019-09-02 19:46:06', 9),
(47, '2019-09-03 09:35:47', NULL, 9),
(48, '2019-09-03 09:35:48', NULL, 9),
(49, '2019-09-03 09:35:50', NULL, 9),
(50, '2019-09-03 09:35:50', NULL, 9),
(51, '2019-09-03 09:35:51', NULL, 9),
(52, '2019-09-03 09:35:51', NULL, 9),
(53, '2019-09-03 09:35:51', NULL, 9),
(54, '2019-09-03 09:35:52', NULL, 9),
(55, '2019-09-03 09:35:52', NULL, 9),
(56, '2019-09-03 09:35:52', NULL, 9),
(57, '2019-09-03 09:35:54', NULL, 9),
(58, '2019-09-03 09:35:54', NULL, 9),
(59, '2019-09-03 09:35:55', NULL, 9),
(60, '2019-09-03 09:35:56', NULL, 9),
(61, '2019-09-03 09:35:57', NULL, 9),
(62, '2019-09-03 09:35:57', NULL, 9),
(63, '2019-09-03 09:35:57', NULL, 9),
(64, '2019-09-03 09:35:58', NULL, 9),
(65, '2019-09-03 09:35:58', NULL, 9),
(66, '2019-09-03 09:35:58', NULL, 9),
(67, '2019-09-03 09:35:58', NULL, 9),
(68, '2019-09-03 09:35:59', NULL, 9),
(69, '2019-09-03 09:36:00', NULL, 9),
(70, '2019-09-03 09:36:00', NULL, 9),
(71, '2019-09-03 09:36:00', NULL, 9),
(72, '2019-09-03 09:36:00', NULL, 9),
(73, '2019-09-03 09:36:01', NULL, 9),
(74, '2019-09-03 09:36:01', NULL, 9),
(75, '2019-09-03 09:36:01', NULL, 9),
(76, '2019-09-03 09:36:01', NULL, 9),
(77, '2019-09-03 09:36:02', NULL, 9),
(78, '2019-09-03 09:36:02', NULL, 9),
(79, '2019-09-03 09:36:03', NULL, 9),
(80, '2019-09-03 09:36:03', NULL, 9),
(81, '2019-09-03 09:36:03', NULL, 9),
(82, '2019-09-03 09:36:06', NULL, 9),
(83, '2019-09-03 09:36:06', NULL, 9),
(84, '2019-09-03 09:36:07', NULL, 9),
(85, '2019-09-03 09:36:08', NULL, 9),
(86, '2019-09-03 09:36:08', NULL, 9),
(87, '2019-09-03 09:36:08', NULL, 9),
(88, '2019-09-03 09:36:09', NULL, 9),
(89, '2019-09-03 09:36:09', NULL, 9),
(90, '2019-09-03 09:36:09', NULL, 9),
(91, '2019-09-03 09:36:10', '2019-09-03 11:00:09', 9),
(92, '2019-09-03 12:30:03', '2019-09-03 12:43:21', 9),
(93, '2019-09-03 12:43:33', '2019-09-03 19:08:09', 9),
(94, '2019-09-04 10:45:21', NULL, 9),
(95, '2019-09-04 10:45:23', NULL, 9),
(96, '2019-09-04 10:45:25', NULL, 9),
(97, '2019-09-04 10:45:25', NULL, 9),
(98, '2019-09-04 10:45:27', NULL, 9),
(99, '2019-09-04 10:45:27', NULL, 9),
(100, '2019-09-04 10:45:29', NULL, 9),
(101, '2019-09-04 10:45:29', NULL, 9),
(102, '2019-09-04 10:45:29', '2019-09-04 13:05:06', 9),
(103, '2019-09-04 14:38:31', NULL, 9),
(104, '2019-09-04 14:38:32', '2019-09-04 19:05:21', 9),
(105, '2019-09-05 09:20:31', '2019-09-05 19:30:15', 9),
(106, '2019-09-11 12:11:53', '2019-09-12 10:27:16', 9),
(107, '2019-09-12 16:09:15', NULL, 9),
(108, '2019-09-12 16:09:17', NULL, 9),
(109, '2019-09-12 16:09:18', NULL, 9),
(110, '2019-09-12 16:09:19', NULL, 9),
(111, '2019-09-12 16:09:20', '2019-09-13 11:38:13', 9),
(112, '2019-09-13 13:06:00', NULL, 9),
(113, '2019-09-13 13:06:01', '2019-09-13 13:06:03', 9),
(114, '2019-09-13 17:42:24', '2019-09-13 19:00:57', 9),
(115, '2019-09-16 09:23:32', NULL, 9),
(116, '2019-09-16 09:23:35', '2019-09-16 19:31:13', 9),
(117, '2019-09-17 10:07:35', NULL, 9),
(118, '2019-09-17 10:07:37', '2019-09-17 14:05:57', 9),
(119, '2019-09-17 14:08:31', '2019-09-17 19:22:05', 9),
(120, '2019-09-18 09:37:19', '2019-09-18 13:10:16', 9),
(121, '2019-09-18 14:20:20', '2019-09-18 19:19:41', 9),
(122, '2019-09-19 09:24:38', NULL, 9),
(123, '2019-09-19 09:24:41', '2019-09-19 13:05:49', 9),
(124, '2019-09-19 13:13:53', '2019-09-19 19:41:41', 9),
(125, '2019-09-20 18:23:48', '2019-09-23 09:54:24', 9),
(126, '2019-09-23 09:55:06', NULL, 9),
(127, '2019-09-23 09:55:11', '2019-09-23 17:46:14', 9),
(128, '2019-09-23 18:06:17', '2019-09-23 18:45:09', 9),
(129, '2019-09-23 18:45:31', '2019-09-23 18:46:49', 7),
(130, '2019-09-25 09:35:12', NULL, 9),
(131, '2019-09-25 09:35:18', '2019-09-26 13:24:22', 9),
(132, '2019-09-26 16:25:42', '2019-09-26 20:03:42', 9),
(133, '2019-09-27 11:45:43', '2019-09-27 14:05:23', 9),
(134, '2019-09-27 14:05:30', '2019-09-27 20:01:39', 9),
(135, '2019-09-30 12:03:07', NULL, 9),
(136, '2019-09-30 12:03:09', '2019-09-30 13:17:42', 9),
(137, '2019-09-30 14:19:55', '2019-10-01 14:28:20', 9),
(138, '2019-10-01 14:29:31', '2019-10-01 14:29:33', 7),
(139, '2019-10-01 14:30:27', '2019-10-01 14:46:23', 7),
(140, '2019-10-01 15:23:16', '2019-10-01 18:21:22', 9),
(141, '2019-10-02 10:32:39', '2019-10-02 13:14:21', 9),
(142, '2019-10-02 15:07:31', '2019-10-02 19:15:07', 9),
(143, '2019-10-03 11:41:39', '2019-10-03 19:47:48', 9),
(144, '2019-10-04 19:06:54', '2019-10-07 09:18:02', 9),
(145, '2019-10-07 09:18:40', '2019-10-07 19:09:56', 9),
(146, '2019-10-08 11:20:07', '2019-10-08 18:57:36', 9),
(147, '2019-10-09 09:47:51', NULL, 9),
(148, '2019-10-09 09:47:55', '2019-10-10 13:11:14', 9),
(149, '2019-10-10 14:13:48', NULL, 9),
(150, '2019-10-10 14:13:51', '2019-10-10 19:05:04', 9),
(151, '2019-10-11 13:09:08', '2019-10-11 19:10:44', 9),
(152, '2019-10-14 19:35:12', '2019-10-15 13:12:58', 9),
(153, '2019-10-15 19:18:18', '2019-10-21 19:06:47', 9),
(154, '2019-10-22 19:26:03', '2019-10-22 19:26:05', 9),
(155, '2019-10-23 09:42:33', '2019-10-23 14:05:48', 9),
(156, '2019-10-23 14:06:06', '2019-10-24 13:05:56', 9),
(157, '2019-10-24 14:19:41', NULL, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `documento` varchar(24) NOT NULL,
  `nombres` varchar(150) NOT NULL,
  `direccion` varchar(250) NOT NULL,
  `edad` varchar(3) NOT NULL,
  `ciudad` varchar(150) NOT NULL,
  `tipo_cliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `documento`, `nombres`, `direccion`, `edad`, `ciudad`, `tipo_cliente`) VALUES
(3, '75601992', 'victor manuel apaza', 'san antonio Mz M Lot 3', '24', 'x', 3),
(4, '15356214', 'jose', 'balta 1', '24', 'x', 3),
(5, 'manuel', '952856201', 'san antonioo m3', '24', 'x', 3),
(6, 'asd', 'manuel', 'ap', '24', 'x', 3),
(7, '47105656', 'EDWIN ROQUE ROJAS', 'MOQUEGUA', '24', 'x', 3),
(8, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '24', 'x', 10),
(9, '76779084', 'RICHARD', 'MOQUEGUA', '24', 'x', 10),
(10, '12345678', 'SR CLIENTE', 'MOQUEGUA', '24', 'x', 3),
(11, '04743823', 'OMAR HUARACHA', 'MOQUEGUA', '24', 'x', 3),
(12, '04411193', 'SAN CARLOS E.IR.L.', 'MOQUEGUA', '0', 'x', 3),
(13, '04411193', 'SAN CARLOS E.IR.L.', 'MOQUEGUA', '0', 'x', 3),
(14, '12345678', 'SR CLIENTE', 'MOQUEGUA', '0', 'x', 3),
(15, '12345678', 'SR CLIENTE', 'MOQUEGUA', '0', 'x', 3),
(16, '00518140', 'CATACORA AURELIA', 'MOQUEGUA', '0', 'x', 3),
(17, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(18, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(19, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(20, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(21, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(22, '12345678', 'SR CLIENTE', 'MOQUEGUA', '0', 'x', 3),
(23, '44313520', 'RONALD SANTOS', 'RAMIRO PRIALE D-5', '0', 'x', 3),
(24, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(25, '43296323', 'ROJAS BUSTAMANTE OSCAR', 'MOQUEGUA', '0', 'x', 3),
(26, '42755216', 'ELISEO QUISPE', 'MOQUEGUA', '0', 'x', 3),
(27, '42755216', 'ELISEO QUISPE', 'MOQUEGUA', '0', 'x', 3),
(28, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(29, '04745081', 'PERCY CALIZAYA', 'MOQUEGUA', '0', 'x', 3),
(30, '12345678', 'JOSE GALLEGOS', 'MOQUEGUA', '0', 'x', 3),
(31, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(32, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(33, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(34, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(35, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(36, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(37, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(38, '46900863', 'PALOMINO MIRANDA RUBEN', 'MOQUEGUA', '0', 'x', 3),
(39, '46900863', 'PALOMINO MIRANDA RUBEN', 'MOQUEGUA', '0', 'x', 3),
(40, '46900863', 'PALOMINO MIRANDA RUBEN', 'MOQUEGUA', '0', 'x', 3),
(41, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(42, '41040879', 'CARLOS MAMANI', 'MOQUEGUA', '0', 'x', 3),
(43, '41040879', 'MAMANI CARLOS', 'MOQUEGUA', '0', 'x', 3),
(44, '41040879', 'MAMANI CARLOS', 'MOQUEGUA', '0', 'x', 3),
(45, '41040879', 'MAMANI CARLOS', 'MOQUEGUA', '0', 'x', 3),
(46, '43512376', 'LUIS SANCHEZ', 'MOQUEGUA', '0', 'x', 3),
(47, '02146181', 'SABINO QUISPE QUISPE', 'MOQUEGUA', '0', 'x', 3),
(48, '04410024', 'FORTUNATO CLAVILLA QUISPE', 'MOQUEGUA', '0', 'x', 3),
(49, '04410024', 'FORTUNATO CLAVILLA QUISPE', 'MOQUEGUA', '0', 'x', 3),
(50, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(51, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(52, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(53, '04745081', 'PERCY CALIZAYA', 'MOQUEGUA', '0', 'x', 3),
(54, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(55, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(56, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(57, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(58, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(59, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(60, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(61, '48743806', 'bojorquez', 'moquegua', '0', 'x', 3),
(62, '48743806', 'bojorquez', 'moquegua', '0', 'x', 3),
(63, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(64, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(65, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(66, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(67, '42077964', 'YON ESTACA', 'A.A.C. M -O LOTE 12', '0', 'x', 3),
(68, '15987465', 'juan gabril', 'sin casa', '0', 'x', 3),
(69, '15987465', 'juan gabril', 'sin casa', '0', 'x', 3),
(70, '15987465', 'juan gabril', 'sin casa', '0', 'x', 3),
(71, '15987465', 'juan gabril', 'sin casa', '0', 'x', 3),
(72, '02146181', 'SABINO QUISPE QUISPE', 'MOQUEGUA', '0', 'x', 3),
(73, '12345678', 'SR CLIENTE', 'MOQUEGUA', '0', 'x', 3),
(74, '12345678', 'SR CLIENTE', 'MOQUEGUA', '0', 'x', 3),
(75, '12345678', 'SR CLIENTE', 'MOQUEGUA', '0', 'x', 3),
(76, '12345678', 'SR CLIENTE', 'MOQUEGUA', '0', 'x', 3),
(77, '12345678', 'SR CLIENTE', 'MOQUEGUA', '0', 'x', 3),
(78, '43296323', 'ROJAS BUSTAMANTE OSCAR', 'MOQUEGUA', '0', 'x', 3),
(79, '47105656', 'EDWIN ROQUE ROJAS', 'MOQUEGUA', '0', 'x', 3),
(80, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(81, '71230648', 'ESTHER PEÑA', 'MOQUEGUA', '0', 'x', 3),
(82, '12345678', 'SR CLIENTE', 'MOQUEGUA', '0', 'x', 3),
(83, '47263007', 'VICTOR CHAMBILLA', 'MOQUEGUA', '0', 'x', 3),
(84, '43261451', 'BRUNO  VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(85, '62054775', 'YAN AGUIRRE', 'MOQUEGUA', '0', 'x', 3),
(86, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(87, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(88, '12345678', 'SR.CLIENTE', 'MOQUEGUA', '0', 'x', 3),
(89, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(90, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(91, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(92, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(93, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(94, '42777685', 'DAVID RODRIGUEZ', 'MOQUEGUA', '0', 'x', 3),
(95, '42777685', 'DAVID RODRIGUEZ', 'MOQUEGUA', '0', 'x', 3),
(96, '044291751', 'FELIPE QUISPE', 'MOQUEGUA', '0', 'x', 3),
(97, '04429175', 'FELIPE INCAHUANACO', 'MLOQUEGUA', '0', 'x', 3),
(98, '04429175', 'FELIPE INCAHUANACO', 'MLOQUEGUA', '0', 'x', 3),
(99, '04429175', 'FELIPQ INCAHUANACO', 'MOQUEGUA', '0', 'x', 3),
(100, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(101, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(102, '931573188', 'LUISA HUACHANI', 'MOQUEGUA', '0', 'x', 3),
(103, '984002072', 'RICARDO OQUENDO', 'MOQUEGUA', '0', 'x', 3),
(104, '947953777', 'CARLOS  MAMANI TOLEDO', 'MOQUEGUA', '0', 'x', 3),
(105, '985466935', 'ESTHER PEÑA', 'MOQUEGUA', '0', 'x', 3),
(106, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(107, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(108, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(109, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(110, '997066736', 'RUBEN PALOMINO', 'MOQUEGUA', '0', 'x', 3),
(111, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(112, '12345678', 'SR.CLIENTE', 'MOQUEGUA', '0', 'x', 3),
(113, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(114, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(115, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(116, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(117, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(118, '44798641', 'MARCO BENAVENTE', 'MOQUEGUA', '0', 'x', 3),
(119, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(120, '41397297', 'JOEL CHAMBILLA', 'MOQUEGUA', '0', 'x', 3),
(121, '44798641', 'MARCO BENAVENTE', 'MOQUEGUA', '0', 'x', 3),
(122, '12345678', 'JUAN CHURATA', 'MOQUEGUA', '0', 'x', 3),
(123, '04429175', 'FELIPE INCAHUANACO', 'MOQUEGUA', '0', 'x', 3),
(124, '44313520', 'RONALD SANTOS MARIA', 'MOQUEGUA', '0', 'x', 3),
(125, '44313520', 'RONALD SANTOS MARIA', 'MOQUEGUA', '0', 'x', 3),
(126, '04429175', 'FELIPE INCAHUANACO', 'MOQUEGUA', '0', 'x', 3),
(127, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(128, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(129, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(130, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(131, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(132, '42611042', 'HILMAR VILEN', 'MOQUEGUA', '0', 'x', 3),
(133, '44798641', 'MARCO BENAVENTE', 'MOQUEGUA', '0', 'x', 3),
(134, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(135, '48708726', 'JOSE COAQUIRA', 'MOQUEGUA', '0', 'x', 3),
(136, '04429175', 'FELIPE INCAHUANACO', 'MOQUEGUA', '0', 'x', 3),
(137, '44313520', 'RONALD SANTOS', 'MOQUEGUA', '0', 'x', 3),
(138, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(139, '42611042', 'HILMAR FIDEL VILCA', 'MOQUEGUA', '0', 'x', 3),
(140, '40852205', 'JUAN CARLOS DURAND M.', 'MOQUEGUA', '0', 'x', 3),
(141, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(142, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(143, '46336036', 'CESAR ROJAS', 'MOQUEGUA', '0', 'x', 3),
(144, '04428503', 'PABLO CALLE', 'MOQUEGUA', '0', 'x', 3),
(145, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(146, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(147, '12345678', 'RICHARD CLIENTE', 'MOQUEGUA', '0', 'x', 3),
(148, '43261451', 'VILCA ALEJO BRUNO', 'MOQUEGUA', '0', 'x', 3),
(149, '12345678', 'IDELFONZO CLIENTE', 'MOQUEGUA', '0', 'x', 3),
(150, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(151, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(152, '04412345', 'FELIPE INCAHUANACO', 'SAMEGUA', '0', 'x', 3),
(153, '76779084', 'RICHARD', 'MOQUEGUA', '0', 'x', 3),
(154, '46836066', 'CHINO APAZA', 'SAN ANTONIO', '0', 'x', 3),
(155, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(156, '41830648', 'ARTURMAQUERA PALO', 'MOQUEGUA', '0', 'x', 3),
(157, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEUA', '0', 'x', 3),
(158, '71847779', 'ZAPATA PINTO RONALDO', 'MOQUEGUA', '0', 'x', 3),
(159, '04429175', 'FELIPE INCAHUANACO', 'MOQUEGUA', '0', 'x', 3),
(160, '04429175', 'FELIPE INCAHUANACO', 'MOQUEGUA', '0', 'x', 3),
(161, '12345678', 'PETER CLIENTEM', 'MOQUEGUA', '0', 'x', 3),
(162, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(163, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(164, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(165, '04434871', 'ISACC NINARAQUI', 'MOQUEGUA', '0', 'x', 3),
(166, '41040879', 'CARLOS MAMANI', 'MOQUEGUA', '0', 'x', 3),
(167, '41040879', 'CARLOS MAMANI', 'MOQUEGUA', '0', 'x', 3),
(168, '04429175', 'FELIPE INCAHUANACO', 'MLOQUEGUA', '0', 'x', 3),
(169, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(170, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(171, '25682233', 'JESUS ARNICA MORALES', 'MORALES', '0', 'x', 3),
(172, '04434871', 'ISACC NINARAQUI', 'MOQUEGUA', '0', 'x', 3),
(173, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(174, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(175, '41774714', 'german asqui', 'moquegua', '0', 'x', 3),
(176, '41474714', 'german asqui', 'moquegua', '0', 'x', 3),
(177, '41474714', 'german asqui', 'moquegua', '0', 'x', 3),
(178, '41474714', 'german asqui', 'moquegua', '0', 'x', 3),
(179, '04429175', 'FELIPE INCAHUANACO', 'MLOQUEGUA', '0', 'x', 3),
(180, '04744307', 'WALTER LECAROS', 'MOQUEGUA', '0', 'x', 3),
(181, '04745522', 'PEDRO MARCA RAMOS', 'MOQUEGUA', '0', 'x', 3),
(182, '12345678', 'sr.cliente', 'moquegua', '0', 'x', 3),
(183, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(184, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(185, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(186, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(187, '98765432', 'CARLOS MAMANI', 'MOQUEGUA', '0', 'x', 3),
(188, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(189, '43198337', 'CESAR CHAMBILLA GUTIERREZ', 'MOQUEGUA', '0', 'x', 3),
(190, '98765432', 'SR CLIENTE', 'AREQUIPA', '0', 'x', 3),
(191, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(192, '13456789', 'CUBA BULEJE', 'MOQUEGUA', '0', 'x', 3),
(193, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(194, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(195, '80113541', 'richard chambi pilco', 'moquegua', '0', 'x', 3),
(196, '43261451', 'bruno vilca alejo', 'moquegua', '0', 'x', 3),
(197, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(198, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(199, '46215545', 'JUAN LIZARDO CONDORI', 'MOQUEGUA', '0', 'x', 3),
(200, '45134645', 'BRANYAN SERRATO', 'MOQUEGUA', '0', 'x', 3),
(201, '972138821', 'juan lizardo condori', 'moquegua', '0', 'x', 3),
(202, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(203, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(204, '43261451', 'BRUNO VILCA', 'MOQUEGUA', '0', 'x', 3),
(205, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(206, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(207, '44798641', 'MARCO BENAVENTE', 'MOQUEGUA', '0', 'x', 3),
(208, '02437742', 'HABRAHAM CALDERON', 'MOQUEGUA', '0', 'x', 3),
(209, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(210, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(211, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(212, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(213, '12345678', 'SR CLIENTE', 'MOQUEGUA', '0', 'x', 3),
(214, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(215, '09876543', 'SABINO QUISPE', 'MOQUEGUA', '0', 'x', 3),
(216, '987654329', 'EVER ESPINOZA', 'MOQUEGUA', '0', 'x', 3),
(217, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(218, '54678932', 'SR CLIENTE', 'MOQUEGUA', '0', 'x', 3),
(219, '48708726', 'JOSE COAQUIRA', 'MOQUEGUA', '0', 'x', 3),
(220, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(221, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(222, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(223, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(224, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(225, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3),
(226, '44798641', 'MARCO BENAVENTE', 'MOQUEGUA', '0', 'x', 3),
(227, '71458588', 'JOHNATAN ARAPA LLAMPI', 'MOQUEGUA', '0', 'x', 3),
(228, '41040879', 'CARLOS MAMANI', 'MOQUEGUA', '0', 'x', 3),
(229, '43261451', 'BRUNO VILCA ALEJO', 'MOQUEGUA', '0', 'x', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `creditos`
--

CREATE TABLE `creditos` (
  `id` int(11) NOT NULL,
  `nombres` varchar(100) DEFAULT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `dni` varchar(15) DEFAULT NULL,
  `direccion` varchar(200) DEFAULT NULL,
  `telefono` varchar(25) DEFAULT NULL,
  `institucion` varchar(250) DEFAULT NULL,
  `oficina` varchar(200) DEFAULT NULL,
  `encargado` varchar(200) DEFAULT NULL,
  `logistico` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `creditos`
--

INSERT INTO `creditos` (`id`, `nombres`, `apellidos`, `dni`, `direccion`, `telefono`, `institucion`, `oficina`, `encargado`, `logistico`) VALUES
(1, 'jose', 'flores', '794956', 'Calle Lima', '7949565', 's', 'of', 'en', 'log'),
(2, 'miguel', 'Mamani', '879498632', 'Callle lima n°153', '953648421', NULL, NULL, NULL, NULL),
(3, 'RONALD', 'SANTOS', '44313520', 'ASOC.RAMIRO PRIALE MZ D LOTE 5 SAN FRANCISCO', '917002864', NULL, NULL, NULL, NULL),
(4, 'VILCA ALEJO BRUNO', NULL, '43261451', 'MOQUEGUA', 'x1x', NULL, NULL, NULL, NULL),
(5, 'CARLOS', 'MAMANI', '41040879', 'MOQUEGUA', '123456789', NULL, NULL, NULL, NULL),
(6, 'SABINO QUISPE QUISPE', NULL, '02146181', 'MOQUEGUA', 'x1x', NULL, NULL, NULL, NULL),
(7, 'EDWIN ROQUE ROJAS', NULL, '47105656', 'MOQUEGUA', 'x1x', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas`
--

CREATE TABLE `cuentas` (
  `id` int(11) NOT NULL,
  `banco` varchar(200) NOT NULL,
  `numero` varchar(100) DEFAULT NULL,
  `proveedors_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuentas`
--

INSERT INTO `cuentas` (`id`, `banco`, `numero`, `proveedors_id`) VALUES
(1, 'BCP', '94956232', 1),
(5, 'BOst', '1546532', 1),
(6, 'Interback', '4486-4776-845', 2),
(7, 'BBC', '984765321', 1),
(8, 'BCP', '95794-476-484', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleventas`
--

CREATE TABLE `detalleventas` (
  `id` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio` decimal(20,2) NOT NULL,
  `subtotal` decimal(12,2) DEFAULT NULL,
  `producto_id` int(11) NOT NULL,
  `venta_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalleventas`
--

INSERT INTO `detalleventas` (`id`, `cantidad`, `precio`, `subtotal`, `producto_id`, `venta_id`) VALUES
(83, 1, 27.00, 27.00, 18, 72),
(84, 1, 27.00, 27.00, 18, 73),
(85, 1, 27.00, 27.00, 18, 76),
(86, 1, 27.00, 27.00, 18, 75),
(87, 1, 27.00, 27.00, 18, 74),
(88, 1, 27.00, 27.00, 18, 77),
(89, 1, 27.00, 27.00, 18, 78),
(91, 1, 27.00, 27.00, 18, 80),
(92, 1, 27.00, 27.00, 18, 79),
(95, 1, 27.00, 27.00, 18, 84),
(96, 1, 27.00, 27.00, 18, 85),
(97, 1, 0.40, 0.40, 37, 86),
(98, 1, 27.00, 27.00, 18, 87),
(99, 1, 27.00, 27.00, 18, 88),
(100, 1, 16.00, 16.00, 49, 89),
(101, 1, 37.00, 37.00, 42, 89),
(102, 1, 12.00, 12.00, 47, 89),
(103, 3, 16.00, 48.00, 49, 90),
(104, 3, 16.00, 48.00, 49, 91),
(105, 4, 0.40, 1.60, 37, 92),
(106, 5, 27.00, 135.00, 18, 93),
(107, 5, 27.00, 135.00, 18, 94),
(108, 1, 27.00, 27.00, 17, 95),
(109, 2, 0.80, 1.60, 55, 96),
(110, 4, 2.80, 11.20, 43, 97),
(111, 4, 2.50, 10.00, 81, 98),
(112, 4, 2.50, 10.00, 81, 99),
(113, 8, 0.20, 1.60, 82, 100),
(114, 4, 3.00, 12.00, 84, 101),
(115, 2, 3.00, 6.00, 84, 102),
(116, 1, 2.80, 2.80, 16, 103),
(117, 20, 8.00, 160.00, 95, 104),
(118, 20, 2.80, 56.00, 43, 104),
(119, 8, 9.50, 76.00, 79, 104),
(120, 2, 3.00, 6.00, 84, 105),
(121, 4, 9.50, 38.00, 78, 105),
(122, 1, 16.00, 16.00, 48, 105),
(123, 2, 2.80, 5.60, 45, 106),
(124, 2, 2.80, 5.60, 43, 106),
(125, 2, 2.80, 5.60, 45, 107),
(126, 1, 3.00, 3.00, 44, 107),
(127, 7, 2.80, 19.60, 43, 107),
(128, 4, 8.50, 34.00, 96, 107),
(129, 7, 1.80, 12.60, 66, 107),
(130, 2, 2.80, 5.60, 45, 108),
(131, 1, 3.00, 3.00, 44, 108),
(132, 7, 2.80, 19.60, 43, 108),
(133, 4, 8.50, 34.00, 96, 108),
(134, 7, 1.80, 12.60, 66, 108),
(135, 6, 0.80, 4.80, 55, 109),
(136, 6, 0.40, 2.40, 37, 110),
(137, 1, 3.50, 3.50, 99, 111),
(138, 1, 7.50, 7.50, 92, 111),
(139, 2, 6.50, 13.00, 91, 112),
(140, 5, 3.00, 15.00, 84, 112),
(141, 1, 5.00, 5.00, 41, 113),
(142, 2, 7.50, 15.00, 92, 114),
(143, 3, 2.80, 8.40, 45, 115),
(144, 13, 6.50, 84.50, 91, 116),
(145, 7, 3.00, 21.00, 44, 117),
(146, 6, 3.00, 18.00, 84, 117),
(147, 1, 2.80, 2.80, 45, 117),
(148, 1, 12.00, 12.00, 46, 118),
(149, 1, 5.50, 5.50, 90, 119),
(150, 1, 3.50, 3.50, 99, 119),
(151, 1, 5.50, 5.50, 90, 120),
(152, 1, 3.50, 3.50, 99, 120),
(153, 1, 5.50, 5.50, 90, 121),
(154, 1, 3.50, 3.50, 99, 121),
(155, 8, 0.20, 1.60, 82, 122),
(156, 2, 5.00, 10.00, 41, 123),
(157, 10, 0.40, 4.00, 38, 123),
(159, 2, 5.00, 10.00, 41, 124),
(160, 10, 0.40, 4.00, 38, 124),
(162, 2, 5.00, 10.00, 41, 125),
(163, 10, 0.40, 4.00, 38, 125),
(165, 4, 4.50, 18.00, 101, 126),
(166, 2, 5.00, 10.00, 41, 126),
(167, 10, 0.40, 4.00, 38, 126),
(168, 3, 1.00, 3.00, 98, 127),
(169, 1, 1.00, 1.00, 98, 127),
(170, 1, 12.00, 12.00, 46, 128),
(171, 1, 16.00, 16.00, 48, 128),
(172, 1, 16.00, 16.00, 49, 128),
(173, 1, 16.00, 16.00, 49, 128),
(174, 2, 3.00, 6.00, 44, 128),
(175, 4, 2.80, 11.20, 43, 128),
(176, 3, 4.00, 12.00, 88, 128),
(177, 1, 4.00, 4.00, 88, 128),
(178, 1, 27.00, 27.00, 18, 128),
(179, 1, 27.00, 27.00, 18, 129),
(180, 1, 2.80, 2.80, 45, 129),
(181, 1, 27.00, 27.00, 18, 130),
(182, 1, 2.80, 2.80, 45, 130),
(183, 13, 5.50, 71.50, 90, 131),
(184, 13, 5.50, 71.50, 90, 132),
(185, 8, 5.50, 44.00, 90, 133),
(186, 1, 3.50, 3.50, 99, 134),
(187, 5, 0.80, 4.00, 55, 135),
(188, 1, 1.80, 1.80, 66, 135),
(189, 1, 1.80, 1.80, 66, 135),
(190, 1, 1.80, 1.80, 66, 135),
(191, 1, 1.80, 1.80, 66, 135),
(192, 1, 1.80, 1.80, 66, 135),
(193, 5, 0.80, 4.00, 55, 136),
(194, 1, 1.80, 1.80, 66, 136),
(195, 1, 1.80, 1.80, 66, 136),
(196, 1, 1.80, 1.80, 66, 136),
(197, 1, 1.80, 1.80, 66, 136),
(198, 1, 1.80, 1.80, 66, 136),
(199, 2, 0.40, 0.80, 38, 137),
(200, 1, 0.40, 0.40, 38, 137),
(201, 1, 0.40, 0.40, 38, 137),
(202, 2, 0.20, 0.40, 82, 137),
(203, 1, 0.20, 0.20, 82, 137),
(204, 1, 0.20, 0.20, 82, 137),
(205, 1, 0.20, 0.20, 82, 137),
(206, 1, 0.20, 0.20, 82, 137),
(207, 1, 0.20, 0.20, 82, 137),
(208, 1, 0.20, 0.20, 82, 137),
(209, 2, 0.40, 0.80, 38, 138),
(210, 1, 0.40, 0.40, 38, 138),
(211, 1, 0.40, 0.40, 38, 138),
(212, 2, 0.20, 0.40, 82, 138),
(213, 1, 0.20, 0.20, 82, 138),
(214, 1, 0.20, 0.20, 82, 138),
(215, 1, 0.20, 0.20, 82, 138),
(216, 1, 0.20, 0.20, 82, 138),
(217, 1, 0.20, 0.20, 82, 138),
(218, 1, 0.20, 0.20, 82, 138),
(219, 2, 0.40, 0.80, 38, 139),
(220, 1, 0.40, 0.40, 38, 139),
(221, 1, 0.40, 0.40, 38, 139),
(222, 2, 0.20, 0.40, 82, 139),
(223, 1, 0.20, 0.20, 82, 139),
(224, 1, 0.20, 0.20, 82, 139),
(225, 1, 0.20, 0.20, 82, 139),
(226, 1, 0.20, 0.20, 82, 139),
(227, 1, 0.20, 0.20, 82, 139),
(228, 1, 0.20, 0.20, 82, 139),
(229, 2, 0.40, 0.80, 38, 140),
(230, 1, 0.40, 0.40, 38, 140),
(231, 1, 0.40, 0.40, 38, 140),
(232, 2, 0.20, 0.40, 82, 140),
(233, 1, 0.20, 0.20, 82, 140),
(234, 1, 0.20, 0.20, 82, 140),
(235, 1, 0.20, 0.20, 82, 140),
(236, 1, 0.20, 0.20, 82, 140),
(237, 1, 0.20, 0.20, 82, 140),
(238, 1, 0.20, 0.20, 82, 140),
(239, 2, 0.40, 0.80, 38, 141),
(240, 1, 0.40, 0.40, 38, 141),
(241, 1, 0.40, 0.40, 38, 141),
(242, 2, 0.20, 0.40, 82, 141),
(243, 1, 0.20, 0.20, 82, 141),
(244, 1, 0.20, 0.20, 82, 141),
(245, 1, 0.20, 0.20, 82, 141),
(246, 1, 0.20, 0.20, 82, 141),
(247, 1, 0.20, 0.20, 82, 141),
(248, 1, 0.20, 0.20, 82, 141),
(249, 1, 2.50, 2.50, 72, 142),
(250, 1, 2.50, 2.50, 72, 143),
(251, 2, 3.00, 6.00, 84, 144),
(252, 1, 3.00, 3.00, 84, 144),
(253, 1, 3.00, 3.00, 84, 144),
(254, 1, 2.80, 2.80, 45, 144),
(255, 1, 2.80, 2.80, 45, 144),
(256, 2, 8.50, 17.00, 96, 144),
(257, 2, 3.00, 6.00, 84, 145),
(258, 1, 3.00, 3.00, 84, 145),
(259, 1, 3.00, 3.00, 84, 145),
(260, 1, 2.80, 2.80, 45, 145),
(261, 1, 2.80, 2.80, 45, 145),
(262, 2, 8.50, 17.00, 96, 145),
(263, 2, 3.00, 6.00, 84, 146),
(264, 1, 3.00, 3.00, 84, 146),
(265, 1, 3.00, 3.00, 84, 146),
(266, 1, 2.80, 2.80, 45, 146),
(267, 1, 2.80, 2.80, 45, 146),
(268, 2, 8.50, 17.00, 96, 146),
(269, 2, 3.00, 6.00, 44, 147),
(270, 5, 3.00, 15.00, 84, 147),
(271, 2, 7.50, 15.00, 92, 148),
(275, 1, 2.80, 0.00, 16, 152),
(276, 1, 1.50, 1.50, 116, 153),
(277, 11, 0.20, 2.20, 82, 154),
(278, 1, 0.20, 0.20, 82, 154),
(279, 1, 2.80, 2.80, 43, 154),
(280, 1, 2.80, 2.80, 43, 154),
(281, 5, 3.00, 15.00, 44, 154),
(282, 1, 3.00, 3.00, 44, 154),
(283, 11, 0.20, 2.20, 82, 155),
(284, 1, 0.20, 0.20, 82, 155),
(285, 1, 2.80, 2.80, 43, 155),
(286, 1, 2.80, 2.80, 43, 155),
(287, 5, 3.00, 15.00, 44, 155),
(288, 1, 3.00, 3.00, 44, 155),
(289, 11, 0.20, 2.20, 82, 156),
(290, 1, 0.20, 0.20, 82, 156),
(291, 1, 2.80, 2.80, 43, 156),
(292, 1, 2.80, 2.80, 43, 156),
(293, 5, 3.00, 15.00, 44, 156),
(294, 1, 3.00, 3.00, 44, 156),
(295, 1, 3.50, 3.50, 99, 157),
(296, 1, 3.50, 3.50, 99, 158),
(297, 5, 2.80, 14.00, 43, 159),
(298, 1, 2.80, 2.80, 16, 160),
(299, 1, 6.00, 6.00, 75, 161),
(300, 1, 6.00, 6.00, 75, 161),
(301, 4, 8.50, 34.00, 96, 162),
(302, 7, 0.40, 2.80, 38, 162),
(303, 1, 0.40, 0.40, 38, 162),
(304, 2, 3.00, 6.00, 44, 162),
(305, 1, 3.00, 3.00, 44, 162),
(306, 1, 2.80, 2.80, 43, 162),
(307, 7, 0.50, 3.50, 114, 162),
(308, 1, 0.50, 0.50, 114, 162),
(309, 1, 7.00, 7.00, 100, 163),
(310, 5, 0.40, 2.00, 38, 163),
(311, 2, 2.80, 5.60, 45, 164),
(312, 1, 6.00, 6.00, 135, 164),
(313, 1, 6.00, 6.00, 135, 164),
(314, 4, 9.50, 38.00, 78, 165),
(315, 2, 0.40, 0.80, 40, 166),
(316, 1, 0.40, 0.40, 40, 166),
(317, 1, 0.40, 0.40, 40, 166),
(318, 1, 15.00, 15.00, 175, 167),
(319, 16, 4.00, 64.00, 176, 167),
(320, 3, 1.00, 3.00, 98, 168),
(321, 1, 1.00, 1.00, 98, 168),
(322, 6, 11.00, 66.00, 97, 168),
(323, 1, 11.00, 11.00, 97, 168),
(324, 1, 11.00, 11.00, 97, 168),
(325, 2, 0.80, 1.60, 55, 169),
(326, 5, 0.20, 1.00, 82, 170),
(327, 1, 0.20, 0.20, 82, 170),
(328, 3, 7.50, 22.50, 92, 171),
(329, 6, 0.60, 3.60, 183, 172),
(330, 3, 5.50, 16.50, 188, 173),
(331, 1, 6.00, 6.00, 75, 174),
(332, 10, 0.20, 2.00, 83, 174),
(333, 1, 88.00, 88.00, 143, 175),
(334, 2, 2.80, 5.60, 43, 175),
(335, 3, 210.00, 630.00, 156, 176),
(336, 19, 1.80, 34.20, 66, 176),
(337, 1, 1.80, 1.80, 66, 176),
(338, 4, 6.00, 24.00, 135, 177),
(339, 1, 210.00, 210.00, 167, 178),
(340, 1, 210.00, 210.00, 167, 178),
(341, 3, 8.00, 24.00, 95, 178),
(342, 1, 8.00, 8.00, 95, 178),
(343, 1, 210.00, 210.00, 167, 179),
(344, 1, 210.00, 210.00, 167, 179),
(345, 3, 8.00, 24.00, 95, 179),
(346, 1, 8.00, 8.00, 95, 179),
(347, 1, 27.00, 27.00, 18, 180),
(348, 1, 175.00, 175.00, 142, 181),
(349, 1, 105.00, 105.00, 178, 181),
(350, 1, 6.00, 6.00, 75, 182),
(351, 1, 6.00, 6.00, 75, 182),
(352, 1, 210.00, 210.00, 154, 183),
(353, 4, 210.00, 840.00, 156, 184),
(354, 2, 210.00, 420.00, 150, 185),
(355, 1, 175.00, 175.00, 142, 186),
(356, 1, 175.00, 175.00, 142, 186),
(357, 1, 27.00, 27.00, 18, 186),
(358, 1, 47.00, 47.00, 25, 186),
(359, 2, 8.50, 17.00, 96, 187),
(360, 2, 3.00, 6.00, 44, 187),
(361, 3, 6.50, 19.50, 91, 188),
(362, 2, 3.00, 6.00, 84, 189),
(363, 1, 6.50, 6.50, 91, 190),
(364, 12, 0.20, 2.40, 82, 191),
(365, 2, 2.80, 5.60, 43, 191),
(366, 6, 3.00, 18.00, 44, 191),
(367, 2, 6.00, 12.00, 75, 192),
(368, 9, 3.50, 31.50, 186, 193),
(369, 5, 2.80, 14.00, 43, 194),
(370, 2, 3.00, 6.00, 44, 194),
(371, 2, 8.50, 17.00, 96, 194),
(372, 4, 2.80, 11.20, 43, 195),
(373, 2, 2.80, 5.60, 45, 195),
(374, 8, 11.00, 88.00, 97, 196),
(375, 4, 1.00, 4.00, 98, 196),
(376, 4, 9.50, 38.00, 78, 197),
(377, 14, 4.00, 56.00, 176, 197),
(378, 1, 15.00, 15.00, 175, 198),
(379, 1, 210.00, 210.00, 189, 199),
(380, 1, 105.00, 105.00, 160, 199),
(381, 2, 7.50, 15.00, 92, 200),
(382, 1, 7.50, 7.50, 92, 200),
(383, 2, 0.50, 1.00, 104, 201),
(384, 1, 5.50, 5.50, 90, 202),
(385, 1, 210.00, 210.00, 156, 203),
(386, 3, 210.00, 630.00, 177, 204),
(387, 6, 2.80, 16.80, 43, 205),
(388, 2, 9.50, 19.00, 79, 205),
(389, 1, 9.50, 9.50, 79, 205),
(390, 2, 7.50, 15.00, 92, 205),
(391, 1, 7.50, 7.50, 92, 205),
(392, 1, 7.50, 7.50, 92, 205),
(393, 1, 7.50, 7.50, 92, 205),
(394, 1, 7.50, 7.50, 92, 205),
(395, 1, 7.50, 7.50, 92, 205),
(396, 1, 7.50, 7.50, 92, 205),
(397, 1, 105.00, 105.00, 155, 205),
(398, 6, 2.80, 16.80, 43, 206),
(399, 2, 9.50, 19.00, 79, 206),
(400, 1, 9.50, 9.50, 79, 206),
(401, 2, 7.50, 15.00, 92, 206),
(402, 1, 7.50, 7.50, 92, 206),
(403, 1, 7.50, 7.50, 92, 206),
(404, 1, 7.50, 7.50, 92, 206),
(405, 1, 7.50, 7.50, 92, 206),
(406, 1, 7.50, 7.50, 92, 206),
(407, 1, 7.50, 7.50, 92, 206),
(408, 1, 105.00, 105.00, 155, 206),
(409, 2, 8.00, 16.00, 95, 207),
(410, 1, 3.50, 3.50, 186, 208),
(411, 1, 6.00, 6.00, 75, 209),
(412, 30, 0.60, 18.00, 183, 210),
(413, 4, 3.00, 12.00, 44, 211),
(414, 2, 2.80, 5.60, 45, 211),
(415, 2, 16.00, 32.00, 49, 211),
(416, 1, 12.00, 12.00, 47, 211),
(417, 1, 8.00, 8.00, 95, 211),
(418, 1, 6.50, 6.50, 91, 211),
(419, 4, 0.60, 2.40, 183, 211),
(420, 4, 0.20, 0.80, 83, 212),
(421, 3, 175.00, 525.00, 142, 213),
(422, 1, 105.00, 105.00, 164, 214),
(423, 1, 210.00, 210.00, 167, 214),
(424, 1, 3.00, 3.00, 85, 215),
(425, 1, 200.00, 200.00, 146, 216),
(426, 1, 200.00, 200.00, 146, 216),
(427, 1, 200.00, 200.00, 148, 216),
(428, 2, 210.00, 420.00, 167, 217),
(429, 2, 100.00, 200.00, 145, 218),
(430, 2, 210.00, 420.00, 153, 218),
(431, 1, 210.00, 210.00, 153, 218),
(432, 1, 8.50, 8.50, 80, 219),
(433, 1, 8.50, 8.50, 80, 219),
(434, 2, 15.00, 30.00, 175, 220),
(435, 6, 8.50, 51.00, 96, 220),
(436, 2, 7.00, 14.00, 100, 220),
(437, 10, 3.50, 35.00, 186, 220),
(438, 2, 200.00, 400.00, 148, 221),
(439, 1, 7.50, 7.50, 92, 222),
(440, 1, 7.50, 7.50, 92, 222),
(441, 3, 0.20, 0.60, 83, 222),
(442, 1, 0.20, 0.20, 83, 222),
(443, 1, 210.00, 210.00, 173, 222),
(444, 1, 5.00, 5.00, 41, 222),
(445, 4, 3.00, 12.00, 195, 222),
(446, 2, 3.50, 7.00, 186, 223),
(447, 1, 3.50, 3.50, 186, 223),
(448, 3, 0.80, 2.40, 55, 224),
(449, 1, 0.80, 0.80, 55, 224),
(450, 11, 2.80, 30.80, 43, 225),
(451, 6, 3.00, 18.00, 44, 225),
(452, 1, 3.00, 3.00, 44, 225),
(453, 1, 3.00, 3.00, 44, 225),
(454, 5, 8.50, 42.50, 96, 225),
(455, 14, 0.20, 2.80, 82, 226),
(456, 1, 0.20, 0.20, 82, 226),
(457, 1, 0.20, 0.20, 82, 226),
(458, 1, 5.00, 5.00, 41, 226),
(459, 2, 175.00, 350.00, 142, 226),
(460, 1, 175.00, 175.00, 142, 226),
(461, 1, 88.00, 88.00, 143, 226),
(462, 12, 0.20, 2.40, 82, 227),
(463, 36, 0.20, 7.20, 82, 228),
(464, 4, 0.60, 2.40, 183, 229),
(465, 2, 4.50, 9.00, 140, 229),
(466, 1, 105.00, 105.00, 164, 230),
(467, 6, 0.20, 1.20, 82, 231),
(468, 2, 1.00, 2.00, 98, 232),
(469, 8, 16.00, 128.00, 49, 233),
(470, 30, 5.50, 165.00, 185, 233),
(471, 8, 1.00, 8.00, 98, 234),
(472, 1, 5.50, 5.50, 90, 235),
(473, 4, 1.50, 7.20, 66, 236),
(474, 2, 9.50, 19.00, 79, 237),
(475, 36, 0.20, 7.20, 83, 238),
(476, 2, 47.00, 94.00, 25, 239),
(477, 2, 2.80, 5.60, 43, 240),
(478, 1, 210.00, 210.00, 177, 241),
(479, 1, 105.00, 105.00, 157, 242),
(480, 6, 5.00, 30.00, 41, 243),
(481, 6, 2.80, 16.80, 44, 244),
(482, 3, 2.80, 8.40, 43, 244),
(483, 1, 2.80, 2.80, 43, 244),
(484, 2, 3.00, 6.00, 84, 245),
(485, 1, 3.00, 3.00, 84, 245),
(486, 1, 3.00, 3.00, 84, 245),
(487, 3, 2.80, 8.40, 44, 245),
(488, 1, 2.80, 2.80, 44, 245),
(489, 1, 7.00, 7.00, 100, 246),
(490, 3, 8.00, 24.00, 95, 246),
(491, 1, 8.00, 8.00, 95, 246),
(492, 4, 1.80, 7.20, 66, 246),
(493, 3, 2.80, 8.40, 43, 246),
(494, 1, 2.80, 2.80, 43, 246),
(495, 1, 0.20, 0.20, 82, 246),
(496, 5, 4.80, 24.00, 184, 246),
(497, 1, 4.80, 4.80, 184, 246),
(498, 1, 35.00, 35.00, 21, 247),
(499, 1, 210.00, 210.00, 156, 248),
(500, 4, 1.00, 4.00, 98, 248),
(501, 1, 1.00, 1.00, 98, 248),
(502, 2, 210.00, 420.00, 189, 249),
(503, 12, 2.80, 33.60, 43, 250),
(504, 11, 2.80, 30.80, 44, 250),
(505, 1, 2.80, 2.80, 44, 250),
(506, 12, 6.50, 78.00, 91, 250),
(507, 8, 5.00, 40.00, 41, 250),
(508, 1, 5.00, 5.00, 41, 250),
(509, 1, 5.00, 5.00, 41, 250),
(510, 1, 5.00, 5.00, 41, 250),
(511, 1, 5.00, 5.00, 41, 250),
(512, 3, 4.00, 12.00, 191, 250),
(513, 1, 4.00, 4.00, 191, 250),
(514, 32, 4.00, 128.00, 191, 251),
(515, 21, 0.60, 12.60, 183, 251),
(516, 1, 0.60, 0.60, 183, 251),
(517, 1, 0.60, 0.60, 183, 251),
(518, 1, 0.60, 0.60, 183, 251),
(519, 11, 1.00, 11.00, 53, 251),
(520, 1, 1.00, 1.00, 53, 251),
(521, 7, 4.50, 31.50, 207, 252),
(522, 1, 4.50, 4.50, 207, 252),
(523, 4, 2.50, 10.00, 74, 253),
(524, 3, 2.00, 6.00, 124, 254),
(525, 6, 0.60, 3.60, 183, 255),
(526, 17, 5.00, 85.00, 187, 256),
(527, 1, 5.00, 5.00, 187, 256),
(528, 16, 4.00, 64.00, 191, 257),
(529, 6, 3.00, 18.00, 84, 257),
(530, 1, 3.00, 3.00, 84, 257),
(531, 1, 3.00, 3.00, 84, 257),
(532, 4, 2.80, 11.20, 44, 257),
(533, 1, 5.50, 5.50, 185, 257),
(534, 1, 27.00, 27.00, 18, 257),
(535, 2, 2.80, 5.60, 45, 258),
(536, 1, 0.60, 0.60, 117, 259),
(537, 1, 0.60, 0.60, 117, 259),
(538, 1, 0.60, 0.60, 118, 259),
(539, 1, 0.60, 0.60, 118, 259),
(540, 1, 0.60, 0.60, 118, 259),
(541, 1, 0.60, 0.60, 118, 259),
(542, 1, 0.60, 0.60, 118, 259),
(543, 1, 0.60, 0.60, 118, 259),
(544, 2, 8.00, 16.00, 95, 260),
(545, 3, 2.80, 8.40, 43, 261),
(546, 2, 2.80, 5.60, 44, 261),
(547, 6, 0.60, 3.60, 183, 262),
(548, 6, 5.50, 33.00, 185, 262),
(549, 2, 0.20, 0.40, 82, 263),
(550, 1, 0.20, 0.20, 82, 263),
(551, 4, 0.20, 0.80, 82, 264),
(552, 1, 210.00, 210.00, 156, 265),
(553, 1, 105.00, 105.00, 157, 265),
(554, 1, 27.00, 27.00, 18, 266),
(555, 1, 6.00, 6.00, 75, 267),
(556, 1, 6.00, 6.00, 75, 267),
(557, 1, 2.80, 2.80, 43, 268),
(558, 18, 0.20, 3.60, 82, 269),
(559, 1, 0.20, 0.20, 82, 269),
(560, 1, 0.20, 0.20, 82, 269),
(561, 3, 0.30, 0.90, 50, 270),
(562, 1, 0.30, 0.30, 50, 270),
(563, 1, 5.00, 5.00, 41, 271),
(564, 1, 1.00, 1.00, 53, 271),
(565, 3, 2.80, 8.40, 43, 272),
(566, 1, 3.50, 3.50, 99, 273),
(567, 68, 3.50, 238.00, 186, 274),
(568, 10, 8.50, 85.00, 80, 275),
(569, 2, 4.50, 9.00, 207, 276),
(570, 3, 8.50, 25.50, 96, 277),
(571, 4, 1.80, 7.20, 66, 277),
(572, 4, 2.80, 11.20, 43, 277),
(573, 8, 6.00, 48.00, 223, 278),
(574, 1, 105.00, 105.00, 179, 279),
(575, 2, 5.50, 11.00, 90, 280),
(576, 82, 3.50, 287.00, 186, 281),
(577, 48, 3.50, 168.00, 186, 282),
(578, 1, 210.00, 210.00, 171, 283),
(579, 1, 105.00, 105.00, 172, 283),
(580, 1, 175.00, 175.00, 142, 283),
(581, 1, 88.00, 88.00, 143, 283),
(582, 2, 175.00, 350.00, 142, 284),
(583, 1, 88.00, 88.00, 143, 284),
(584, 2, 210.00, 420.00, 165, 285),
(585, 1, 105.00, 105.00, 166, 285),
(586, 1, 34.00, 34.00, 31, 286),
(587, 4, 6.50, 26.00, 91, 287),
(588, 1, 0.20, 0.20, 82, 287),
(589, 4, 0.20, 0.80, 82, 288),
(590, 1, 6.00, 6.00, 215, 289),
(591, 1, 15.00, 15.00, 175, 290),
(592, 48, 3.50, 168.00, 186, 291),
(593, 4, 3.50, 14.00, 186, 292),
(594, 1, 105.00, 105.00, 178, 293),
(595, 4, 2.80, 11.20, 43, 293),
(596, 2, 8.50, 17.00, 96, 293),
(597, 7, 4.50, 31.50, 207, 293),
(598, 1, 200.00, 200.00, 148, 294),
(599, 3, 175.00, 525.00, 142, 295),
(600, 1, 175.00, 175.00, 142, 295),
(601, 1, 88.00, 88.00, 143, 295),
(602, 1, 105.00, 105.00, 166, 296),
(603, 1, 210.00, 210.00, 165, 296),
(604, 1, 1.00, 1.00, 98, 297),
(605, 1, 1.00, 1.00, 98, 297),
(606, 4, 2.80, 11.20, 43, 297),
(607, 1, 2.80, 2.80, 43, 297),
(608, 7, 0.20, 1.40, 83, 297),
(609, 1, 0.20, 0.20, 83, 297),
(610, 3, 8.50, 25.50, 96, 297),
(611, 1, 8.50, 8.50, 96, 297),
(612, 12, 3.00, 36.00, 84, 298),
(613, 11, 2.80, 30.80, 44, 298),
(614, 1, 2.80, 2.80, 44, 298),
(615, 30, 4.00, 120.00, 191, 299),
(616, 10, 6.50, 65.00, 91, 299),
(617, 14, 5.00, 70.00, 41, 299),
(618, 1, 5.00, 5.00, 41, 299),
(619, 1, 37.00, 37.00, 42, 300),
(620, 4, 5.00, 20.00, 41, 301),
(621, 8, 5.00, 40.00, 41, 302),
(622, 1, 5.00, 5.00, 41, 302),
(623, 1, 5.00, 5.00, 41, 302),
(624, 1, 5.00, 5.00, 41, 302),
(625, 20, 0.60, 12.00, 183, 303),
(626, 8, 8.00, 64.00, 247, 304),
(627, 1, 6.00, 6.00, 221, 304),
(628, 1, 200.00, 200.00, 146, 305),
(629, 1, 210.00, 210.00, 156, 306),
(630, 20, 175.00, 3500.00, 142, 307);

--
-- Disparadores `detalleventas`
--
DELIMITER $$
CREATE TRIGGER `nombre_del_trigger` AFTER INSERT ON `detalleventas` FOR EACH ROW UPDATE productos SET stock = stock - New.cantidad WHERE id = New.producto_id
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `id` int(11) NOT NULL,
  `fecha` datetime DEFAULT NULL,
  `monto` decimal(12,2) DEFAULT NULL,
  `usuario` varchar(250) DEFAULT NULL,
  `credito_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`id`, `fecha`, `monto`, `usuario`, `credito_id`) VALUES
(1, '2017-01-03 00:00:00', 20.00, 'jose', 1),
(2, '2017-01-31 10:55:38', 20.00, 'manuel', 1),
(3, '2017-01-31 10:57:39', 5.00, 'manuel apaza', 1),
(4, '2017-01-31 10:58:02', 2.30, 'manuel apaza', 1),
(5, '2017-01-31 11:10:45', 15.50, 'manuel apaza', 1),
(6, '2019-09-20 15:36:07', 121.70, 'DEYSI MAQUERA GOMEZ', 6),
(7, '2019-09-21 10:41:06', 2.00, 'mario matinez', 7),
(8, '2019-09-23 17:50:17', 121.70, 'BRUNO VILCA ALEJO', 6),
(9, '2019-09-23 17:50:26', 121.70, 'BRUNO VILCA ALEJO', 6),
(10, '2019-10-10 21:04:44', 210.30, 'admin root', 3),
(11, '2019-10-22 16:51:28', 243.40, 'DEYSI MAQUERA GOMEZ', 6),
(12, '2019-10-22 16:51:58', 243.40, 'DEYSI MAQUERA GOMEZ', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `codigo` varchar(200) DEFAULT NULL,
  `nombre` varchar(250) DEFAULT NULL,
  `marca` varchar(250) NOT NULL,
  `color` varchar(45) DEFAULT NULL,
  `caracteristica` text DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `stock_minimo` int(11) DEFAULT NULL,
  `precioMayor` decimal(12,2) DEFAULT NULL,
  `precioVenta` decimal(12,2) DEFAULT NULL,
  `precioCompra` decimal(12,2) DEFAULT NULL,
  `tipoproducto_id` int(11) NOT NULL,
  `proveedors_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `codigo`, `nombre`, `marca`, `color`, `caracteristica`, `stock`, `stock_minimo`, `precioMayor`, `precioVenta`, `precioCompra`, `tipoproducto_id`, `proveedors_id`) VALUES
(16, '000006', 'CANGREJO LATERAL', 'FORTE', 'PLATA', 'METAL', -3, 10, 2.00, 2.80, 1.40, 11, 1),
(17, '000001', '4X50', 'DURAFIX', 'AMARILLO', 'CAJA AMARILLA', -1, 10, 26.00, 27.00, 19.00, 12, 1),
(18, '000012', '4X50', 'DURAFIX', 'AMARILLO', 'CAJA AMARILLA', 25, 5, 25.00, 27.00, 19.00, 12, 1),
(19, '000008', '4X30', 'DMUEBLE', 'AMARILLO', 'CAJA DORADA', 1, 1, 45.00, 58.00, 40.00, 12, 7),
(20, '000001', '3.5X15', 'DURAFIX', 'AMARILLO', 'CAJA AMARILLA', 22, 8, 20.00, 30.00, 13.00, 12, 1),
(21, '000002', '3.5X16', 'DURAFIX', 'AMARILLO', 'CAJA AMARILLA', 1, 1, 25.00, 35.00, 14.00, 12, 7),
(22, '000004', '3.5X25', 'DURAFIX', 'AMARILLO', 'CAJA AMARILLA', 6, 2, 26.00, 36.00, 18.00, 12, 7),
(23, '000005', '3.5X30', 'DURAFIX', 'AMARILLO', 'CAJA AMARILLA', 2, 1, 30.00, 40.00, 20.00, 12, 7),
(24, '000007', '4X25', 'DURAFIX', 'AMARILLO', 'CAJA AMARILLA', 8, 2, 35.00, 43.00, 21.50, 12, 7),
(25, '000009', '4X35', 'DURAFIX', 'AMARILLO', 'CAJA AMARILLA', 2, 2, 37.00, 47.00, 23.50, 12, 7),
(26, '000010', '4X45', 'DURAFIX', 'AMARILLO', 'CAJA AMARILLA', 11, 5, 25.00, 30.00, 16.00, 12, 7),
(27, '000011', '5X30', 'DURAFIX', 'AMARILLO', 'CAJA AMARILLA', 6, 2, 28.00, 31.00, 15.90, 12, 7),
(28, '000013', '5x40', 'durafix', 'amarillo', 'caja amarilla y blanca', 13, 3, 34.00, 44.00, 22.00, 12, 7),
(29, '000014', '5X50', 'DURAFIX', 'AMARILLO', 'CAJA AMARILLA', 15, 2, 40.00, 50.00, 25.00, 12, 7),
(30, '000016', '5X70', 'DURAFIX', 'AMARILLO', 'CAJA AMARILLA', 27, 5, 25.00, 32.00, 16.00, 12, 7),
(31, '000017', '5X80', 'DURAFIX', 'AMARILLO', 'CAJA AMARILLA', 9, 2, 26.00, 34.00, 17.00, 12, 7),
(32, '000018', '6X60', 'DURAFIX', 'AMARILLO', 'CAJA AMARILLA', 8, 2, 30.00, 38.00, 19.00, 12, 7),
(33, '000019', '6X70', 'DURAFIX', 'AMARILLO', 'CAJA AMARILLA', 36, 5, 38.00, 44.00, 22.00, 12, 7),
(34, '000020', '6X80', 'DURAFIX', 'AMARILLO', 'CAJA AMARILLA', 14, 5, 45.00, 50.00, 25.00, 12, 7),
(35, '000021', '6X90', 'TINKUR', 'BLANCO', 'CAJA BLANCO /DORADO', 3, 1, 40.00, 56.00, 33.00, 12, 7),
(36, '000022', '6X120', 'DURAFIX', 'AMARILLO', 'CAJA AMARILLA', 48, 8, 38.00, 46.00, 23.00, 12, 7),
(37, '000023', 'DESLIZADOR EN U', 'DMUEBLE', 'GRIS', 'DESLIZADOR EN U', 965, 100, 0.30, 0.40, 0.20, 20, 7),
(38, '000026', 'DESLIZADOR BLANCO', 'DMUEBLE', 'BLANCO', 'DESLIZADORES EN U', 273, 100, 0.30, 0.40, 0.20, 20, 7),
(39, '000024', 'DESLIZADOR CEDRO', 'DMUEBLE', 'CEDRO', 'DESLIZADOR EN U', 138, 50, 0.30, 0.40, 0.20, 20, 7),
(40, '000025', 'DESLIZADOR NEGRO', 'DMUEBLE', 'NEGRO', 'DESLIZADOR EN U', 20, 50, 0.30, 0.40, 0.20, 20, 7),
(41, '000027', 'CHAPA DE ESCRITORIO', 'DMUEBLE', 'ROJO Y BLANCO', 'CAJA DMUEBLE', 0, 10, 3.00, 5.00, 1.70, 28, 7),
(42, '000003', '3.5X17', 'DMUEBLE', 'AMARILLO', 'CAJA DORADO', 1, 1, 27.00, 37.00, 19.00, 12, 7),
(43, '000030', 'BISAGRA LATERAL', 'FORTE', 'BLANCO', 'EMPAQUE BLANCO', 173, 20, 2.30, 2.80, 1.30, 11, 1),
(44, '000031', 'BISAGRA CENTRAL', 'MOVIL', 'METAL', 'EMPAQUE TRANSPARENTE', 201, 20, 2.00, 2.80, 1.30, 11, 1),
(45, '000032', 'BISAGRA INTERIOR', 'FORTE', 'METAL', 'EMPAQUE BLANCO', 124, 10, 2.00, 2.80, 1.30, 11, 7),
(46, '000033', 'D. ANGULAR 175', 'FORMENT&GIOVENZANA', 'METAL', 'EMPAQUE VERDE TRANSPARENTE', 0, 1, 11.00, 12.00, 10.00, 43, 7),
(47, '000034', 'D.ANGULAR135', 'FORMENTI&GIOVENZANA', 'METAL', 'VERDETRANSPARENTE', 9, 1, 11.00, 12.00, 10.00, 43, 7),
(48, '000035', 'D.ANGULAR165', 'MIYASATO', 'METAL', 'EMPAQUEPLOMOAZUL', 0, 1, 14.00, 16.00, 13.00, 43, 7),
(49, '000036', 'D.ANGULAR180', 'LECCO', 'METAL', 'EMPAQUE BLANCO CON VERDE', 10, 1, 14.00, 16.00, 13.00, 43, 1),
(50, '000037', 'SOPORTE DE VIDRIO', 'DMUEBLE', 'TRANPARENTE', 'METAL ACRILICO', 1000, 1, 0.20, 0.30, 0.10, 34, 7),
(51, '000037', 'SOPORTE DE VIDRIO', 'DMUEBLE', 'TRANPARENTE', 'METAL ACRILICO', 4, 1, 0.20, 0.30, 0.10, 34, 7),
(52, '000038', 'SOPORTE DE VIDRIO', 'DMUEBLE', 'METAL', 'METAL/CAUCHO', 52, 1, 1.30, 1.50, 1.00, 34, 7),
(53, '000039', 'PLATINA', 'FORTE', 'METAL', 'METAL 4 HUECOS', 56, 15, 0.70, 1.00, 0.60, 37, 7),
(54, '000039', 'PLATINA', 'FORTE', 'METAL', 'METAL 4 HUECOS', 69, 15, 0.70, 1.00, 0.60, 37, 7),
(55, '000040', 'ANGULO 1´´', 'FORTE', 'METAL', 'METAL', 18, 15, 0.60, 0.80, 0.50, 35, 7),
(66, '000041', 'ANGULO 2', 'FORTE', 'METAL', 'METAL', 10, 15, 1.50, 1.80, 1.00, 35, 7),
(72, '000043', 'PASACABLENEGRO', 'DMUEBLE', 'NEGRO', 'REDONDO', 98, 20, 2.00, 2.50, 1.80, 23, 7),
(73, '000044', 'PASACABLE GRIS', 'DMUEBLE', 'BLANCO', 'REDONDO', 24, 5, 2.00, 2.50, 1.80, 23, 7),
(74, '000045', 'PASACABLE BLANCO', 'DMUEBLE', 'BLANCO', 'REDONDO', 96, 20, 2.00, 2.50, 1.80, 23, 7),
(75, '000046', 'CHAPA A PRESION', 'DMUEBLE', 'METL', 'EMPAQUE TRANSPARENTE', 4, 3, 5.50, 6.00, 5.00, 44, 7),
(76, '000047', 'PLATOS GIRATORIOS', 'FORTE', 'NEGRO', 'PLATOS GIRATORIOS', 7, 2, 20.00, 22.00, 17.00, 32, 7),
(77, '000048', 'PISTON 60', 'AEROLIFT', 'PLOMO', 'EMPAQUETRANSPARENTE', 54, 10, 8.00, 8.50, 7.00, 24, 7),
(78, '000049', 'PISTON80', 'AEROLIFT', 'PLOMO', 'EMPAQUETRASPARENTE', 17, 9, 8.50, 9.50, 8.00, 24, 7),
(79, '000050', 'PISTON100', 'FORMENTI&GIOVENZANA', 'PLOMO', 'EMPAQUETRANSPARENTE', 89, 20, 8.50, 9.50, 8.00, 24, 7),
(80, '000051', 'S.PUSH', 'DMUEBLE', 'PLOMO', 'PARA PUERTAS PUSH', 146, 5, 5.00, 8.50, 3.80, 27, 1),
(81, '000052', 'GARRUCHAS', 'VENEZIA', 'METAL', 'LLANTA METAL', 8, 5, 2.00, 2.50, 1.80, 13, 7),
(82, '000028', 'DESLIZADOR C/ CLAVO', 'DMUEBLE', 'BLANCO', 'BLANCO CON CLAVO', 262, 30, 0.20, 0.20, 0.10, 20, 7),
(83, '000029', 'DESLIZADOR C/CLAVO', 'DMUEBLE', 'NEGRO', 'DESLIZADOR CON CLAVO', 1132, 30, 0.20, 0.20, 0.10, 20, 7),
(84, '000053', 'BISAGRA EUROLOCK', 'EUROLOCKS', 'BLANCO /ROJO', 'EMPAQUE  EUROLOCKS', 22, 15, 2.80, 3.00, 1.32, 11, 7),
(85, '000054', 'CORREDERA SIMPLE', 'MOVIL', 'BLANCO', 'CORREDERA  BLANCA', 43, 5, 2.80, 3.00, 1.20, 21, 7),
(86, '000055', 'CORREDERA SIMPLE', 'MOVIL', 'NEGRO', 'NEGRO 30CM', 28, 5, 2.80, 3.00, 1.20, 21, 7),
(87, '000056', 'CORREDERA 35', 'MOVIL', 'NEGRO', 'SIMPLE35', 6, 1, 3.00, 3.50, 1.80, 21, 7),
(88, '000057', 'CORREDERA SIMPLE', 'MOVIL', 'NEGRO Y BLANCO', 'SIMPLE 40CM', 45, 1, 3.80, 4.00, 2.00, 21, 7),
(89, '000058', 'CORREDERA SIMPLE', 'ASTER', 'NEGRO Y BLANCO', 'CORREDERA 60CM', 11, 1, 5.50, 6.00, 2.90, 21, 7),
(90, '000059', 'TELESCOPICA 30CM', 'EUROLOCK´\'S', 'METAL', 'CORREDERA3OCM', 16, 5, 5.00, 5.50, 3.50, 22, 7),
(91, '000060', 'CORREDERA 35CM', 'DMUEBLE', 'METAL', 'CORREDERA TELESCOPICA', 14, 4, 5.50, 6.50, 4.50, 22, 7),
(92, '000061', 'CORREDERA 40CM', 'VENEZIA', 'METAL', 'TELESCOPICA DE 40CM', -12, 5, 7.00, 7.50, 6.00, 22, 7),
(93, '000061', 'CORREDERA 40CM', 'VENEZIA', 'METAL', 'TELESCOPICA DE 40CM', 20, 5, 7.00, 7.50, 6.00, 22, 7),
(94, '000061', 'CORREDERA 40CM', 'VENEZIA', 'METAL', 'C.TELESCOPICA DE 40CM', 40, 2, 7.00, 7.50, 6.00, 22, 7),
(95, '000062', 'corredera 45cm', 'VENEZIA', 'METAL', 'TELESCOPICA DE 45', 23, 20, 7.80, 8.00, 7.00, 22, 1),
(96, '000063', 'CORREDERA DE 50CM', 'IMPACTO', 'METAL', 'TELESCOPICA DE 50CM', 89, 10, 8.30, 8.50, 7.80, 22, 7),
(97, '000064', 'corredera pesada 60', 'movil', 'metal', 'telescopica pesada60', 64, 35, 10.50, 11.00, 7.30, 22, 7),
(98, '000042', 'CANOPLAS', 'DMUEBLE', 'METAL', 'PARA TUBO COLGADOR', 810, 50, 0.90, 1.00, 0.60, 33, 7),
(99, '000065', '1/2 ciento 4x50', 'DURAFIX', 'AMARILLO', 'EMPAQUE TRANSPARENTE', 3, 1, 3.30, 3.50, 3.00, 12, 7),
(100, '000066', '1CIENTO 4X50', 'DURAFIX', 'AMARILLO', 'EMPAQUE TRANSPARENTE', 2, 1, 6.80, 7.00, 6.50, 12, 7),
(101, '000086', 'JALADOR TINA', 'GIANDELLI', 'METAL', 'EMPAQUE TRANSPARENTE', 135, 1, 4.30, 4.50, 3.00, 14, 7),
(102, '000067', 'BOTON NEGRO', 'DMUEBLE', 'NEGRO', 'JALADOR BOTON', 63, 5, 0.40, 0.50, 0.30, 15, 7),
(103, '000068', 'BOTON AMARILLO', 'DMUEBLE', 'AMARILLO', 'JALADOR BOTON', 9, 1, 0.45, 0.50, 0.30, 15, 7),
(104, '000069', 'BOTON VERDE', 'DMUEBLE', 'VERDE', 'JALADOR BOTON', 46, 5, 0.45, 0.50, 0.30, 15, 7),
(105, '000070', 'BOTON NARANJA', 'DMUEBLE', 'NARANJA', 'JALADOR BOTON', 92, 5, 0.45, 0.50, 0.30, 15, 7),
(106, '000071', 'BOTON FUCCIA', 'DMUEBLE', 'FUXCIA', 'JALADOR FUXCIA', 20, 5, 0.45, 0.50, 0.30, 15, 7),
(107, '000072', 'BOTON MARRON', 'DMUEBLE', 'MARRON', 'JALADOR BOTON', 50, 5, 0.45, 0.50, 0.30, 15, 7),
(108, '000072', 'BOTON MARRON', 'DMUEBLE', 'MARRON', 'JALADOR BOTON', 50, 5, 0.45, 0.50, 0.30, 15, 7),
(109, '000072', 'BOTON MARRON', 'DMUEBLE', 'MARRON', 'JALADOR BOTON', 50, 5, 0.45, 0.50, 0.30, 15, 7),
(110, '000073', 'ASA MARRON', 'DVP', 'MARRON', 'EMPAQUE TRANSPARENTE', 70, 5, 1.00, 1.20, 0.60, 15, 7),
(111, '000073', 'ASA MARRON', 'DVP', 'MARRON', 'EMPAQUE TRANSPARENTE', 70, 5, 1.00, 1.20, 0.60, 15, 7),
(112, '000074', 'ASA GRIS', 'DVP', 'GRIS', 'JALADOR GRIS', 35, 2, 1.00, 1.20, 0.60, 15, 7),
(113, '000074', 'ASA GRIS', 'DVP', 'GRIS', 'JALADOR GRIS', 35, 2, 1.00, 1.20, 0.60, 15, 7),
(114, '000075', 'PVC BLANCO', 'DMUEBLE', 'BLANCO', 'JALADOR BLANCO', 119, 30, 0.45, 0.50, 0.30, 15, 1),
(115, '000075', 'PVC BLANCO', 'DMUEBLE', 'BLANCO', 'JALADOR BLANCO', 127, 5, 0.45, 0.50, 0.30, 15, 7),
(116, '000076', 'VAQUELITA', 'DMUEBLE', 'ALMENDRA', 'EMPAQUE TRASPARENTE', 37, 3, 1.40, 1.50, 0.90, 15, 7),
(117, '000077', 'JALADOR NARANJA', 'DMUEBLE', 'NARANJA', 'JALADOR NARANJA', 60, 5, 0.50, 0.60, 0.30, 15, 7),
(118, '000078', 'JALADOR GRIS', 'DMUEBLE', 'GRIS METALICO', 'JALADOR GRIS', 90, 5, 0.50, 0.60, 0.40, 15, 7),
(119, '000079', 'JALADOR VERDE', 'DMUEBLE', 'VERDE', 'JALADOR VERDE', 28, 2, 0.50, 0.60, 0.40, 15, 7),
(120, '000080', 'JALADOR GRIS', 'DMUEBLE', 'GRIS', 'JALADOR GRIS', 71, 5, 0.50, 0.60, 0.40, 15, 7),
(121, '000081', 'JALADOR NEGRO', 'DMUEBLE', 'NEGRO', 'JALADOR NEGRO', 63, 3, 0.40, 0.50, 0.30, 15, 7),
(122, '000082', 'J.UNICOLORES', 'DMUEBLE', 'COLORES', 'JALADOR COLORES', 25, 2, 0.40, 0.50, 0.30, 15, 7),
(123, '000083', 'JALADOR MARRON', 'DMUEBLE', 'MARRON', 'MARRON OVALADO', 77, 5, 1.80, 2.00, 1.00, 15, 7),
(124, '000084', 'JALADOR CREMA', 'DMUEBLE', 'CREMA', 'CREMA OVALADO', 68, 5, 1.80, 2.00, 1.00, 15, 7),
(125, '000085', 'JALADOR NARANJA', 'DMUEBLE', 'NARANJA', 'NARANJA LARGO', 34, 4, 2.60, 3.00, 1.60, 15, 1),
(126, '000087', 'JALADOR MARRON', 'DMUEBLE', 'MARRON', 'MARRON GRANDE', 18, 2, 2.80, 3.00, 1.60, 15, 7),
(127, '000088', 'TINA DECORATIVA', 'GIANDELLI', 'METAL', 'CAJA BLANCA', 7, 1, 5.80, 6.00, 5.50, 14, 7),
(128, '000090', 'BARRA DECORATIVA', 'GIANNELLI', 'METAL', 'JALADOR MEDIANO', 162, 10, 4.30, 4.50, 4.00, 14, 1),
(129, '000091', 'BARRA DECORATIVA', 'GIANDELLI', 'METAL', 'BARRA GRANDE', 65, 5, 5.80, 6.00, 5.50, 14, 7),
(130, '000092', 'JAALDOR ALUMINIO', 'GIANDELLI', 'METAL', 'CAJA BLANCA', 55, 5, 5.70, 6.00, 5.50, 14, 7),
(131, '000094', 'ALUMINIO OVALADO', 'MOBILE', 'ALUMINIO', 'JALADOR ALUMINIO', 89, 10, 6.50, 7.00, 6.30, 14, 7),
(132, '000095', 'PARIS 128', 'GIANDELLI', 'ALUMINIO', 'JALADOR OVALADO', 27, 5, 7.20, 7.50, 7.00, 14, 7),
(133, '000096', 'TIRADOR ZERALDHY', 'GIANDELLI', 'ALUMINIO', 'TIRADOR CURVEADO', 16, 3, 7.00, 7.50, 6.80, 14, 7),
(134, '000097', 'TIRADOR ZAMAK', 'ZAMAK', 'VARIOS', 'METAL/COLOR', 24, 5, 11.50, 12.00, 10.00, 14, 7),
(135, '000098', 'METAL ALUMINIO', 'GIANDELLI', 'ALIMINIO', 'ALUMINIO /METAL', 99, 10, 5.70, 6.00, 5.00, 14, 7),
(136, '000093', 'ALUMINIO MEDIANO', 'GIANDELII', 'ALUMINIO', 'JALADOR MEDIANO', 29, 5, 4.50, 5.00, 2.70, 14, 7),
(137, '000099', 'ALUMINIO GRANDE', 'GIANDELLI', 'ALUMINIO', 'JALADOR ALUMINIO', 62, 5, 4.50, 5.00, 4.00, 14, 7),
(138, '000100', 'ALUMINIO PEQUEÑO', 'GIANDELLI', 'ALUMINIO', 'JALADOR ALUMINIO', 66, 5, 3.80, 4.00, 3.50, 14, 7),
(139, '000101', 'JALADOR BARRA D', 'GIANDELLI', 'MATE', 'BARRA DECORATIVA', 47, 4, 4.70, 5.00, 4.50, 14, 7),
(140, '000102', 'ALUMINIO 96', 'GIANDELLI', 'ALUMINIO', 'JALADOR  ALUMINIO', 61, 3, 4.20, 4.50, 4.00, 14, 1),
(141, '000103', 'ALUMINIO EN U', 'GIANDELLI', 'ALUMINIO', 'ALUMINIO EN U', 37, 7, 4.30, 4.50, 3.80, 14, 7),
(142, '000124', 'MELAMINA BLANCO', 'VESTO', 'BLANCO', 'BLANCO DE 18MM', 14, 5, 170.00, 175.00, 125.10, 47, 7),
(143, '000125', 'BLANCO MITAD', 'VESTO', 'BLANCO', 'BLANCO 18MM', 23, 4, 80.00, 88.00, 62.60, 47, 7),
(144, '000126', 'GRIS HUMO', 'VESTO', 'GRIS HUMO', 'GRIS HUMO18MM', 14, 4, 195.00, 200.00, 164.64, 48, 7),
(145, '000127', 'GRIS MITAD', 'VESTO', 'GRIS HUMO', 'GRIS H 18MM', 8, 5, 95.00, 100.00, 82.32, 48, 7),
(146, '000128', 'ALMENDRA', 'VESTO', 'ALMENDRA', 'PLC.ENTERA', 12, 3, 195.00, 200.00, 164.40, 48, 7),
(147, '000129', 'ALMENDRA MITAD', 'VESTO', 'ALMENDRA', 'ALMENDRA MITAD 18MM', 8, 2, 98.00, 100.00, 82.32, 48, 7),
(148, '000130', 'NEGRO ENTERO', 'VESTO', 'NEGRO', 'NEGRO 18MM ENTERO', 11, 3, 195.00, 200.00, 164.64, 48, 7),
(149, '000131', 'NEGRO MITAD', 'VESTO', 'NEGRO', 'NEGRO MITAD 18MM', 8, 2, 98.00, 100.00, 82.32, 48, 7),
(150, '000132', 'TEKA ARTICO ENTERO', 'VESTO', 'TEKA ARTICO', 'T.A.ENTERO 18MM', 8, 2, 205.00, 210.00, 176.97, 49, 7),
(151, '000133', 'TEKA ARTICO MITAD', 'VESTO', 'TEKA ARTICO', 'MITAD TEKA 18MM', 12, 3, 103.00, 105.00, 88.49, 49, 1),
(152, '000135', 'SEDA N.MITAD', 'VESTO', 'SEDA NOTTE', 'MITAD SEDA NOTTE', 10, 2, 103.00, 105.00, 84.66, 49, 1),
(153, '000136', 'JEREZ ENTERO', 'VESTO', 'JEREZ', 'JEREZ ENTERO 18MM', 12, 2, 208.00, 210.00, 169.32, 49, 7),
(154, '000134', 'SEDA NOTTE', 'VESTO', 'SEDA NOTTE ENTERO', 'NUEVA TENDENCIA', 13, 1, 205.00, 210.00, 169.32, 49, 7),
(155, '000137', 'JEREZ MITAD', 'VESTO', 'JEREZ', 'MITAD JEREZ', 6, 2, 103.00, 105.00, 84.66, 49, 7),
(156, '000138', 'WENGUE', 'VESTO', 'WENGUE ENTERO', NULL, 3, 2, 205.00, 210.00, 157.90, 49, 7),
(157, '000139', 'WENGUE MITAD', 'VESTO', 'WENGUE', 'WENGUE MITAD', 8, 2, 103.00, 105.00, 78.95, 49, 7),
(158, '000140', 'VENEZIA ENTERO', 'VESTO', 'VENEZIA', 'NUEVA TENDENCIA', 15, 2, 208.00, 210.00, 169.32, 49, 7),
(159, '000141', 'VENEZIA MITAD', 'VESTO', 'VENEZIA MITAD', 'NUEVA TENDENCIA', 8, 2, 103.00, 105.00, 84.66, 49, 7),
(160, '000143', 'CEREZO MITAD', 'VESTO', 'CEREZO', 'CEREZO 18MM', 7, 2, 103.00, 105.00, 84.16, 49, 7),
(161, '000144', 'ROBLE CAVA', 'VESTO 18MM', 'ROBLE CAVA', 'PLC. ENTERO', 15, 2, 208.00, 210.00, 176.97, 49, 7),
(162, '000145', 'ROBLE CAVA MITAD', 'VESTO 18MM', 'ROBLE CAVA', 'MITAD DE PLC', 8, 2, 103.00, 105.00, 88.49, 49, 7),
(163, '000146', 'OLMO PARDO', 'VESTO 18MM', 'OLMO PARDO', 'PLC. ENTERO', 15, 2, 208.00, 210.00, 176.97, 49, 7),
(164, '000147', 'OLMO PARDO MITAD', 'VESTO 18MM', 'OLMO PARDO', 'MITAD DE PLC.', 6, 2, 103.00, 105.00, 88.49, 49, 7),
(165, '000148', 'TOSCANA', 'VESTO 18MM', 'TOSCANA', 'PLC.ENTERO', 12, 2, 208.00, 210.00, 176.97, 49, 7),
(166, '000149', 'TOSCANA', 'VESTO 18MM', 'TOSCANA', 'MITAD DE PLC.', 6, 2, 103.00, 105.00, 88.49, 49, 7),
(167, '000150', 'ASERRADO NORDICO', 'VESTO 18MM', 'ASERRADO NORDICO', 'PLC.ENTERO', 8, 2, 208.00, 210.00, 176.97, 49, 7),
(168, '000151', 'ASERRADO NORDICO MITAD', 'VESTO 18MM', 'ASERRADO NORDICO', 'MITAD DE PLC.', 8, 2, 103.00, 105.00, 88.49, 49, 7),
(169, '000152', 'NOGAL TERRACOTA', 'VESTO 18MM', 'NOGAL TERRACOTA', 'PLC. ENTERA', 15, 2, 208.00, 210.00, 176.97, 49, 7),
(170, '000153', 'NOGAL T. MITAD', 'VESTO 18MM', 'NOGAL TERRACOTA', 'MITAD DE PLC.', 8, 2, 103.00, 105.00, 88.49, 49, 7),
(171, '000154', 'NOCCE MILANO', 'VESTO 18MM', 'NOCCE MILANO', 'PLC.ENTERA', 14, 2, 208.00, 210.00, 176.97, 49, 7),
(172, '000155', 'NOCCE MILANO MITAD', 'VESTO 18MM', 'NOCCE MILANO', 'MITAD DE PLC.', 7, 2, 103.00, 105.00, 88.49, 49, 7),
(173, '000156', 'CENDA ESCADRINAVO', 'VESTO 18MM', 'CENDRA ESCADRINAVO', 'PLC. ENTERA', 14, 2, 208.00, 210.00, 176.97, 49, 7),
(174, '000157', 'CENDRA ESCADRINAVO MITAD', 'VESTO 18MM', 'CENDRA ESCADRINAVO', 'MITAD DE PLC.', 8, 2, 103.00, 105.00, 88.49, 49, 7),
(175, '000164', 'SISTEMA D 25', 'FORTE', 'METAL', 'EMPAQUE TRANSPARENTE', 5, 2, 14.00, 15.00, 8.00, 26, 7),
(176, '000105', 'BARRA MATE MEDIANA', 'DMUEBLE', 'METAL MATE', 'EMPAQUE TRANSPARENTE', 0, 2, 3.80, 4.00, 3.00, 31, 7),
(177, '000158', 'AWOURA ENTERO', 'VESTO 18MM', 'AWOURA', 'PLC.ENTERA', 11, 1, 208.00, 210.00, 176.97, 49, 1),
(178, '000159', 'AWOURA MITAD', 'VESTO 18MM', 'AWOURA', 'MEDIA PLC.', 6, 2, 103.00, 105.00, 88.49, 49, 7),
(179, '000161', 'FRSNO EUROPEO', 'VESTO 18MM', 'FRSNO EUROPEO', 'MEDIA PLC.', 7, 2, 103.00, 105.00, 88.45, 49, 7),
(180, '000161', 'FRSNO EUROPEO', 'VESTO 18MM', 'FRSNO EUROPEO', 'MEDIA PLC.', 8, 2, 103.00, 105.00, 88.45, 49, 7),
(181, '000162', 'SEDA GIORNO', 'VESTO 18MM', 'SERA GIORNO', 'PLC. ENTERO', 15, 2, 208.00, 210.00, 169.32, 49, 7),
(182, '000163', 'SEDA GIORNO MITAD', 'VESTO 18MM', 'SEDA GIORNO', 'NUEVA TENDENCIA', 15, 2, 103.00, 105.00, 84.70, 49, 7),
(183, '000166', 'ESTOBOLES', 'DMUEBLE', 'DORADO', 'TORNILLO ESTOBOL', 100, 5, 0.50, 0.60, 0.40, 12, 7),
(184, '000104', 'JALADOR ALUMINIO EN U', 'GIANDELLI', 'ALUMINIO', 'EMPAQUE TRANSPARENTE', 36, 5, 4.50, 4.80, 4.50, 14, 7),
(185, '000106', 'JALADOR BARRA  CROMADO', 'GIANDELLI', 'METAL', 'EMPAQUE TRANSPARENTE', 5, 5, 5.00, 5.50, 4.80, 31, 7),
(186, '000108', 'JALADOR CORBATA MEDIANO', 'DMUEBLE', 'CROMADO', 'EMPAQUE TRANSPARENTE', 548, 2, 3.30, 3.50, 3.00, 14, 1),
(187, '000109', 'JALADOR CORBATA NEGRO', 'GIANDELLI', 'NEGRO', 'EMPAQUE TRANSPARENTE', 29, 5, 4.70, 5.00, 4.50, 14, 7),
(188, '000110', 'JALADOR CRUVO', 'GIANDELLI', 'ALUMINIO', 'EMPAQUE TRANSPARENTE', 14, 17, 5.30, 5.50, 5.00, 14, 7),
(189, '000142', 'CEREZO ENTERO', 'VESTO', 'CEREZO', 'PLC.ENTERO', 12, 2, 208.00, 210.00, 168.12, 49, 7),
(190, '000111', 'JALADOR CROMADO 160mm', 'GIANDELLI', 'CROMADO', 'EMPAQUE TRANSPARENTE', 38, 4, 7.50, 8.00, 6.00, 14, 7),
(191, '000112', 'JALADOR  ASTRAL', 'GIANDELLI', 'CROMADO', 'EMPAQUE TRANSPARENTE', 7, 5, 3.80, 4.00, 3.50, 14, 7),
(192, '000113', 'PERILLA ICARO 20mm', 'GIANDELLI', 'CROMADO', 'EMPAQUE TRANSPARENTE', 68, 5, 3.80, 4.00, 3.50, 14, 7),
(193, '000114', 'BOTON NEGRO', 'GIANDELLI', 'NEGRO', 'EMPAQUE TRANSPARENTE', 18, 2, 3.80, 4.00, 3.50, 14, 7),
(194, '000115', 'BOTON MATE', 'GIANDELLI', 'MATE', 'EMPAQUE TRANSPARENTE', 10, 1, 4.00, 4.30, 3.80, 14, 7),
(195, '000116', 'TINA REDONDO', 'GIANDELLI', 'CROMADO', 'EMPAQUE TRANSPARENTE', 29, 3, 2.80, 3.00, 2.50, 14, 7),
(196, '000117', 'BARRA CROMADA 160', 'GIANDELLI', 'CROMADA', 'EMPAQUE TRANSPARENTE', 247, 10, 6.00, 6.50, 5.50, 14, 7),
(197, '000118', 'CORBATA NEGRO/DORADO', 'GIANDELLI', 'NEGRO DORADO', 'EMPAQUE TRANSPARENTE', 74, 5, 5.50, 6.00, 5.00, 14, 7),
(198, '000119', 'CORBATA DORADO', 'GIANDELLI', 'DORADO', 'EMPAQUE TRANSPARENTE', 50, 5, 5.50, 6.00, 5.00, 14, 7),
(199, '000120', 'BARRA MEDIANO', 'GIANDELLI', 'DORADO', 'EMPAQUE TRANSPARENTE', 42, 5, 4.80, 5.00, 4.50, 14, 7),
(200, '000121', 'COLGADOR', 'GIANDELLI', 'CROMADO', 'EMPAQUE TRANSPARENTE', 18, 2, 3.00, 3.50, 2.80, 14, 7),
(201, '000122', 'COLGADOR DOBLE', 'GIANDELLI', 'CROMADO', 'EMPAQUE TRANPARENTE', 29, 2, 3.80, 4.00, 3.50, 14, 7),
(202, '000123', 'MEDIA LUNA DORADO', 'GIANDELLI', 'DORADO', 'EMPAQUE TRANSPARENTE', 49, 4, 3.30, 3.50, 3.00, 14, 7),
(203, '000165', 'ANGULOS MARRON', 'DMUEBLE', 'MARRON', 'ANGULOS PVC', 45, 2, 0.60, 0.70, 0.50, 36, 7),
(204, '000167', 'ANGULOS NEGRO', 'DMUEBLE', 'NEGRO', 'ANGULOS PVC', 59, 5, 0.60, 0.70, 0.50, 36, 7),
(205, '000168', 'ANGULOS  BLANCO', 'DMUEBLE', 'BLANCO', 'ANGULOS PVC', 43, 4, 0.60, 0.70, 0.60, 36, 7),
(206, '000169', 'ANGULOS GRIS', 'DMUEBLE', 'GRIS', 'ANGULOS PVC', 12, 2, 0.60, 0.70, 0.50, 36, 7),
(207, '000107', 'JALADOR CORBATA GRANDE', 'GIANDELLI', 'CROMADO', 'EMPAQUE TRANSPARENTE', 112, 2, 4.30, 4.50, 4.00, 14, 7),
(208, '000170', 'TP T. BLANCO', 'DMUEBLE', 'BLANCO', 'BLISTER BLANCO', 22, 2, 5.80, 6.00, 3.00, 25, 7),
(209, '000171', 'TP.T.NEGRO', 'DMUEBLE', 'NEGRO', 'BLISTER NEGRO', 13, 2, 5.80, 6.00, 3.00, 25, 7),
(210, '000172', 'TP.T. WENGUE', 'DMUEBLE', 'WENGUE', 'BLISTER WENGUE', 11, 2, 5.80, 6.00, 3.00, 25, 7),
(211, '000173', 'TP.T.FUXIA', 'DMUEBLE', 'FUXIA', 'BLISTER FUXIA', 10, 2, 5.80, 6.00, 3.00, 25, 7),
(212, '000174', 'TP.T.ROJO', 'DMUEBLE', 'ROJO', 'BLISTER ROJO', 8, 1, 5.80, 6.00, 3.00, 25, 7),
(213, '000175', 'TP. T.TEKA ARTICO', 'DMUEBLE', 'TEKA ARTICO', 'BLISTER TEKA', 11, 2, 5.80, 6.00, 3.00, 25, 7),
(214, '000176', 'TP. T. CEDRO', 'DMUEBLE', 'CEDRO', 'BLISTER CEDRO', 13, 2, 5.80, 6.00, 3.00, 25, 7),
(215, '000177', 'TP. T. OLMO PARDO', 'DMUEBLE', 'OLMO PARDO', 'BLISTER OLMO PARDO', 8, 2, 5.80, 6.00, 3.00, 25, 7),
(216, '000178', 'TP. T. ALMENDRA', 'DMUEBLE', 'ALMENDRA', 'BLISTER ALMENDRA', 16, 2, 5.80, 6.00, 3.00, 25, 7),
(217, '000179', 'TP. T. HAYA', 'DMUEBLE', 'HAYA', 'BLISTER HAYA', 9, 2, 5.80, 6.00, 3.00, 25, 7),
(218, '000180', 'TP. T. JEREZ', 'DMUEBLE', 'JEREZ', 'BLISTER JEREZ', 8, 1, 5.80, 6.00, 3.00, 25, 7),
(219, '000182', 'TP.T. ROBLE RUSTICO', 'DMUEBLE', 'JEREZ', 'BLISTER ROBLE RUSTICO', 10, 2, 5.80, 6.00, 3.00, 25, 7),
(220, '000181', 'TP. T. GRIS', 'DMUEBLE', 'GRIS', 'BLISTER GRIS', 8, 1, 5.80, 6.00, 3.00, 25, 7),
(221, '000183', 'TP. T. TOSCANA', 'DMUEBLE', 'TOSCANA', 'BLISTER TOSCANA', 2, 1, 5.80, 6.00, 3.00, 25, 7),
(222, '000184', 'TP. T. FRESNO EUROPEO', 'DMUEBLE', 'FRESNO EUROPEO', 'BLISTER FRESNO EUROPEO', 7, 1, 5.80, 6.00, 3.00, 25, 7),
(223, '000185', 'TP. T. NOGAL TERRACOTA', 'DMUEBLE', 'NOGAL TERRACOTA', 'BLISTER NOGAL', 0, 2, 5.80, 6.00, 3.00, 25, 7),
(224, '000186', 'TAPACANTO NEGRO', 'DMUEBLE', 'NEGRO', 'TAPACANTO DELGADO NEGRO', 2, 1, 68.00, 70.00, 50.00, 29, 7),
(225, '000187', 'TAPACANTO D . NEGRO X METRO', 'DMUEBLE', 'NEGRO', 'DELGADO NEGRO X METRO', 200, 5, 0.40, 0.50, 0.30, 29, 1),
(226, '000188', 'TAPACANTO D. ROJO', 'DMUEBLE', 'ROJO', 'ROLLO DELGADO ROLLO', 2, 1, 68.00, 70.00, 50.00, 29, 7),
(227, '000189', 'TAPACANTO D.ROJO X METRO', 'DMUEBLE', 'ROJO', 'DELGADO ROJO X  METRO', 200, 5, 0.40, 0.50, 0.30, 29, 7),
(228, '000190', 'TAPACANTO DELGADO AWOURA', 'DMUEBLE', 'AWOURA', 'ROLLO AMADERADO', 1, 1, 68.00, 70.00, 50.00, 29, 7),
(229, '000191', 'TAPACANTO D. AWOURA  X METRO', 'DMUEBLE', 'AWOURA', 'AWOURA X METRO', 200, 5, 0.40, 0.50, 0.30, 29, 7),
(230, '000192', 'TAPACANTO D.NOCCE MILANO', 'DMUEBLE', 'NOCCE  MILANO', 'ROLLO ENTERO NOCCE MILANO', 1, 1, 65.00, 70.00, 50.00, 29, 1),
(231, '000193', 'TAPACANTO.D.NOCCE MILANO', 'DMUEBLE', 'NOCCE MILANO', 'TAPACANTO X METRO', 200, 5, 0.40, 0.50, 0.30, 29, 1),
(232, '000194', 'TAPACANTO .D.TEKA ARTICO', 'DMUEBLE', 'TEKA ARTICO', 'ROLLO ENTERO TEKA ARTICO', 3, 1, 65.00, 70.00, 50.00, 29, 1),
(233, '000195', 'TAPACANTO.D.TEKA ARTICO', 'DMUEBLE', 'TEKA ARTICO', 'TEKA ARTICO X METRO', 200, 5, 0.40, 0.50, 0.30, 29, 1),
(234, '000196', 'TAPACANTO.D.CEDRO', 'DMUEBLE', 'CEDRO', 'ROLLO ENTERO CEDRO', 3, 1, 65.00, 70.00, 50.00, 29, 1),
(235, '000197', 'TAPACANTO D.CEDRO X METRO', 'DMUEBLE', 'CEDRO', 'X METRO CEDRO', 200, 5, 0.40, 0.50, 0.30, 29, 1),
(236, '000198', 'TAPACANTO D.WENGUE ROLLO ENTERO', 'DMUEBLE', 'WENGUE', 'ROLLO ENTERO WENGUE', 3, 1, 65.00, 70.00, 50.00, 29, 1),
(237, '000199', 'TAPACANTO D.WENGUE X METRO', 'DMUEBLE', 'WENGUE', 'X METRO WENGUE', 200, 5, 0.40, 0.50, 0.30, 29, 1),
(238, '000200', 'TAPACANTO D.BLANCO ROLLO ENTERO', 'DMUBLE', 'BLANCO', 'ROLLO ENTERO BLANCO', 4, 1, 45.00, 50.00, 40.00, 29, 1),
(239, '000201', 'TAPACANTO D.BLANCO X METRO', 'DMUEBLE', 'BLANCO', 'X METRO BLANCO', 200, 5, 0.40, 0.50, 0.30, 29, 1),
(240, '000202', 'TAPACANTO.D.OLMO PARDO ROLLO ENTERO', 'DMUEBLE', 'OLMO PARDO', 'ROLLO ENTERO', 2, 1, 65.00, 70.00, 50.00, 29, 1),
(241, '000203', 'TAPACANTO .D.OLMO PARDO X METRO', 'DMUEBLE', 'OLMO PARDO', 'X METRO OLMO PARDO', 200, 5, 0.40, 0.50, 0.30, 29, 1),
(242, '000204', 'TAPACANTO D.TWIS /SEDA NOTTE', 'DMUEBLE', 'TWIS /SEDA N.', 'ROLLO ENTERO', 1, 1, 65.00, 70.00, 50.00, 29, 1),
(243, '000205', 'TAPACANTO D TWIS /SEDA N. X METRO', 'DMUEBLE', 'TWIS / SEDA N,', 'X METRO TWIS/ SEDA N.', 200, 5, 0.40, 0.50, 0.30, 29, 1),
(244, '000206', 'TP.D ROBLE CAVA ROLLO ENTERO', 'DMUEBLE', 'ROBLE CAVA', 'ROLLO ENTERO ROBLE CAVA', 2, 1, 65.00, 70.00, 50.00, 29, 7),
(245, '000207', 'TAPACANTO D. ROBLE CAVA X METRO', 'DMUEBLE', 'ROBLE CAVA', 'X METRO ROBLE CAVA', 200, 5, 0.40, 0.50, 0.30, 29, 1),
(246, '000230', 'PATAS CROMADAS M', 'DMUEBLE', 'CROMADO', 'MEDIANA DE 8CM', 41, 3, 5.50, 6.00, 5.00, 19, 7),
(247, '000231', 'PATAS CROMADAS G', 'DMUEBLE', 'CROMADO', 'GRANDES DE 10CM', 10, 1, 7.50, 8.00, 7.00, 19, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedors`
--

CREATE TABLE `proveedors` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `ruc` varchar(45) NOT NULL,
  `direccion` varchar(150) NOT NULL,
  `correo` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedors`
--

INSERT INTO `proveedors` (`id`, `nombre`, `ruc`, `direccion`, `correo`) VALUES
(1, 'jose ', '', '', 'jose@hotmmail.com'),
(2, 'Zea', '', '', 'bad@hotmail.com'),
(3, 'Gamboa', '', '', 'ganboa@sac.com'),
(4, 'Nice', '', '', 'nice@gmail.com'),
(5, 'Mariano', '6497784632', 'Calle ayacucho M°3', 'mariano@gmail.com'),
(6, 'generico', '3216546543', 'samegua', 'gmail@gmail.com'),
(7, 'GENERICO', '10033259334', 'MOQUEGUA', 'GENERICO@hotmmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registros`
--

CREATE TABLE `registros` (
  `id` int(11) NOT NULL,
  `accion` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `producto_id` int(11) NOT NULL,
  `cantidad` int(6) DEFAULT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `registros`
--

INSERT INTO `registros` (`id`, `accion`, `descripcion`, `producto_id`, `cantidad`, `fecha`) VALUES
(27, 'agregar', '', 29, 10, '2019-08-23 15:15:20'),
(28, 'quitar', '', 29, 1, '2019-08-27 11:34:49'),
(29, 'agregar', '', 16, 199, '2019-08-29 11:14:17'),
(30, 'quitar', '', 16, 398, '2019-08-29 11:46:00'),
(31, 'quitar', '', 17, 150, '2019-08-29 16:51:05'),
(32, 'quitar', '', 49, 3, '2019-08-29 17:13:32'),
(33, 'quitar', '', 20, 1, '2019-08-30 14:38:52'),
(34, 'agregar', '', 21, 1, '2019-08-30 14:39:25'),
(35, 'quitar', '', 21, 2, '2019-08-30 14:40:09'),
(36, 'quitar', '', 66, 66, '2019-09-02 15:58:47'),
(37, 'quitar', '', 93, 17, '2019-09-04 16:35:17'),
(38, 'quitar', '', 94, 17, '2019-09-04 16:35:27'),
(39, 'agregar', '', 90, 17, '2019-09-04 18:23:36'),
(40, 'agregar', '', 96, 19, '2019-09-05 11:52:23'),
(41, 'agregar', '', 96, 18, '2019-09-05 12:16:47'),
(42, 'agregar', '', 96, 19, '2019-09-05 12:48:17'),
(43, 'quitar', '', 96, 19, '2019-09-05 12:49:01'),
(44, 'agregar', '', 96, 19, '2019-09-05 12:50:35'),
(45, 'agregar', '', 96, 16, '2019-09-05 12:50:42'),
(46, 'agregar', '', 98, 598, '2019-09-05 15:02:15'),
(47, 'agregar', '', 52, 50, '2019-09-06 11:51:03'),
(48, 'agregar', '', 97, 45, '2019-09-06 11:58:14'),
(49, 'agregar', '', 99, 1, '2019-09-11 12:36:52'),
(50, 'agregar', '', 49, 10, '2019-09-11 15:25:45'),
(51, 'quitar', '', 49, 10, '2019-09-13 10:13:01'),
(52, 'quitar', '', 106, 30, '2019-09-17 15:54:33'),
(53, 'agregar', '', 101, 20, '2019-09-19 15:33:14'),
(54, 'agregar', '', 101, 60, '2019-09-19 15:41:14'),
(55, 'agregar', '', 101, 23, '2019-09-19 15:53:15'),
(56, 'agregar', '', 83, 1000, '2019-09-26 16:08:06'),
(57, 'quitar', '', 91, 1, '2019-09-27 16:40:39'),
(58, 'agregar', '', 87, 40, '2019-09-27 16:41:07'),
(59, 'agregar', '', 88, 40, '2019-09-27 16:41:27'),
(60, 'quitar', '', 87, 40, '2019-09-27 16:42:05'),
(61, 'agregar', '', 87, 1, '2019-09-27 16:42:26'),
(62, 'agregar', '', 91, 40, '2019-09-27 16:42:35'),
(63, 'agregar', '', 94, 40, '2019-09-27 16:43:45'),
(64, 'agregar', '', 95, 40, '2019-09-27 16:44:03'),
(65, 'agregar', '', 43, 200, '2019-09-27 16:56:54'),
(66, 'agregar', '', 44, 200, '2019-09-27 16:57:03'),
(67, 'agregar', '', 47, 10, '2019-10-02 15:41:10'),
(68, 'agregar', '', 49, 20, '2019-10-02 15:49:32'),
(69, 'agregar', '', 49, 2, '2019-10-02 15:50:13'),
(70, 'agregar', '', 140, 13, '2019-10-11 17:57:26'),
(71, 'agregar', '', 18, 12, '2019-10-15 16:51:25'),
(72, 'quitar', '', 19, 3, '2019-10-15 16:52:57'),
(73, 'quitar', '', 186, 4, '2019-10-16 16:26:27'),
(74, 'agregar', '', 186, 150, '2019-10-16 16:26:48'),
(75, 'agregar', '', 50, 1000, '2019-10-16 17:22:32'),
(76, 'agregar', 'CORBATA', 186, 200, '2019-10-19 16:45:16'),
(77, 'agregar', 'CORBATA', 186, 200, '2019-10-19 16:45:27'),
(78, 'quitar', '', 186, 352, '2019-10-21 10:21:50'),
(79, 'agregar', '', 186, 200, '2019-10-21 10:23:22'),
(80, 'agregar', '', 186, 400, '2019-10-23 09:42:26'),
(81, 'agregar', '', 93, 20, '2019-10-24 12:17:47'),
(82, 'quitar', '', 92, 0, '2019-10-24 12:18:13');

--
-- Disparadores `registros`
--
DELIMITER $$
CREATE TRIGGER `EditProducts` AFTER INSERT ON `registros` FOR EACH ROW BEGIN
	IF (NEW.accion= 'agregar')
    THEN
    UPDATE productos
        SET stock = stock + New.cantidad                      
    WHERE id = NEW.producto_id;
    END IF;
    IF (NEW.accion= 'quitar')
    THEN
    UPDATE productos
        SET stock = stock - New.cantidad                      
    WHERE id = NEW.producto_id;
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefonos`
--

CREATE TABLE `telefonos` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(80) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `proveedors_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefonos`
--

INSERT INTO `telefonos` (`id`, `descripcion`, `telefono`, `proveedors_id`) VALUES
(1, 'Oficina', '9562322', 1),
(2, 'Casa', '69499565', 1),
(3, 'Trabajo', '95864876', 1),
(4, 'otros', '61312645', 1),
(5, 'Trabajo', '95236415', 2),
(6, 'Tienda', '9476563', 1),
(7, 'Casa de jefe', '947653215', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoproductos`
--

CREATE TABLE `tipoproductos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipoproductos`
--

INSERT INTO `tipoproductos` (`id`, `nombre`, `estado`) VALUES
(1, 'escritorio', 0),
(2, 'regalo', 0),
(3, 'jardin', 0),
(4, 'juegos de mesa', 0),
(5, 'papeleria', 0),
(6, 'tecnologia', 0),
(7, 'Zea', 0),
(8, 'Camisa', 0),
(9, 'dwqd', 0),
(10, 'nuevasooo', 0),
(11, 'BISAGRA', 1),
(12, 'TORNILLOS', 1),
(13, 'GARUCHAS', 1),
(14, 'JALADORES', 0),
(15, 'JALADORES  PVC', 1),
(16, 'TAPACANTOS', 0),
(17, 'MELAMINA', 0),
(18, 'TAPACANTOS', 0),
(19, 'PATAS CROMADAS', 1),
(20, 'DESLIZADORES', 1),
(21, 'CORREDERAS', 0),
(22, 'CORREDERAS  TELESCOPICAS', 1),
(23, 'PASACABLES', 1),
(24, 'PISTONES', 1),
(25, 'TAPATORNILLOS', 1),
(26, 'JUEGO DUQUESA', 1),
(27, 'SITEMA PUSH', 1),
(28, 'CHAPAS DE ESCRITORIO', 1),
(29, 'TAPACANTO DELGADO', 1),
(30, 'TAPACANTO GRUESO', 1),
(31, 'JALADORES', 1),
(32, 'PLATOS GIRATORIOS', 1),
(33, 'CANOPLAS', 1),
(34, 'SOPORTE DE VIDRIO', 1),
(35, 'ANGULO DE METAL', 1),
(36, 'ANGULO DE PLASTICO', 1),
(37, 'PLATINAS', 1),
(38, 'COLGADORES', 1),
(39, 'TUBO COLGADOR', 1),
(40, 'PLANCHA DE NORDEX', 1),
(41, 'PLANCHA DE MELAMINA', 1),
(42, 'SISTEMA CORREDISO', 1),
(43, 'BISAGRA DOBLE ANGULAR', 1),
(44, 'CHAPA PUSH', 1),
(45, 'UNION DOBLE PVC', 1),
(46, 'JALADOR PVC', 1),
(47, 'MELAMINA BLANCO', 1),
(48, 'MELAMINA UNICOLOR', 1),
(49, 'MELAMINA AMADERADO', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipousers`
--

CREATE TABLE `tipousers` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipousers`
--

INSERT INTO `tipousers` (`id`, `nombre`) VALUES
(1, 'Adminstrador'),
(2, 'Empleado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nombres` varchar(100) DEFAULT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `dni` varchar(12) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `estado` varchar(2) NOT NULL,
  `remember_token` varchar(200) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `tipousers_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `nombres`, `apellidos`, `dni`, `email`, `password`, `estado`, `remember_token`, `updated_at`, `created_at`, `tipousers_id`) VALUES
(5, 'admin', 'root', '000001', 'admin', '$2y$10$fZw1448Sxu/MIhNCfw5RBuldD.OWOSL7b8qUV8OUymfjo/gSnDA0u', '1', 'ygf9SgfSaiecQZVvv3MGx9IRGaQtYI56BzzfF7Zvf0jjq2TtR8AgYQueggvu', '2019-06-22 03:47:11', '2019-06-20 05:53:46', 1),
(7, 'mario', 'matinez', '798964123', 'caja1', '$2y$10$9iiMJK8xs0rlDO/ZcIFz4uVZrsUQ7X7Ecod82z3RP5MBsIyDl0.mi', '1', NULL, '2019-08-11 10:08:47', '2019-08-11 10:08:47', 2),
(8, 'SR FREDDY', 'FLORES MITA', '10044350446', 'FREDDY', '$2y$10$S1hnYMHewFcml2pCarmm7eIqqvatqSPiVkjHPZuHvf8ELsPt3/tSS', '1', NULL, '2019-08-17 09:49:43', '2019-08-17 09:49:43', 1),
(9, 'DEYSI', 'MAQUERA GOMEZ', '47981303', 'DEYSI', '$2y$10$ACLk8dpwyf5hToDUpfgMOeWZPRAdQweGqEXqSLsoehP6bqXtMcJpS', '1', NULL, '2019-08-21 09:58:57', '2019-08-21 09:58:57', 2),
(10, 'BRUNO', 'VILCA ALEJO', '43261451', 'BRUNO', '$2y$10$rY.Pj.StbTgewTzaejr.ieOO/kgTvUah0mEo2umr.dqNyrG0AnLEm', '1', NULL, '2019-08-21 10:03:36', '2019-08-21 10:03:36', 1);

--
-- Disparadores `users`
--
DELIMITER $$
CREATE TRIGGER `oner` AFTER INSERT ON `users` FOR EACH ROW INSERT INTO asistencias values(null,now(),now(),NEW.id)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id` int(11) NOT NULL,
  `dni` varchar(150) DEFAULT NULL,
  `documento` varchar(60) DEFAULT NULL,
  `cliente` varchar(250) DEFAULT NULL,
  `direccion` text DEFAULT NULL,
  `total` decimal(12,2) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `usuario` varchar(150) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `deudo` varchar(2) DEFAULT NULL,
  `serie` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id`, `dni`, `documento`, `cliente`, `direccion`, `total`, `fecha`, `usuario`, `estado`, `deudo`, `serie`) VALUES
(70, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 2.80, '2019-08-21 10:44:02', 'mario matinez', 'contado', 'SI', ''),
(71, '76779084', '10', 'RICHARD', 'MOQUEGUA', 27.00, '2019-08-22 11:33:02', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(72, '43261451', '10', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 27.00, '2019-08-27 12:37:04', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(73, '43261451', '10', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 27.00, '2019-08-27 12:37:04', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(74, '43261451', '10', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 27.00, '2019-08-27 12:37:04', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(75, '43261451', '10', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 27.00, '2019-08-27 12:37:04', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(76, '43261451', '10', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 27.00, '2019-08-27 12:37:04', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(77, '43261451', '10', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 27.00, '2019-08-27 12:37:04', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(78, '43261451', '10', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 27.00, '2019-08-27 12:37:06', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(79, '43261451', '10', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 27.00, '2019-08-27 12:37:06', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(80, '43261451', '10', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 27.00, '2019-08-27 12:37:06', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(81, '43261451', '10', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 27.00, '2019-08-27 12:37:06', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(82, '43261451', '10', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 27.00, '2019-08-27 12:37:06', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(83, '43261451', '10', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 27.00, '2019-08-27 12:37:06', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(84, '43261451', '10', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 27.00, '2019-08-27 12:37:08', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(85, '43261451', '10', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 27.00, '2019-08-27 12:37:08', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(86, '43261451', '03', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 0.40, '2019-08-28 12:34:49', 'mario matinez', 'contado', 'SI', 'BM0001'),
(87, '12345678', '03', 'SR CLIENTE', 'MOQUEGUA', 27.00, '2019-08-28 13:24:03', 'mario matinez', 'contado', 'SI', 'BM0002'),
(88, '4743823', '03', 'OMAR HUARACHA', 'MOQUEGUA', 27.00, '2019-08-29 11:13:43', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', 'BM0003'),
(89, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 65.00, '2019-08-29 15:22:12', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', 'BM0004'),
(90, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 48.00, '2019-08-29 17:12:53', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(91, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 48.00, '2019-08-29 17:12:53', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(92, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 1.60, '2019-08-29 19:22:54', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(93, '4411193', '20', 'SAN CARLOS E.IR.L.', 'MOQUEGUA', 135.00, '2019-09-02 18:39:17', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(94, '4411193', '20', 'SAN CARLOS E.IR.L.', 'MOQUEGUA', 135.00, '2019-09-02 18:39:18', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(95, '12345678', '03', 'SR CLIENTE', 'MOQUEGUA', 27.00, '2019-09-03 11:10:51', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', 'BM0005'),
(96, '12345678', '03', 'SR CLIENTE', 'MOQUEGUA', 1.60, '2019-09-03 12:42:54', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', 'BM0006'),
(97, '518140', '20', 'CATACORA AURELIA', 'MOQUEGUA', 11.20, '2019-09-03 14:41:29', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(98, '43261451', '20', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 10.00, '2019-09-04 12:18:07', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(99, '43261451', '20', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 10.00, '2019-09-04 12:18:07', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(100, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 1.60, '2019-09-04 14:39:29', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', 'BM0007'),
(101, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 12.00, '2019-09-04 14:58:04', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', 'BM0008'),
(102, '43261451', '03', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 6.00, '2019-09-04 15:24:29', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', 'BM0009'),
(103, '12345678', '03', 'SR CLIENTE', 'MOQUEGUA', 2.80, '2019-09-05 10:57:42', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', 'BM0010'),
(104, '44313520', '20', 'RONALD SANTOS', 'RAMIRO PRIALE D-5', 292.00, '2019-09-05 11:04:30', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(105, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 60.00, '2019-09-05 15:22:20', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', 'BM0011'),
(106, '43296323', '03', 'ROJAS BUSTAMANTE OSCAR', 'MOQUEGUA', 11.20, '2019-09-05 17:26:09', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', 'BM0012'),
(107, '42755216', '03', 'ELISEO QUISPE', 'MOQUEGUA', 74.80, '2019-09-05 19:14:40', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', 'BM0013'),
(108, '42755216', '03', 'ELISEO QUISPE', 'MOQUEGUA', 74.80, '2019-09-05 19:14:42', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', 'BM0013'),
(109, '43261451', '20', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 4.80, '2019-09-06 11:50:08', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', ''),
(110, '4745081', '03', 'PERCY CALIZAYA', 'MOQUEGUA', 2.40, '2019-09-06 13:00:29', 'DEYSI MAQUERA GOMEZ', 'contado', 'SI', 'BM0015'),
(111, '12345678', '10', 'JOSE GALLEGOS', 'MOQUEGUA', 11.00, '2019-09-11 12:10:34', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(112, '43261451', '10', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 28.00, '2019-09-11 15:42:57', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(113, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 5.00, '2019-09-11 16:13:15', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0016'),
(114, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 15.00, '2019-09-11 16:44:56', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0017'),
(115, '43261451', '20', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 8.40, '2019-09-12 09:52:32', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(116, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 84.50, '2019-09-12 16:08:56', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0018'),
(117, '43261451', '20', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 41.80, '2019-09-13 09:58:35', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(118, '43261451', '20', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 12.00, '2019-09-13 10:11:51', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(119, '46900863', '03', 'PALOMINO MIRANDA RUBEN', 'MOQUEGUA', 9.00, '2019-09-13 17:23:32', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0019'),
(120, '46900863', '03', 'PALOMINO MIRANDA RUBEN', 'MOQUEGUA', 9.00, '2019-09-13 17:23:33', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0019'),
(121, '46900863', '03', 'PALOMINO MIRANDA RUBEN', 'MOQUEGUA', 9.00, '2019-09-13 17:23:33', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0019'),
(122, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 1.60, '2019-09-13 17:50:13', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0022'),
(123, '41040879', '03', 'CARLOS MAMANI', 'MOQUEGUA', 32.00, '2019-09-16 09:42:51', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0023'),
(124, '41040879', '20', 'MAMANI CARLOS', 'MOQUEGUA', 32.00, '2019-09-16 09:43:28', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(125, '41040879', '20', 'MAMANI CARLOS', 'MOQUEGUA', 32.00, '2019-09-16 09:43:54', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(126, '41040879', '03', 'MAMANI CARLOS', 'MOQUEGUA', 32.00, '2019-09-16 09:49:02', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0024'),
(127, '43512376', '03', 'LUIS SANCHEZ', 'MOQUEGUA', 4.00, '2019-09-16 11:41:31', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0025'),
(128, '2146181', '03', 'SABINO QUISPE QUISPE', 'MOQUEGUA', 120.20, '2019-09-16 18:12:33', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0026'),
(129, '4410024', '03', 'FORTUNATO CLAVILLA QUISPE', 'MOQUEGUA', 29.80, '2019-09-17 15:59:35', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0027'),
(130, '4410024', '03', 'FORTUNATO CLAVILLA QUISPE', 'MOQUEGUA', 29.80, '2019-09-17 15:59:35', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0027'),
(131, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 71.50, '2019-09-17 16:20:07', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0029'),
(132, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 71.50, '2019-09-17 16:20:07', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0029'),
(133, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 44.00, '2019-09-17 18:21:49', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0031'),
(134, '4745081', '03', 'PERCY CALIZAYA', 'MOQUEGUA', 3.50, '2019-09-18 09:38:55', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0032'),
(135, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 13.00, '2019-09-18 14:39:57', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0033'),
(136, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 13.00, '2019-09-18 14:39:59', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0033'),
(137, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 3.20, '2019-09-18 16:51:15', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0035'),
(138, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 3.20, '2019-09-18 16:51:16', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0035'),
(139, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 3.20, '2019-09-18 16:51:18', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0035'),
(140, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 3.20, '2019-09-18 16:51:29', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0035'),
(141, '43261451', '03', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 3.20, '2019-09-18 16:51:33', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0035'),
(142, '48743806', '03', 'bojorquez', 'moquegua', 2.50, '2019-09-19 09:36:43', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0040'),
(143, '48743806', '03', 'bojorquez', 'moquegua', 2.50, '2019-09-19 09:36:44', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0040'),
(144, '43261451', '03', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 34.60, '2019-09-19 09:43:37', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0040'),
(145, '43261451', '03', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 34.60, '2019-09-19 09:43:37', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0040'),
(146, '43261451', '03', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 34.60, '2019-09-19 09:43:38', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0040'),
(147, '43261451', '03', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 21.00, '2019-09-19 09:45:16', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0045'),
(148, '42077964', '03', 'YON ESTACA', 'A.A.C. M -O LOTE 12', 15.00, '2019-09-19 13:12:47', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0046'),
(149, '15987465', '03', 'juan gabril', 'sin casa', 0.00, '2019-09-19 13:24:24', 'mario matinez', 'Contado', 'NO', 'BM0047'),
(150, '15987465', '03', 'juan gabril', 'sin casa', 0.00, '2019-09-19 13:24:38', 'mario matinez', 'Contado', 'NO', 'BM0047'),
(151, '15987465', '03', 'juan gabril', 'sin casa', 0.00, '2019-09-19 13:24:48', 'mario matinez', 'Contado', 'NO', 'BM0047'),
(152, '15987465', '03', 'juan gabril', 'sin casa', 0.00, '2019-09-19 13:25:23', 'mario matinez', 'Contado', 'NO', 'BM0047'),
(153, '2146181', '03', 'SABINO QUISPE QUISPE', 'MOQUEGUA', 1.50, '2019-09-19 18:50:18', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0051'),
(154, '12345678', '03', 'SR CLIENTE', 'MOQUEGUA', 26.00, '2019-09-20 10:24:58', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0052'),
(155, '12345678', '03', 'SR CLIENTE', 'MOQUEGUA', 26.00, '2019-09-20 10:24:59', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0052'),
(156, '12345678', '03', 'SR CLIENTE', 'MOQUEGUA', 26.00, '2019-09-20 10:24:59', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0052'),
(157, '12345678', '03', 'SR CLIENTE', 'MOQUEGUA', 3.50, '2019-09-20 10:26:44', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0055'),
(158, '12345678', '03', 'SR CLIENTE', 'MOQUEGUA', 3.50, '2019-09-20 10:26:44', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0055'),
(159, '43296323', '03', 'ROJAS BUSTAMANTE OSCAR', 'MOQUEGUA', 14.00, '2019-09-20 15:32:50', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0057'),
(160, '47105656', '10', 'EDWIN ROQUE ROJAS', 'MOQUEGUA', 2.80, '2019-09-21 10:39:05', 'mario matinez', 'Credito', 'SI', ''),
(161, '43261451', '03', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 12.00, '2019-09-23 17:57:33', 'BRUNO VILCA ALEJO', 'Contado', 'NO', 'BM0058'),
(162, '71230648', '10', 'ESTHER PEÑA', 'MOQUEGUA', 53.00, '2019-09-26 11:17:47', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(163, '12345678', '03', 'SR CLIENTE', 'MOQUEGUA', 9.00, '2019-09-26 16:07:36', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0059'),
(164, '47263007', '03', 'VICTOR CHAMBILLA', 'MOQUEGUA', 17.60, '2019-09-27 10:38:17', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0060'),
(165, '43261451', '10', 'BRUNO  VILCA ALEJO', 'MOQUEGUA', 38.00, '2019-09-27 14:49:35', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(166, '62054775', '20', 'YAN AGUIRRE', 'MOQUEGUA', 1.60, '2019-09-27 14:58:03', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(167, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 79.00, '2019-09-27 15:04:52', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(168, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 92.00, '2019-09-27 15:06:43', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(169, '12345678', '10', 'SR.CLIENTE', 'MOQUEGUA', 1.60, '2019-09-27 16:15:21', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(170, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 1.20, '2019-09-27 18:54:45', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(171, '43261451', '03', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 22.50, '2019-09-30 12:51:02', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0061'),
(172, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 3.60, '2019-09-30 12:54:21', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(173, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 16.50, '2019-09-30 12:58:08', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(174, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 8.00, '2019-09-30 17:45:12', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(175, '42777685', '10', 'DAVID RODRIGUEZ', 'MOQUEGUA', 93.60, '2019-09-30 18:01:05', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(176, '42777685', '10', 'DAVID RODRIGUEZ', 'MOQUEGUA', 666.00, '2019-09-30 18:07:16', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(177, '44291751', '10', 'FELIPE QUISPE', 'MOQUEGUA', 24.00, '2019-09-30 18:08:57', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(178, '4429175', '03', 'FELIPE INCAHUANACO', 'MLOQUEGUA', 452.00, '2019-09-30 18:21:15', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0062'),
(179, '4429175', '03', 'FELIPE INCAHUANACO', 'MLOQUEGUA', 452.00, '2019-09-30 18:21:17', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0062'),
(180, '4429175', '10', 'FELIPQ INCAHUANACO', 'MOQUEGUA', 27.00, '2019-09-30 18:27:01', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(181, '43261451', '03', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 280.00, '2019-09-30 18:30:36', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0064'),
(182, '43261451', '03', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 12.00, '2019-09-30 18:33:21', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0065'),
(183, '931573188', '03', 'LUISA HUACHANI', 'MOQUEGUA', 210.00, '2019-09-30 18:37:52', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0066'),
(184, '984002072', '20', 'RICARDO OQUENDO', 'MOQUEGUA', 840.00, '2019-09-30 18:40:25', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(185, '947953777', '20', 'CARLOS  MAMANI TOLEDO', 'MOQUEGUA', 420.00, '2019-09-30 18:42:31', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(186, '985466935', '20', 'ESTHER PEÑA', 'MOQUEGUA', 424.00, '2019-09-30 18:52:05', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(187, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 23.00, '2019-10-01 12:34:22', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(188, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 19.50, '2019-10-01 12:39:03', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(189, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 6.00, '2019-10-01 12:41:16', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(190, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 6.50, '2019-10-01 12:43:36', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(191, '997066736', '20', 'RUBEN PALOMINO', 'MOQUEGUA', 26.00, '2019-10-01 12:51:31', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(192, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 12.00, '2019-10-01 12:53:45', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(193, '12345678', '10', 'SR.CLIENTE', 'MOQUEGUA', 31.50, '2019-10-01 12:56:11', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(194, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 37.00, '2019-10-01 13:05:28', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(195, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 16.80, '2019-10-01 13:14:55', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(196, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 92.00, '2019-10-01 13:17:06', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(197, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 94.00, '2019-10-01 13:22:45', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(198, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 15.00, '2019-10-01 15:23:29', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(199, '44798641', '20', 'MARCO BENAVENTE', 'MOQUEGUA', 315.00, '2019-10-01 15:30:38', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(200, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 22.50, '2019-10-01 16:02:05', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(201, '41397297', '20', 'JOEL CHAMBILLA', 'MOQUEGUA', 1.00, '2019-10-01 16:22:14', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(202, '44798641', '20', 'MARCO BENAVENTE', 'MOQUEGUA', 5.50, '2019-10-01 16:37:23', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(203, '12345678', '20', 'JUAN CHURATA', 'MOQUEGUA', 210.00, '2019-10-01 16:41:22', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(204, '4429175', '20', 'FELIPE INCAHUANACO', 'MOQUEGUA', 630.00, '2019-10-01 16:51:00', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(205, '44313520', '20', 'RONALD SANTOS MARIA', 'MOQUEGUA', 210.30, '2019-10-02 11:34:38', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(206, '44313520', '20', 'RONALD SANTOS MARIA', 'MOQUEGUA', 210.30, '2019-10-02 11:34:39', 'DEYSI MAQUERA GOMEZ', 'Credito0', 'SI', ''),
(207, '4429175', '20', 'FELIPE INCAHUANACO', 'MOQUEGUA', 16.00, '2019-10-02 12:41:19', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(208, '43261451', '03', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 3.50, '2019-10-02 15:08:09', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0067'),
(209, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 6.00, '2019-10-02 15:20:06', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(210, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 18.00, '2019-10-02 15:36:25', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(211, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 78.50, '2019-10-02 16:35:41', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(212, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 0.80, '2019-10-02 17:54:18', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(213, '42611042', '20', 'HILMAR VILEN', 'MOQUEGUA', 525.00, '2019-10-02 19:07:41', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(214, '44798641', '20', 'MARCO BENAVENTE', 'MOQUEGUA', 315.00, '2019-10-02 19:09:04', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(215, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 3.00, '2019-10-03 09:54:51', 'BRUNO VILCA ALEJO', 'Contado', 'NO', ''),
(216, '48708726', '20', 'JOSE COAQUIRA', 'MOQUEGUA', 600.00, '2019-10-03 11:27:15', 'BRUNO VILCA ALEJO', 'Contado', 'NO', ''),
(217, '4429175', '20', 'FELIPE INCAHUANACO', 'MOQUEGUA', 420.00, '2019-10-03 11:32:05', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(218, '44313520', '20', 'RONALD SANTOS', 'MOQUEGUA', 830.00, '2019-10-03 11:37:15', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(219, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 17.00, '2019-10-03 16:24:04', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(220, '42611042', '20', 'HILMAR FIDEL VILCA', 'MOQUEGUA', 130.00, '2019-10-03 17:09:18', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(221, '40852205', '20', 'JUAN CARLOS DURAND M.', 'MOQUEGUA', 400.00, '2019-10-03 19:11:34', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(222, '43261451', '20', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 242.80, '2019-10-04 16:25:46', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(223, '43261451', '20', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 10.50, '2019-10-04 16:28:17', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(224, '46336036', '20', 'CESAR ROJAS', 'MOQUEGUA', 3.20, '2019-10-04 17:06:27', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(225, '4428503', '03', 'PABLO CALLE', 'MOQUEGUA', 97.30, '2019-10-08 11:27:01', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0068'),
(226, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 621.20, '2019-10-08 16:01:50', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(227, '43261451', '20', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 2.40, '2019-10-08 16:57:12', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(228, '12345678', '20', 'RICHARD CLIENTE', 'MOQUEGUA', 7.20, '2019-10-08 18:54:29', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(229, '43261451', '20', 'VILCA ALEJO BRUNO', 'MOQUEGUA', 11.40, '2019-10-09 10:45:08', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(230, '12345678', '20', 'IDELFONZO CLIENTE', 'MOQUEGUA', 105.00, '2019-10-09 10:45:55', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(231, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 1.20, '2019-10-09 11:19:09', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(232, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 2.00, '2019-10-09 12:08:56', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(233, '4412345', '03', 'FELIPE INCAHUANACO', 'SAMEGUA', 293.00, '2019-10-09 13:35:43', 'BRUNO VILCA ALEJO', 'Contado', 'NO', 'BM0069'),
(234, '76779084', '03', 'RICHARD', 'MOQUEGUA', 8.00, '2019-10-09 13:47:49', 'BRUNO VILCA ALEJO', 'Contado', 'NO', 'BM0070'),
(235, '46836066', '10', 'CHINO APAZA', 'SAN ANTONIO', 5.50, '2019-10-09 16:13:44', 'BRUNO VILCA ALEJO', 'Contado', 'NO', ''),
(236, '43261451', '03', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 7.20, '2019-10-09 16:18:50', 'BRUNO VILCA ALEJO', 'Credito', 'SI', 'BM0071'),
(237, '41830648', '10', 'ARTURMAQUERA PALO', 'MOQUEGUA', 19.00, '2019-10-09 17:57:44', 'BRUNO VILCA ALEJO', 'Contado', 'NO', ''),
(238, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEUA', 7.20, '2019-10-10 09:44:55', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(239, '71847779', '10', 'ZAPATA PINTO RONALDO', 'MOQUEGUA', 94.00, '2019-10-10 11:35:27', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(240, '4429175', '20', 'FELIPE INCAHUANACO', 'MOQUEGUA', 5.60, '2019-10-10 12:05:59', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(241, '4429175', '20', 'FELIPE INCAHUANACO', 'MOQUEGUA', 210.00, '2019-10-10 12:07:25', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(242, '12345678', '03', 'PETER CLIENTEM', 'MOQUEGUA', 105.00, '2019-10-10 12:08:39', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0072'),
(243, '43261451', '03', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 30.00, '2019-10-10 12:59:42', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0073'),
(244, '43261451', '03', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 28.00, '2019-10-10 13:04:37', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0074'),
(245, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 23.20, '2019-10-10 15:24:14', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(246, '4434871', '03', 'ISACC NINARAQUI', 'MOQUEGUA', 86.40, '2019-10-10 16:43:30', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0075'),
(247, '41040879', '03', 'CARLOS MAMANI', 'MOQUEGUA', 35.00, '2019-10-11 09:09:24', 'BRUNO VILCA ALEJO', 'Contado', 'NO', 'BM0076'),
(248, '41040879', '20', 'CARLOS MAMANI', 'MOQUEGUA', 215.00, '2019-10-11 13:11:22', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(249, '4429175', '20', 'FELIPE INCAHUANACO', 'MLOQUEGUA', 420.00, '2019-10-11 13:13:43', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(250, '43261451', '03', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 221.20, '2019-10-11 13:17:20', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0077'),
(251, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 154.40, '2019-10-11 13:19:45', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(252, '25682233', '20', 'JESUS ARNICA MORALES', 'MORALES', 36.00, '2019-10-11 14:29:51', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(253, '4434871', '03', 'ISACC NINARAQUI', 'MOQUEGUA', 10.00, '2019-10-11 16:04:37', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0078'),
(254, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 6.00, '2019-10-11 16:39:59', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(255, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 3.60, '2019-10-11 17:57:04', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(256, '41474714', '10', 'german asqui', 'moquegua', 90.00, '2019-10-12 09:22:48', 'BRUNO VILCA ALEJO', 'Contado', 'NO', ''),
(257, '4429175', '03', 'FELIPE INCAHUANACO', 'MLOQUEGUA', 131.70, '2019-10-12 09:46:56', 'BRUNO VILCA ALEJO', 'Contado', 'NO', 'BM0079'),
(258, '4744307', '20', 'WALTER LECAROS', 'MOQUEGUA', 5.60, '2019-10-14 18:15:52', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(259, '4745522', '03', 'PEDRO MARCA RAMOS', 'MOQUEGUA', 4.80, '2019-10-14 18:44:42', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', 'BM0080'),
(260, '12345678', '20', 'sr.cliente', 'moquegua', 16.00, '2019-10-15 12:11:55', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(261, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 14.00, '2019-10-15 12:20:01', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(262, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 36.60, '2019-10-15 12:34:53', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(263, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 0.60, '2019-10-15 15:44:55', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(264, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 0.80, '2019-10-15 17:06:12', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(265, '98765432', '20', 'CARLOS MAMANI', 'MOQUEGUA', 315.00, '2019-10-15 17:24:06', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(266, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 27.00, '2019-10-15 19:15:41', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(267, '43198337', '20', 'CESAR CHAMBILLA GUTIERREZ', 'MOQUEGUA', 12.00, '2019-10-16 12:30:20', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(268, '98765432', '20', 'SR CLIENTE', 'AREQUIPA', 2.80, '2019-10-16 12:40:29', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(269, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 4.00, '2019-10-16 16:44:58', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(270, '13456789', '20', 'CUBA BULEJE', 'MOQUEGUA', 1.20, '2019-10-16 17:22:06', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(271, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 6.00, '2019-10-16 17:24:18', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(272, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 8.40, '2019-10-16 19:20:35', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(273, '80113541', '03', 'richard chambi pilco', 'moquegua', 3.50, '2019-10-18 08:45:56', 'BRUNO VILCA ALEJO', 'Contado', 'NO', 'BM0081'),
(274, '43261451', '10', 'bruno vilca alejo', 'moquegua', 238.00, '2019-10-18 08:52:51', 'BRUNO VILCA ALEJO', 'Contado', 'NO', ''),
(275, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 85.00, '2019-10-18 11:14:29', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(276, '43261451', '03', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 9.00, '2019-10-18 12:56:36', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', 'BM0082'),
(277, '46215545', '10', 'JUAN LIZARDO CONDORI', 'MOQUEGUA', 43.90, '2019-10-18 16:13:21', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(278, '45134645', '20', 'BRANYAN SERRATO', 'MOQUEGUA', 48.00, '2019-10-18 16:44:48', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(279, '972138821', '10', 'juan lizardo condori', 'moquegua', 105.00, '2019-10-18 18:27:32', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(280, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 11.00, '2019-10-19 15:30:43', 'BRUNO VILCA ALEJO', 'Contado', 'NO', ''),
(281, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 287.00, '2019-10-19 16:07:08', 'BRUNO VILCA ALEJO', 'Contado', 'NO', ''),
(282, '43261451', '10', 'BRUNO VILCA', 'MOQUEGUA', 168.00, '2019-10-19 16:56:52', 'BRUNO VILCA ALEJO', 'Contado', 'NO', ''),
(283, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 578.00, '2019-10-21 11:12:14', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(284, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 438.00, '2019-10-21 11:14:40', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(285, '44798641', '20', 'MARCO BENAVENTE', 'MOQUEGUA', 525.00, '2019-10-21 11:18:31', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(286, '2437742', '20', 'HABRAHAM CALDERON', 'MOQUEGUA', 34.00, '2019-10-21 12:01:39', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(287, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 26.20, '2019-10-21 15:30:18', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(288, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 0.80, '2019-10-21 15:34:29', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(289, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 6.00, '2019-10-21 15:35:19', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(290, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 15.00, '2019-10-21 16:03:13', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(291, '12345678', '20', 'SR CLIENTE', 'MOQUEGUA', 168.00, '2019-10-21 16:51:13', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(292, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 14.00, '2019-10-21 16:52:23', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(293, '9876543', '20', 'SABINO QUISPE', 'MOQUEGUA', 164.70, '2019-10-21 18:48:25', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(294, '987654329', '20', 'EVER ESPINOZA', 'MOQUEGUA', 200.00, '2019-10-21 18:51:29', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(295, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 788.00, '2019-10-22 12:20:07', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(296, '54678932', '20', 'SR CLIENTE', 'MOQUEGUA', 315.00, '2019-10-22 12:27:57', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(297, '48708726', '20', 'JOSE COAQUIRA', 'MOQUEGUA', 51.60, '2019-10-22 12:34:47', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(298, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 69.60, '2019-10-22 12:41:32', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(299, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 260.00, '2019-10-22 16:10:12', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(300, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 37.00, '2019-10-22 17:36:50', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(301, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 20.00, '2019-10-23 10:43:14', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(302, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 55.00, '2019-10-23 10:44:22', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(303, '43261451', '20', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 12.00, '2019-10-23 12:06:07', 'DEYSI MAQUERA GOMEZ', 'Credito', 'SI', ''),
(304, '44798641', '20', 'MARCO BENAVENTE', 'MOQUEGUA', 70.00, '2019-10-23 12:20:38', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(305, '71458588', '20', 'JOHNATAN ARAPA LLAMPI', 'MOQUEGUA', 200.00, '2019-10-23 18:21:43', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(306, '41040879', '20', 'CARLOS MAMANI', 'MOQUEGUA', 210.00, '2019-10-24 11:14:39', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', ''),
(307, '43261451', '10', 'BRUNO VILCA ALEJO', 'MOQUEGUA', 3500.00, '2019-10-24 16:07:10', 'DEYSI MAQUERA GOMEZ', 'Contado', 'NO', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `asistencias`
--
ALTER TABLE `asistencias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_asistencia_users1_idx` (`users_id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `creditos`
--
ALTER TABLE `creditos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cuentas_proveedors1_idx` (`proveedors_id`);

--
-- Indices de la tabla `detalleventas`
--
ALTER TABLE `detalleventas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_detalleventas_productos1_idx` (`producto_id`),
  ADD KEY `fk_detalleventas_venta1_idx` (`venta_id`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pagos_creditos_idx` (`credito_id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_productos_tipoproducto1_idx` (`tipoproducto_id`),
  ADD KEY `fk_productos_proveedors1_idx` (`proveedors_id`);

--
-- Indices de la tabla `proveedors`
--
ALTER TABLE `proveedors`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `registros`
--
ALTER TABLE `registros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_registro_productos_idx` (`producto_id`);

--
-- Indices de la tabla `telefonos`
--
ALTER TABLE `telefonos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_telefonos_proveedors1_idx` (`proveedors_id`);

--
-- Indices de la tabla `tipoproductos`
--
ALTER TABLE `tipoproductos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipousers`
--
ALTER TABLE `tipousers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_tipousers_idx` (`tipousers_id`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `asistencias`
--
ALTER TABLE `asistencias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=230;

--
-- AUTO_INCREMENT de la tabla `creditos`
--
ALTER TABLE `creditos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `detalleventas`
--
ALTER TABLE `detalleventas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=631;

--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=248;

--
-- AUTO_INCREMENT de la tabla `proveedors`
--
ALTER TABLE `proveedors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `registros`
--
ALTER TABLE `registros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT de la tabla `telefonos`
--
ALTER TABLE `telefonos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tipoproductos`
--
ALTER TABLE `tipoproductos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `tipousers`
--
ALTER TABLE `tipousers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=308;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `asistencias`
--
ALTER TABLE `asistencias`
  ADD CONSTRAINT `fk_asistencia_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD CONSTRAINT `fk_cuentas_proveedors1` FOREIGN KEY (`proveedors_id`) REFERENCES `proveedors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalleventas`
--
ALTER TABLE `detalleventas`
  ADD CONSTRAINT `fk_detalleventas_productos1` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_detalleventas_venta1` FOREIGN KEY (`venta_id`) REFERENCES `ventas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD CONSTRAINT `fk_pagos_creditos` FOREIGN KEY (`credito_id`) REFERENCES `creditos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `fk_productos_proveedors1` FOREIGN KEY (`proveedors_id`) REFERENCES `proveedors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_productos_tipoproducto1` FOREIGN KEY (`tipoproducto_id`) REFERENCES `tipoproductos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `registros`
--
ALTER TABLE `registros`
  ADD CONSTRAINT `fk_registro_productos` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `telefonos`
--
ALTER TABLE `telefonos`
  ADD CONSTRAINT `fk_telefonos_proveedors1` FOREIGN KEY (`proveedors_id`) REFERENCES `proveedors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_tipousers` FOREIGN KEY (`tipousers_id`) REFERENCES `tipousers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
