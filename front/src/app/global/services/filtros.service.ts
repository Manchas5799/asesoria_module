import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FiltrosService {
  
  constructor() {

  }
  filtroData(data:any[],text:any,indice:number):any[]{
    if (text.toString()==''){
      return data
    }
    const resultados = []
    let index:number =indice, ele: any 
      data.forEach(element => {
        let elemento = Object.entries(element)
        ele = elemento[index][1]+''    //Forzamos a que sea STRING
        //console.log(ele)
        if(ele.toString().toLowerCase().indexOf(text.toLowerCase())>-1){
          resultados.push(element)
        }
      });
    return resultados
  }
  /*filtroDataId(data,text):any[]{
    let datosGr = Object.entries(data)
    console.log(datosGr)
    if (text=='') return data
    let datos = data.filter( item => item.id.toString().includes(text))
    return datos;
  }
  filtroDataNombre(data,text):any[]{
    if (text=='') return data
    let datos = data.filter( item => item.nombre.toLowerCase().includes(text.toLowerCase()))
    return datos;
  }
  filtroDataSigla(data,text):any[]{
    if (text=='') return data
    let datos = data.filter( item => item.sigla.toLowerCase().includes(text.toLowerCase()))
    return datos;
  }
  */
  
}