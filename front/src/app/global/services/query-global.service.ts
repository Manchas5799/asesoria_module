import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LocalStoreService} from '../../shared/services/local-store.service';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class QueryGlobalService {
    private baseUrl;
    headers: any;
    token: any;
    ls = window.localStorage;
    httpOptions: any;

    constructor(
        private http: HttpClient,
        private store: LocalStoreService,
        private router: Router
    ) {
        this.baseUrl = environment.rutas.backEnd;
    }

    insertHead() {
        this.ls.getItem('unamToken');
        const data = this.store.getItem('unamToken');
        this.token = data['access_token'];
        if (data['access_token']) {
            this.httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type':  'application/json',
                    'Authorization': 'Bearer ' + this.token
                })
            };
        }
    }

    enviarServidor(url, data) {
        this.insertHead();
        return this.http.post(`${this.baseUrl}/tram/guardar`, data, this.httpOptions);
    }


    /**
     *
     * @param url - /grl/xxxx
     * @param data - object
     * @param anonimo - def false
     * @param headers - def  true
     */
    datosServidor(url, data, anonimo = false, headers = true) {
        if (anonimo) {
            return this.http.post(`${this.baseUrl}/grl/dataexistenteAnonimo`, data);
        }
        if (headers) {
            this.insertHead();
            return this.http.post(`${this.baseUrl}/grl/dataexistente`, data, this.httpOptions);
        } else {
            return this.http.post(`${this.baseUrl}/grl/dataexistente`, data);
        }
    }
}
