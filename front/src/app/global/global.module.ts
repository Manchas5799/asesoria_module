import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeV4Component } from './components/font-awesome-v4/font-awesome-v4.component';
/*import { UploadFilesComponent } from './components/upload-files/upload-files.component';
import { TableComponent } from './components/table/table.component';
import { TableColumnComponent } from './components/table/table-column/table-column.component';
import { TableHeaderComponent } from './components/table/table-header/table-header.component';
import { TableCellsComponent } from './components/table/table-cells/table-cells.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { FileUploadDirective } from './directives/file-upload.directive';
*/
import { LoadingComponent } from './components/loading/loading.component';

/*import { ConsultaSIAFComponent } from './components/consulta-siaf/consulta-siaf.component'
import { PersonaComponent } from './components/persona/persona.component'
import { PersonaGeneralComponent } from './components/persona-general/persona-general.component';
import { EspecificasGiComponent } from './components/especificas-gi/especificas-gi.component';
*/

import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ReactiveFormsModule } from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ToastrModule } from 'ngx-toastr';
import { ResaltarPipe } from './pipes/resaltar.pipe';
//import {ResaltarPipe} from './../global/pipes/resaltar.pipes'



@NgModule({
  declarations: [
    FontAwesomeV4Component, 
    ResaltarPipe,
/*    UploadFilesComponent, 
    TableComponent, 
    TableColumnComponent, 
    TableHeaderComponent, 
    FileUploadComponent,
    */
    LoadingComponent,
   // ResaltarPipe
/*    FileUploadDirective,
    TableCellsComponent,
    ConsultaSIAFComponent,
    PersonaComponent,
    PersonaGeneralComponent,
    EspecificasGiComponent,
    */
    ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgxDatatableModule,
    ToastrModule,
    NgbModule,
    //ResaltarPipe
    
  ],
  exports:[
  /*  TableComponent,
    TableColumnComponent,
    TableHeaderComponent,
    TableCellsComponent,
    FileUploadComponent,*/
    LoadingComponent,
    FontAwesomeV4Component,
    ResaltarPipe
 /*   UploadFilesComponent,
    ConsultaSIAFComponent,
    PersonaComponent,
    PersonaGeneralComponent,
    EspecificasGiComponent
    */
   //ResaltarPipe
  ],
  providers:[
   // ResaltarPipe
  ]
})
export class GlobalModule { }
