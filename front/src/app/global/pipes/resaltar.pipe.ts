import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Pipe({
  name: 'resaltar'
})
export class ResaltarPipe implements PipeTransform {

  constructor(private _sanitizer: DomSanitizer) { }

  transform(datos: any, searchText: string): SafeHtml{

    if (!datos) { return []; }
    if (!searchText) { return datos; }
    
    const value = datos.replace(
      searchText.toUpperCase(), `<span style='background-color:yellow'>${searchText.toUpperCase()}</span>` );
    
    return this._sanitizer.bypassSecurityTrustHtml(value);  
  }
}