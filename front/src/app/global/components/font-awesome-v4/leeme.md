# FontAwesome 4.7
Insertar iconos de Font Awesome

## Uso

`<app-ht-font-awesome name="eye"></app-ht-font-awesome>`

    <app-ht-font-awesome name="eye"></app-ht-font-awesome>

## Parametros Posibles

- `name`    Nombre del icono **importante**
- Otros de Font Awesome.
