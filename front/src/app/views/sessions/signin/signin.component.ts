import { Component, OnInit } from '@angular/core';
import { SharedAnimations } from 'src/app/shared/animations/shared-animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../../shared/services/auth.service';
import { ToastrService } from 'ngx-toastr'
import { Router, RouteConfigLoadStart, ResolveStart, RouteConfigLoadEnd, ResolveEnd } from '@angular/router';
import { environment } from 'src/environments/environment'
@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.scss'],
    animations: [SharedAnimations]
})
export class SigninComponent implements OnInit {
    loading: boolean;
    loadingText: string;
    nombreInstitucion:string = environment.entidad.nombre
    nombreModulo:string = environment.aplicacion.nombre
    signinForm: FormGroup;
    usuario: string;
    clave: string
    show:any = true
    constructor(
        private fb: FormBuilder,
        private auth: AuthService,
        private router: Router,
        private toast:ToastrService
    ) { }

    ngOnInit() {
        this.router.events.subscribe(event => {
            if (event instanceof RouteConfigLoadStart || event instanceof ResolveStart) {
                this.loadingText = 'Cargando Modulos...';

                this.loading = true;
            }
            if (event instanceof RouteConfigLoadEnd || event instanceof ResolveEnd) {
                this.loading = false;
            }
        });

        this.signinForm = this.fb.group({
            usuario: [this.usuario, Validators.required],
            password: [this.clave, Validators.required],
            modulo: [ environment.aplicacion.modulo ]

        });
    }

    signin() {
        this.loading = true;
        this.loadingText = 'Verificando...';
        this.auth.signin(this.signinForm.value)
        this.loading = true;
        setTimeout(function() {
            // this.toast.error('Tiempo de espera agotado, verifique su conección a internet','Ups..')
            this.loading = false;
        }.bind(this), 10000);
    }

}
