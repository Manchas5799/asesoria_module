import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import * as io from 'socket.io-client';
import { environment } from './../../environments/environment'
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root',
})
export class RealTimeService {
  socket: SocketIOClient.Socket;
  dcode = ''
  data:string = ''
  constructor() {
    // this.dcode = code
    this.socket = io.connect(environment.rutas.asincronico);
    // this.call()
  }
  asynEvent(code){
    this.socket.emit('asyn',code);
  }
  // call(code){
  //   this.socket.on('code:' + code, (data: string) => {
  //     console.log('filtro 122 :' + data)
  //     this.data = data
  //   });
  // }
  public getMessages = (code) => {
    console.log('code:' + code)
    return Observable.create((observer) => {
        this.socket.on('code:' + code, (message) => {
            console.log('averrrrr acaaa')
            observer.next(message);
        });
    });
  }
}