import { NotificacionesExpedientesModule } from './../subModulos/notificaciones-expedientes/notificaciones-expedientes.module';
import { MisNotificacionesComponent } from './../subModulos/notificaciones-expedientes/mis-notificaciones/mis-notificaciones.component';
import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule  } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { FilterPipeModule } from 'ngx-filter-pipe';
import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';


import { NgxEchartsModule } from 'ngx-echarts';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { GlobalModule } from './../global/global.module'
import { MatTooltipModule } from '@angular/material/tooltip';

registerLocaleData(localeEs, 'es')

const routes: Routes = [
    
    {
    path: 'perfil',
    component: ProfileComponent,
    },
    {
      path: 'resetPassword',
      component: ResetPasswordComponent,
    }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    
  ],
  exports: [RouterModule],
  providers: [ { provide: LOCALE_ID, useValue: 'es' } ],
  
})
export class PersonalRutasModule { }