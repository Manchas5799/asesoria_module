import { Component, OnInit } from '@angular/core';
import { QueryService } from './../../servicios/query.services'
import { LocalService }   from './../../servicios/local.services'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  correo:string  = ""
  celular:string  = ""
  newPass:string 
  repPass:string 

  registerForm: FormGroup;
  submitted = false;


  constructor(
    private query:QueryService,
    private local:LocalService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.getPersonData()
    this.registerForm = this.formBuilder.group({
      celular: [this.celular, Validators.required,Validators.min(9)],
      correo: [this.correo, Validators.required],
      newPass: ['', [Validators.required, Validators.min(6)]],
      repPass: ['', [Validators.required, Validators.min(6)]]
    }); 
  }
  get f() { return this.registerForm.controls; }
  getPersonData(){
    
  }
  savePersonData(){
    let code = this.local.getItem('codigo')
    let dataEstudent = this.local.getItem('userInfo')
    let fil
    let carr
    
      dataEstudent.estudiante.forEach(e => {
        if(e.cEstudCodUniv == code){
          fil = e.iFilId
          carr = e.iCarreraId
        }
      });
      let data = {
        correo: this.correo,
        telefono: this.celular,
        clave: this.newPass,
        clave2: this.repPass,
        codigoUniv: code,
        filId: fil,
        carreraId:carr
      }
  }
}
