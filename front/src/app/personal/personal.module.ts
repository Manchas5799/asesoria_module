import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { GlobalModule } from './../global/global.module';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ProfileComponent } from './profile/profile.component';
import { NotificacionesExpedientesModule } from './../subModulos/notificaciones-expedientes/notificaciones-expedientes.module';
import { MisNotificacionesComponent } from './../subModulos/notificaciones-expedientes/mis-notificaciones/mis-notificaciones.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonalRutasModule } from './personal-rutas.module'

import { NgxEchartsModule } from 'ngx-echarts';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { FilterPipeModule } from 'ngx-filter-pipe';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarModule } from 'angular-calendar';
import { DateAdapter } from '@angular/material/core';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [ProfileComponent, ResetPasswordComponent],
  imports: [
    CommonModule,
    PersonalRutasModule,
    NgxEchartsModule,
    SharedComponentsModule,
    NgxDatatableModule,
    NotificacionesExpedientesModule,
    MatTooltipModule,
    ReactiveFormsModule,
    NgxEchartsModule,
    SharedComponentsModule,
    NgxDatatableModule,
    FilterPipeModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    GlobalModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    MatTooltipModule,
    MatButtonModule,
    MatIconModule
  ]
})
export class PersonalModule { }
