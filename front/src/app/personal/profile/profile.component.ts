import { CalendarAppEvent } from 'src/app/shared/models/calendar-event.model';
import { addHours, endOfMonth, startOfMonth } from "date-fns";
import { QueryService } from "./../../servicios/query.services";
import { Component, OnInit } from "@angular/core";
import { LocalService } from "./../../servicios/local.services";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { EChartOption } from "echarts";
import { echartStyles } from "../../shared/echart-styles";
import { environment } from "src/environments/environment";
import { forkJoin } from "rxjs";
import * as moment from "moment";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"],
})
export class ProfileComponent implements OnInit {
  nombreModulo: string;
  info: any;
  codigo: string = "";
  names: string = "";
  loading: boolean = false;
  rutas = {
    select_pendientes: "/pendientes_listado",
    select_citaciones_pendientes: "/citaciones_pendientes",
  };
  pendientes: any = [];
  pendientesConvertidos: any = [];
  citacionesConvertidas: any = [];
  citaciones: any = [];
  mesactual = moment().locale("es").format("MMMM");
  constructor(
    private local: LocalService,
    private modalService: NgbModal,
    private query: QueryService
  ) {
    this.nombreModulo = environment.aplicacion.nombre;
  }
  mostrarMisCitaciones(modal) {
    this.modalService.open(modal, {
      backdrop: "static",
      keyboard: false,
      size: "lg",
    });
  }
  ngOnInit() {
    let data = this.local.getItem("userInfo");
    this.names = `${data["grl_persona"].cPersNombre} ${data["grl_persona"].cPersPaterno} ${data["grl_persona"].cPersMaterno}`;

    this.info = data["grl_persona"];
    this.cargarPendiente();

    //console.log(this.info)
  }
  cargarPendiente() {
    this.loading = true;
    let fechaInicial = startOfMonth(new Date());
    let fechaFinal = endOfMonth(new Date());
    forkJoin({
      pendientes: this.query.getDatosApi(this.rutas.select_pendientes),
      citacionesPendientes: this.query.getDatosPostApi(
        this.rutas.select_citaciones_pendientes,
        {
          fechaInicial,
          fechaFinal,
        }
      ),
    }).subscribe({
      next: (res: any) => {
        this.pendientes = res.pendientes;
        this.citaciones = res.citacionesPendientes;
        this.pendientes.forEach((item) => {
          item.vencido = 1;
          let it = moment(item.fecha_limite);
          let diferencia = it.diff(moment(), "days");
          if (diferencia < 5) {
            item.vencido = 2;
            if (diferencia < 0) {
              item.vencido = 3;
            }
          }
        });
        this.pendientesConvertidos = this.initPendientes(this.pendientes);
        this.citacionesConvertidas = this.initEvents(this.citaciones);
      },
      complete: () => {
        this.loading = false;
      },
    });
  }
  private initEvents(events): CalendarAppEvent[] {
    return events.map((event) => {
      event.start = event.fecha_citacion;
      event.end = addHours(event.start, 1);
      event._id = event.citacion_id;
      event.title = event.citacion_numero;
      event.persona_asesoria_id = event.iPersonaAsesoriaId;
      event.expediente_etapa = event.etapa_id;
      event.citacion_sumilla = event.sumilla;
      event.citacion_lugar = event.lugar_citacion;
      event.citacion_resolucion = event.resolucion;
      return new CalendarAppEvent(event);
    });
  }
  private initPendientes(pendientes): CalendarAppEvent[] {
    return pendientes.map((event) => {
      event.start = event.fecha_limite;
      event.recepcion = event.fecha_realizado;
      event.end = addHours(event.start, 1);
      event._id = event.pendiente_id;
      event.title = event.descripcion;
      event.involucrado = event.encargado;
      event.persona_asesoria_id = event.encargado[0].p_ase_id;
      event.citacion_sumilla = event.descripcion;

      return new CalendarAppEvent(event);
    });
  }
}
