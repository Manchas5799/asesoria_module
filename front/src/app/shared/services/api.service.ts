import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LocalStoreService } from "./local-store.service";
import { environment } from './../../../environments/environment'

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private baseUrl:string
  private baseRecursos:string
  headers:any
  token:any
  ls = window.localStorage;
  httpOptions:any
  constructor(private http: HttpClient,private store: LocalStoreService, ) {
    this.baseUrl = environment.rutas.auth
    this.baseRecursos = environment.rutas.recursos
  }
  insertHead(){
    this.ls.getItem('unamToken');
    let data = this.store.getItem('unamToken');
    this.token = data['access_token'];
    if (data['access_token']) {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.token,
        }),
      };
    }
  }
  getUser(data) {
    return this.http.post(`${this.baseUrl}/auth/login`, data);
  }
  getFoto(gg){
    this.insertHead()
    let data = {
      code:gg
    }
    return this.http.post(`${this.baseUrl}/fotografia`, data,this.httpOptions)
  }
  getProfile(){
    this.insertHead()
    return this.http.get(`${this.baseUrl}/obtenerInfoCredencial`,this.httpOptions)
  }
  verificarLoggued(mod){
    this.insertHead()
    return this.http.get(`${this.baseUrl}/verificarLogueo/${mod}`,this.httpOptions)
  }

}
