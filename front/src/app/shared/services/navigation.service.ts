import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

export interface IMenuItem {
  id?: string;
  title?: string;
  description?: string;
  type: string; // Possible values: link/dropDown/extLink
  name?: string; // Used as display text for item and title for separator type
  state?: string; // Router state
  icon?: string; // Material icon name
  tooltip?: string; // Tooltip text
  disabled?: boolean; // If true, item will not be appeared in sidenav.
  sub?: IChildItem[]; // Dropdown items
  badges?: IBadge[];
  active?: boolean;
}
export interface IChildItem {
  id?: string;
  parentId?: string;
  type?: string;
  name: string; // Display text
  state?: string; // Router state
  icon?: string;
  sub?: IChildItem[];
  active?: boolean;
}

interface IBadge {
  color: string; // primary/accent/warn/hex color codes(#fff000)
  value: string; // Display text
}

interface ISidebarState {
  sidenavOpen?: boolean;
  childnavOpen?: boolean;
}

@Injectable({
  providedIn: "root",
})
export class NavigationService {
  public sidebarState: ISidebarState = {
    sidenavOpen: true,
    childnavOpen: false,
  };
  constructor() {}

  defaultMenu: IMenuItem[] = [
    {
      name: "Expedientes",
      description: "",
      type: "link",
      state: "/expedientes",
      icon: "i-Inbox-Into",
    },
    {
      name: "Citaciones | Tareas",
      description: "",
      type: "link",
      state: "/notificaciones/N",
      icon: "i-Calendar-4",
      // sub: [
      //     {   icon: 'i-Inbox-Into',
      //         name: 'Inventario Inicial',
      //         state: 'almacenes',
      //         type: 'dropDown',
      //         sub: [
      //             {
      //                 icon: 'i-Computer-Secure', name: 'Almacén Central', state: '/control/almacenes', type: 'link'
      //             },
      //             {
      //                 icon: 'i-Computer-Secure', name: 'Almacén Secundario', state: '/control/almacenes', type: 'link'
      //             }
      //         ]
      //     },
      //     {   icon: 'i-Newspaper', name: 'Ingresos por Aceptar', state: '/almacen/docentes',
      //         type: 'dropDown',
      //         sub: [
      //             {
      //                 icon: 'i-Computer-Secure', name: 'Entrada', state: '/control/almacenes', type: 'link'
      //             },
      //             {
      //                 icon: 'i-Repeat2', name: 'Entrada por Préstamo', state: '/control/almacenes', type: 'link'
      //             }
      //         ]

      //     },
      //     { icon: 'i-Ticket', name: 'Entrada desde SIGA', state: '/almacen/docentes', type: 'link' },

      // ]
    },
    {
      name: "Consultas SIGEUN",
      description: "Administración de Salidas de Almacén",
      type: "link",
      state: "/consultasexternas",
      icon: "i-Gears",
      // sub: [
      //     { icon: 'i-Dropbox', name: 'Nota de Pedido', state: '/control/almacenes', type: 'dropDown'
      //     ,
      //     sub: [
      //         { icon: 'i-Paper-Plane', name: 'Salida', state: '/control/almacenes', type: 'link'
      //         },
      //         { icon: 'i-Repeat2', name: 'Salida por Préstamo', state: '/control/almacenes', type: 'link'
      //         }
      //     ] },
      //     { icon: 'i-File-Horizontal-Text', name: 'Intenamiento Saldos (Parciales)', state: '/almacen/docentes', type: 'link' },
      // ]
    },
    {
      name: "Enlaces Externos",
      description: "Administración de Combustible",
      type: "link",
      state: "/sistemasexternos",
      icon: "i-Link",
      // sub: [
      //     { icon: 'i-Door', name: 'Solicitud de Combustible', state: '/control/almacenes', type: 'link' },
      //     { icon: 'i-Up', name: 'Aceptar Solicitud (Alm. Central)', state: '/almacen/docentes', type: 'link' },

      // ]
    },
    {
      name: "Reportes",
      description: "Reportes del Módulo Almacenes",
      state: "/reportes",
      type: "link",
      icon: "i-Bar-Chart",
    },
    // {
    //     name: 'Herramientas',
    //     description: 'Herramientas del Módulo Almacenes',
    //     type: 'dropDown',
    //     icon: 'i-Gears',
    //     sub: [
    //         { icon: 'i-Safe-Box1', name: 'Pre-Cierre Mensual', state: '/control/almacenes', type: 'link' },
    //         { icon: 'i-Safe-Box1', name: 'Cierre Anual', state: '/control/almacenes', type: 'link' },
    //         { icon: 'i-Safe-Box1', name: 'Cierre de Almacén', state: '/control/almacenes', type: 'link' },

    //     ]
    // },
    {
      name: "Tablas Maestras",
      description: "Administración de Tablas base",
      type: "dropDown",
      icon: "i-Split-Vertical",
      sub: [
        {
          icon: "i-Clothing-Store",
          name: "Órganos Jurisdiccionales",
          state: "/tablas-maestras/organos",
          type: "link",
        },
        // {
        //   icon: "i-Business-ManWoman",
        //   name: "Tipo de Personal",
        //   state: "/tablas-maestras/tipo_personas",
        //   type: "link",
        // },
        // {
        //   icon: "i-Business-ManWoman",
        //   name: "Personal de Asesoría",
        //   state: "/tablas-maestras/personas_asesoria",
        //   type: "link",
        // },
        {
          icon: "i-File-Horizontal-Text",
          name: "Tipos de Expedientes",
          state: "/tablas-maestras/tipo_expedientes",
          type: "link",
        },
        {
          icon: "i-File-Horizontal-Text",
          name: "Materias",
          state: "/tablas-maestras/materias",
          type: "link",
        },
        {
          icon: "i-Windows-Microsoft",
          name: "Etapa Procesal",
          state: "/tablas-maestras/etapas",
          type: "link",
        },
        {
          icon: "i-File-Horizontal-Text",
          name: "Partes Procesales",
          state: "/tablas-maestras/calidades_participación",
          type: "link",
        },
        {
          icon: "i-Link",
          name: "Enlaces Externos",
          state: "/tablas-maestras/enlaces",
          type: "link",
        },
        {
          icon: "i-Danger",
          name: "Prioridad del Expediente",
          state: "/tablas-maestras/prioridades",
          type: "link",
        },
        // { icon: 'i-Ambulance',             name: 'Tipos de Vehículos', state: '/tablas-maestras/tipos_veh',    type: 'link' },
        {
          icon: "i-File-Horizontal-Text",
          name: "Tipo de Citación",
          state: "/tablas-maestras/tipo_citacion",
          type: "link",
        },
        {
          icon: "i-File-Horizontal-Text",
          name: "Acto Procesal",
          state: "/tablas-maestras/acto_procesal",
          type: "link",
        },
        {
          icon: "i-File-Horizontal-Text",
          name: "Estado de Expediente",
          state: "/tablas-maestras/estado_expediente",
          type: "link",
        },
        {
          icon: "i-File-Horizontal-Text",
          name: "Distritos Judiciales",
          state: "/tablas-maestras/distritos_judiciales",
          type: "link",
        },
      ],
    },
  ];

  // sets iconMenu as default;
  menuItems = new BehaviorSubject<IMenuItem[]>(this.defaultMenu);
  // navigation component has subscribed to this Observable
  menuItems$ = this.menuItems.asObservable();
}
