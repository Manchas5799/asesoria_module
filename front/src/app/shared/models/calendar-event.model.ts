import { CalendarEventAction, CalendarEvent } from "angular-calendar";
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
} from "date-fns";
import * as moment from "moment";

export class CalendarAppEvent implements CalendarEvent {
  _id?: string;
  start: Date;
  end?: Date;
  recepcion?: Date;
  title: string;
  color?: {
    primary: string;
    secondary: string;
  };
  actions?: CalendarEventAction[];
  allDay?: boolean;
  cssClass?: string;
  resizable?: {
    beforeStart?: boolean;
    afterEnd?: boolean;
  };
  draggable?: boolean;
  meta?: {
    location: string;
    notes: string;
  };
  // LLAVES FORANEAS
  expediente_id?: number;
  expediente_etapa?: number;
  expediente_nombre?: string;
  prioridad_id?: number;
  acto_id?: number;
  tipo_id?: number;
  finalidad_id?: number;
  persona_asesoria_id?: number;
  pers_ase_id?: number;
  tramite_doc_id?: number;
  etapa_id?: number;
  // TEXTO
  citacion_numero?: string;
  fecha_emision?: Date;
  fecha_recibido?: Date;
  fecha_citación?: Date;
  citacion_sumilla?: string;
  citacion_lugar?: string;
  citacion_referencia?: string;
  citacion_resolucion?: string;
  resolucion_fecha?: Date;
  citacion_observacion?: string;
  involucrado?: any;
  encargado?: any;
  observacion?: any;
  estado?: any;

  constructor(data?) {
    data = data || {};
    this.start = data.start ? moment(data.start,'YYYY-MM-DD[T]HH:mm').toDate() : null;
    this.resolucion_fecha = data.resolucion_fecha
      ? moment(data.resolucion_fecha).toDate()
      : null;
    this.end = data.end ? moment(data.end).toDate() : null;
    this.recepcion = data.recepcion ? moment(data.recepcion).toDate() : null;
    this._id = data._id || "";
    this.title = data.title || "";
    this.color = {
      primary: (data.color && data.color.primary) || "#247ba0",
      secondary: (data.color && data.color.secondary) || "#D1E8FF",
    };
    this.draggable = data.draggable || false;
    this.resizable = {
      beforeStart: (data.resizable && data.resizable.beforeStart) || false,
      afterEnd: (data.resizable && data.resizable.afterEnd) || false,
    };
    this.actions = data.actions || [];
    this.allDay = data.allDay || false;
    this.cssClass = data.cssClass || "";
    this.meta = {
      location: (data.meta && data.meta.location) || "",
      notes: (data.meta && data.meta.notes) || "",
    };
    // FORANEAS
    this.persona_asesoria_id = data.persona_asesoria_id || "";
    this.pers_ase_id = data.pers_ase_id || "";
    this.prioridad_id = data.prioridad_id || "";
    this.finalidad_id = data.finalidad_id || "";
    this.expediente_id = data.expediente_id || "";
    this.acto_id = data.acto_id || "";
    this.tipo_id = data.tipo_id || "";
    this.expediente_etapa = data.expediente_etapa || 0;
    this.expediente_nombre = data.expediente_nombre || "";
    this.citacion_sumilla = data.citacion_sumilla || "";
    this.citacion_lugar = data.citacion_lugar || "";
    this.citacion_numero = data.citacion_numero || "";
    this.citacion_observacion = data.citacion_observacion || "";
    this.citacion_referencia = data.citacion_referencia || "";
    this.citacion_resolucion = data.citacion_resolucion || "";
    this.involucrado = data.involucrado ? data.involucrado[0] : "";
    this.encargado = data.encargado ? data.encargado[0] : "";
    this.observacion = data.observacion || "";
    this.estado = data.estado || 0;
  }
}
