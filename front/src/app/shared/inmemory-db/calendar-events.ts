import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
} from "date-fns";

export class CalendarEventDB {
  private colors: any = {
    red: {
      primary: "#f44336",
      secondary: "#FAE3E3",
    },
    blue: {
      primary: "#247ba0 ",
      secondary: "#D1E8FF",
    },
    yellow: {
      primary: "#ffd97d",
      secondary: "#FDF1BA",
    },
  };

  public events: any[] = [
    {
      _id: "100",
      start: new Date(),
      recepcion: new Date(),
      end: addHours(new Date(), 1),
      title: "titulo 1",
      color: this.colors.red,
      persona_asesoria_id: 1,
      prioridad_id: 1,
      finalidad_id: 1,
      expediente_id: 1,
      citacion_sumilla: "sumilla 1",
      citacion_lugar: "citacion 1",
      citacion_numero: "numero 1",
      citacion_observacion: "obs 1",
      citacion_referencia: "ref 1",
      citacion_resolucion: "reso 1",
    },
    // {
    //   _id: "101",
    //   start: startOfDay(new Date()),
    //   title: "An event with no end date",
    //   color: this.colors.yellow,
    // },
    // {
    //   _id: "102",
    //   start: subDays(endOfMonth(new Date()), 3),
    //   end: addDays(endOfMonth(new Date()), 3),
    //   title: "A long event that spans 2 months",
    //   color: this.colors.blue,
    // },
    // {
    //   _id: "103",
    //   start: addHours(startOfDay(new Date()), 2),
    //   end: new Date(),
    //   title: "A draggable and resizable event",
    //   color: this.colors.yellow,
    //   resizable: {
    //     beforeStart: true,
    //     afterEnd: true,
    //   },
    //   draggable: true,
    // },
  ];
}
