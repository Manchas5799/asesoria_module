import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioSignComponent } from './inicio-sign/inicio-sign.component';
import { VistaCalendarioComponent } from './vista-calendario/vista-calendario.component';

const routes: Routes = [
  {
    path: '',
    component: VistaCalendarioComponent
  },
  {
    path: "api",
    component: InicioSignComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalendarMSRoutingModule { }
