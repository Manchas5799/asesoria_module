import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalendarMSRoutingModule } from './calendar-ms-routing.module';
import { InicioSignComponent } from './inicio-sign/inicio-sign.component';
import { VistaCalendarioComponent } from './vista-calendario/vista-calendario.component';

@NgModule({
  declarations: [InicioSignComponent, VistaCalendarioComponent],
  imports: [
    CommonModule,
    CalendarMSRoutingModule
  ]
})
export class CalendarMSModule { }
