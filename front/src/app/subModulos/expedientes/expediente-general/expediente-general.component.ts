import { LocalService } from "./../../../servicios/local.services";
import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { QueryService } from "src/app/servicios/query.services";
import { FiltrosService } from "src/app/global/services/filtros.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { JsPDFService } from "src/app/global/services/jsPDF.service";
import Swal from "sweetalert2";
import { forkJoin } from "rxjs";
import { environment } from "src/environments/environment";
import * as moment from "moment";
import { NgxSpinnerService } from "ngx-spinner";
import { startOfMonth } from "date-fns";

@Component({
  selector: "app-expedientes-general",
  templateUrl: "./expediente-general.component.html",
  styleUrls: ["./expediente-general.component.scss"],
})
export class ExpedientesGeneralComponent implements OnInit {
  titulo: string = "EXPEDIENTES";
  encabezadosExpedientes: string[] = [
    "NÚMERO",
    "NOMBRE",
    "TIPO EXPEDIENTE",
    "AGRAVIADO",
    "DELITO",
    "ACCIONES",
  ];
  encabezadosInvolucrados: string[] = [
    "NOMBRE COMPLETO",
    "TIPO PERSONA",
    "ACCIONES",
  ];
  msgBuscando: String = "Buscando..";
  /** listas */
  listado: any = [];
  listadoGral: any = [];
  listadoPer: any = [];
  listadoPersonas: any = [];
  listaSalas: any = [];
  listaDistritos: any = [];
  listaEstados: any = [];
  listaPrioridad: any = [];
  listaTipos: any = [];
  listaEtapas: any = [];
  listaCalidades: any = [];
  listaMaterias: any = [];
  listaMateriasOrdenadas: any = [];
  listaMateriasFiltradas: any = [];
  registrosPersonas: string = "";
  dataTotal: any;
  involucrados: any = [];
  actosProceso: any = [];
  estadosExpedientes: any = [];
  expedienteActual: any;
  public ruta: any = environment.rutas.backEnd;
  /*Campos para Filtros */
  campoFiltro = {
    // nombreInvolucrado: "",
    // nombreTipoInvolucrado: "",
    // nombreCalidadInvolucrado: "",
    numero: "",
    nombre: "",
    sala: "",
    nombre_materia_expediente: "",
    nombre_tipo_expediente: "",
    juez: "",
    fiscal: "",
    especialista: "",
    nombrePer: "",
    idPer: "",
    nombrePerI: "",
    nombreTipoI: "",
    acto: "",
    materia: "",
  };

  /**pagination */
  rows = [];
  iPageSize = environment.paginacion.tamPag;
  totalItems: number = 0;
  p: number = 1;
  pE: number = 1;
  pI: number = 1;
  pA: number = 1;

  /** forms */
  formulario: FormGroup;
  formBuscarPersona: FormGroup;
  formularioPersonasExpediente: FormGroup;
  formGlobal: FormGroup;
  formEstadoProceso: FormGroup;
  //buscar:string

  //   formFiltroData: FormGroup;
  tituloForm: string = "";

  modalForm = null;
  /* loading */
  loading: boolean = false;

  /**check */
  checks: any = [];
  chkTodos: boolean = false;

  /** Tipo de reporte */
  typeReport: string = "PDF";
  pagReport: number = 1;

  /**auto-inicio filtro*/
  iOpcionFiltro = 0;
  /*URLS */
  url = {
    select: "/expedientes",
    update: "/expedientes_save",
    delete: "/expedientes_del",
    personas: "/get_personas",
    save_personas: "/expedientes_personas",
    del_personas: "/expedientes_personas_del",
    select_salas: "/salas",
    select_tipos: "/tipo_expedientes",
    select_etapas: "/etapas",
    select_calidades: "/calidades",
    select_materias: "/materias",
    select_distritos: "/distrito_judicial",
    select_estados: "/estado_expediente",
    select_prioridad: "/prioridades",
    descarga_excel: "/reportes/expedientes_excel",
    // select_procesos: "/estado_proceso",
    update_estado_proceso: "/estado_proceso_save",
    delete_estado_proceso: "/estado_proceso_del",
    select_seguimiento: "/tramites_seguimiento",
    select_tramite: "/tramite_externo",
  };
  seguimientos: any = [];
  tramiteActual: any = null;
  pS = 1;
  constructor(
    private query: QueryService,
    private filtro: FiltrosService,
    private route: ActivatedRoute,
    private router: Router,
    private localService: LocalService,
    private toastr: ToastrService,
    private modal: NgbModal,
    private fb: FormBuilder,
    private jsPDF: JsPDFService,
    private spinner: NgxSpinnerService // private modalService:ModalsService
  ) {}
  seguimiento(registro, modal) {
    if (registro.tramite_doc_id !== null) {
      this.spinner.show();
      this.seguimientos = [];
      forkJoin({
        tramite: this.query.getDatosApi(
          this.url.select_tramite + `/${registro.tramite_doc_id}`
        ),
        seguimiento: this.query.getDatosApi(
          this.url.select_seguimiento + `/${registro.tramite_doc_id}`
        ),
      }).subscribe({
        next: (data: any) => {
          this.tramiteActual = data.tramite[0];
          const res = data.seguimiento;
          this.seguimientos = res;
          this.seguimientos.map((item) => {
            item.cEstadoTramiteNombre = item.cEstadoTramiteNombre.slice(2);
            let dS = "";
            if (item.dtTramMovFechaHoraRecibido !== null) {
              item.entrada = moment(item.dtTramMovFechaHoraEnvio)
                .format("LT")
                .toString();
              item.salida = moment(item.dtTramMovFechaHoraRecibido)
                .format("LT")
                .toString();
              const hI: any = moment(item.entrada, "HH:mm");
              const hS: any = moment(item.salida, "HH:mm");
              dS = moment
                .duration(hS - hI)
                .locale("es")
                .humanize();
              item.duracion = dS;
            }
          });
        },
        complete: () => {
          this.spinner.hide("tramite");
          this.modal.open(modal, {
            size: "lg",
            backdrop: "static",
            keyboard: false,
          });
        },
      });
    } else {
      this.toastr.info(
        "No cuenta con seguimiento a tramite documentario",
        "MOSTRAR SEGUIMIENTO"
      );
    }
  }
  imprimirSeguimiento() {
    this.spinner.show();
    this.prepararPdf();
    this.spinner.hide("tramite");
  }
  prepararPdf() {
    this.jsPDF.generaPDF(
      this.prepararPdfSeguimiento(),
      1,
      this.prepararcabeceraPdf()
    );
  }
  prepararPdfSeguimiento() {
    let listadoPDF: any = [];
    let index: number = 1;
    this.seguimientos.forEach((element) => {
      listadoPDF.push([
        index++,
        element.dtTramMovFechaHoraEnvio,
        element.cDepenEmisorNombre,
        element.dtTramMovFechaHoraRecibido,
        element.cDepenReceptorNombre,
        element.cTramDocumentoTramite,
        element.cTramAsunto,
        element.cEstadoTramiteNombre,
        element.duracion,
      ]);
    });
    let estilosColumnas = {
      0: { halign: "center" },
      1: { halign: "center" },
      2: { halign: "center" },
      3: { halign: "center" },
      4: { halign: "center" },
      5: { halign: "center" },
      6: { halign: "center" },
      7: { halign: "center", textalign: "center" },
      8: { halign: "center" },
      9: { halign: "center" },
    };
    let datosPDF = {
      orientacion: "l", //Un valor distinto a 'p' hará que sea horizontal (Se sugiere 'l')
      titulo:
        "TRAMITE DOCUMENTARIO REG. N° " +
        this.tramiteActual.cTramNumeroDocumento +
        " - " +
        this.tramiteActual.iTramYearRegistro,

      cabeceras: [
        [
          "#",
          "FECHA ENVÍO",
          "DEPENDENCIA ENVÍO",
          "FECHA RECIBÍDO",
          "DEPENDENCIA RECIBÍDO",
          "DOCUMENTO",
          "ASUNTO",
          "ESTADO",
          "TIEMPO",
        ],
      ],
      datos: listadoPDF,
      columnaStyles: estilosColumnas,
      headerStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
  prepararcabeceraPdf() {
    let listadoPDF: any = [];
    let asunto =
      this.tramiteActual.cTramAsuntoDocumento.substring(0, 130) + " ...";
    listadoPDF.push([this.tramiteActual.cTramDocumentoTramite, asunto]);
    let estilosColumnas = {
      0: { halign: "left" },
      1: { halign: "left" },
    };
    let datosPDF = {
      headers: [["DOCUMENTO", "ASUNTO"]],
      data: listadoPDF,
      columnaStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
  ngOnInit() {
    // this.seleccionar();
    this.obtenerTodo();
    this.formulario = this.fb.group({
      id: [""],
      nombre: ["", Validators.required],
      numero: [0, Validators.required],
      sala: ["", Validators.required],
      estado: [1, Validators.required],
      descripcion: ["", Validators.required],
      juez: ["", Validators.required],
      fiscal: ["", Validators.required],
      especialista: ["", Validators.required],
      materia: ["", Validators.required],
      acto: ["", Validators.required],
      idtramitedoc: [""],
      etapa: ["", Validators.required],
      fechaEtapa: ["", Validators.required],
      obsEtapa: [""],
    });
    this.formBuscarPersona = this.fb.group({
      buscar: ["", Validators.required],
    });
    this.formularioPersonasExpediente = this.fb.group({
      id: [""],
      nombrePersona: [[{ value: "", disabled: true }]],
      expediente: ["", Validators.required],
      nombre: ["", Validators.required],
      calidad: ["", Validators.required],
      fecha: ["", Validators.required],
      etapa: ["", Validators.required],
    });
    this.formGlobal = this.fb.group({
      materiaFiltro: [""],
      etapaFiltro: [""],
      fechaIFiltro: [
        moment(startOfMonth(new Date())).format("YYYY-MM-DD"),
        Validators.required,
      ],
      fechaFFiltro: [
        moment(new Date()).format("YYYY-MM-DD"),
        Validators.required,
      ],
    });
  }
  mostrarModalInvolucrados(modal, item) {
    this.modal.open(modal, {
      size: "lg",
      backdrop: "static",
      keyboard: false,
    });
    this.expedienteActual = item[0];
    this.involucrados = this.expedienteActual.involucrados;
  }
  crearFormOpenEstados(modal) {
    this.modalFormOpenPersonas(modal);
    this.asignarFormEstadoProceso(this.expedienteActual);
  }
  mostrarModalEstadoProceso(modal, item) {
    this.modal.open(modal, {
      backdrop: "static",
      keyboard: false,
    });
    this.expedienteActual = item[0];
    this.estadosExpedientes = this.expedienteActual.estado_proceso;
  }
  asignarFormEstadoProceso(expediente, item = null) {
    if (item === null) {
      this.formEstadoProceso = this.fb.group({
        id: [],
        iExpediente: [expediente.id, Validators.required],
        documento: ["", Validators.required],
        fecha: [moment().format("YYYY-MM-DD").toString(), Validators.required],
        descripcion: ["", Validators.required],
      });
    } else {
      this.formEstadoProceso = this.fb.group({
        id: [item.estado_proceso_id],
        iExpediente: [expediente.id, Validators.required],
        documento: [item.documento, Validators.required],
        fecha: [
          moment(item.fecha_doc).format("YYYY-MM-DD").toString(),
          Validators.required,
        ],
        descripcion: [item.descripcion, Validators.required],
      });
    }
  }
  editarFormEstadoProceso(modal, item) {
    this.modalFormOpenPersonas(modal);
    this.asignarFormEstadoProceso(this.expedienteActual, item);
  }

  eliminarEstadoProceso(item) {
    Swal.fire({
      title: "¿Continuar?",
      html:
        "Se eliminará el  registro <br><b>N° " +
        item.estado_proceso_id +
        "-" +
        item.documento +
        ".</b><br> <b>¿Desea continuar?</b>",
      //input: 'textarea',
      inputPlaceholder: "Observación...",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Continuar",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (!result.dismiss) {
        let idE = item.estado_proceso_id;
        this.query.delDatoApi(this.url.delete_estado_proceso, idE).subscribe(
          (data) => {
            let info: any = data;

            if (info.datos[0]["iResult"] == 1) {
              Swal.fire("Eliminado", info.msg, "success");
              this.estadosExpedientes.splice(
                this.estadosExpedientes.findIndex(
                  (item) => item.estado_proceso_id === idE
                ),
                1
              );
              this.seleccionar();
            } else {
              Swal.fire("Cancelado", info.msg, "error");
            }
          },
          (error) => {
            Swal.fire("Cancelado", error.msg, "error");
          }
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }
  modalFormPersonaDemanda(formulario, tipo) {
    this.modal.open(formulario, { size: "lg" }).result.then(
      (result) => {
        switch (tipo) {
          case 1:
            this.formulario.controls["juez"].setValue(result[0].nombre);
            break;
          case 2:
            this.formulario.controls["fiscal"].setValue(result[0].nombre);
            break;
          case 3:
            this.formulario.controls["especialista"].setValue(result[0].nombre);
            break;
        }
        console.log(this.formularioPersonasExpediente.value);
      },
      (reason) => {
        //this.toastr.info(reason)
      }
    );
  }
  modalFormPersona(formulario) {
    this.modal.open(formulario, { size: "lg" }).result.then(
      (result) => {
        this.formularioPersonasExpediente.controls["nombre"].setValue(
          result[0].nombre
        );
      },
      (reason) => {
        //this.toastr.info(reason)
      }
    );
  }
  modalFormOpenPersonas(modal) {
    this.modal.open(modal, {
      size: "lg",
      backdrop: "static",
      keyboard: false,
    });
  }
  crearFormOpenPersonas(modal) {
    this.modalFormOpenPersonas(modal);
    this.asignarDatosFormPersonas(this.expedienteActual);
  }
  editarFormOpenPersonas(modal, item) {
    this.modalFormOpenPersonas(modal);
    this.asignarDatosFormPersonas(this.expedienteActual, item);
  }
  modalFormOpen(formulario) {
    this.tituloForm = "Nuevo Expediente";
    this.modalForm = this.modal.open(formulario, {
      size: "lg",
      backdrop: "static",
      keyboard: false,
    });
    this.asignarDatosForm(null);
  }
  validarParteUno() {
    return !this.formulario.controls["cuerpo_uno"].valid;
  }
  asignarDatosForm(dato) {
    if (dato == null) {
      this.formulario = this.fb.group({
        cuerpo_uno: this.fb.group({
          id: [""],
          nombre: ["", Validators.required],
          numero: [0, Validators.required],
          sala: ["", Validators.required],
          estado: [1, Validators.required],
          monto: [""],
          descripcion: ["", Validators.required],
          materia: ["", Validators.required],
          acto: ["", Validators.required],
          idtramitedoc: [""],
          distrito: ["", Validators.required],
          estadoEx: ["", Validators.required],
          prioridad: ["", Validators.required],
        }),
        juez: ["", Validators.pattern("[A-Z a-z À-ÿ \u00f1 \u00d1]*")], //[0-9 A-Z a-z À-ÿ \u00f1 \u00d1]*
        fiscal: ["", Validators.pattern("[A-Z a-z À-ÿ \u00f1 \u00d1]*")],
        especialista: ["", Validators.pattern("[A-Z a-z À-ÿ \u00f1 \u00d1]*")],
        etapa: [
          this.listaEtapas[0] ? this.listaEtapas[0].id : null,
          Validators.required,
        ],
        fechaEtapa: [moment().format("DD-MM-YYYY"), Validators.required],
        obsEtapa: [""],
      });
    } else {
      this.formulario = this.fb.group({
        cuerpo_uno: this.fb.group({
          id: [dato[0].id],
          nombre: [dato[0].nombre, Validators.required],
          numero: [dato[0].numero, Validators.required],
          sala: [dato[0].salas_id, Validators.required],
          estado: [parseInt(dato[0].estado_bit, 10), Validators.required],
          monto: [dato[0].monto_indem],
          descripcion: [dato[0].descripcion, Validators.required],
          materia: [dato[0].materia_id, Validators.required],
          acto: [dato[0].acto_procesal, Validators.required],
          idtramitedoc: [dato[0].tramite_doc_id],
          distrito: [dato[0].distrito_id, Validators.required],
          estadoEx: [dato[0].estado_id, Validators.required],
          prioridad: [dato[0].prioridad_id, Validators.required],
        }),
        juez: [dato[0].juez],
        fiscal: [dato[0].fiscal],
        especialista: [dato[0].esp_legal],
        etapa: [dato[0].etapa_actual, Validators.required],
        fechaEtapa: [dato[0].etapa[0].etapa_fecha, Validators.required],
        obsEtapa: [dato[0].etapa[0].etapa_obs],
        etapaexp: [dato[0].etapa[0].id],
      });
      console.log(this.formulario.value);
    }
    this.validarCamposForm(this.formulario);
  }
  obtenerTodo() {
    this.spinner.show("general");
    forkJoin({
      salas: this.query.getDatosApi(this.url.select_salas),
      tipos: this.query.getDatosApi(this.url.select_tipos),
      etapas: this.query.getDatosApi(this.url.select_etapas),
      calidades: this.query.getDatosApi(this.url.select_calidades),
      materias: this.query.getDatosApi(this.url.select_materias),
      distritos: this.query.getDatosApi(this.url.select_distritos),
      estados: this.query.getDatosApi(this.url.select_estados),
      prioridad: this.query.getDatosApi(this.url.select_prioridad),
    }).subscribe({
      next: (res) => {
        console.log(res);
        // this.totalItems = res.expes[0] ? res.expes[0].TotalExpedientes : 0;
        // this.dataTotal = res.expes;
        // this.listado = this.dataTotal;
        // this.listadoGral = this.listado;
        this.listaSalas = res.salas;
        this.listaTipos = res.tipos;
        this.listaEtapas = res.etapas;
        this.listaCalidades = res.calidades;
        this.listaMaterias = res.materias;
        this.listaDistritos = res.distritos;
        this.listaEstados = res.estados;
        this.listaPrioridad = res.prioridad;
        const materiasOrd = [];
        this.listaMaterias.forEach((e) => {
          materiasOrd.push(parseInt(e.tipo_id));
        });
        this.listaMateriasOrdenadas = materiasOrd.filter(
          (el, index) => materiasOrd.indexOf(el) === index
        );
        const listaMateriasAgrupadas = [];
        this.listaMateriasOrdenadas.forEach((e) => {
          let materias = [];
          let materiasAgrupadas = {
            tipo_id: e,
            nombre_tipo: "",
            materias: [],
          };
          this.listaMaterias.forEach((m, key) => {
            if (parseInt(m.tipo_id) === materiasAgrupadas.tipo_id) {
              materiasAgrupadas.nombre_tipo = m.tipo_nombre;
              materias.push(m);
            }
          });
          materiasAgrupadas.materias = materias;
          listaMateriasAgrupadas.push(materiasAgrupadas);
        });
        this.listaMateriasFiltradas = listaMateriasAgrupadas;
        console.log(listaMateriasAgrupadas);
      },
      complete: () => {
        this.loading = false;
        this.spinner.hide("general");
        this.seleccionar();
      },
      error: (er) => {
        console.log(er);
        this.loading = false;
      },
    });
  }
  seleccionar() {
    this.spinner.show("general");
    if (this.formGlobal.valid) {
      this.query
        .saveDatosApi(this.url.select, {
          etapa: this.formGlobal.value.etapaFiltro,
          materia: this.formGlobal.value.materiaFiltro,
          inicio: this.formGlobal.value.fechaIFiltro,
          fin: this.formGlobal.value.fechaFFiltro,
        })
        .subscribe(
          (data) => {
            console.log(data);
            this.totalItems = data[0] ? data[0].TotalExpedientes : 0;
            this.dataTotal = data;
            //this.rows = []
            this.listado = this.dataTotal;
            this.listadoGral = this.listado;
            this.loading = false;
            this.spinner.hide("general");
          },
          (error) => {
            console.log(error);
            this.spinner.hide("general");
            // this.toastr.warning(
            //   error["error"]["message"].substring(
            //     71,
            //     error["error"]["message"].indexOf("(SQL:")
            //   ),
            //   "Importante"
            // );
            this.loading = false;
          }
        );
    }
    // this.loading = false
  }
  filtrar() {
    this.listado = this.listadoGral;
    console.table(this.listado);
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.numero,
      7
    );
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.nombre,
      5
    );
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.nombre_materia_expediente,
      12
    );
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.nombre_tipo_expediente,
      32
    );
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.sala,
      4
    );
    // this.listado = this.filtro.filtroData(
    //   this.listado,
    //   this.campoFiltro.fiscal,
    //   28
    // );
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.especialista,
      29
    );
  }
  filtrarPersona() {
    this.listadoPer = this.listadoPersonas;
    this.listadoPer = this.filtro.filtroData(
      this.listadoPer,
      this.campoFiltro.idPer,
      0
    );
    this.listadoPer = this.filtro.filtroData(
      this.listadoPer,
      this.campoFiltro.nombrePer,
      1
    );
  }
  validarCamposForm(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validarCamposForm(control);
      }
    });
  }
  guardar() {
    if (this.formulario.valid) {
      this.formulario.disable();
      console.log(this.formulario.value);
      let materiaActual = this.listaMaterias.find(
        (e) => e.id === this.formulario.value.cuerpo_uno.materia
      );
      let formData = {
        id: this.formulario.value.cuerpo_uno.id,
        nombre: this.formulario.value.cuerpo_uno.nombre,
        sala: this.formulario.value.cuerpo_uno.sala,
        numero: this.formulario.value.cuerpo_uno.numero,
        estado: this.formulario.value.cuerpo_uno.estado,
        monto: this.formulario.value.cuerpo_uno.monto,
        descripcion: this.formulario.value.cuerpo_uno.descripcion,
        tramitedoc: this.formulario.value.cuerpo_uno.idtramitedoc,
        materia: this.formulario.value.cuerpo_uno.materia,
        cMateria: materiaActual.nombre,
        acto: this.formulario.value.cuerpo_uno.acto,
        distrito: this.formulario.value.cuerpo_uno.distrito,
        prioridad: this.formulario.value.cuerpo_uno.prioridad,
        estadoEx: this.formulario.value.cuerpo_uno.estadoEx,
        etapa: this.formulario.value.etapa,
        expFecha: this.formulario.value.fechaEtapa,
        expMotivo: this.formulario.value.obsEtapa,
        expEtapa: this.formulario.value.etapaexp,
        juez: this.formulario.value.juez,
        fiscal: this.formulario.value.fiscal,
        especialista: this.formulario.value.especialista,
      };
      this.query.saveDatosApi(this.url.update, formData).subscribe(
        (data) => {
          this.formulario.enable();
          this.modal.dismissAll();
          Swal.fire("Éxito!", data["msg"], "success");
          this.seleccionar();
        },
        (error) => {
          // console.log("msg", error);
          this.formulario.enable();
          // this.modal.dismissAll();
          this.toastr.error(error.msg, "Problema al guardar");
          // Swal.fire(
          //   "Error!",
          //   error["error"]["message"].substring(
          //     71,
          //     error["error"]["message"].indexOf("(SQL: EXEC")
          //   ),
          //   "error"
          // );
        }
      );
    } else {
      this.toastr.warning("Complete los campos requeridos", "Importante");
    }
  }
  guardarEstadoProceso() {
    const data = this.formEstadoProceso.value;
    this.query
      .saveDatosApi(this.url.update_estado_proceso, data)
      .subscribe((res) => {
        const rpt: any = res;
        if (rpt.error) {
          this.toastr.error("Error al guardar los datos", "ESTADO DE PROCESO");
          return;
        }
        // this.modal.dismissAll();
        let guardar = 0;
        if (this.estadosExpedientes !== null) {
          this.estadosExpedientes.forEach((e) => {
            if (e.estado_proceso_id === data.id) {
              e.documento = data.documento;
              e.fecha = data.fecha;
              e.descripcion = data.descripcion;
              guardar = 1;
            }
          });
        } else {
          this.estadosExpedientes = [];
        }
        if (guardar === 0) {
          this.estadosExpedientes.push({
            estado_proceso_id: rpt.datos.iEstadoProcesoId,
            documento: data.documento,
            fecha_doc: data.fecha,
            descripcion: data.descripcion,
          });
        }

        this.toastr.success(rpt.msg, "ESTADO DE PROCESO", { timeOut: 2000 });
        this.seleccionar();
      });
  }
  asignarDatosFormPersonas(expediente, dato = null) {
    if (dato == null) {
      let fechaActual = moment().format("YYYY-MM-DD[T]hh:mm");
      this.formularioPersonasExpediente = this.fb.group({
        id: [""],
        expediente: [expediente.id, Validators.required],
        documento: [""],
        nombre: ["", Validators.required],
        calidad: ["", Validators.required],
        fecha: [fechaActual, Validators.required],
        etapa: [expediente.etapa[0].etapa_id, Validators.required],
        etapaexp: [expediente.etapa[0].id, Validators.required],
        observacion: [""],
      });
    } else {
      this.formularioPersonasExpediente = this.fb.group({
        id: [dato.id],
        expediente: [expediente.id, Validators.required],
        documento: [""],
        nombre: [dato.nombre_pers, Validators.required],
        calidad: [dato.calidad_id, Validators.required],
        fecha: [
          moment(dato.fecha).format("YYYY-MM-DD[T]hh:mm"),
          Validators.required,
        ],
        etapa: [expediente.etapa[0].etapa_id, Validators.required],
        etapaexp: [expediente.etapa[0].id, Validators.required],
        observacion: [dato.observacion],
      });
    }
  }
  guardarPersona() {
    if (this.formularioPersonasExpediente.valid) {
      this.formularioPersonasExpediente.disable();
      let formData = {
        expediente: this.formularioPersonasExpediente.value.expediente,
        nombre: this.formularioPersonasExpediente.getRawValue().nombre,
        calidad: this.formularioPersonasExpediente.value.calidad,
        fecha: this.formularioPersonasExpediente.value.fecha,
        etapa: this.formularioPersonasExpediente.value.etapa,
        id: this.formularioPersonasExpediente.value.id,
        observacion: this.formularioPersonasExpediente.value.observacion,
      };
      this.query.saveDatosApi(this.url.save_personas, formData).subscribe(
        (data: any) => {
          this.formulario.enable();
          // this.modal.dismissAll();
          Swal.fire("Éxito!", data["msg"], "success");
          this.seleccionar();
          let guardar = 0;
          const calidadSeleccionada = this.listaCalidades.find(
            (e) => parseInt(e.id, 10) === parseInt(formData.calidad)
          );
          if (this.involucrados !== null) {
            this.involucrados.forEach((e) => {
              if (e.id === formData.id) {
                e.nombre_pers = formData.nombre;
                e.calidad_id = formData.calidad;
                e.calidad = calidadSeleccionada.nombre;
                e.fecha = formData.fecha;
                e.observacion = formData.observacion;
                guardar = 1;
              }
            });
          } else {
            this.involucrados = [];
          }
          if (guardar === 0) {
            this.involucrados.push({
              id: data.datos.iExpedientesId,
              nombre_pers: formData.nombre,
              calidad_id: formData.calidad,
              calidad: calidadSeleccionada.nombre,
              fecha: formData.fecha,
              observacion: formData.observacion,
            });
          }
        },
        (error) => {
          // console.log("msg", error);
          this.formulario.enable();
          // this.modal.dismissAll();
          this.toastr.error(error.msg, "Problema al guardar");
          // Swal.fire(
          //   "Error!",
          //   error["error"]["message"].substring(
          //     71,
          //     error["error"]["message"].indexOf("(SQL: EXEC")
          //   ),
          //   "error"
          // );
        }
      );
    } else {
      this.toastr.warning("Complete los campos requeridos", "Importante");
    }
  }
  editar(formulario, list) {
    this.tituloForm = "Editando Expediente";
    this.modalForm = this.modal.open(formulario, {
      size: "lg",
      backdrop: "static",
      keyboard: false,
    });
    this.asignarDatosForm(list);
  }
  eliminar(list) {
    Swal.fire({
      title: "¿Continuar?",
      html:
        "Se eliminará el  registro <br><b>N° " +
        list[0].id +
        "-" +
        list[0].nombre +
        ".</b><br> <b>¿Desea continuar?</b>",
      //input: 'textarea',
      inputPlaceholder: "Observación...",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Continuar",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (!result.dismiss) {
        let idE = list[0].id;
        this.query.delDatoApi(this.url.delete, idE).subscribe(
          (data) => {
            let info: any = data;

            if (info.datos[0]["iResult"] == 1) {
              Swal.fire("Eliminado", info.msg, "success");

              this.seleccionar();
            } else {
              Swal.fire("Cancelado", info.msg, "error");
            }
          },
          (error) => {
            Swal.fire("Cancelado", error.msg, "error");
          }
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }
  eliminarPersonaDelCaso(personaCaso, posi) {
    Swal.fire({
      title: "¿Continuar?",
      html: `Se eliminará a <br><b>${personaCaso.nombre_pers}
         del caso.</b><br> <b>¿Desea continuar?</b>`,
      //input: 'textarea',
      inputPlaceholder: "Observación...",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Continuar",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (!result.dismiss) {
        let idE = personaCaso.id;
        this.query.delDatoApi(this.url.del_personas, idE).subscribe(
          (data) => {
            let info: any = data;
            if (!info.error) {
              Swal.fire("Eliminado", info.msg, "success");
              // this.seleccionar();
              this.involucrados.splice(posi, 1);
            } else {
              Swal.fire("Cancelado", info.msg, "error");
            }
          },
          (error) => {
            Swal.fire("Cancelado", error.msg, "error");
          }
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }
  buscarPersona() {
    this.loading = true;
    let formData = {
      texto: this.formBuscarPersona.value.buscar,
    };
    // console.log("Buscando Persona: " + this.formBuscarPersona.value.buscarr);
    this.query.getDatosPostApi(this.url.personas, formData).subscribe(
      (data) => {
        this.listadoPersonas = data;
        this.listadoPer = this.listadoPersonas;
        this.registrosPersonas = this.listadoPersonas.length;
        this.loading = false;
      },
      (error) => {
        this.toastr.warning(
          error["error"]["message"].substring(
            71,
            error["error"]["message"].indexOf("(SQL:")
          ),
          "Importante"
        );
        this.loading = false;
      }
    );
  }
  seleccionarPersona(persona) {
    console.log(persona[0]);
    this.formulario.controls["idresponsable"].setValue(persona[0].id);
    this.formulario.controls["responsable"].setValue(persona[0].nombre);
    //this.modalService.close('modalResponsable')
  }
  downloadReport(typeReport, registro = null) {
    this.loading = true;
    switch (typeReport) {
      case "EXCEL":
        let materiaActual = this.listaMaterias.find(
          (item) => item.id === this.formGlobal.value.materiaFiltro
        );
        this.query
          .getDatosPostApi(this.url.descarga_excel, {
            listado: this.listado,
            tipo: materiaActual ? materiaActual.tipo_nombre : "TODOS",
          })
          .subscribe({
            next: (res) => {
              console.log(res);
              let data: any = res;
              window.open(environment.rutas.recursos + data.ruta, "_blank");
            },
            complete: () => {},
          });
        break;
      default:
        // Por defecto PDF
        console.log("Descarga PDF");
        this.expedienteActual = registro;
        this.downloadPDF();
        break;
    }
    this.loading = false;
  }
  downloadPDF() {
    if (this.expedienteActual.citaciones === null) {
      this.toastr.warning(
        "EL EXPEDIENTE NO CUENTA CON CITACIONES REGISTRADAS",
        "CITACIONES"
      );
      return 0;
    }
    this.jsPDF.generaPDF(this.prepararPDF(), 1, this.prepararCabecera());
  }
  private prepararPDF() {
    // Verificar this.listado
    let listadoPDF: any = [];
    let index: number = 1;
    this.expedienteActual.citaciones.forEach((element, key) => {
      listadoPDF.push([
        key + 1,
        element.lugar_citacion,
        element.sumilla,
        moment(element.fecha_recibido).format("YYYY-MM-DD").toString(),
        moment(element.fecha_citacion).format("YYYY-MM-DD hh:mm a").toString(),
        element.citado[0].nombre,
        element.observacion || "",
      ]);
    });
    let estilosColumnas = {
      0: { halign: "center" },
      1: { halign: "center" },
      2: { halign: "center" },
      3: { halign: "center" },
      4: { halign: "center" },
      5: { halign: "center" },
      6: { halign: "center" },
    };
    let datosPDF = {
      orientacion: "l", //Un valor distinto a 'p' hará que sea horizontal (Se sugiere 'l')
      titulo: this.titulo,
      cabeceras: [
        [
          "ITEM",
          "LUGAR",
          "SUMILLA",
          "NOTIFICADO",
          "CITACIÓN",
          "ENCARGADO",
          "OBSERVACION",
        ],
      ],
      datos: listadoPDF,
      columnaStyles: estilosColumnas,
      headerStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
  private prepararCabecera() {
    let listadoPDF: any = [];

    listadoPDF.push([
      this.expedienteActual.nombre,
      this.expedienteActual.numero,
    ]);
    let estilosColumnas = {
      0: { halign: "left" },
      1: { halign: "left" },
    };
    let datosPDF = {
      headers: [["NOMBRE DE EXPEDIENTE", "NÚMERO DE EXPEDIENTE"]],
      data: listadoPDF,
      columnaStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
  /// BUSCAR EN PIDE
  buscarPide(event) {
    // console.log(event);
    this.loading = true;
    let docu = event.target.value;
    // this.personaBuscada = null;
    // this.empresaBuscada = null;
    // this.rutaFoto = "";

    let campo = "";
    let tipo = "";
    if (docu.toString().length === 0) {
      this.formularioPersonasExpediente.controls["nombre"].setValue("");
      return;
    }
    if (docu.toString().length === 8) {
      this.formularioPersonasExpediente.disable();
      campo = "dni";
      tipo = "reniec";
      this.query
        .getDatosApi(`/pide/${tipo}?${campo}=${docu}`)
        .subscribe((res: any) => {
          if (res.error) {
            this.toastr.error(
              "No se encontró a la persona o empresa que buscaba."
            );
          } else {
            const rpta = res.data;
            let nom = "";
            if (campo === "dni") {
              nom = `${rpta.cReniecNombres} ${rpta.cReniecApel_pate} ${rpta.cReniecApel_mate}`;
            } else {
              nom = `${rpta.nombre}`;
            }
            this.formularioPersonasExpediente.controls["nombre"].setValue(nom);
            this.formularioPersonasExpediente.controls["nombre"].disable();
          }
          this.loading = false;
          this.formularioPersonasExpediente.enable();
        });
    } else if (docu.toString().length === 11) {
      this.formularioPersonasExpediente.disable();
      campo = "ruc";
      tipo = "sunat";
      this.query
        .getDatosApi(`/pide/${tipo}?${campo}=${docu}`)
        .subscribe((res: any) => {
          if (res.error) {
            this.toastr.error(
              "No se encontró a la persona o empresa que buscaba."
            );
          } else {
            const rpta = res.data;
            let nom = "";
            if (campo === "dni") {
              nom = `${rpta.cReniecNombres} ${rpta.cReniecApel_pate} ${rpta.cReniecApel_mate}`;
            } else {
              nom = `${rpta.nombre}`;
            }
            this.formularioPersonasExpediente.controls["nombre"].setValue(nom);
            this.formularioPersonasExpediente.controls["nombre"].disable();
          }
          this.loading = false;
          this.formularioPersonasExpediente.enable();
        });
    }
  }
  goToCitaciones(registro) {
    this.localService.setItem("expediente_buscado", registro);
    this.router.navigateByUrl("/notificaciones/C");
  }
  goToTareas(registro) {
    this.localService.setItem("expediente_buscado", registro);
    this.router.navigateByUrl("/notificaciones/T");
  }
  mostrarModalTramite(modal) {
    this.modal
      .open(modal, {
        size: "lg",
        backdrop: "static",
        keyboard: false,
      })
      .result.then((res) => {
        console.log(this.formulario.controls);
        // console.log(this.formulario.value, res);
        const valorInterno: any = this.formulario.controls["cuerpo_uno"];
        valorInterno.controls["idtramitedoc"].setValue(res.iTramId);
        // this.formulario.value.cuerpo_uno.idtramitedoc = parseInt(
        //   res.iTramId,
        //   10
        // );
      });
  }
}
