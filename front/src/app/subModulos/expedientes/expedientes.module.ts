import { ConsultasExternasModule } from "./../consultas-externas/consultas-externas.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ExpedientesRoutingModule } from "./expedientes-routing.module";
import { ExpedientesGeneralComponent } from "./expediente-general/expediente-general.component";
import { SharedComponentsModule } from "src/app/shared/components/shared-components.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { GlobalModule } from "src/app/global/global.module";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { NgxPaginationModule } from "ngx-pagination";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ResaltarPipe } from "src/app/global/pipes/resaltar.pipe";
import {
  MatFormFieldModule,
  MatFormFieldControl,
} from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { MatDialogModule } from "@angular/material/dialog";
import { MatStepperModule } from "@angular/material/stepper";
import { MatButtonModule } from "@angular/material/button";
import { MatMenuModule } from "@angular/material/menu";
import { MatIconModule } from "@angular/material/icon";
import { MatTooltipModule } from "@angular/material/tooltip";
import { NgxSpinnerModule } from "ngx-spinner";
@NgModule({
  declarations: [ExpedientesGeneralComponent],
  imports: [
    CommonModule,
    SharedComponentsModule,
    NgbModule,
    GlobalModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgxPaginationModule,
    FormsModule,
    ExpedientesRoutingModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatStepperModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatTooltipModule,
    NgxSpinnerModule,
    ConsultasExternasModule,
    // ResaltarPipe
  ],
})
export class ExpedientesModule {}
