import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExpedientesGeneralComponent } from './expediente-general/expediente-general.component';

const routes: Routes = [
  {
    path:  "",
    component: ExpedientesGeneralComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExpedientesRoutingModule { }
