import { environment } from "./../../../../environments/environment.prod";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import * as moment from "moment";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { FiltrosService } from "src/app/global/services/filtros.service";
import { JsPDFService } from "src/app/global/services/jsPDF.service";
import { QueryService } from "src/app/servicios/query.services";

@Component({
  selector: "app-presupuesto-ext",
  templateUrl: "./presupuesto-ext.component.html",
  styleUrls: ["./presupuesto-ext.component.scss"],
})
export class PresupuestoExtComponent implements OnInit {
  formBusquedaBiblio: FormGroup;
  listaLocales: any = [];
  listaSancionados: any = [];
  listaGrl: any = [];
  listaanios: any = [];
  url = {
    select_locales: "/biblioteca_locales",
    select_sancionados: "/presupuesto_cc",
  };
  iPageSize = environment.paginacion.tamPag;
  p = 1;
  campoFiltro = {
    cc: "",
    sf: "",
    dep: "",
    mn: "",
    usu: "",
    cant: "",
  };
  constructor(
    private query: QueryService,
    private filtro: FiltrosService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private modal: NgbModal,
    private fb: FormBuilder,
    private jsPDF: JsPDFService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.formBusquedaBiblio = this.fb.group({
      anio: ["", Validators.required],
    });
    // this.cargarDatos();
    let anioActual = parseInt(moment().format("YYYY").toString());
    for (let i = 0; i <= anioActual - 2017; i++) {
      this.listaanios.push(2017 + i);
    }
  }
  buscarPresupuesto() {
    this.spinner.show();
    this.query
      .getDatosApi(
        this.url.select_sancionados + `/${this.formBusquedaBiblio.value.anio}`
      )
      .subscribe({
        next: (data: any) => {
          this.listaGrl = data;
          this.listaSancionados = data;
        },
        complete: () => {
          this.spinner.hide();
        },
        error: () => {
          this.spinner.hide();
        },
      });
  }
  filtrar() {
    this.listaSancionados = this.listaGrl;
    this.listaSancionados = this.filtro.filtroData(
      this.listaSancionados,
      this.campoFiltro.cc,
      4
    );
    this.listaSancionados = this.filtro.filtroData(
      this.listaSancionados,
      this.campoFiltro.sf,
      1
    );
    this.listaSancionados = this.filtro.filtroData(
      this.listaSancionados,
      this.campoFiltro.dep,
      5
    );
    this.listaSancionados = this.filtro.filtroData(
      this.listaSancionados,
      this.campoFiltro.mn,
      2
    );
    this.listaSancionados = this.filtro.filtroData(
      this.listaSancionados,
      this.campoFiltro.usu,
      6
    );
    this.listaSancionados = this.filtro.filtroData(
      this.listaSancionados,
      this.campoFiltro.cant,
      3
    );
  }
}
