import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import * as moment from "moment";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { FiltrosService } from "src/app/global/services/filtros.service";
import { JsPDFService } from "src/app/global/services/jsPDF.service";
import { QueryService } from "src/app/servicios/query.services";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-rhh-ext",
  templateUrl: "./rhh-ext.component.html",
  styleUrls: ["./rhh-ext.component.scss"],
})
export class RhhExtComponent implements OnInit {
  formBusquedaBiblio: FormGroup;
  listaLocales: any = [];
  listaUsuarios: any = [];
  listaDatos: any = [];
  url = {
    select_datos: "/rhh_externo_datos",
    select_usuarios: "/rhh_externo",
  };
  iPageSize = environment.paginacion.tamPag;
  p = 1;
  p2 = 1;
  totalItems = 0;
  totalItems2 = 0;
  usuarioActual: any = null;
  constructor(
    private query: QueryService,
    private filtro: FiltrosService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private modal: NgbModal,
    private fb: FormBuilder,
    private jsPDF: JsPDFService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.formBusquedaBiblio = this.fb.group({
      campo: ["", [Validators.required, Validators.pattern("[0-9 A-Z a-z À-ÿ \u00f1 \u00d1]*")]],
    });
  }
  buscarUsuarios(pagina = 1) {
    this.p = pagina;
    this.listaUsuarios = [];
    this.spinner.show();
    this.query
      .saveDatosApi(this.url.select_usuarios, {
        campo: this.formBusquedaBiblio.value.campo,
        pagina: this.p,
        total: this.iPageSize,
      })
      .subscribe({
        next: (data: any) => {
          if (data.length > 0) {
            this.totalItems = data[0].iTotalRegistros;
            console.log(data, "manchas");
            this.listaUsuarios = data;
          }
        },
        complete: () => {
          this.spinner.hide();
        },
        error: () => {
          this.spinner.hide();
        },
      });
  }
  mostrarModal(modal, registro) {
    this.usuarioActual = registro;
    this.modal.open(modal, {
      backdrop: "static",
      keyboard: false,
      size: "lg",
    });
    this.buscarUsuariosDatos();
  }
  buscarUsuariosDatos(pagina = 1) {
    this.p2 = pagina;
    this.spinner.show();
    this.listaDatos = [];
    this.query
      .saveDatosApi(this.url.select_datos, {
        iPersId: this.usuarioActual.iPersId,
        pagina: this.p,
        total: this.iPageSize,
      })
      .subscribe({
        next: (data: any) => {
          if (data.length > 0) {
            this.totalItems2 = data[0].iTotalRegistros;
            console.log(data, "manchas");
            this.listaDatos = data;
          }
        },
        complete: () => {
          this.spinner.hide();
        },
        error: () => {
          this.spinner.hide();
        },
      });
  }
}
