import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { FiltrosService } from "src/app/global/services/filtros.service";
import { JsPDFService } from "src/app/global/services/jsPDF.service";
import { QueryService } from "src/app/servicios/query.services";

@Component({
  selector: "app-biblioteca-ext",
  templateUrl: "./biblioteca-ext.component.html",
  styleUrls: ["./biblioteca-ext.component.scss"],
})
export class BibliotecaExtComponent implements OnInit {
  formBusquedaBiblio: FormGroup;
  listaLocales: any = [];
  listaSancionados: any = [];
  url = {
    select_locales: "/biblioteca_locales",
    select_sancionados: "/biblioteca_sancionados",
  };
  constructor(
    private query: QueryService,
    private filtro: FiltrosService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private modal: NgbModal,
    private fb: FormBuilder,
    private jsPDF: JsPDFService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.formBusquedaBiblio = this.fb.group({
      local: ["", Validators.required],
    });
    this.cargarDatos();
  }
  cargarDatos() {
    this.spinner.show();
    this.query.getDatosApi(this.url.select_locales).subscribe({
      next: (data: any) => {
        this.listaLocales = data;
      },
      complete: () => {
        this.spinner.hide();
      },
    });
  }
  buscarSancionados() {
    this.spinner.show();
    this.query
      .getDatosApi(
        this.url.select_sancionados + `/${this.formBusquedaBiblio.value.local}`
      )
      .subscribe({
        next: (data: any) => {
          this.listaSancionados = data;
        },
        complete: () => {
          this.spinner.hide();
        },
      });
  }
}
