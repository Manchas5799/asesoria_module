import { RhhExtComponent } from './rhh-ext/rhh-ext.component';
import { PresupuestoExtComponent } from './presupuesto-ext/presupuesto-ext.component';
import { BibliotecaExtComponent } from './biblioteca-ext/biblioteca-ext.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrincipalCeComponent } from './principal-ce/principal-ce.component';
import { TramiteDocumentarioExtComponent } from './tramite-documentario-ext/tramite-documentario-ext.component';

const routes: Routes = [
  {
    path: '',
    component: PrincipalCeComponent
  },
  {
    path: 'tramite',
    component: TramiteDocumentarioExtComponent
  },
  {
    path: 'tramite/:estado',
    component: TramiteDocumentarioExtComponent
  },
  {
    path: "biblioteca",
    component: BibliotecaExtComponent
  },
  {
    path: "presupuesto",
    component: PresupuestoExtComponent
  },
  {
    path: "rhh",
    component: RhhExtComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsultasExternasRoutingModule { }
