import { environment } from "src/environments/environment";
import { startWith, map } from "rxjs/operators";
import { Observable } from "rxjs";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
  ViewChildren,
} from "@angular/core";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { FiltrosService } from "src/app/global/services/filtros.service";
import { JsPDFService } from "src/app/global/services/jsPDF.service";
import { QueryService } from "src/app/servicios/query.services";
import * as moment from "moment";
import { MatAutocomplete } from "@angular/material/autocomplete";

@Component({
  selector: "app-tramite-documentario-ext",
  templateUrl: "./tramite-documentario-ext.component.html",
  styleUrls: ["./tramite-documentario-ext.component.scss"],
})
export class TramiteDocumentarioExtComponent implements OnInit {
  @Input() tipo? = 1;
  @Output() salida = new EventEmitter<any>();
  formBusquedaTramites: FormGroup;
  // DATOS
  estadoBusqueda: number = 1;
  listaCriterios: any = [];
  listadoTotalDependencias: any = [];
  listadoDependencias: any = [];
  listadoTramites: any = [];
  listadoTramiteGrl: any = [];
  seguimientos: any = [];
  listadoDependenciasFiltrado: Observable<any>;
  dependencia = new FormControl();
  listaanios: any = [];
  tramiteActual: any = null;
  // RUTAS
  url = {
    select_datos: "/tramites_externos_base",
    select_tramites: "/tramites_externos",
    select_seguimiento: "/tramites_seguimiento",
  };
  // FILTRO
  campoFiltro = {
    registro: "",
    documento: "",
    asunto: "",
    emisor: "",
    tipo_documento: "",
    numero: "",
  };
  iPageSize = environment.paginacion.tamPag;
  p = 1;
  pS = 1;
  @ViewChild("auto", { static: false }) auto: MatAutocomplete;
  constructor(
    private query: QueryService,
    private filtro: FiltrosService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private modal: NgbModal,
    private fb: FormBuilder,
    private jsPDF: JsPDFService,
    private spinner: NgxSpinnerService
  ) {}
  seleccionarSalida(registro) {
    this.salida.emit(registro);
  }
  ngOnInit() {
    this.seleccionar();
    this.formBusquedaTramites = this.fb.group({
      dependencia: ["", Validators.required],
      criterio: [""],
      anio: [""],
      valor: [""],
    });
    this.listadoDependenciasFiltrado = this.dependencia.valueChanges.pipe(
      startWith(""),
      map((value) => this._filter(value))
    );
    let anioActual = parseInt(moment().format("YYYY").toString());
    for (let i = 0; i <= anioActual - 2017; i++) {
      this.listaanios.push(2017 + i);
    }
  }
  private _filter(value): any {
    console.log("valor", value);
    const filterValue = value; //.toLowerCase();
    if (typeof filterValue === "string") {
      if (filterValue === "") {
        this.formBusquedaTramites.value.dependencia = "";
      }
      return this.listadoDependencias.filter((option) =>
        option.cDepenNombre.toLowerCase().includes(filterValue.toLowerCase())
      );
    } else {
      return this.listadoDependencias;
    }
  }
  displayFn(dep?): string | undefined {
    return dep ? dep.cDepenNombre : undefined;
  }
  seleccionarDependencia(event) {
    let dependencia = event.option.value;
    this.formBusquedaTramites.patchValue({
      dependencia: dependencia.iDepenId,
    });
  }
  buscarTramites() {
    let data = this.formBusquedaTramites.value;
    this.spinner.show();
    this.query.saveDatosApi(this.url.select_tramites, data).subscribe({
      next: (data) => {
        this.listadoTramiteGrl = data;
        this.listadoTramites = data;
        this.spinner.hide("tramite");
      },
      error: () => {
        this.spinner.hide("tramite");
        this.toastr.warning("Revise los campos", "Advertencia");
      },
    });
  }
  filtrar() {
    this.listadoTramites = this.listadoTramiteGrl;
    this.listadoTramites = this.filtro.filtroData(
      this.listadoTramites,
      this.campoFiltro.numero,
      27
    );
    this.listadoTramites = this.filtro.filtroData(
      this.listadoTramites,
      this.campoFiltro.emisor,
      10
    );
    this.listadoTramites = this.filtro.filtroData(
      this.listadoTramites,
      this.campoFiltro.documento,
      29
    );
    this.listadoTramites = this.filtro.filtroData(
      this.listadoTramites,
      this.campoFiltro.asunto,
      33
    );
    this.listadoTramites = this.filtro.filtroData(
      this.listadoTramites,
      this.campoFiltro.tipo_documento,
      25
    );
  }
  seleccionar() {
    this.spinner.show();
    this.query.getDatosApi(this.url.select_datos).subscribe({
      next: (data) => {
        console.log(data);
        let datos: any = data;
        //this.rows = []
        this.listaCriterios = datos.criterios;
        // this.filtrarDependencias(this.listadoFiliales[0].id);
        this.listadoDependencias = datos.dependencias;
        // this.listadoDependenciasFiltrado = this.listadoDependencias;
        this.spinner.hide("tramite");
      },
      complete: () => {
        this.route.paramMap.subscribe((params: ParamMap) => {
          if (params.get("estado")) {
            this.asignarFormulario();
          }
        });
      },
      error: (error) => {
        this.toastr.warning(
          error["error"]["message"].substring(
            71,
            error["error"]["message"].indexOf("(SQL:")
          ),
          "Importante"
        );
        this.spinner.hide("tramite");
      },
    });
  }
  asignarFormulario() {
    const dependencia = this.listadoDependencias.find((dep) => {
      return dep.cDepenNombre == "OFICINA DE ASESORÍA LEGAL";
    });
    this.dependencia.setValue(dependencia);
    this.formBusquedaTramites.patchValue({
      dependencia: dependencia.iDepenId,
      criterio: 3,
      valor: "opin",
    });
    this.buscarTramites();
  }
  seguimiento(registro, modal) {
    this.spinner.show();
    this.tramiteActual = registro;
    this.seguimientos = [];
    this.query
      .getDatosApi(this.url.select_seguimiento + `/${registro.iTramId}`)
      .subscribe({
        next: (res) => {
          this.seguimientos = res;
          this.seguimientos.map((item) => {
            item.cEstadoTramiteNombre = item.cEstadoTramiteNombre.slice(2);
            let dS = "";
            if (item.dtTramMovFechaHoraRecibido !== null) {
              item.entrada = moment(item.dtTramMovFechaHoraEnvio)
                .format("LT")
                .toString();
              item.salida = moment(item.dtTramMovFechaHoraRecibido)
                .format("LT")
                .toString();
              const hI: any = moment(item.entrada, "HH:mm");
              const hS: any = moment(item.salida, "HH:mm");
              dS = moment
                .duration(hS - hI)
                .locale("es")
                .humanize();
              item.duracion = dS;
            }
          });
        },
        complete: () => {
          this.spinner.hide("tramite");
          this.modal.open(modal, {
            size: "lg",
            backdrop: "static",
            keyboard: false,
          });
        },
      });
  }
  imprimirSeguimiento() {
    this.spinner.show();
    this.prepararPdf();
    this.spinner.hide("tramite");
  }
  prepararPdf() {
    this.jsPDF.generaPDF(
      this.prepararPdfSeguimiento(),
      1,
      this.prepararcabeceraPdf()
    );
  }
  prepararPdfSeguimiento() {
    let listadoPDF: any = [];
    let index: number = 1;
    this.seguimientos.forEach((element) => {
      listadoPDF.push([
        index++,
        element.dtTramMovFechaHoraEnvio,
        element.cDepenEmisorNombre,
        element.dtTramMovFechaHoraRecibido,
        element.cDepenReceptorNombre,
        element.cTramDocumentoTramite,
        element.cTramAsunto,
        element.cEstadoTramiteNombre,
        element.duracion,
      ]);
    });
    let estilosColumnas = {
      0: { halign: "center" },
      1: { halign: "center" },
      2: { halign: "center" },
      3: { halign: "center" },
      4: { halign: "center" },
      5: { halign: "center" },
      6: { halign: "center" },
      7: { halign: "center", textalign: "center" },
      8: { halign: "center" },
      9: { halign: "center" },
    };
    let datosPDF = {
      orientacion: "l", //Un valor distinto a 'p' hará que sea horizontal (Se sugiere 'l')
      titulo:
        "TRAMITE DOCUMENTARIO REG. N° " +
        this.tramiteActual.cTramNumeroDocumento +
        " - " +
        this.tramiteActual.iTramYearRegistro,

      cabeceras: [
        [
          "#",
          "FECHA ENVÍO",
          "DEPENDENCIA ENVÍO",
          "FECHA RECIBÍDO",
          "DEPENDENCIA RECIBÍDO",
          "DOCUMENTO",
          "ASUNTO",
          "ESTADO",
          "TIEMPO",
        ],
      ],
      datos: listadoPDF,
      columnaStyles: estilosColumnas,
      headerStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
  prepararcabeceraPdf() {
    let listadoPDF: any = [];
    let asunto =
      this.tramiteActual.cTramAsuntoDocumento.substring(0, 130) + " ...";
    listadoPDF.push([this.tramiteActual.cTramDocumentoTramite, asunto]);
    let estilosColumnas = {
      0: { halign: "left" },
      1: { halign: "left" },
    };
    let datosPDF = {
      headers: [["DOCUMENTO", "ASUNTO"]],
      data: listadoPDF,
      columnaStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
}
