import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ConsultasExternasRoutingModule } from "./consultas-externas-routing.module";
import { PrincipalCeComponent } from "./principal-ce/principal-ce.component";
import { TramiteDocumentarioExtComponent } from "./tramite-documentario-ext/tramite-documentario-ext.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule } from "@angular/material/dialog";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatMenuModule } from "@angular/material/menu";
import { MatSelectModule } from "@angular/material/select";
import { MatStepperModule } from "@angular/material/stepper";
import { MatTooltipModule } from "@angular/material/tooltip";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { NgxPaginationModule } from "ngx-pagination";
import { NgxSpinnerModule } from "ngx-spinner";
import { GlobalModule } from "src/app/global/global.module";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { BibliotecaExtComponent } from './biblioteca-ext/biblioteca-ext.component';
import { PresupuestoExtComponent } from './presupuesto-ext/presupuesto-ext.component';
import { RhhExtComponent } from './rhh-ext/rhh-ext.component';

@NgModule({
  declarations: [PrincipalCeComponent, TramiteDocumentarioExtComponent, BibliotecaExtComponent, PresupuestoExtComponent, RhhExtComponent],
  imports: [
    CommonModule,
    ConsultasExternasRoutingModule,
    NgbModule,
    GlobalModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgxPaginationModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatStepperModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatTooltipModule,
    NgxSpinnerModule,
    MatAutocompleteModule,
  ],
  exports: [
    TramiteDocumentarioExtComponent
  ]
})
export class ConsultasExternasModule {}
