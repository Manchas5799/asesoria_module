import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportesRoutingModule } from './reportes-routing.module';
import { PrincipalReportesComponent } from './principal-reportes/principal-reportes.component';

@NgModule({
  declarations: [PrincipalReportesComponent],
  imports: [
    CommonModule,
    ReportesRoutingModule
  ]
})
export class ReportesModule { }
