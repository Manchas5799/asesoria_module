import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrincipalReportesComponent } from './principal-reportes/principal-reportes.component';

const routes: Routes = [
  {
    path: "",
    component: PrincipalReportesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportesRoutingModule { }
