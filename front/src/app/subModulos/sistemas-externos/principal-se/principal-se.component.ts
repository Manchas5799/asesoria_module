import { Component, OnInit } from '@angular/core';
import { QueryService } from 'src/app/servicios/query.services';

@Component({
  selector: 'app-principal-se',
  templateUrl: './principal-se.component.html',
  styleUrls: ['./principal-se.component.scss']
})
export class PrincipalSeComponent implements OnInit {
  listadoEnlaces: any = [];
  loading = false;
  rutas = {
    select_enlaces: "/enlaces"
  }
  constructor(
    private query: QueryService
  ) { }

  ngOnInit() {
    this.obtenerEnlaces();
  }
  obtenerEnlaces(){
    this.loading = true;
    this.query.getDatosApi(this.rutas.select_enlaces).subscribe(res => {
      this.listadoEnlaces = res;
      this.loading = false;
    })
  }

}
