import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SistemasExternosRoutingModule } from './sistemas-externos-routing.module';
import { PrincipalSeComponent } from './principal-se/principal-se.component';

@NgModule({
  declarations: [PrincipalSeComponent],
  imports: [
    CommonModule,
    SistemasExternosRoutingModule
  ]
})
export class SistemasExternosModule { }
