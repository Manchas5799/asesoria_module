import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PrincipalSeComponent } from "./principal-se/principal-se.component";

const routes: Routes = [
  {
    path: "",
    component: PrincipalSeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SistemasExternosRoutingModule {}
