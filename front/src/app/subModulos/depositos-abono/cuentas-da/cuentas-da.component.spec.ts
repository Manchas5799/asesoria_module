import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuentasDaComponent } from './cuentas-da.component';

describe('CuentasDaComponent', () => {
  let component: CuentasDaComponent;
  let fixture: ComponentFixture<CuentasDaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuentasDaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuentasDaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
