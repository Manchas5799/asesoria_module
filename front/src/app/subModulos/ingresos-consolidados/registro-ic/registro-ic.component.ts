import { Component, OnInit,  ViewChild, ElementRef,Input,Output, EventEmitter,ViewEncapsulation } from '@angular/core';
import {formatDate} from '@angular/common';
import { QueryService } from './../../../servicios/query.services'
import { GlobalService } from './../../../servicios/global.service'
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup ,Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import swal from 'sweetalert2'
import { LocalService } from './../../../servicios/local.services'
import { environment }  from '../../../../environments/environment'
import { isArray } from 'util';
declare var $: any;
@Component({
  selector: 'app-registro-ic',
  templateUrl: './registro-ic.component.html',
  styleUrls: ['./registro-ic.component.scss']
})
export class RegistroIcComponent implements OnInit {
/** input */
/** output */
/** listas */
listaIngresosConsolidados:any =[]
/**pagination */
pagination : boolean=true
rows = [];
iPageNumber=1
iPageSize=50
/** combos filtro */
comboTiposRecibosFiltro:any=[]
comboPeriodoFiltro:any=[]
comboMesFiltro:any=[]
comboCriterioFiltro:any=[]

comboDocumentoRecibo:any=[]
comboFiliales:any=[]
comboPeriodoEjecucionSIAF:any=[]
/** combos registro */
// comboTiposChequeras:any=[]
/** formularios */
formFiltroIngresosConsolidados: FormGroup
formIngresosConsolidados: FormGroup
/** modals */
modalIngresosConsolidados=null
/** secciones filtro */
sectionFiltroTipoDoc:boolean=true
sectionFiltroFecha:boolean=false
sectionFiltroPeriodoMes:boolean=false
sectionFiltroRangoFechas:boolean=false
sectionFiltroCriterio:boolean=false
/** varibales iniciales */
iOpcionFiltro=0
//iTipoRecIdFiltro=1 //primer filtro tipo recibo
iYearFiltro=""
iMonthFiltro=""
iYearCriterioFiltro=""
iCritIdFiltro=""
/** varibales globales */
iEntId:string
cSec_ejec:string

iCredEmisorId:string=""
typeReport:string='pdf'

Ano_eje_readonly:boolean=false
Ano_eje:string
/** check */
checks=[]
chkTodos:boolean = false
checksValidate = [];
/** seguros */
segModal:boolean = true
/** loading */
loading:boolean = false
load:boolean = false
/** select */
selectedRow : Number;
setClickedRow : Function;
  constructor(
    private query:QueryService,
    private global:GlobalService,
    private toastr: ToastrService,
    private local:LocalService,
    private modal:NgbModal,    
  ) {
    let data = this.local.getItem('userInfo')
    this.iCredEmisorId = data['iCredId']

    this.setClickedRow = function(index){
      this.selectedRow = index;
    }

    this.iEntId = environment.entidad.id
    this.cSec_ejec = environment.entidad.secFuncional

   }
  ngOnInit() {
    this.selPeriodosIC()
    this.selMesesIC()    
    this.selTiposRecibosIC()
    //this.selPeriodoEjecucionSIAFIC(this.cSec_ejec)
    /**auto-inicios de formularios*/
    this.formFiltroIngresosConsolidados = new FormGroup ({
      iOpcionFiltro:new FormControl(0),
      iTipoRecIdFiltro:new FormControl({value: 1, disabled: true}),
    })

    /**lista inicial */
    let data ={
      iOpcionFiltro:this.iOpcionFiltro,
      iEntId:this.iEntId,
      iTipoRecId:1,
      iPageNumber:this.iPageNumber,
      iPageSize:this.iPageSize,
    }
    this.selIngresosConsolidados(data)
    ///sessions/signin
  }

  filtIngresosConsolidados(pag){
    this.iPageNumber=pag
   let data = { 
     iOpcionFiltro:this.formFiltroIngresosConsolidados.value.iOpcionFiltro,
     iEntId:this.iEntId,
     iTipoRecId:this.formFiltroIngresosConsolidados.value.iTipoRecIdFiltro,
     cFecha:this.formFiltroIngresosConsolidados.value.cFechaFiltro,
     iYear:this.formFiltroIngresosConsolidados.value.iYearFiltro,
     iMonth:this.formFiltroIngresosConsolidados.value.iMonthFiltro,
     cFechaDesde:this.formFiltroIngresosConsolidados.value.cFechaDesdeFiltro,
     cFechaHasta:this.formFiltroIngresosConsolidados.value.cFechaHastaFiltro,

     iYearCriterio:this.formFiltroIngresosConsolidados.value.iYearCriterioFiltro,
     iCritId:this.formFiltroIngresosConsolidados.value.iCritIdFiltro,
     cCritVariable:this.formFiltroIngresosConsolidados.value.cCritVariableFiltro,
     iPageNumber:pag,
     iPageSize:this.iPageSize,
   }
   //console.log(this.iEntId)
   this.selIngresosConsolidados(data)
 }

  selIngresosConsolidados(data):void {
    /*
    this.chkTodos=false
    if(this.formFiltroIngresosConsolidados.valid)
    {
      this.load = true
      this.formFiltroIngresosConsolidados.disable()
      this.query.selIngresosConsolidados(data).subscribe(
        data => {
          this.rows = []
          if(data[0]){
            for (let index = 0; index < data[0]['iTotalRegistros']; index++) {
              this.rows.push({data:'1'})
            }
            /**adicionar datos a lista 
            for(let x in data){
                data[x]['check'] = false
            }
            this.listaIngresosConsolidados = data
          }else{
            this.listaIngresosConsolidados = []
          }
          this.load = false
          this.formFiltroIngresosConsolidados.enable()
        },
        error =>{
          this.formFiltroIngresosConsolidados.enable()
          this.global.errorEstatusServ(error)
          //this.toastr.warning('Estatus: '+resp['status']+' - '+resp['mensaje'],'Importante');      
          this.load = false
        }
      )
    }else{
      this.global.validateAllFormFields(this.formFiltroIngresosConsolidados) 
     // this.toastr.warning('Complete los campos requeridos','Importante');
    }
    */
  } 

  modalFormIngresosConsolidados(content,iRecId=''): void{
    /*
  this.selTiposDocumentoRecibo()
  this.selFilialesIC()
  if(iRecId == '')
  {
      this.Ano_eje_readonly=false
      this.modalIngresosConsolidados = this.modal.open(content, {size : "lg", backdrop: 'static', keyboard: false});
      this.formIngresosConsolidados = new FormGroup ({ 
        iRecId:new FormControl(''), 
        Ano_eje:new FormControl('',Validators.required), 
        Expediente:new FormControl(''),
        iRecNumero:new FormControl({value: '', disabled: true}),
        dRecFecha:new FormControl(formatDate(new Date(), 'yyyy-MM-dd', 'en'),Validators.required),
        iTipoDocId:new FormControl('',Validators.required),
        cRecNroDoc:new FormControl('',Validators.required),
        iFilId:new FormControl('',Validators.required),
        cRecObs:new FormControl('',Validators.required),
        nRecMonto:new FormControl('',Validators.required),
      }) 
    }else{
      this.Ano_eje_readonly=true
      if(this.segModal===true){
        this.segModal=false
        this.query.datIngresosConsolidados(iRecId).subscribe(
          data => {
            this.modalIngresosConsolidados = this.modal.open(content, {size : "lg", backdrop: 'static', keyboard: false});
              this.formIngresosConsolidados = new FormGroup ({   
                iRecId:new FormControl(data[0]['iRecId']), 
                Ano_eje:new FormControl({value: data[0]['Ano_eje'], disabled: true},Validators.required), 
                Expediente:new FormControl(data[0]['Expediente']),
                iRecNumero:new FormControl({value:data[0]['iRecNumero'], disabled: true}),
                dRecFecha:new FormControl(data[0]['dRecFecha'],Validators.required),
                iTipoDocId:new FormControl(data[0]['iTipoDocId'],Validators.required),
                cRecNroDoc:new FormControl(data[0]['cRecNroDoc'],Validators.required),
                iFilId:new FormControl(data[0]['iFilId'],Validators.required),
                cRecObs:new FormControl(data[0]['cRecObs'],Validators.required),
                nRecMonto:new FormControl(parseFloat(data[0]['nRecMonto']).toFixed(2),Validators.required),

              })  
              this.segModal=true
          },
          error =>{
            this.modalIngresosConsolidados.close()
            this.loading = false
            this.segModal=true
            this.global.errorEstatusServ(error)
          }
        )
      }else{
          this.toastr.warning('La petición aun se esta prosesando, espere','Importante'); 
      }
    }
    */
  }

  savIngresosConsolidados(){
    /*
    if(this.formIngresosConsolidados.valid)
    {
      this.formIngresosConsolidados.disable(); 
      this.loading=true
      var formData = { 
        iEntId:this.iEntId,
        iRecId:this.formIngresosConsolidados.value.iRecId,
        Ano_eje:this.formIngresosConsolidados.value.Ano_eje,
        Expediente:this.formIngresosConsolidados.value.Expediente,
        dRecFecha:this.formIngresosConsolidados.value.dRecFecha,
        iTipoDocId:this.formIngresosConsolidados.value.iTipoDocId,
        cRecNroDoc:this.formIngresosConsolidados.value.cRecNroDoc,
        iFilId:this.formIngresosConsolidados.value.iFilId,
        cRecObs:this.formIngresosConsolidados.value.cRecObs,
        nRecMonto:this.formIngresosConsolidados.value.nRecMonto,
      }; 
      this.query.savIngresosConsolidados(formData).subscribe(
        data => {
          if(data[0]['iResult']==1)
          {
            this.loading=false
            /**envia resultado Éxito a campo id 
            this.formIngresosConsolidados.controls.iRecId.setValue(data[0]['iRecId']);          
            this.formIngresosConsolidados.controls.iRecNumero.setValue(data[0]['iRecNumero']);          
            this.formIngresosConsolidados.enable(); 
            swal.fire({
              title: 'Guardado con Éxito',
              html: '<h3>Número Recibo: '+data[0]['iRecNumero']+'</h3><b>¿Desea cerrar el formulario?</b>',
              type: 'success',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'No, corregir',
              cancelButtonText: 'Si, cerrar',
              reverseButtons: true,
              focusCancel:true,
              customClass: {
                cancelButton: 'btn btn-primary btn-lg m-2',
                confirmButton: 'btn btn-secondary btn-lg m-2'
              },
              buttonsStyling: false
            }).then((result) => {
                if (!result.dismiss) {
              } else if (result.dismiss === swal.DismissReason.cancel) {
                this.modalIngresosConsolidados.close('Finish');  
              }
            })
              /**si el id existe se carga la pagina actual de lo contrario regresa a pagina inicial
              if(this.formIngresosConsolidados.value.iRecId)
              {
                this.filtIngresosConsolidados(this.iPageNumber)
              }else{
                this.filtIngresosConsolidados(1)            
              }
          }else{
            this.loading=false
            this.toastr.error('El procedimiento almacenado no devolvio la confirmación de insercción o actualización','Error');
          }
        },
        error => {
          this.loading=false
          this.formIngresosConsolidados.enable(); 
          this.global.errorEstatusServ(error)
        });
      }
      else{
        this.global.validateAllFormFields(this.formIngresosConsolidados)        
        this.toastr.warning('Complete los campos requeridos','Importante');
      }
      */
  }
  delIngresosConsolidados(list){  
    /* 
    swal.fire({
      title: '¿Continuar?',
      html: 'Se eliminará el registro COD:' + list[0].iRecId +'.<br> <b>¿Desea continuar?</b>',
      //input: 'textarea',
      inputPlaceholder: 'Observacion...',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
      focusConfirm:true,
      customClass: {
        confirmButton: 'btn btn-danger btn-lg m-2',
        cancelButton: 'btn btn-secondary btn-lg m-2'
      },
      buttonsStyling: false

    }).then((result) => {
        if (!result.dismiss) {
          let data = {
            'iRecId': list[0].iRecId
          }
          this.query.delIngresosConsolidados(data).subscribe(
            data => {
              if(data[0]['iResult'] == 1)
              {
                
                let timerInterval
                swal.fire({
                  title: 'Eliminado',
                  html: 'El Registro se elimino con Éxito',
                  timer: 2000,
                  type: 'success',
           
                  onClose: () => {
                    clearInterval(timerInterval)
                  }
                }).then((result) => {
                  /* Read more about handling dismissals below 
                  if (result.dismiss === swal.DismissReason.timer) {
                    console.log('I was closed by the timer')
                  }
                })
                // swal.fire(
                //   'Eliminado',
                //   'El registro fue eliminado con Éxito',
                //   'success'
                // )     
                
                //console.log(data)
                this.filtIngresosConsolidados(this.iPageNumber)
              }else{
                swal.fire(
                  'Cancelado',
                  'no se pudo eliminar',
                  'error'
                )               
              }
            }, 
          error => {
            swal.fire(
              'Cancelado',
              error['error']['message'].substring(71,error['error']['message'].indexOf("(SQL:")),
              'error'
            )
          });
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal.fire(
          'Cancelado',
          'la eliminación se cancelo con éxito',
          'error'
        )
      }
    })
    */
  }

  changeSectionFilter(iOpcionFiltro){
    this.listaIngresosConsolidados=[]
    this.rows = [];
    switch (iOpcionFiltro)
    {
      case '0'://todo
     
        this.sectionFiltroTipoDoc=true
        this.sectionFiltroFecha =false
        this.sectionFiltroPeriodoMes =false
        this.sectionFiltroRangoFechas =false
        this.sectionFiltroCriterio =false 
        this.formFiltroIngresosConsolidados = new FormGroup ({ 
          iOpcionFiltro:new FormControl(0),  
          iTipoRecIdFiltro:new FormControl(this.formFiltroIngresosConsolidados.value.iTipoRecIdFiltro),  
        })     
        this.pagination=true    
      break;      
      case '1'://fecha
      this.sectionFiltroTipoDoc=true
        this.sectionFiltroFecha =true
        this.sectionFiltroPeriodoMes =false
        this.sectionFiltroRangoFechas =false
        this.sectionFiltroCriterio =false
        this.formFiltroIngresosConsolidados = new FormGroup ({ 
          iOpcionFiltro:new FormControl(1),
          cFechaFiltro:new FormControl(formatDate(new Date(), 'yyyy-MM-dd', 'en'),Validators.required),
          iTipoRecIdFiltro:new FormControl(this.formFiltroIngresosConsolidados.value.iTipoRecIdFiltro),  

        })    
        this.pagination=false            
      break;
      case '2':
 


        this.sectionFiltroTipoDoc=true
        this.sectionFiltroFecha =false
        this.sectionFiltroPeriodoMes =true
        this.sectionFiltroRangoFechas =false
        this.sectionFiltroCriterio =false
        this.formFiltroIngresosConsolidados = new FormGroup ({ 
          iOpcionFiltro:new FormControl(2),
          iYearFiltro:new FormControl('',Validators.required),
          iMonthFiltro:new FormControl('',Validators.required),   
          iTipoRecIdFiltro:new FormControl(this.formFiltroIngresosConsolidados.value.iTipoRecIdFiltro),  
        })        
        this.pagination=false                    
      break;
      case '3':
        this.sectionFiltroTipoDoc=true
        this.sectionFiltroFecha =false
        this.sectionFiltroPeriodoMes =false
        this.sectionFiltroRangoFechas =true
        this.sectionFiltroCriterio =false 
        this.formFiltroIngresosConsolidados = new FormGroup ({ 
          iOpcionFiltro:new FormControl(3),
          cFechaDesdeFiltro:new FormControl(formatDate(new Date(), 'yyyy-MM-dd', 'en'),Validators.required),
          cFechaHastaFiltro:new FormControl(formatDate(new Date(), 'yyyy-MM-dd', 'en'),Validators.required),    
          iTipoRecIdFiltro:new FormControl(this.formFiltroIngresosConsolidados.value.iTipoRecIdFiltro),  
        })         
        this.pagination=false                    
      break;
      case '4':
        this.selCriterioIC() 
        this.sectionFiltroTipoDoc=true
        this.sectionFiltroFecha =false
        this.sectionFiltroPeriodoMes =false
        this.sectionFiltroRangoFechas =false
        this.sectionFiltroCriterio =true   
        this.formFiltroIngresosConsolidados = new FormGroup ({ 
          iOpcionFiltro:new FormControl(4),
          iYearCriterioFiltro:new FormControl('',Validators.required),
          iCritIdFiltro:new FormControl('',Validators.required),
          cCritVariableFiltro:new FormControl('',Validators.required),    
          iTipoRecIdFiltro:new FormControl(this.formFiltroIngresosConsolidados.value.iTipoRecIdFiltro),       
        })  
        this.pagination=false            
      break;      
      default:
        this.sectionFiltroTipoDoc=false
        this.sectionFiltroFecha =false
        this.sectionFiltroPeriodoMes =false
        this.sectionFiltroRangoFechas =false
        this.sectionFiltroCriterio =false 
        this.pagination=false            
      break;
    }
  }

  selTiposRecibosIC(){
    /*
    this.query.selTiposRecibosIC().subscribe(
      data => {
          this.comboTiposRecibosFiltro = data
      },
      error =>{
        this.global.errorEstatusServ(error)
        //this.toastr.warning('Estatus: '+resp['status']+' - '+resp['mensaje'],'Importante');          
      }
    )   
    */
  }
  selPeriodoEjecucionSIAFIC(cSec_ejec){
    /*
    this.query.selPeriodoEjecucionSIAFIC(cSec_ejec).subscribe(
      data => {
          this.comboPeriodoEjecucionSIAF = data
          if(data[0]){
            this.Ano_eje = data[0]['Codigo'];
          } 

        
          
      },
      error =>{
        this.global.errorEstatusServ(error)
        //this.toastr.warning('Estatus: '+resp['status']+' - '+resp['mensaje'],'Importante');          
      }
    )   
    */
  }


  

  selPeriodosIC(){
    /*
    this.query.selPeriodosIC().subscribe(
      data => {
          this.comboPeriodoFiltro = data
          this.comboPeriodoEjecucionSIAF = data
          if(data[0]){
            //para filtro
            this.iYearFiltro = data[0]['iYearId'];
            //para criterio
            this.iYearCriterioFiltro = data[0]['iYearId'];
            //ppara formulario
            this.Ano_eje = data[0]['iYearId'];
          }  
          
      },
      error =>{
        this.global.errorEstatusServ(error)
        //this.toastr.warning('Estatus: '+resp['status']+' - '+resp['mensaje'],'Importante');          
      }
    )   
    */
  }

  selMesesIC(){
    /*
    this.query.selMesesIC().subscribe(
      data => {
          this.comboMesFiltro = data
      },
      error =>{
        this.global.errorEstatusServ(error)
        // this.toastr.warning('Estatus: '+resp['status']+' - '+resp['mensaje'],'Importante');          
      }
    )  
    */
  }
  selCriterioIC():void{
    /*
    this.query.selCriterioIC().subscribe(
      data => {
          this.comboCriterioFiltro = data
      },
      error =>{
        this.global.errorEstatusServ(error)
        // this.toastr.warning('Estatus: '+resp['status']+' - '+resp['mensaje'],'Importante');          
      }
    )  
    */
  }

  selTiposDocumentoRecibo():void{
    /*
    this.query.selTiposDocumentoRecibo().subscribe(
      data => {
          this.comboDocumentoRecibo = data
      },
      error =>{
        this.global.errorEstatusServ(error)
        // this.toastr.warning('Estatus: '+resp['status']+' - '+resp['mensaje'],'Importante');          
      }
    )  
    */
  }
  selFilialesIC():void{
    /*
    this.query.selFilialesIC().subscribe(
      data => {
          this.comboFiliales = data
      },
      error =>{
        this.global.errorEstatusServ(error)
        // this.toastr.warning('Estatus: '+resp['status']+' - '+resp['mensaje'],'Importante');          
      }
    )  
    */
  }
  checkall():void{
    for( let x in this.listaIngresosConsolidados){
      if(this.chkTodos){
        this.listaIngresosConsolidados[x].check = true
      }else{
        this.listaIngresosConsolidados[x].check = false
      }
      
    }
  }

  generateReport(){
    this.checksValidate=[]
    for( let x in this.listaIngresosConsolidados){
      if(this.listaIngresosConsolidados[x].check){
        this.checksValidate.push(this.listaIngresosConsolidados[x].iRecId)
      }
    }
    if(this.checksValidate.length>0){
      let modulo = 'tesoreria'
      let typeReport = this.typeReport
      let nameReport = 'ingresosConsolidados_Registro'
      let id = ''
      let ids = this.checksValidate
      let parameters ={
        p1:'golalllsdsd',
        p2:'',
        p3:''
      }
     this.global.rptPDFCrystal(modulo,nameReport,typeReport,id,ids,parameters)
    }
    else{
        swal.fire(
          'Advertencia',
          'Primero marque los registros de los cuales desea generar el reporte',
          'warning'
        ) 
    }
  }
 
}