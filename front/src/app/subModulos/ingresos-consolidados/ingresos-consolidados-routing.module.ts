import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { TagInputModule } from 'ngx-chips';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { ReactiveFormsModule } from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ToastrModule } from 'ngx-toastr';


import { GlobalModule } from './../../global/global.module'
import { SharedComponentsModule } from './../../shared/components/shared-components.module';


import { RegistroIcComponent } from './registro-ic/registro-ic.component';
import { PrincipalIcComponent } from './principal-ic/principal-ic.component';

const routes: Routes = [  {
  path: 'principal',
  component: PrincipalIcComponent,
},];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    NgbModule.forRoot(),
    ScrollToModule.forRoot(),
    ToastrModule.forRoot(),
    CommonModule, 
    TagInputModule,
    CKEditorModule,
    GlobalModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgxPaginationModule,
    SharedComponentsModule,
    FormsModule,
  ],
  exports: [RouterModule],
  declarations: [
    PrincipalIcComponent,RegistroIcComponent]
})
export class IngresosConsolidadosRoutingModule { }
