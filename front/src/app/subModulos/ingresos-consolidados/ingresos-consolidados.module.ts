import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxPaginationModule } from 'ngx-pagination';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from './../../shared/shared.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { IngresosConsolidadosRoutingModule } from './ingresos-consolidados-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    IngresosConsolidadosRoutingModule,
    ReactiveFormsModule,
    SharedComponentsModule,
    NgxDatatableModule,
    SharedModule,
    NgxPaginationModule,
    NgbModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'})    
  ]
})
export class IngresosConsolidadosModule { }
