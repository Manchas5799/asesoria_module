import { LocalService } from './../../../servicios/local.services';
import { NotificacionesModalComponent } from "./../notificaciones-modal/notificaciones-modal.component";
import { Component, OnInit, ViewChild, OnChanges } from "@angular/core";
import { Subject, forkJoin } from "rxjs";
import { CalendarAppEvent } from "src/app/shared/models/calendar-event.model";
import {
  CalendarEventAction,
  CalendarEvent,
  CalendarEventTimesChangedEvent,
} from "angular-calendar";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { CalendarAppService } from "src/app/views/calendar/calendar-app.service";
import { Utils } from "src/app/shared/utils";
import {
  isSameMonth,
  isSameDay,
  addHours,
  subDays,
  addDays,
  subWeeks,
  startOfMonth,
  endOfMonth,
} from "date-fns";
import { CalendarioCitacionesModalComponent } from "../calendario-citaciones-modal/calendario-citaciones-modal.component";
import { NgxSpinnerService } from "ngx-spinner";
import { JsPDFService } from "src/app/global/services/jsPDF.service";
import { FiltrosService } from "src/app/global/services/filtros.service";
import { QueryService } from "src/app/servicios/query.services";
import { ToastrService } from "ngx-toastr";
import * as moment from "moment";
import Swal from "sweetalert2";
import { ActivatedRoute, ParamMap } from "@angular/router";

@Component({
  selector: "app-calendario-citaciones",
  templateUrl: "./calendario-citaciones.component.html",
  styleUrls: ["./calendario-citaciones.component.scss"],
})
export class CalendarioCitacionesComponent implements OnInit, OnChanges {
  public view = "week";
  public viewDate = new Date();
  @ViewChild("eventDeleteConfirm", { static: true }) eventDeleteConfirm;
  public activeDayIsOpen = true;
  public refresh: Subject<any> = new Subject();
  public events: CalendarAppEvent[] = [];
  public pendientes: CalendarAppEvent[] = [];
  private actions: CalendarEventAction[];
  private actionsNotificaciones: CalendarEventAction[];
  titulo = "CITACIONES";
  /** Tipo de reporte */
  typeReport: string = "PDF";
  pagReport: number = 1;
  //   RUTAS
  rutas = {
    select_personas: "/personas_asesoria",
    select_actos: "/acto_procesal",
    select_tipos: "/tipo_citacion",
    select_etapas: "/etapas",
    select_notificaciones: "/expedientes_notificaciones_lista",
    select_pendientes: "/pendientes_lista",
    save_pendientes: "/pendientes_save",
    save_notificaciones: "/expedientes_notificaciones",
    delete: "/expedientes_notificaciones_del",
  };
  // grupos
  tipos: any = [];
  actos: any = [];
  personas: any = [];
  expedientesTotal: any = [];
  expedientesFiltrado: any = [];
  loading: boolean = false;
  fechaStart: string;
  fechaEnd: string;
  constructor(
    private modalService: NgbModal,
    private calendarService: CalendarAppService,
    private spinner: NgxSpinnerService,
    private jsPDF: JsPDFService,
    private filtro: FiltrosService,
    private query: QueryService,
    private toast: ToastrService,
    private route: ActivatedRoute,
    private localService: LocalService
  ) {
    this.actions = [
      {
        label: '<i class="i-Edit m-1 text-edit-label" ></i>',
        onClick: ({ event }: { event: CalendarEvent }): void => {
          this.handleEvent("editar", event);
        },
      },
      {
        label: '<i class="i-Close m-1 text-danger"></i>',
        onClick: ({ event }: { event: CalendarEvent }): void => {
          this.removeEvent(event);
        },
      },
    ];
    this.actionsNotificaciones = [
      {
        label: '<i class="i-Edit m-1 text-edit-label" ></i>',
        onClick: ({ event }: { event: CalendarEvent }): void => {
          this.editarNotificacion("editar", event);
        },
      },
      {
        label: '<i class="i-Close m-1 text-danger"></i>',
        onClick: ({ event }: { event: CalendarEvent }): void => {
          this.eliminarNotificacion(event);
        },
      },
    ];
  }

  ngOnInit() {
    this.cambioFecha();
    // this.loadEvents();
    this.cargarDatos();
  }
  mostrarMisCitaciones(modal) {
    this.modalService.open(modal, {
      backdrop: "static",
      keyboard: false,
      size: "lg",
    });
  }
  ngOnChanges(cambios) {
    console.log(cambios);
  }
  cambioFecha() {
    console.log(this.viewDate);
    console.log(this.view);
    let fechaInicial, fechaFinal;
    if (this.view === "week") {
      let posDay = this.viewDate.getDay();
      fechaInicial = subDays(this.viewDate, posDay);
      fechaFinal = addDays(fechaInicial, 6);
    } else if (this.view === "month") {
      // let posDay = this.viewDate.getDay();
      fechaInicial = startOfMonth(this.viewDate);
      fechaFinal = endOfMonth(this.viewDate);
    } else {
      fechaInicial = this.viewDate;
      fechaFinal = this.viewDate;
    }
    this.fechaStart = moment(fechaInicial).format("YYYY-MM-DD").toString();
    this.fechaEnd = moment(fechaFinal).format("YYYY-MM-DD").toString();
    console.log(
      fechaInicial.getDate(),
      fechaInicial.getMonth(),
      fechaInicial.getFullYear()
    );
    console.log(
      fechaFinal.getDate(),
      fechaFinal.getMonth(),
      fechaFinal.getFullYear()
    );
    this.loadEvents(
      moment(fechaInicial).format("YYYY-MM-DD").toString(),
      moment(fechaFinal).format("YYYY-MM-DD").toString()
    );
    this.loadPendientes(
      moment(fechaInicial).format("YYYY-MM-DD").toString(),
      moment(fechaFinal).format("YYYY-MM-DD").toString()
    );
    // return { fechaInicial, fechaFinal };
  }
  // cargarFechas
  cargarDatos() {
    this.loading = true;
    forkJoin({
      personas: this.query.getDatosApi(this.rutas.select_personas),
      actos: this.query.getDatosApi(this.rutas.select_actos),
      tipos: this.query.getDatosApi(this.rutas.select_tipos),
    }).subscribe({
      next: (res) => {
        this.tipos = res.tipos;
        this.personas = res.personas;
        this.actos = res.actos;
      },
      complete: () => {
        //    FIN SPINNER
        this.loading = false;
        this.route.paramMap.subscribe((parametros: ParamMap) => {
          let tipoTarget = parametros.get("expediente");
          if (tipoTarget === "C") {
            this.addEvent();
          } else if (tipoTarget === "T") {
            this.addENotificacion();
          }
        });
      },
      error: () => {
        this.loading = false;
      }
    });
  }
  private initEvents(events): CalendarAppEvent[] {
    return events.map((event) => {
      event.actions = this.actions;
      event.start = event.fecha_citacion;
      event.end = addHours(event.start, 1);
      event._id = event.citacion_id;
      event.title = event.citacion_numero;
      event.persona_asesoria_id = event.iPersonaAsesoriaId;
      event.expediente_etapa = event.etapa_id;
      event.citacion_sumilla = event.sumilla;
      event.citacion_lugar = event.lugar_citacion;
      event.citacion_resolucion = event.resolucion;
      return new CalendarAppEvent(event);
    });
  }
  private initPendientes(pendientes): CalendarAppEvent[] {
    return pendientes.map((event) => {
      event.actions = this.actionsNotificaciones;
      event.start = event.fecha_limite;
      event.recepcion = event.fecha_realizado;
      event.end = addHours(event.start, 1);
      event.color = {
        primary: "#000",
        secondary: "#F3FA90",
      };
      event._id = event.pendiente_id;
      event.title = event.descripcion;
      event.involucrado = event.encargado;
      event.persona_asesoria_id = event.encargado[0].p_ase_id;
      event.citacion_sumilla = event.descripcion;

      return new CalendarAppEvent(event);
    });
  }

  public loadEvents(fechaInicial = null, fechaFinal = null) {
    if (fechaFinal === null) {
      fechaInicial = this.fechaStart;
      fechaFinal = this.fechaEnd;
    }
    this.query
      .getDatosPostApi(this.rutas.select_notificaciones, {
        fechaInicial,
        fechaFinal,
      })
      .subscribe((events) => {
        console.log(events);
        this.events = this.initEvents(events);
        this.refresh.next(true);
        // events.map
        // let eventos: any = events;
        // this.events = eventos;
      });
    // this.calendarService.getEvents().subscribe((events: CalendarEvent[]) => {
    //   this.events = this.initEvents(events);
    // });
  }
  public loadPendientes(fechaInicial = null, fechaFinal = null) {
    if (fechaFinal === null) {
      fechaInicial = this.fechaStart;
      fechaFinal = this.fechaEnd;
    }
    this.query
      .getDatosPostApi(this.rutas.select_pendientes, {
        fechaInicial,
        fechaFinal,
      })
      .subscribe((events) => {
        console.log(events);
        this.pendientes = this.initPendientes(events);
        console.log(this.pendientes);
        this.refresh.next(true);
        // events.map
        // let eventos: any = events;
        // this.events = eventos;
      });
  }
  public removeEvent(event) {
    this.modalService
      .open(this.eventDeleteConfirm, {
        ariaLabelledBy: "modal-basic-title",
        centered: true,
      })
      .result.then(
        (result) => {
          let idE = event._id;
          this.query.delDatoApi(this.rutas.delete, idE).subscribe(
            (data) => {
              let info: any = data;
              if (!info.error) {
                Swal.fire("Eliminado", info.msg, "success");
                this.loadEvents();
                this.refresh.next(true);
              } else {
                Swal.fire("Cancelado", info.msg, "error");
              }
            },
            (error) => {
              Swal.fire("Cancelado", error.error.msg, "error");
            }
          );
        },
        (reason) => {}
      );
  }
  public eliminarNotificacion(event) {
    this.modalService
      .open(this.eventDeleteConfirm, {
        ariaLabelledBy: "modal-basic-title",
        centered: true,
      })
      .result.then(
        (result) => {
          let idE = event._id;
          this.query.delDatoApi(this.rutas.delete, idE).subscribe(
            (data) => {
              let info: any = data;
              if (!info.error) {
                Swal.fire("Eliminado", info.msg, "success");
                this.loadEvents();
                this.loadPendientes();
                this.refresh.next(true);
              } else {
                Swal.fire("Cancelado", info.msg, "error");
              }
            },
            (error) => {
              Swal.fire("Cancelado", error.error.msg, "error");
            }
          );
        },
        (reason) => {}
      );
  }

  public addEvent() {
    const dialogRef = this.modalService.open(
      CalendarioCitacionesModalComponent,
      { centered: true, size: "lg", backdrop: "static", keyboard: false }
    );
    dialogRef.componentInstance.data = {
      action: "add",
      date: new Date(),
      personas: this.personas,
      actos: this.actos,
      tipos: this.tipos,
    };
    dialogRef.result
      .then((res) => {
        // console.log(res.event);
        this.localService.setItem('expediente_buscado',null);
        if (!res) {
          return;
        }
        const dialogAction = res.action;
        const responseEvent = res.event;
        // responseEvent.start = Utils.ngbDateToDate(responseEvent.start);
        responseEvent.end = addHours(responseEvent.start, 1);
        // this.calendarService.addEvent(responseEvent).subscribe((events) => {
        //   this.events = this.initEvents(events);
        //   this.refresh.next(true);
        //   console.log(this.events);
        // });
        this.guardarEvento(responseEvent);
      })
      .catch((e) => {
        console.log(e);
      });
  }
  public addENotificacion() {
    const dialogRef = this.modalService.open(NotificacionesModalComponent, {
      centered: true,
      size: "lg",
      backdrop: "static",
      keyboard: false,
    });
    dialogRef.componentInstance.data = {
      action: "add",
      date: new Date(),
      personas: this.personas,
    };
    dialogRef.result
      .then((res) => {
        // console.log(res.event);
        this.localService.setItem('expediente_buscado',null);
        if (!res) {
          return;
        }
        const dialogAction = res.action;
        const responseEvent = res.event;
        responseEvent.end = addHours(responseEvent.start, 1);
        this.guardarPendiente(responseEvent);
      })
      .catch((e) => {
        console.log(e);
      });
  }
  private guardarEvento(evento) {
    const event = {
      id: evento._id,
      expediente: evento.expediente_id,
      acto: evento.acto_id,
      tipo: evento.tipo_id,
      numero: evento.title,
      emision: null, // evento.fecha_emision
      recibido: evento.recepcion,
      citacion: evento.start,
      sumilla: evento.citacion_sumilla,
      lugar: evento.citacion_lugar,
      referencia: evento.citacion_referencia,
      resolucion: evento.citacion_resolucion,
      resolucion_fecha: evento.resolucion_fecha,
      observacion: evento.citacion_observacion,
      fechaobs: null,
      persona: evento.persona_asesoria_id,
      codtramite: "",
      etapa: evento.expediente_etapa,
    };
    this.query.saveDatosApi(this.rutas.save_notificaciones, event).subscribe(
      (res) => {
        this.events.push(evento);
        this.loadEvents();
        this.toast.success("REGISTRO AGREGADO", "CITACIONES");
      },
      (err) => {
        this.toast.error(err.msg, "Problema al guardar");
      }
    );
  }
  private guardarPendiente(evento) {
    const event = {
      id: evento._id,
      expediente: evento.expediente_id,
      recibido: evento.recepcion,
      citacion: evento.start,
      estado: evento.estado,
      sumilla: evento.citacion_sumilla,
      observacion: evento.citacion_observacion,
      persona: evento.persona_asesoria_id,
    };
    this.query.saveDatosApi(this.rutas.save_pendientes, event).subscribe(
      (res) => {
        // this.events.push(evento);
        this.loadPendientes();
        this.toast.success("REGISTRO AGREGADO", "Notififación");
      },
      (err) => {
        this.toast.error(err.msg, "Problema al guardar");
      }
    );
  }
  public handleEvent(action: string, event: CalendarAppEvent): void {
    const dialogRef = this.modalService.open(
      CalendarioCitacionesModalComponent,
      { centered: true, size: "lg", backdrop: "static", keyboard: false }
    );
    dialogRef.componentInstance.data = {
      event,
      action,
      personas: this.personas,
      actos: this.actos,
      tipos: this.tipos,
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }
        const dialogAction = res.action;
        const responseEvent = res.event;
        // responseEvent.start = Utils.ngbDateToDate(responseEvent.start);
        responseEvent.end = addHours(responseEvent.start, 1);
        this.guardarEvento(responseEvent);
      })
      .catch((e) => {
        console.log(e);
      });
  }
  public editarNotificacion(action: string, event: CalendarAppEvent): void {
    const dialogRef = this.modalService.open(NotificacionesModalComponent, {
      centered: true,
      size: "lg",
      backdrop: "static",
      keyboard: false,
    });
    dialogRef.componentInstance.data = {
      event,
      action,
      personas: this.personas,
    };
    dialogRef.result
      .then((res) => {
        if (!res) {
          return;
        }
        const dialogAction = res.action;
        const responseEvent = res.event;
        // responseEvent.start = Utils.ngbDateToDate(responseEvent.start);
        responseEvent.end = addHours(responseEvent.start, 1);
        this.guardarPendiente(responseEvent);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  public dayClicked({
    date,
    events,
  }: {
    date: Date;
    events: CalendarEvent[];
  }): void {
    console.log(date, events);
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  public eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;

    this.calendarService.updateEvent(event).subscribe((events) => {
      this.events = this.initEvents(events);
      this.refresh.next();
    });
  }
  downloadReport() {
    // this.loading = true;
    switch (this.typeReport) {
      case "WORD":
        console.log("Descarga WORD");
        break;
      case "EXCEL":
        console.log("Descarga EXCEL");
        break;
      default:
        // Por defecto PDF
        console.log("Descarga PDF");
        this.downloadPDF();
        break;
    }
    // this.loading = false;
  }
  downloadPDF() {
    this.jsPDF.generaPDF(this.prepararPDF(), 1);
  }
  private prepararPDF() {
    // Verificar this.listado
    let listadoPDF: any = [];
    let index: number = 1;
    this.events.forEach((element) => {
      listadoPDF.push([
        element.citacion_numero,
        element.expediente_nombre,
        element.citacion_lugar,
        element.citacion_sumilla,
        moment(element.fecha_recibido).format("YYYY-MM-DD").toString(),
        moment(element.start).format("YYYY-MM-DD hh:mm a").toString(),
        // element.nombre_tipo_expediente,
        // element.demandado,
        // element.demandante,
        // element.etapa[0].nombre_etapa,
        // element.etapa[0].fecha_etapa,
      ]);
    });
    let estilosColumnas = {
      0: { halign: "center" },
      1: { halign: "center" },
      2: { halign: "center" },
      3: { halign: "center" },
      4: { halign: "center" },
      5: { halign: "center" },
      6: { halign: "center" },
    };
    let datosPDF = {
      orientacion: "l", //Un valor distinto a 'p' hará que sea horizontal (Se sugiere 'l')
      titulo: this.titulo,
      cabeceras: [
        [
          "NÚMERO",
          "EXPEDIENTE",
          "LUGAR",
          "SUMILLA",
          "RECIBIDO",
          "NOTIFICACIÓN",
        ],
      ],
      datos: listadoPDF,
      columnaStyles: estilosColumnas,
      headerStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
}
