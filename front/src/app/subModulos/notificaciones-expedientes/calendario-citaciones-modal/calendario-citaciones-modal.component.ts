import { LocalService } from "./../../../servicios/local.services";
import { Component, OnInit } from "@angular/core";
import { CalendarEvent } from "angular-calendar";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { NgbActiveModal, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { CalendarAppEvent } from "src/app/shared/models/calendar-event.model";
import { Utils } from "src/app/shared/utils";
import * as moment from "moment";
import { FiltrosService } from "src/app/global/services/filtros.service";
import { QueryService } from "src/app/servicios/query.services";
import { ActivatedRoute, ParamMap } from "@angular/router";

interface DialogData {
  event?: CalendarEvent;
  action?: string;
  date?: Date;
  personas?: any;
  tipos?: any;
  actos?: any;
}

@Component({
  selector: "app-calendario-citaciones-modal",
  templateUrl: "./calendario-citaciones-modal.component.html",
  styleUrls: ["./calendario-citaciones-modal.component.scss"],
})
export class CalendarioCitacionesModalComponent implements OnInit {
  msgBuscando: String = "Buscando..";
  loading: boolean = false;
  data: DialogData;
  event: CalendarEvent;
  personas: any = [];
  tipos: any = [];
  actos: any = [];
  expedientes: any = [];
  expedientesFiltrados: any = [];
  dialogTitle: string = "";
  eventForm: FormGroup;
  action: string;
  campoFiltro = {
    nombre: "",
    numero: "",
  };
  rutas = {
    select_expedientes: "/expedientes_nombre",
  };
  pE: number = 1;
  iPageSize = 5;
  formBuscarExpediente: FormGroup;
  constructor(
    private query: QueryService,
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    public modalService: NgbModal,
    private filtro: FiltrosService,
    private modal: NgbModal,
    private route: ActivatedRoute,
    private LocalService: LocalService
  ) {}

  ngOnInit() {
    setTimeout(() => {
      console.log(this.data);
      if (this.data.action === "editar") {
        this.dialogTitle = this.data.event.title;
        this.event = new CalendarAppEvent(this.data.event);
      } else {
        this.dialogTitle = "AGREGAR NUEVA CITACIÓN";
        this.event = new CalendarAppEvent(this.data.event);
      }
      this.eventForm = this.buildEventForm(this.data.event);
      const expedienteActual = this.LocalService.getItem("expediente_buscado");
      console.log(expedienteActual);
      if (expedienteActual !== null) {
        this.eventForm.controls["expediente_id"].setValue(expedienteActual.id);
        this.eventForm.controls["expediente_nombre"].setValue(
          expedienteActual.nombre
        );
        this.eventForm.controls["citacion_lugar"].setValue(
          expedienteActual.nombre_sala
        );
        this.eventForm.controls["expediente_etapa"].setValue(
          expedienteActual.etapa_actual
        );
      }
    }, 100);
    this.eventForm = this.buildEventForm(this.data.event);
    this.actos = this.data.actos;
    this.personas = this.data.personas;
    this.tipos = this.data.tipos;
    this.formBuscarExpediente = this.formBuilder.group({
      buscar: ["", Validators.required],
    });
  }
  filtrarExpediente() {
    this.expedientesFiltrados = this.expedientes;
    // console.table(this.listado);
    this.expedientesFiltrados = this.filtro.filtroData(
      this.expedientesFiltrados,
      this.campoFiltro.numero,
      7
    );
    this.expedientesFiltrados = this.filtro.filtroData(
      this.expedientesFiltrados,
      this.campoFiltro.nombre,
      5
    );
  }
  buscarExpediente() {
    this.loading = true;
    this.query
      .getDatosPostApi(this.rutas.select_expedientes, {
        campo: this.formBuscarExpediente.get("buscar").value,
      })
      .subscribe((res) => {
        this.loading = false;
        this.expedientes = res;
        this.expedientesFiltrados = this.expedientes;
      });
  }
  buildEventForm(
    event: CalendarAppEvent = {
      start: null,
      title: null,
      color: { primary: "", secondary: "" },
      // meta: { location: "", notes: "" },
      persona_asesoria_id: null,
      acto_id: null,
      tipo_id: null,
      expediente_id: null,
      citacion_sumilla: "",
      citacion_lugar: "",
      citacion_numero: "",
      citacion_observacion: "",
      citacion_referencia: "",
      citacion_resolucion: "",
      resolucion_fecha: null,
      expediente_nombre: "",
    }
  ) {
    return new FormGroup({
      _id: new FormControl(event._id),
      title: new FormControl(event.title, Validators.required),
      start: new FormControl(
        event.start
          ? moment(event.start).format("YYYY-MM-DD[T]HH:mm")
          : moment().format("YYYY-MM-DD[T]HH:mm"),
        Validators.required
      ),
      end: new FormControl(
        event.end
          ? moment(event.end).format("YYYY-MM-DD")
          : moment().format("YYYY-MM-DD")
      ),
      recepcion: new FormControl(
        event.recepcion
          ? moment(event.recepcion).format("YYYY-MM-DD")
          : moment().format("YYYY-MM-DD")
      ),
      allDay: new FormControl(event.allDay),
      // color: this.formBuilder.group({
      //   primary: new FormControl(event.color.primary),
      //   secondary: new FormControl(event.color.secondary),
      // }),
      // meta: this.formBuilder.group({
      //   location: new FormControl(event.meta.location),
      //   notes: new FormControl(event.meta.notes),
      // }),
      persona_asesoria_id: new FormControl(
        event.persona_asesoria_id,
        Validators.required
      ),
      expediente_nombre: new FormControl(
        { disabled: true, value: event.expediente_nombre },
        Validators.required
      ),
      acto_id: new FormControl(event.acto_id, Validators.required),
      tipo_id: new FormControl(event.tipo_id, Validators.required),
      expediente_id: new FormControl(event.expediente_id, Validators.required),
      citacion_sumilla: new FormControl(
        event.citacion_sumilla,
        Validators.required
      ),
      citacion_referencia: new FormControl(event.citacion_referencia),
      citacion_resolucion: new FormControl(event.citacion_resolucion),
      resolucion_fecha: new FormControl(
        event.resolucion_fecha
          ? moment(event.resolucion_fecha).format("YYYY-MM-DD")
          : ""
      ),
      citacion_observacion: new FormControl(event.citacion_observacion),
      citacion_lugar: new FormControl(
        event.citacion_lugar,
        Validators.required
      ),
      expediente_etapa: new FormControl(
        event.expediente_etapa,
        Validators.required
      ),
    });
  }
  modalForm(formulario) {
    this.modal
      .open(formulario, { size: "lg", backdrop: "static", keyboard: false })
      .result.then(
        (result) => {
          this.eventForm.controls["expediente_id"].setValue(result[0].id);
          this.eventForm.controls["expediente_nombre"].setValue(
            result[0].nombre
          );
          this.eventForm.controls["citacion_lugar"].setValue(
            result[0].nombre_sala
          );
          this.eventForm.controls["expediente_etapa"].setValue(
            result[0].etapa_actual
          );
        },
        (reason) => {
          //this.toastr.info(reason)
        }
      );
  }
}
