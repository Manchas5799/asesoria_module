import { MisNotificacionesComponent } from "./mis-notificaciones/mis-notificaciones.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CalendarioCitacionesComponent } from "./calendario-citaciones/calendario-citaciones.component";

const routes: Routes = [
  {
    path: ":expediente",
    component: CalendarioCitacionesComponent,
  },
  {
    path: "api",
    redirectTo: "",
    pathMatch: "full",
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotificacionesExpedientesRoutingModule {}
