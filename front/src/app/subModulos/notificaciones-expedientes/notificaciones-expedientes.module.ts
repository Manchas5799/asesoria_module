import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { NotificacionesExpedientesRoutingModule } from "./notificaciones-expedientes-routing.module";
import { CalendarioCitacionesComponent } from "./calendario-citaciones/calendario-citaciones.component";
import { SharedComponentsModule } from "src/app/shared/components/shared-components.module";
import { NgbActiveModal, NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { GlobalModule } from "src/app/global/global.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { NgxPaginationModule } from "ngx-pagination";
import { MatSelectModule } from "@angular/material/select";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatDialogModule } from "@angular/material/dialog";
import { MatStepperModule } from "@angular/material/stepper";
import { MatButtonModule } from "@angular/material/button";
import { MatMenuModule } from "@angular/material/menu";
import { MatIconModule } from "@angular/material/icon";
import { MatTooltipModule } from "@angular/material/tooltip";
import { NgxSpinnerModule } from "ngx-spinner";
import { CalendarioCitacionesModalComponent } from "./calendario-citaciones-modal/calendario-citaciones-modal.component";
import { ColorPickerModule } from "ngx-color-picker";
import { CalendarModule, DateAdapter } from "angular-calendar";
import { adapterFactory } from "angular-calendar/date-adapters/date-fns";
import { MisNotificacionesComponent } from "./mis-notificaciones/mis-notificaciones.component";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { NotificacionesModalComponent } from './notificaciones-modal/notificaciones-modal.component';
import { MisPendientesComponent } from './mis-pendientes/mis-pendientes.component';
import {MatRadioModule} from '@angular/material/radio';

@NgModule({
  declarations: [
    CalendarioCitacionesComponent,
    CalendarioCitacionesModalComponent,
    MisNotificacionesComponent,
    NotificacionesModalComponent,
    MisPendientesComponent,
  ],
  imports: [
    CommonModule,
    NotificacionesExpedientesRoutingModule,
    SharedComponentsModule,
    NgbModule,
    GlobalModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgxPaginationModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatStepperModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatTooltipModule,
    NgxSpinnerModule,
    ColorPickerModule,
    MatSlideToggleModule,
    MatRadioModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
  ],
  exports: [
    MisNotificacionesComponent,
    MisPendientesComponent
  ],
  entryComponents: [
    CalendarioCitacionesModalComponent,
    MisNotificacionesComponent,
    MisPendientesComponent,
    NotificacionesModalComponent
  ],
  providers: [NgbActiveModal],
})
export class NotificacionesExpedientesModule {}
