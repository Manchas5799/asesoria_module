import { environment } from "./../../../../environments/environment.prod";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormControl } from "@angular/forms";
import { MatSlideToggleChange } from "@angular/material/slide-toggle";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import * as moment from "moment";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { FiltrosService } from "src/app/global/services/filtros.service";
import { JsPDFService } from "src/app/global/services/jsPDF.service";
import { LocalService } from "src/app/servicios/local.services";
import { QueryService } from "src/app/servicios/query.services";

@Component({
  selector: "app-mis-pendientes",
  templateUrl: "./mis-pendientes.component.html",
  styleUrls: ["./mis-pendientes.component.scss"],
})
export class MisPendientesComponent implements OnInit {
  @Input() citaciones: any = [];
  notificaciones: any = [];
  citacionesFiltradas: any = [];
  @Output() salida = new EventEmitter();
  rutas = {
    save_notificaciones: "/pendientes_save",
  };
  iPageSize = environment.paginacion.tamPag;
  p = 1;
  iPersId = 0;
  toggleSlide = new FormControl(false);
  constructor(
    public modal: NgbModal,
    private spinner: NgxSpinnerService,
    private jsPDF: JsPDFService,
    private filtro: FiltrosService,
    private query: QueryService,
    private toast: ToastrService,
    private local: LocalService
  ) {}

  ngOnInit() {
    this.iPersId = parseInt(this.local.getItem("userInfo").iPersId, 10);
    this.citaciones = JSON.parse(JSON.stringify(this.citaciones));
    // this.citaciones.map((e) => {
    //   e.involucrado = e.encargado[0];
    // });
    this.citacionesFiltradas = this.citaciones.filter(
      (e) => e.involucrado.pers_id === this.iPersId
    );
    console.log(this.citaciones);
  }
  mostrarTodos($event: MatSlideToggleChange) {
    if ($event.checked) {
      this.citacionesFiltradas = this.citaciones;
    } else {
      this.citacionesFiltradas = this.citaciones.filter(
        (e) => e.involucrado.pers_id === this.iPersId
      );
    }
  }
  guardar(evento) {
    const event = {
      id: evento._id,
      expediente: evento.expediente_id,
      recibido: moment().format("YYYY-MM-DD HH:mm").toString(),
      citacion: moment(evento.start).format("YYYY-MM-DD[T]HH:mm"),
      estado: evento.estado,
      sumilla: evento.citacion_sumilla,
      observacion: evento.observacion,
      persona: evento.persona_asesoria_id,
    };
    this.query.saveDatosApi(this.rutas.save_notificaciones, event).subscribe(
      (res) => {
        this.salida.emit(true);

        this.toast.success("REGISTRO ACTUALIZADO", "TAREAS");
      },
      (err) => {
        this.toast.error(err.msg, "Problema al guardar");
      }
    );
  }
}
