import { LocalService } from "./../../../servicios/local.services";
import { ModalsService } from "./../../../global/services/modals.service";
import { environment } from "./../../../../environments/environment";
import { NgbActiveModal, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { FiltrosService } from "src/app/global/services/filtros.service";
import { JsPDFService } from "src/app/global/services/jsPDF.service";
import { QueryService } from "src/app/servicios/query.services";
import * as moment from "moment";
import { MatSlideToggleChange } from "@angular/material/slide-toggle";
import { FormControl } from "@angular/forms";

@Component({
  selector: "app-mis-notificaciones",
  templateUrl: "./mis-notificaciones.component.html",
  styleUrls: ["./mis-notificaciones.component.scss"],
})
export class MisNotificacionesComponent implements OnInit {
  @Input() citaciones: any = [];
  citacionesFiltradas: any = [];
  @Output() salida = new EventEmitter();
  rutas = {
    save_notificaciones: "/expedientes_notificaciones",
  };
  iPageSize = environment.paginacion.tamPag;
  p = 1;
  iPersId = 0;
  toggleSlide = new FormControl(false);
  constructor(
    public modal: NgbModal,
    private spinner: NgxSpinnerService,
    private jsPDF: JsPDFService,
    private filtro: FiltrosService,
    private query: QueryService,
    private toast: ToastrService,
    private local: LocalService
  ) {}

  ngOnInit() {
    this.iPersId = parseInt(this.local.getItem("userInfo").iPersId, 10);
    this.citaciones = this.citaciones.map((e) => {
      e.start = moment(e.start).format("YYYY-MM-DD").toString();
      e.end = moment(e.end).format("YYYY-MM-DD").toString();
      return e;
    });
    this.citacionesFiltradas = this.citaciones.filter(
      (e) => e.involucrado.idd === this.iPersId
    );
    console.log(this.citaciones);
  }
  mostrarTodos($event: MatSlideToggleChange) {
    if ($event.checked) {
      this.citacionesFiltradas = this.citaciones;
    } else {
      this.citacionesFiltradas = this.citaciones.filter(
        (e) => e.involucrado.idd === this.iPersId
      );
    }
  }
  guardar(evento) {
    const event = {
      id: evento._id,
      expediente: evento.expediente_id,
      prioridad: evento.prioridad_id,
      finalidad: evento.finalidad_id,
      numero: evento.title,
      emision: null, // evento.fecha_emision
      recibido: evento.recepcion,
      citacion: evento.start,
      sumilla: evento.citacion_sumilla,
      lugar: evento.citacion_lugar,
      referencia: evento.citacion_referencia,
      resolucion: evento.citacion_resolucion,
      observacion: evento.citacion_observacion,
      fechaobs: moment().format("YYYY-MM-DD").toString(),
      persona: evento.persona_asesoria_id,
      codtramite: "",
      etapa: evento.expediente_etapa,
    };
    this.query.saveDatosApi(this.rutas.save_notificaciones, event).subscribe(
      (res) => {
        this.salida.emit(true);

        this.toast.success("REGISTRO AGREGADO", "CITACIONES");
      },
      (err) => {
        this.toast.error(err.msg, "Problema al guardar");
      }
    );
  }
}
