import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { QueryService } from "src/app/servicios/query.services";
import { FiltrosService } from "src/app/global/services/filtros.service";
import { ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { JsPDFService } from "src/app/global/services/jsPDF.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-finalidades",
  templateUrl: "./finalidad.component.html",
  styleUrls: ["./finalidad.component.scss"],
})
export class FinalidadesComponent implements OnInit {
  titulo: string = "FINALIDAD DE LA CITACIÓN";
  encabezados: string[] = ["ID", "NOMBRE", "ACCIONES"];
  msgBuscando: String = "Buscando..";
  /** listas */
  listado: any = [];
  listadoGral: any = [];
  listadoPer: any = [];
  listadoPersonas: any = [];
  registrosPersonas: string = "";
  dataTotal: any;

  /*Campos para Filtros */
  campoFiltro = {
    // id: "",
    // nombre: "",
    // direccion: "",
    id: "",
    nombre: "",
    direccion: "",
  };

  /**pagination */
  rows = [];
  iPageSize = 10;
  p: number = 1;
  pE: number = 1;

  /** forms */
  formulario: FormGroup;
  formBuscarPersona: FormGroup;
  //buscar:string

  //   formFiltroData: FormGroup;
  tituloForm: string = "";

  modalForm = null;
  /* loading */
  loading: boolean = false;

  /**check */
  checks: any = [];
  chkTodos: boolean = false;

  /** Tipo de reporte */
  typeReport: string = "PDF";
  pagReport: number = 1;

  /**auto-inicio filtro*/
  iOpcionFiltro = 0;
  /*URLS */
  url = {
    select: "/finalidades",
    update: "/finalidades_save",
    delete: "/finalidades_del",
    personas: "/salas_get_personas",
  };
  constructor(
    private query: QueryService,
    private filtro: FiltrosService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private modal: NgbModal,
    private fb: FormBuilder,
    private jsPDF: JsPDFService // private modalService:ModalsService
  ) {}
  ngOnInit() {
    this.seleccionar();
    this.formulario = this.fb.group({
      id: [""],
      nombre: ["", Validators.required],
    });
    this.formBuscarPersona = this.fb.group({
      buscar: this.fb.control("", [
        Validators.required,
        Validators.minLength(3),
      ]),
    });
  }
  modalFormPersona(formulario) {
    this.modal.open(formulario, { size: "lg" }).result.then(
      (result) => {
        console.log(result);
        this.formulario.controls["idresponsable"].setValue(result[0].id);
        this.formulario.controls["responsable"].setValue(result[0].nombre);
      },
      (reason) => {
        //this.toastr.info(reason)
      }
    );
  }
  modalFormOpen(formulario) {
    this.tituloForm = "Nueva Finalidad";
    this.modalForm = this.modal.open(formulario, {
      size: "sm",
      backdrop: "static",
      keyboard: false,
    });
    this.asignarDatosForm(null);
  }
  asignarDatosForm(dato) {
    if (dato == null) {
      this.formulario = this.formulario = this.fb.group({
        id: [""],
        nombre: ["", Validators.required],
      });
    } else {
      this.formulario = this.fb.group({
        id: dato[0].id,
        nombre: dato[0].nombre,
      });
    }
  }

  seleccionar() {
    this.loading = true;
    this.query.getDatosApi(this.url.select).subscribe(
      (data) => {
        console.log(data);
        this.dataTotal = data;
        //this.rows = []
        this.listado = this.dataTotal;
        this.listadoGral = this.listado;
        this.loading = false;
      },
      (error) => {
        console.log(error);
        // this.toastr.warning(
        //   error["error"]["message"].substring(
        //     71,
        //     error["error"]["message"].indexOf("(SQL:")
        //   ),
        //   "Importante"
        // );
        this.loading = false;
      }
    );

    // this.loading = false
  }
  filtrar() {
    this.listado = this.listadoGral;
    this.listado = this.filtro.filtroData(this.listado, this.campoFiltro.id, 0);
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.nombre,
      1
    );
  }
  filtrarPersona() {
    //this.listado =this.listadoPersonas
    // this.listadoPer = this.listadoPersonas;
    // this.listadoPer = this.filtro.filtroData(
    //   this.listadoPer,
    //   this.campoFiltro.idPer,
    //   0
    // );
    // this.listadoPer = this.filtro.filtroData(
    //   this.listadoPer,
    //   this.campoFiltro.nombrePer,
    //   1
    // );
  }

  validarCamposForm(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validarCamposForm(control);
      }
    });
  }

  guardar() {
    if (this.formulario.valid) {
      this.formulario.disable();
      let formData = {
        id: this.formulario.value.id,
        nombre: this.formulario.value.nombre,
      };
      this.query.saveDatosApi(this.url.update, formData).subscribe(
        (data) => {
          this.formulario.enable();
          this.modal.dismissAll();
          Swal.fire("Éxito!", data["msg"], "success");
          this.seleccionar();
        },
        (error) => {
          // console.log("msg", error);
          this.formulario.enable();
          // this.modal.dismissAll();
          this.toastr.error(error.error.msg, "Problema al guardar");
          // Swal.fire(
          //   "Error!",
          //   error["error"]["message"].substring(
          //     71,
          //     error["error"]["message"].indexOf("(SQL: EXEC")
          //   ),
          //   "error"
          // );
        }
      );
    } else {
      this.toastr.warning("Complete los campos requeridos", "Importante");
    }
  }
  editar(formulario, list) {
    this.tituloForm = "Editando Finalidad";
    this.modalForm = this.modal.open(formulario, {
      size: "sm",
      backdrop: "static",
      keyboard: false,
    });
    this.asignarDatosForm(list);
  }
  eliminar(list) {
    Swal.fire({
      title: "¿Continuar?",
      html:
        "Se eliminará el  registro <br><b>N° " +
        list[0].id +
        "-" +
        list[0].nombre +
        ".</b><br> <b>¿Desea continuar?</b>",
      //input: 'textarea',
      inputPlaceholder: "Observación...",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Continuar",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (!result.dismiss) {
        let idE = list[0].id;
        this.query.delDatoApi(this.url.delete, idE).subscribe(
          (data) => {
            let info: any = data;

            if (!info.error) {
              Swal.fire("Eliminado", info.msg, "success");

              this.seleccionar();
            } else {
              Swal.fire("Cancelado", info.msg, "error");
            }
          },
          (error) => {
            Swal.fire("Cancelado", error.error.msg, "error");
          }
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }
  buscarPersona() {
    this.loading = true;
    let formData = {
      //let //const // var
      texto: this.formBuscarPersona.value.buscar,
    };
    console.log("Buscando Persona: " + this.formBuscarPersona.value.buscarr);
    this.query.getDatosPostApi(this.url.personas, formData).subscribe(
      (data) => {
        this.listadoPersonas = data;
        this.listadoPer = this.listadoPersonas;
        this.registrosPersonas = this.listadoPersonas.length;
        this.loading = false;
      },
      (error) => {
        this.toastr.warning(
          error["error"]["message"].substring(
            71,
            error["error"]["message"].indexOf("(SQL:")
          ),
          "Importante"
        );
        this.loading = false;
      }
    );
  }
  seleccionarPersona(persona) {
    console.log(persona[0]);
    this.formulario.controls["idresponsable"].setValue(persona[0].id);
    this.formulario.controls["responsable"].setValue(persona[0].nombre);
    //this.modalService.close('modalResponsable')
  }
  downloadReport() {
    this.loading = true;
    switch (this.typeReport) {
      case "WORD":
        console.log("Descarga WORD");
        break;
      case "EXCEL":
        console.log("Descarga EXCEL");
        break;
      default:
        // Por defecto PDF
        console.log("Descarga PDF");
        this.downloadPDF();
        break;
    }
    this.loading = false;
  }
  downloadPDF() {
    this.jsPDF.generaPDF(this.prepararPDF(), 1);
  }
  private prepararPDF() {
    // Verificar this.listado
    let listadoPDF: any = [];
    let index: number = 1;
    this.listado.forEach((element) => {
      listadoPDF.push([index++, element.nombre]);
    });
    let estilosColumnas = {
      0: { halign: "center" },
      1: { halign: "center" },
    };
    let datosPDF = {
      orientacion: "p", //Un valor distinto a 'p' hará que sea horizontal (Se sugiere 'l')
      titulo: this.titulo,

      cabeceras: [["ITEM", "NOMBRE"]],
      datos: listadoPDF,
      columnaStyles: estilosColumnas,
      headerStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
}
