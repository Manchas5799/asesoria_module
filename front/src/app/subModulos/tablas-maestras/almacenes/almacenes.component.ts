import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { QueryService } from './../../../servicios/query.services'
import { FiltrosService} from './../../../global/services/filtros.service'
import { JsPDFService } from './../../../global/services/jsPDF.service'
// import { ModalsService } from './../../../global/services/modals.service'
import { NgbActiveModal, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormControl, FormGroup ,Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import {formatDate } from '@angular/common';

import swal from 'sweetalert2'

@Component({
  selector: 'app-almacenes',
  templateUrl: './almacenes.component.html',
  styleUrls: ['./almacenes.component.scss']
})
export class AlmacenesComponent implements OnInit {
  titulo:string = 'ALMACENES'
  msgBuscando:String ='Buscando..'
  /** listas */
  listado:any =[]
  listadoGral:any =[]
  listadoPer:any = []
  listadoPersonas:any =[]
  registrosPersonas:string = ''
  dataAlmacenes:any

  /*Campos para Filtros */
  campoFiltro = {
    id:'',
    filial:'',
    nombre:'',
    estado:'',
    responsable:'',
    dependede:'',
    idPer:'',
    nombrePer:''
  }
  
  /**pagination */
  rows = [];
  iPageSize=10
  p: number = 1;
  pE: number = 1;

   /** forms */
   formulario: FormGroup
   formBuscarPersona:FormGroup
   //buscar:string
   
//   formFiltroData: FormGroup;
   tituloForm:string = ''
    
   modalForm = null
    /* loading */
    loading:boolean = false
    
    /**check */
    checks:any = []
    chkTodos:boolean = false
    
    /** Tipo de reporte */
    typeReport:string =  'PDF'
    pagReport: number =1

    /**auto-inicio filtro*/
    iOpcionFiltro=0
    /*URLS */
    url = {
      select : '/almacenes',
      update: '/almacenes_save',
      delete: '/almacenes_del',
      personas: '/almacenes_get_personas'
    }
  constructor(
    private query:QueryService,
    private filtro: FiltrosService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private modal:NgbModal,
    private fb: FormBuilder,
    private jsPDF: JsPDFService,
    // private modalService:ModalsService
  ) {
    
  }
  ngOnInit() {
    // this.loading=true 
        
    this.seleccionar()

    this.formulario = this.fb.group({
      id: [''],
      nombre: ['', Validators.required],
      idestado:[null, Validators.required],
      
      idfilial:[null,Validators.required],
      idresponsable:['',Validators.required],
      responsable:['',Validators.required],
      idalmacenprincipal:[null],
      almdepende:['']
    })
    this.formBuscarPersona = this.fb.group({
      buscar:this.fb.control('',
              [ Validators.required,
                Validators.minLength(3)
              ])
    })
    // this.loading = false
  }
  modalFormPersona(formulario){
    this.modal.open(formulario, {size: 'lg'}).result
        .then((result) => {
          console.log(result)
          this.formulario.controls['idresponsable'].setValue(result[0].id)
          this.formulario.controls['responsable'].setValue(result[0].nombre)
        }, (reason) => {
          //this.toastr.info(reason)
        });
  }
  modalFormOpen(formulario){
    this.tituloForm = 'Nuevo Almacen'
    this.modalForm= this.modal.open(formulario, {size : "lg", backdrop: 'static', keyboard: false})
    this.asignarDatosForm(null)
  }
  asignarDatosForm(dato){
    if(dato==null){
      
      this.formulario = this.fb.group({
        id: [''],
        nombre: ['', Validators.required],
        idestado:[null, Validators.required],
       
        idfilial:[null,Validators.required],

        idresponsable:['',Validators.required],
        responsable:['',Validators.required],
        idalmacenprincipal:[null],
        almdepende:['']
      })
    }else{
      
      this.formulario = this.fb.group({
        id: dato[0].id,
        nombre: dato[0].nombre,
        idestado: dato[0].idestado,
        
        idfilial: dato[0].idfilial,

        idresponsable: dato[0].idresponsable,
        responsable: dato[0].responsable,
        idalmacenprincipal:dato[0].idalmacenprincipal,
        almdepende:dato[0].almdepende
      })
    }
  }

  seleccionar(){
    this.loading = true
        
  //  if(this.formFiltroData.valid) {
      
   //   this.formFiltroData.disable()  //Desactivamos los filtros
      //this.query.getEstados().subscribe( /almacen/estados
      this.query.getDatosApi(this.url.select).subscribe(
        data =>{
          console.log(data)
          this.dataAlmacenes=data
          //this.rows = []
          this.listado = this.dataAlmacenes.datos
          this.listadoGral = this.listado
          this.loading = false
        },
        error => {
          this.toastr.warning(error['error']['message'].substring(71,error['error']['message'].indexOf("(SQL:")),'Importante');
          this.loading = false
        }
      )        
  
    // this.loading = false
  }
  filtrar(){
    this.listado =this.listadoGral
    
    this.listado=this.filtro.filtroData(this.listado, this.campoFiltro.id,0)
    this.listado=this.filtro.filtroData(this.listado, this.campoFiltro.filial,8)
    this.listado=this.filtro.filtroData(this.listado, this.campoFiltro.nombre,1)
    this.listado=this.filtro.filtroData(this.listado, this.campoFiltro.estado,3)
    this.listado=this.filtro.filtroData(this.listado, this.campoFiltro.responsable,7)
    this.listado=this.filtro.filtroData(this.listado, this.campoFiltro.dependede,9)
  }
  filtrarPersona(){
    //this.listado =this.listadoPersonas
    this.listadoPer =this.listadoPersonas
    this.listadoPer=this.filtro.filtroData(this.listadoPer, this.campoFiltro.idPer,0)
    this.listadoPer=this.filtro.filtroData(this.listadoPer, this.campoFiltro.nombrePer,1)
    
  }
  
  validarCamposForm(formGroup: FormGroup) {         
    Object.keys(formGroup.controls).forEach(field => {  
      const control = formGroup.get(field);            
      if (control instanceof FormControl) {             
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        
        this.validarCamposForm(control);            
      }  
    });
  }

  guardar(){
  //  console.log ('Guardando')
    if(this.formulario.valid)
    {
      this.formulario.disable(); 
      let formData = {  //let //const // var
        id:this.formulario.value.id,
        nombre:this.formulario.value.nombre,
        idEstado:this.formulario.value.idestado,
        idAlmacenPrincipal:this.formulario.value.idalmacenprincipal,
        idDependencia:this.formulario.value.idfilial,
        idResponsable:this.formulario.value.idresponsable,
      };
      this.query.saveDatosApi(this.url.update,formData).subscribe(
        data => {
          this.formulario.enable(); 
          this.modal.dismissAll()
          swal.fire(
            'Éxito!',
            'Se guardó el Almacén Exitosamente!',
            'success'
          )
          
          this.seleccionar()
        },
        error => {
          console.log(formData)
          this.modal.dismissAll()
          this.formulario.enable(); 
          swal.fire(
            'Error!',
            error['error']['message'].substring(71,error['error']['message'].indexOf("(SQL: EXEC")),
            'error'
          )     
        });
      }
      else{
        this.toastr.warning('Complete los campos requeridos','Importante');
      }
  }
  editar(formulario,list){
    this.tituloForm = 'Editando Almacén'
    this.modalForm= this.modal.open(formulario, {size : "lg", backdrop: 'static', keyboard: false})
    this.asignarDatosForm(list)
  }
  eliminar(list){
    swal.fire({
      title: '¿Continuar?',
      html: 'Se eliminará el  registro <br><b>N° ' + list[0].id+ '-' + list[0].nombre + '.</b><br> <b>¿Desea continuar?</b>',
      //input: 'textarea',
      inputPlaceholder: 'Observación...',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (!result.dismiss) {
         let idE = list[0].id
          this.query.delDatoApi(this.url.delete,idE).subscribe(
            data => {
              let info:any=data 
              
              if(info.datos[0]['iResult'] == 1)
              {
                swal.fire(
                  'Eliminado',
                  info.msg,
                  'success'
                )              
                
                this.seleccionar()
              }else{
                swal.fire(
                  'Cancelado',
                  info.msg,
                  'error'
                )               
              }
            }, 
          error => {
            swal.fire(
              'Cancelado',
              error['error']['message'].substring(71,error['error']['message'].indexOf("(SQL:")),
              'error'
            )
          });
      } else if (result.dismiss === swal.DismissReason.cancel) {
      
      }
    })
    
  }
  buscarPersona(){
    this.loading = true
    let formData = {  //let //const // var
      texto:this.formBuscarPersona.value.buscar
    };
    console.log ('Buscando Persona: '+this.formBuscarPersona.value.buscarr)
    this.query.getDatosPostApi(this.url.personas,formData).subscribe(
      data => { 
        this.listadoPersonas = data
        this.listadoPer = this.listadoPersonas
        this.registrosPersonas=this.listadoPersonas.length 
        this.loading = false
      },
      error => { 
        this.toastr.warning(error['error']['message'].substring(71,error['error']['message'].indexOf("(SQL:")),'Importante');
        this.loading = false
      })
  }
  seleccionarPersona(persona){
    console.log (persona[0])
    this.formulario.controls['idresponsable'].setValue(persona[0].id)
    this.formulario.controls['responsable'].setValue(persona[0].nombre)
    //this.modalService.close('modalResponsable')
  }
  downloadReport(){
    this.loading = true
    switch (this.typeReport){
      case 'WORD':
        console.log('Descarga WORD')
        break
      case 'EXCEL':
        console.log('Descarga EXCEL')
        break
      default:  // Por defecto PDF
        console.log('Descarga PDF')
        this.downloadPDF()
        break
    }
    this.loading = false
  }
  downloadPDF(){
    this.jsPDF.generaPDF(this.prepararPDF(),1)
  }
  private prepararPDF(){
  // Verificar this.listado
  let listadoPDF:any =[]
  let index:number=1
  this.listado.forEach(element => {
    listadoPDF.push(
      [
        index++,
        element.filial,
        element.nombre,
        element.estado,
        element.responsable,
        element.almdepende
      ]
      
    )
  });
  let estilosColumnas={ 
    0: { halign: 'center'}, 
    1: { halign: 'center' },
    2: { halign: 'left' },
    3: { halign: 'center' },
    4: { halign: 'left' },
    5: { halign: 'left' },
    
    };
  let datosPDF={
    orientacion: 'p',  //Un valor distinto a 'p' hará que sea horizontal (Se sugiere 'l')
    titulo: this.titulo,
    
   cabeceras:[
     ['ITEM','FILIAL','ALMACÉN','ESTADO','RESPONSABLE','DEPENDE DE..']
   ],
    datos: listadoPDF,
    columnaStyles: estilosColumnas
  }
  //console.log(datosPDF)
  return datosPDF
  }

}
