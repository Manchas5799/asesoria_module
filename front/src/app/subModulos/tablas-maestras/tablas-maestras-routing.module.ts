import { ActosComponent } from './actos/actos.component';
import { DistritosJudicialesComponent } from "./distritos-judiciales/distritos-judiciales.component";
import { EstadoExpedienteComponent } from "./estado-expediente/estado-expediente.component";
import { EstadoProcesoComponent } from "./estado-proceso/estado-proceso.component";
import { TipoCitacionComponent } from "./tipo-citacion/tipo-citacion.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";

import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ReactiveFormsModule } from "@angular/forms";

import { NgxPaginationModule } from "ngx-pagination";

import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { ToastrModule } from "ngx-toastr";

import { GlobalModule } from "./../../global/global.module";
import { SharedComponentsModule } from "./../../shared/components/shared-components.module";
import { AlmacenesComponent } from "./almacenes/almacenes.component";
import { SalasComponent } from "./sala/sala.component";
import { TipoPersonaComponent } from "./tipo-persona/tipo-persona.component";
import { PrioridadesComponent } from "./prioridad/prioridad.component";
import { FinalidadesComponent } from "./finalidad/finalidad.component";
import { EtapasComponent } from "./etapa/etapa.component";
import { CalidadParticipacionComponent } from "./calidad-participacion/calidad-participacion.component";
import { TipoExpedientesComponent } from "./tipo-expediente/tipo-expediente.component";
import { PersonasAsesoriaComponent } from "./personas-asesoria/personas-asesoria.component";
import { MateriasComponent } from "./materias/materias.component";
import { EnlacesExternosComponent } from "./enlaces-externos/enlaces-externos.component";

const routes: Routes = [
  {
    path: "organos",
    component: SalasComponent,
  },
  {
    path: "tipo_personas",
    component: TipoPersonaComponent,
  },
  {
    path: "prioridades",
    component: PrioridadesComponent,
  },
  {
    path: "personas_asesoria",
    component: PersonasAsesoriaComponent,
  },
  {
    path: "finalidades",
    component: FinalidadesComponent,
  },
  {
    path: "etapas",
    component: EtapasComponent,
  },
  {
    path: "calidades_participación",
    component: CalidadParticipacionComponent,
  },
  {
    path: "expedientes",
    component: AlmacenesComponent,
  },
  {
    path: "tipo_expedientes",
    component: TipoExpedientesComponent,
  },
  {
    path: "materias",
    component: MateriasComponent,
  },
  {
    path: "enlaces",
    component: EnlacesExternosComponent,
  },
  {
    path: "tipo_citacion",
    component: TipoCitacionComponent,
  },
  {
    path: "acto_procesal",
    component: ActosComponent,
  },
  {
    path: "estado_expediente",
    component: EstadoExpedienteComponent,
  },
  {
    path: "distritos_judiciales",
    component: DistritosJudicialesComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    NgbModule.forRoot(),
    ToastrModule.forRoot(),
    CommonModule,
    GlobalModule,
    SharedComponentsModule,
  ],
  exports: [RouterModule],
  declarations: [],
})
export class TablasMaestrasRoutingModule {}
