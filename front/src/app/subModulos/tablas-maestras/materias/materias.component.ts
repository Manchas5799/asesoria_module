import { Component, OnInit } from "@angular/core";
import Swal from "sweetalert2";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { QueryService } from "src/app/servicios/query.services";
import { FiltrosService } from "src/app/global/services/filtros.service";
import { ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { JsPDFService } from "src/app/global/services/jsPDF.service";
import { forkJoin } from "rxjs";

@Component({
  selector: "app-materias",
  templateUrl: "./materias.component.html",
  styleUrls: ["./materias.component.scss"],
})
export class MateriasComponent implements OnInit {
  titulo: string = "MATERIAS";
  encabezados: string[] = ["ID", "NOMBRE", "TIPO EXPEDIENTE", "ACCIONES"];
  msgBuscando: String = "Buscando..";
  /** listas */
  listado: any = [];
  listadoGral: any = [];
  listaTipos: any = [];
  dataTotal: any;

  /*Campos para Filtros */
  campoFiltro = {
    // id: "",
    // nombre: "",
    // direccion: "",
    id: "",
    nombre: "",
    tipo_nombre: "",
  };

  /**pagination */
  rows = [];
  iPageSize = 10;
  p: number = 1;
  pE: number = 1;

  /** forms */
  formulario: FormGroup;
  formBuscarPersona: FormGroup;
  //buscar:string

  //   formFiltroData: FormGroup;
  tituloForm: string = "";

  modalForm = null;
  /* loading */
  loading: boolean = false;

  /**check */
  checks: any = [];
  chkTodos: boolean = false;

  /** Tipo de reporte */
  typeReport: string = "PDF";
  pagReport: number = 1;

  /**auto-inicio filtro*/
  iOpcionFiltro = 0;
  /*URLS */
  url = {
    select: "/materias",
    update: "/materias_save",
    delete: "/materias_del",
    select_tipos: "/tipo_expedientes",
  };
  constructor(
    private query: QueryService,
    private filtro: FiltrosService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private modal: NgbModal,
    private fb: FormBuilder,
    private jsPDF: JsPDFService // private modalService:ModalsService
  ) {}
  ngOnInit() {
    this.seleccionar();
    this.formulario = this.fb.group({
      id: [""],
      nombre: ["", Validators.required],
      tipo: ["", Validators.required],
    });
  }
  modalFormOpen(formulario) {
    this.tituloForm = "Nueva Materia";
    this.modalForm = this.modal.open(formulario, {
      size: "sm",
      backdrop: "static",
      keyboard: false,
    });
    this.asignarDatosForm(null);
  }
  asignarDatosForm(dato) {
    if (dato == null) {
      this.formulario = this.formulario = this.fb.group({
        id: [""],
        nombre: [ "", [Validators.required, Validators.pattern("[0-9 A-Z a-z À-ÿ \u00f1 \u00d1]*")]],
        tipo: ["", Validators.required],
      });
    } else {
      this.formulario = this.fb.group({
        id: dato[0].id,
        nombre: [ dato[0].nombre, [Validators.required, Validators.pattern("[0-9 A-Z a-z À-ÿ \u00f1 \u00d1]*")]],
        tipo: [dato[0].tipo_id],
      });
    }
  }

  seleccionar() {
    this.loading = true;
    forkJoin({
      materias: this.query.getDatosApi(this.url.select),
      tipos: this.query.getDatosApi(this.url.select_tipos),
    }).subscribe({
      next: (data) => {
        //this.rows = []
        this.listado = data.materias;
        this.listadoGral = this.listado;
        this.listaTipos = data.tipos;
        this.loading = false;
      },
    });
  }
  filtrar() {
    this.listado = this.listadoGral;
    this.listado = this.filtro.filtroData(this.listado, this.campoFiltro.id, 0);
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.nombre,
      1
    );
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.tipo_nombre,
      3
    );
  }
  validarCamposForm(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((field) => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validarCamposForm(control);
      }
    });
  }

  guardar() {
    if (this.formulario.valid) {
      this.formulario.disable();
      let formData = {
        id: this.formulario.value.id,
        nombre: this.formulario.value.nombre,
        tipo: this.formulario.value.tipo,
      };
      this.query.saveDatosApi(this.url.update, formData).subscribe(
        (data) => {
          this.formulario.enable();
          this.modal.dismissAll();
          Swal.fire("Éxito!", data["msg"], "success");
          this.seleccionar();
        },
        (error) => {
          // console.log("msg", error);
          this.formulario.enable();
          // this.modal.dismissAll();
          this.toastr.error(error.error.msg, "Problema al guardar");
          // Swal.fire(
          //   "Error!",
          //   error["error"]["message"].substring(
          //     71,
          //     error["error"]["message"].indexOf("(SQL: EXEC")
          //   ),
          //   "error"
          // );
        }
      );
    } else {
      this.toastr.warning("Complete los campos requeridos", "Importante");
    }
  }
  editar(formulario, list) {
    this.tituloForm = "Editando Materia";
    this.modalForm = this.modal.open(formulario, {
      size: "sm",
      backdrop: "static",
      keyboard: false,
    });
    this.asignarDatosForm(list);
  }
  eliminar(list) {
    Swal.fire({
      title: "¿Continuar?",
      html:
        "Se eliminará el  registro <br><b>N° " +
        list[0].id +
        "-" +
        list[0].nombre +
        ".</b><br> <b>¿Desea continuar?</b>",
      //input: 'textarea',
      inputPlaceholder: "Observación...",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Continuar",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (!result.dismiss) {
        let idE = list[0].id;
        this.query.delDatoApi(this.url.delete, idE).subscribe(
          (data) => {
            let info: any = data;

            if (!info.error) {
              Swal.fire("Eliminado", info.msg, "success");

              this.seleccionar();
            } else {
              Swal.fire("Cancelado", info.msg, "error");
            }
          },
          (error) => {
            Swal.fire("Cancelado", error.error.msg, "error");
          }
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    });
  }

  downloadReport() {
    this.loading = true;
    switch (this.typeReport) {
      case "WORD":
        console.log("Descarga WORD");
        break;
      case "EXCEL":
        console.log("Descarga EXCEL");
        break;
      default:
        // Por defecto PDF
        console.log("Descarga PDF");
        this.downloadPDF();
        break;
    }
    this.loading = false;
  }
  downloadPDF() {
    this.jsPDF.generaPDF(this.prepararPDF(), 1);
  }
  private prepararPDF() {
    // Verificar this.listado
    let listadoPDF: any = [];
    let index: number = 1;
    this.listado.forEach((element) => {
      listadoPDF.push([index++, element.nombre, element.direccion]);
    });
    let estilosColumnas = {
      0: { halign: "center" },
      1: { halign: "center" },
    };
    let datosPDF = {
      orientacion: "p", //Un valor distinto a 'p' hará que sea horizontal (Se sugiere 'l')
      titulo: this.titulo,

      cabeceras: [["ITEM", "NOMBRE"]],
      datos: listadoPDF,
      columnaStyles: estilosColumnas,
      headerStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
}
