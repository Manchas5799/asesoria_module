import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SharedComponentsModule } from "src/app/shared/components/shared-components.module";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { TablasMaestrasRoutingModule } from "./tablas-maestras-routing.module";
import { GlobalModule } from "../../global/global.module";
import { SalasComponent } from "./sala/sala.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { NgxPaginationModule } from "ngx-pagination";
import { PrioridadesComponent } from "./prioridad/prioridad.component";
import { FinalidadesComponent } from "./finalidad/finalidad.component";
import { CalidadParticipacionComponent } from "./calidad-participacion/calidad-participacion.component";
import { TipoPersonaComponent } from "./tipo-persona/tipo-persona.component";
import { EtapasComponent } from "./etapa/etapa.component";
import { AlmacenesComponent } from "./almacenes/almacenes.component";
import { TipoExpedientesComponent } from "./tipo-expediente/tipo-expediente.component";
import { PersonasAsesoriaComponent } from "./personas-asesoria/personas-asesoria.component";
import { MatSelectModule } from "@angular/material/select";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatDialogModule } from "@angular/material/dialog";
import { MatStepperModule } from "@angular/material/stepper";
import { MatButtonModule } from "@angular/material/button";
import { MatMenuModule } from "@angular/material/menu";
import { MatIconModule } from "@angular/material/icon";
import { MatTooltipModule } from "@angular/material/tooltip";
import { NgxSpinnerModule } from "ngx-spinner";
import { MateriasComponent } from './materias/materias.component';
import { EnlacesExternosComponent } from './enlaces-externos/enlaces-externos.component';
import { DistritosJudicialesComponent } from './distritos-judiciales/distritos-judiciales.component';
import { EstadoExpedienteComponent } from './estado-expediente/estado-expediente.component';
import { EstadoProcesoComponent } from './estado-proceso/estado-proceso.component';
import { TipoCitacionComponent } from './tipo-citacion/tipo-citacion.component';
import { ActosComponent } from './actos/actos.component';

@NgModule({
  declarations: [
    SalasComponent,
    AlmacenesComponent,
    PrioridadesComponent,
    FinalidadesComponent,
    CalidadParticipacionComponent,
    TipoPersonaComponent,
    EtapasComponent,
    TipoExpedientesComponent,
    PersonasAsesoriaComponent,
    MateriasComponent,
    EnlacesExternosComponent,
    DistritosJudicialesComponent,
    EstadoExpedienteComponent,
    EstadoProcesoComponent,
    TipoCitacionComponent,
    ActosComponent,
  ],
  imports: [
    CommonModule,
    TablasMaestrasRoutingModule,
    SharedComponentsModule,
    NgbModule,
    GlobalModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgxPaginationModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatStepperModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatTooltipModule,
    NgxSpinnerModule,
    // ResaltarPipe
  ],
})
export class TablasMaestrasModule {}
