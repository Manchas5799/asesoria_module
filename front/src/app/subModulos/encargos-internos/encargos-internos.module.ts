import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EncargosInternosRoutingModule } from './encargos-internos-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EncargosInternosRoutingModule
  ]
})
export class EncargosInternosModule { }
