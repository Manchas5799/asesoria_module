import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EncargosInternosPrincipalComponent } from './encargos-internos-principal.component';

describe('EncargosInternosPrincipalComponent', () => {
  let component: EncargosInternosPrincipalComponent;
  let fixture: ComponentFixture<EncargosInternosPrincipalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EncargosInternosPrincipalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncargosInternosPrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
