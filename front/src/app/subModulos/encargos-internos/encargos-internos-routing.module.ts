import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { EncargosInternosPrincipalComponent } from './encargos-internos-principal/encargos-internos-principal.component';

const routes: Routes = [  {
  path: 'principal',
  component: EncargosInternosPrincipalComponent,
},];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule
  ],
  exports: [RouterModule],
  declarations: [EncargosInternosPrincipalComponent]
})
export class EncargosInternosRoutingModule { }
