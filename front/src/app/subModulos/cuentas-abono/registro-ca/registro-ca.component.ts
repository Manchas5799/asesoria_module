import { Component, OnInit,  ViewChild, ElementRef,Input,Output, EventEmitter,ViewEncapsulation } from '@angular/core';
import {formatDate} from '@angular/common';
import { QueryService } from './../../../servicios/query.services'
import { GlobalService } from './../../../servicios/global.service'
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup ,Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import swal from 'sweetalert2'
import { LocalService } from './../../../servicios/local.services'
import { environment }  from '../../../../environments/environment'
import { isArray } from 'util';
declare var $: any;
@Component({
  selector: 'app-registro-ca',
  templateUrl: './registro-ca.component.html',
  styleUrls: ['./registro-ca.component.scss']
})
export class RegistroCaComponent implements OnInit {

/**valores para componentes hijos */
_iRecId:any=0
/** input */
/** output */
/** listas */
listaRecibosSaldos:any =[]
/**pagination */
pagination : boolean=true
rows = [];
iPageNumber=1
iPageSize=15
/** combos filtro */

/** combos registro */

// comboTiposChequeras:any=[]
/** formularios */
formFiltroRecibosSaldos: FormGroup
formRecibosSaldos: FormGroup
/** modals */
modalRecibosSaldos=null
/** secciones filtro */
sectionFiltroTipoDoc:boolean=false
sectionFiltroFecha:boolean=false
sectionFiltroPeriodoMes:boolean=false
sectionFiltroRangoFechas:boolean=false
sectionFiltroCriterio:boolean=false
/** varibales iniciales */
iOpcionFiltro=0
//iTipoRecIdFiltro=1 //primer filtro tipo recibo
iYearFiltro=""
iMonthFiltro=""
iYearCriterioFiltro=""
iCritIdFiltro=""
/** varibales globales */
iEntId:string
cSec_ejec:string

iCredEmisorId:string=""
typeReport:string='pdf'

/** check */
checks=[]
chkTodos:boolean = false
checksValidate = [];
/** seguros */
segModal:boolean = true
/** loading */
loading:boolean = false
load:boolean = false
/** select */
selectedRow : Number;
setClickedRow : Function;

  constructor(
    private query:QueryService,
    private global:GlobalService,
    private toastr: ToastrService,
    private local:LocalService,
    private modal:NgbModal,     
  ) { 

    let data = this.local.getItem('userInfo')
    this.iCredEmisorId = data['iCredId']

    this.setClickedRow = function(index){
      this.selectedRow = index;
    }

    this.iEntId = environment.entidad.id
    this.cSec_ejec = environment.entidad.secFuncional     
  }

  ngOnInit() {
  }

}
