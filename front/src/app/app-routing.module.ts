import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthLayoutComponent } from "./shared/components/layouts/auth-layout/auth-layout.component";
import { AuthGaurd } from "./shared/services/auth.gaurd";
import { AdminLayoutSidebarLargeComponent } from "./shared/components/layouts/admin-layout-sidebar-large/admin-layout-sidebar-large.component";
import { IconsModule } from "./views/icons/icons.module";
import { BlankLayoutComponent } from "./shared/components/layouts/blank-layout/blank-layout.component";
//import {ResaltarPipe} from './global/pipes/resaltar.pipes'

const moduleRoutes: Routes = [
  {
    path: "user",
    loadChildren: () =>
      import("./personal/personal.module").then((m) => m.PersonalModule),
  },
  {
    path: "tablas-maestras",
    loadChildren: () =>
      import("./subModulos/tablas-maestras/tablas-maestras.module").then(
        (m) => m.TablasMaestrasModule
      ),
  },
  {
    path: "notificaciones",
    loadChildren: () =>
      import(
        "./subModulos/notificaciones-expedientes/notificaciones-expedientes.module"
      ).then((m) => m.NotificacionesExpedientesModule),
  },
  {
    path: "expedientes",
    loadChildren: () =>
      import("./subModulos/expedientes/expedientes.module").then(
        (m) => m.ExpedientesModule
      ),
  },
  {
    path: "consultasexternas",
    loadChildren: () =>
      import("./subModulos/consultas-externas/consultas-externas.module").then(
        (m) => m.ConsultasExternasModule
      ),
  },
  {
    path: "sistemasexternos",
    loadChildren: () =>
      import("./subModulos/sistemas-externos/sistemas-externos.module").then(
        (m) => m.SistemasExternosModule
      ),
  },
  {
    path: "reportes",
    loadChildren: () =>
      import("./subModulos/reportes/reportes.module").then(
        (m) => m.ReportesModule
      ),
  },
];
const routes: Routes = [
  {
    path: "",
    redirectTo: "user/perfil",
    pathMatch: "full",
  },

  {
    path: "",
    component: AuthLayoutComponent,
    children: [
      {
        path: "sessions",
        loadChildren: () =>
          import("./views/sessions/sessions.module").then(
            (m) => m.SessionsModule
          ),
      },
    ],
  },
  {
    path: "",
    component: BlankLayoutComponent,
    children: [
      {
        path: "icons1",
        loadChildren: () =>
          import("./views/icons/icons.module").then((m) => m.IconsModule),
      },
    ],
  },
  {
    path: "",
    component: AdminLayoutSidebarLargeComponent,
    canActivate: [AuthGaurd],
    children: moduleRoutes,
  },
  {
    path: "**",
    redirectTo: "others/404",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
