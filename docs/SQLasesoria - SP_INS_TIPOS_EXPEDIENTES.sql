ALTER PROCEDURE [ase].[Sp_INS_tipo_expediente]
	@_iTiposId	 INTEGER,
	@_cTiposNombre varchar(50),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cSalasOpenUrs varchar(1),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Insertar Nuevo Tipo de Expedientes
* 
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	
	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cTiposNombre=RTRIM(LTRIM(@_cTiposNombre))

	
	IF COALESCE(@_cTiposNombre,'') = ''
		BEGIN
			SET @cMensaje='Debe especificar el nombre del Tipo de expediente.'
			RAISERROR (@cMensaje,18,1,1)
			GOTO ErrorCapturado
		END
	IF @_cTiposNombre IS NOT NULL
		BEGIN
			IF EXISTS(SELECT iTiposId FROM ase.tipos_expedientes WHERE UPPER(RTRIM(LTRIM(cTiposNombre))) COLLATE SQL_Latin1_General_Cp1_CI_AI=UPPER(RTRIM(LTRIM(@_cTiposNombre))))
				BEGIN
					SET @cMensaje='El tipo de expediente '+UPPER(RTRIM(LTRIM(@_cTiposNombre)))+' ya se encuentra registrado, verifique por favor...'
					RAISERROR (@cMensaje,18,1,1)
					GOTO ErrorCapturado					
				END
		END

	
	DECLARE @iTiposId INTEGER,@iCodigoError INTEGER
	set @_cTiposNombre = UPPER(@_cTiposNombre)

	INSERT ase.tipos_expedientes
	(cTiposNombre, cTiposUsuarioSis, dtTiposFechaSis, cTiposEquipoSis, cTiposIpSis, cTiposOpenUrs,cTiposMAcNicSis)
	VALUES ( UPPER(@_cTiposNombre),
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	SELECT @iTiposId=@@IDENTITY,@iCodigoError=@@ERROR

	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@iTiposId AS iEtapasId
	RETURN @iTiposId

ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0