ALTER PROCEDURE [ase].[Sp_INS_tipo_persona]
	@_iTipoId	 INTEGER,
	@_cTipoNombre varchar(50),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Insertar Nuevo Tipo Persona
* 
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	
	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cTipoNombre=RTRIM(LTRIM(@_cTipoNombre))

	
	IF COALESCE(@_cTipoNombre,'') = ''
		BEGIN
			SET @cMensaje='Debe especificar el nombre del tipo de Persona.'
			RAISERROR (@cMensaje,18,1,1)
			GOTO ErrorCapturado
		END
	IF @_cTipoNombre IS NOT NULL
		BEGIN
			IF EXISTS(SELECT iTipoId FROM ase.tipo_persona WHERE UPPER(RTRIM(LTRIM(cTipoNombre))) COLLATE SQL_Latin1_General_Cp1_CI_AI=UPPER(RTRIM(LTRIM(@_cTipoNombre))))
				BEGIN
					SET @cMensaje='El tipo persona '+UPPER(RTRIM(LTRIM(@_cTipoNombre)))+' ya se encuentra registrado, verifique por favor...'
					RAISERROR (@cMensaje,18,1,1)
					GOTO ErrorCapturado					
				END
		END

	
	DECLARE @iTipoId INTEGER,@iCodigoError INTEGER
	set @_cTipoNombre = UPPER(LTRIM(RTRIM(@_cTipoNombre)))

	INSERT ase.tipo_persona
	(cTipoNombre, cTipoUsuarioSis, dTipoFechaSis, cTipoEquipoSis, cTipoIpSis, cTipoOpenUrs,cTipoMAcNicSis)
	VALUES ( @_cTipoNombre,
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	SELECT @iTipoId=@@IDENTITY,@iCodigoError=@@ERROR

	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@iTipoId AS iTipoId
	RETURN @iTipoId

ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0