ALTER PROCEDURE [ase].[Sp_INS_expedientes_y_exp_etapas]
	@_iExpedientesId INTEGER,
	@_iSalasId INTEGER,
	@_cExpedientesNombre varchar(250),
	@_cExpedientesDescripcion varchar(max),
	@_cExpedientesNumero varchar(100),
	@_bExpedientesEstado bit,
	@_cExpedienteDemandante varchar(400),
	@_cExpedienteDemandado varchar(400),
	@_cExpedienteMateria varchar(200),
	@_cExpedienteActoProcesal varchar(400),
	@_iTiposId INTEGER,
	@_cExpedientesIdTramiteDoc varchar(20),

	@_iExpedientesEtapasId INTEGER,
	@_iEtapasId INTEGER,
	@_dtExpedienteFecha DATETIME,
	@_cExpedientesMotivo VARCHAR(MAX),
	--@_cExpedientesIdTramiteDoc VARCHAR(20),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Insertar Nuevo EXPEDIENTE y ETAPA_EXPEDIENTE
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	
	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cExpedientesNumero=RTRIM(LTRIM(@_cExpedientesNumero))

	
	IF COALESCE(@_cExpedientesNumero,'') = ''
		BEGIN
			SET @cMensaje='Debe especificar el número de expediente.'
			RAISERROR (@cMensaje,18,1,1)
			GOTO ErrorCapturado
		END
	IF @_cExpedientesNumero IS NOT NULL
		BEGIN
			IF EXISTS(SELECT iExpedientesId FROM ase.expedientes WHERE UPPER(RTRIM(LTRIM(cExpedientesNumero))) COLLATE SQL_Latin1_General_Cp1_CI_AI=UPPER(RTRIM(LTRIM(@_cExpedientesNumero))))
				BEGIN
					SET @cMensaje='El Expediente '+UPPER(RTRIM(LTRIM(@_cExpedientesNumero)))+' ya se encuentra registrado, verifique por favor...'
					RAISERROR (@cMensaje,18,1,1)
					GOTO ErrorCapturado					
				END
		END

	
	DECLARE @iExpedientesId INTEGER,@iCodigoError INTEGER
	set @_cExpedientesNombre = UPPER(LTRIM(RTRIM(@_cExpedientesNumero)))
	set @_cExpedientesDescripcion = UPPER(LTRIM(RTRIM(@_cExpedientesDescripcion)))
	set @_cExpedientesNumero = UPPER(LTRIM(RTRIM(@_cExpedientesNumero)))
	set @_cExpedienteDemandante = UPPER(LTRIM(RTRIM(@_cExpedienteDemandante)))
	set @_cExpedienteDemandado = UPPER(LTRIM(RTRIM(@_cExpedienteDemandado)))
	set @_cExpedienteMateria = UPPER(LTRIM(RTRIM(@_cExpedienteMateria)))
	set @_cExpedienteActoProcesal= UPPER(LTRIM(RTRIM(@_cExpedienteActoProcesal)))

	INSERT ase.expedientes
	(iSalasId, cExpedientesNombre, cExpedientesDescripcion, cExpedientesNumero, bExpedientesEstado, cExpedienteDemandante, cExpedienteDemandado, cExpedienteMateria, cExpedienteActoProcesal, iTiposId, iEtapasId, cExpedientesIdTramiteDoc, cExpedientesUsuarioSis, dtExpedientesFechaSis, cExpedientesEquipoSis, cExpedientesIpSis, cExpedientesOpenUrs,cExpedientesMacNicSis)
	VALUES ( @_iSalasId, @_cExpedientesNombre, @_cExpedientesDescripcion, @_cExpedientesNumero, @_bExpedientesEstado, @_cExpedienteDemandante, @_cExpedienteDemandado, @_cExpedienteMateria, @_cExpedienteActoProcesal, @_iTiposId, @_iEtapasId, @_cExpedientesIdTramiteDoc,
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	SELECT @iExpedientesId=@@IDENTITY,@iCodigoError=@@ERROR

	INSERT ase.expedientes_etapas
	(iExpedientesId, iEtapasId, dtExpedienteFecha, cExpedientesMotivo, cExpedientesIdTramiteDoc,cEtapasUsuarioSis, dtEtapasFechaSis, cEtapasEquipoSis, cEtapasIpSis, cEtapasOpenUrs,cEtapasMacNicSis)
	VALUES ( @iExpedientesId,@_iEtapasId, @_dtExpedienteFecha, UPPER(LTRIM(RTRIM(@_cExpedientesMotivo))), @_cExpedientesIdTramiteDoc,
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@iExpedientesId AS iExpedientesId
	RETURN @iExpedientesId

ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0
