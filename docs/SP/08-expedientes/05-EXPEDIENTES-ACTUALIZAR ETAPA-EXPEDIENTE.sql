ALTER PROCEDURE [ase].[Sp_INS_expedientes_etapas]
	@_iExpedientesId INTEGER,
	@_iSalasId INTEGER,
	@_cExpedientesNombre varchar(250),
	@_cExpedientesDescripcion varchar(max),
	@_cExpedientesNumero varchar(100),
	@_bExpedientesEstado bit,
	@_cExpedienteDemandante varchar(400),
	@_cExpedienteDemandado varchar(400),
	@_cExpedienteMateria varchar(200),
	@_cExpedienteActoProcesal varchar(400),
	@_iTiposId INTEGER,
	@_cExpedientesIdTramiteDoc varchar(20),

	@_iExpedientesEtapasId INTEGER,
	@_iEtapasId INTEGER,
	@_dtExpedienteFecha DATETIME,
	@_cExpedientesMotivo VARCHAR(MAX),
	--@_cExpedientesIdTramiteDoc VARCHAR(20),
	
	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Actualización de EXPEDIENTES ETAPAS
* 
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	IF @@ERROR<>0 GOTO ErrorCapturado

	DECLARE @cMensaje VARCHAR(MAX)

	IF (select iEtapasId from ase.expedientes where iExpedientesId=@_iExpedientesId)!=@_iEtapasId
		BEGIN 
			set @_cExpedientesMotivo = UPPER(LTRIM(RTRIM(@_cExpedientesMotivo)))

			INSERT ase.expedientes_etapas
			(iExpedientesId, iEtapasId, dtExpedienteFecha, cExpedientesMotivo, cExpedientesIdTramiteDoc,cEtapasUsuarioSis, dtEtapasFechaSis, cEtapasEquipoSis, cEtapasIpSis, cEtapasOpenUrs,cEtapasMacNicSis)
			VALUES ( @_iExpedientesId,@_iEtapasId, @_dtExpedienteFecha,@_cExpedientesMotivo, @_cExpedientesIdTramiteDoc,
				/*Campos de auditoria*/
				@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
			)
		END


	set @_cExpedientesNombre = UPPER(LTRIM(RTRIM(@_cExpedientesNumero)))
	set @_cExpedientesDescripcion = UPPER(LTRIM(RTRIM(@_cExpedientesDescripcion)))
	set @_cExpedientesNumero = UPPER(LTRIM(RTRIM(@_cExpedientesNumero)))
	set @_cExpedienteDemandante = UPPER(LTRIM(RTRIM(@_cExpedienteDemandante)))
	set @_cExpedienteDemandado = UPPER(LTRIM(RTRIM(@_cExpedienteDemandado)))
	set @_cExpedienteMateria = UPPER(LTRIM(RTRIM(@_cExpedienteMateria)))
	set @_cExpedienteActoProcesal= UPPER(LTRIM(RTRIM(@_cExpedienteActoProcesal)))

	UPDATE ase.expedientes
	SET iSalasId = @_iSalasId,
		cExpedientesNombre = @_cExpedientesNombre,
		cExpedientesDescripcion = @_cExpedientesDescripcion,
		cExpedientesNumero = @_cExpedientesNumero,
		bExpedientesEstado = @_bExpedientesEstado,
		cExpedienteDemandante = @_cExpedienteDemandante,
		cExpedienteDemandado = @_cExpedienteDemandado,
		cExpedienteMateria = @_cExpedienteMateria,
		cExpedienteActoProcesal = @_cExpedienteActoProcesal,
		iTiposId = @_iTiposId,
		cExpedientesIdTramiteDoc= @_cExpedientesIdTramiteDoc,
		iEtapasId = @_iEtapasId,

		/*Campos de auditoria*/
		cExpedientesUsuarioSis=@cUsuarioSis,
		dtExpedientesFechaSis=GETDATE(),
		cExpedientesEquipoSis=@_cEquipoSis,
		cExpedientesIpSis=@_cIpSis,
		cExpedientesOpenUrs='E',
		cExpedientesMacNicSis=@_cMacNicSis
	WHERE iExpedientesId=@_iExpedientesId
	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@_iExpedientesId AS iExpedientesId
	RETURN @_iExpedientesId
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0