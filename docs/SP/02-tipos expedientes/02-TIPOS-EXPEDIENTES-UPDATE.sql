CREATE PROCEDURE [ase].[Sp_UPD_tipos_expedientes]
	@_iTiposId INTEGER,
	@_cTiposNombre varchar(50),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Actualización de Tipos Expedientes
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	IF @@ERROR<>0 GOTO ErrorCapturado

	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cTiposNombre=UPPER(RTRIM(LTRIM(@_cTiposNombre)))

	UPDATE ase.tipos_expedientes
	SET cTiposNombre = @_cTiposNombre,

		/*Campos de auditoria*/
		cTiposUsuarioSis=@cUsuarioSis,
		dtTiposFechaSis=GETDATE(),
		cTiposEquipoSis=@_cEquipoSis,
		cTiposIpSis=@_cIpSis,
		cTiposOpenUrs='E',
		cTiposMacNicSis=@_cMacNicSis
	WHERE iTiposId=@_iTiposId
	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@_iTiposId AS iTiposId
	RETURN @_iTiposId
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0