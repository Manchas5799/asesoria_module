
CREATE PROCEDURE [ase].[Sp_INS_citaciones]
	@_iCitacionesId INTEGER,
	@_iExpedientesId INTEGER,
	@_iPrioridadesId INTEGER,
	@_iFinalidadesId INTEGER,
	@_cCitacionesNumero varchar(100),
	@_dtCitacionesFechaEmision DATETIME,
	@_dtCitacionesFechaRecibido DATETIME,
	@_dtCitacionesFechaCitacion DATETIME,
	@_cCitacionesPretencioSumilla VARCHAR(max),
	@_cLugarCitacion VARCHAR(200),
	@_cCitacionesReferencia VARCHAR(200),
	@_cCitacionesResolucion VARCHAR(100),
	@_cCitacionesObservacion VARCHAR(max),
	@_dtCitacionesFechaobservacion DATETIME,
	@_iPersonaAsesoriaId INTEGER,
	@_cExpedientesIdTramiteDoc VARCHAR(20),
	@_iEtapasId INTEGER,

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Insertar Nueva CITACIÓN
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	
	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cCitacionesNumero=RTRIM(LTRIM(@_cCitacionesNumero))

	
	IF COALESCE(@_cCitacionesNumero,'') = ''
		BEGIN
			SET @cMensaje='Debe especificar el número de citación.'
			RAISERROR (@cMensaje,18,1,1)
			GOTO ErrorCapturado
		END
	IF @_cCitacionesNumero IS NOT NULL
		BEGIN
			IF EXISTS(SELECT iCitacionesId FROM ase.citaciones WHERE UPPER(RTRIM(LTRIM(cCitacionesNumero))) COLLATE SQL_Latin1_General_Cp1_CI_AI=UPPER(RTRIM(LTRIM(@_cCitacionesNumero))))
				BEGIN
					SET @cMensaje='La Citación '+UPPER(RTRIM(LTRIM(@_cCitacionesNumero)))+' ya se encuentra registrado, verifique por favor...'
					RAISERROR (@cMensaje,18,1,1)
					GOTO ErrorCapturado					
				END
		END

	
	DECLARE @iCitacionesId INTEGER,@iCodigoError INTEGER
	set @_cCitacionesNumero = UPPER(LTRIM(RTRIM(@_cCitacionesNumero)))
	set @_cCitacionesPretencioSumilla = UPPER(LTRIM(RTRIM(@_cCitacionesPretencioSumilla)))
	set @_cLugarCitacion = UPPER(LTRIM(RTRIM(@_cLugarCitacion)))
	set @_cCitacionesReferencia = UPPER(LTRIM(RTRIM(@_cCitacionesReferencia)))
	set @_cCitacionesResolucion = UPPER(LTRIM(RTRIM(@_cCitacionesResolucion)))
	set @_cCitacionesObservacion = UPPER(LTRIM(RTRIM(@_cCitacionesObservacion)))

	INSERT ase.citaciones
	(iExpedientesId, iPrioridadesId, iFinalidadesId, cCitacionesNumero, dtCitacionesFechaEmision, dtCitacionesFechaRecibido, dtCitacionesFechaCitacion, cCitacionesPretencionSumilla, cLugarCitacion, cCitacionesReferencia, cCitacionesResolucion, cCitacionesObservacion, dtCitacionesFechaObservacion, iPersonaAsesoriaId, cExpedientesIdTramiteDoc, iEtapasId, cCitacionesUsuarioSis, dtCitacionesFechaSis, cCitacionesEquipoSis, cCitacionesIpSis, cCitacionesOpenUrs,cCitacionesMacNicSis)
	VALUES ( @_iExpedientesId, @_iPrioridadesId, @_iFinalidadesId, @_cCitacionesNumero, @_dtCitacionesFechaEmision, @_dtCitacionesFechaRecibido, @_dtCitacionesFechaCitacion, @_cCitacionesPretencioSumilla, @_cLugarCitacion, @_cCitacionesReferencia, @_cCitacionesResolucion, @_cCitacionesObservacion, @_dtCitacionesFechaobservacion, @_iPersonaAsesoriaId, @_cExpedientesIdTramiteDoc, @_iEtapasId,
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	SELECT @iCitacionesId=@@IDENTITY,@iCodigoError=@@ERROR

	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@iCitacionesId AS iCitacionesId
	RETURN @iCitacionesId

ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0

select * from ase.citaciones