CREATE PROCEDURE ase.Sp_DEL_personas_casos
	@_iPersonasCasosId INTEGER
AS
BEGIN TRANSACTION
	SET NOCOUNT ON

	DELETE FROM ase.personas_casos WHERE iPersonasCasosId = @_iPersonasCasosId
	IF @@ERROR<>0 GOTO ErrorCapturado

	COMMIT TRANSACTION
	SELECT 1 AS iResult
	RETURN 1
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0

