ALTER PROCEDURE [ase].[Sp_INS_etapas]
	@_iEtapasId	 INTEGER,
	@_cEtapasNombre varchar(50),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cSalasOpenUrs varchar(1),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Insertar Nueva Etapa
* 
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	
	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cEtapasNombre=RTRIM(LTRIM(@_cEtapasNombre))

	
	IF COALESCE(@_cEtapasNombre,'') = ''
		BEGIN
			SET @cMensaje='Debe especificar el nombre de la etapa.'
			RAISERROR (@cMensaje,18,1,1)
			GOTO ErrorCapturado
		END
	IF @_cEtapasNombre IS NOT NULL
		BEGIN
			IF EXISTS(SELECT iEtapasId FROM ase.etapas WHERE UPPER(RTRIM(LTRIM(cEtapasNombre))) COLLATE SQL_Latin1_General_Cp1_CI_AI=UPPER(RTRIM(LTRIM(@_cEtapasNombre))))
				BEGIN
					SET @cMensaje='La etapa '+UPPER(RTRIM(LTRIM(@_cEtapasNombre)))+' ya se encuentra registrado, verifique por favor...'
					RAISERROR (@cMensaje,18,1,1)
					GOTO ErrorCapturado					
				END
		END

	
	DECLARE @iEtapasId INTEGER,@iCodigoError INTEGER
	set @_cEtapasNombre = UPPER(LTRIM(RTRIM(@_cEtapasNombre)))

	INSERT ase.etapas
	(cEtapasNombre, cEtapasUsuarioSis, dtEtapasFechaSis, cEtapasEquipoSis, cEtapasIpSis, cEtapasOpenUrs,cEtapasMAcNicSis)
	VALUES ( @_cEtapasNombre,
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	SELECT @iEtapasId=@@IDENTITY,@iCodigoError=@@ERROR

	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@iEtapasId AS iEtapasId
	RETURN @iEtapasId

ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0