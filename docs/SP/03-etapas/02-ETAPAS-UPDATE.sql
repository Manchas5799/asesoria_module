ALTER PROCEDURE [ase].[Sp_UPD_etapas]
	@_iEtapasId INTEGER,
	@_cEtapasNombre varchar(50),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Actualización de Etapas
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	IF @@ERROR<>0 GOTO ErrorCapturado

	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cEtapasNombre=UPPER(RTRIM(LTRIM(@_cEtapasNombre)))

	UPDATE ase.etapas
	SET cEtapasNombre = @_cEtapasNombre,

		/*Campos de auditoria*/
		cEtapasUsuarioSis=@cUsuarioSis,
		dtEtapasFechaSis=GETDATE(),
		cEtapasEquipoSis=@_cEquipoSis,
		cEtapasIpSis=@_cIpSis,
		cEtapasOpenUrs='E',
		cEtapasMacNicSis=@_cMacNicSis
	WHERE iEtapasId=@_iEtapasId
	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@_iEtapasId AS iEtapasId
	RETURN @_iEtapasId
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0