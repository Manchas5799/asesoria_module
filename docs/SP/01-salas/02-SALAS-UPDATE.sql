CREATE PROCEDURE [ase].[Sp_UPD_salas]
	@_iSAlasId INTEGER,
	@_cSalasNombre varchar(200),
	@_cSalasDireccion varchar(150),
	
	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Actualización de Salas
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	IF @@ERROR<>0 GOTO ErrorCapturado

	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cSalasNombre=UPPER(RTRIM(LTRIM(@_cSalasNombre)))

	UPDATE ase.salas
	SET cSalasNombre = UPPER(@_cSalasNombre),
		cSalasDireccion = UPPER(@_cSalasDireccion),

		/*Campos de auditoria*/
		cSalasUsuarioSis=@cUsuarioSis,
		dtSalasFechaSis=GETDATE(),
		cSalasEquipoSis=@_cEquipoSis,
		cSalasIpSis=@_cIpSis,
		cSalasOpenUrs='E',
		cSalasMacNicSis=@_cMacNicSis
	WHERE iSalasId=@_iSalasId
	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@_iSalasId AS iSalasId
	RETURN @_iSalasId
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0