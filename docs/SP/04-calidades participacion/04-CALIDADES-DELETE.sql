CREATE PROCEDURE [ase].[Sp_DEL_calidades_participacion]	
	@_iCalidadId INTEGER
AS
BEGIN TRANSACTION
	SET NOCOUNT ON

	DELETE FROM ase.calidades_participacion WHERE iCalidadesId = @_iCalidadId
	IF @@ERROR<>0 GOTO ErrorCapturado

	COMMIT TRANSACTION
	SELECT 1 AS iResult
	RETURN 1
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0