CREATE PROCEDURE [ase].[Sp_DEL_prioridades]	
	@_iPrioridadesId INTEGER
AS
BEGIN TRANSACTION
	SET NOCOUNT ON

	DELETE FROM ase.prioridades WHERE iPrioridadesId = @_iPrioridadesId
	IF @@ERROR<>0 GOTO ErrorCapturado

	COMMIT TRANSACTION
	SELECT 1 AS iResult
	RETURN 1
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0