ALTER PROCEDURE [ase].[Sp_UPD_prioridades]
	@_iPrioridadesId INTEGER,
	@_cPrioridadesNombre varchar(25),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cSalasOpenUrs varchar(1),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Actualización de Prioridades
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	IF @@ERROR<>0 GOTO ErrorCapturado

	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cPrioridadesNombre=UPPER(RTRIM(LTRIM(@_cPrioridadesNombre)))

	UPDATE ase.prioridades
	SET cPrioridadNombre = @_cPrioridadesNombre,

		/*Campos de auditoria*/
		cPrioridadesUsuarioSis=@cUsuarioSis,
		dtPrioridadesFechaSis=GETDATE(),
		cPrioridadesEquipoSis=@_cEquipoSis,
		cPrioridadesIpSis=@_cIpSis,
		cPrioridadesOpenUrs='E',
		cPrioridadesMacNicSis=@_cMacNicSis
	WHERE iPrioridadesId=@_iPrioridadesId
	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@_iprioridadesId AS iPrioridadesId
	RETURN @_iPrioridadesId
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0