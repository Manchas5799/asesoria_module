
CREATE PROCEDURE [ase].[Sp_UPD_expedientes]
	@_iExpedientesId INTEGER,
	@_iSalasId INTEGER,
	@_cExpedientesNombre varchar(250),
	@_cExpedientesNumero varchar(100),
	@_bExpedientesEstado bit,
	@_cExpedientesDescripcion varchar(max),
	@_cExpedientesAgraviado varchar(400),
	@_cExpedienteDelito varchar(200),
	@_iTiposId INTEGER,
	@_cExpedientesIdTramiteDoc varchar(20),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Actualización de Calidades
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	IF @@ERROR<>0 GOTO ErrorCapturado

	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cExpedientesNombre=UPPER(RTRIM(LTRIM(@_cExpedientesNombre)))

	UPDATE ase.expedientes
	SET cExpedientesNombre = @_cExpedientesNombre,
		cExpedientesNumero = @_cExpedientesNumero,
		iSalasId = @_iSalasId,
		bExpedientesEstado= @_bExpedientesEstado,
		cExpedientesDescripcion = @_cExpedientesDescripcion,
		cExpedienteAgraviado = @_cExpedientesAgraviado,
		cExpedienteDelito = @_cExpedienteDelito,
		iTiposId = @_iTiposId,
		cExpedientesIdTramiteDoc = @_cExpedientesIdTramiteDoc,

		/*Campos de auditoria*/
		cExpedientesUsuarioSis=@cUsuarioSis,
		dtExpedientesFechaSis=GETDATE(),
		cExpedientesEquipoSis=@_cEquipoSis,
		cExpedientesIpSis=@_cIpSis,
		cExpedientesOpenUrs='E',
		cExpedientesMacNicSis=@_cMacNicSis
	WHERE iExpedientesId=@_iExpedientesId
	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@_iExpedientesId AS iExpedientesId
	RETURN @_iExpedientesId
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0