
CREATE PROCEDURE [ase].[Sp_UPD_tipo_persona]
	@_iTipoId INTEGER,
	@_cTipoNombre varchar(50),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Actualización de Etapas
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	IF @@ERROR<>0 GOTO ErrorCapturado

	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cTipoNombre=UPPER(RTRIM(LTRIM(@_cTipoNombre)))

	UPDATE ase.tipo_persona
	SET cTipoNombre = @_cTipoNombre,

		/*Campos de auditoria*/
		cTipoUsuarioSis=@cUsuarioSis,
		dTipoFechaSis=GETDATE(),
		cTipoEquipoSis=@_cEquipoSis,
		cTipoIpSis=@_cIpSis,
		cTipoOpenUrs='E',
		cTipoMacNicSis=@_cMacNicSis
	WHERE iTipoId=@_iTipoId
	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@_iTipoId AS iTipoId
	RETURN @_iTipoId
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0