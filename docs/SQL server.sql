-------------------------------------------------------------
CREATE PROCEDURE [ase].[Sp_UPD_prioridades]
	@_iPrioridadesId INTEGER,
	@_cPrioridadesNombre varchar(25),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Actualización de Prioridades
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	IF @@ERROR<>0 GOTO ErrorCapturado

	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cPrioridadesNombre=UPPER(RTRIM(LTRIM(@_cPrioridadesNombre)))

	UPDATE ase.prioridades
	SET cPrioridadNombre = @_cPrioridadesNombre,

		/*Campos de auditoria*/
		cPrioridadesUsuarioSis=@cUsuarioSis,
		dtPrioridadesFechaSis=GETDATE(),
		cPrioridadesEquipoSis=@_cEquipoSis,
		cPrioridadesIpSis=@_cIpSis,
		cPrioridadesOpenUrs='E',
		cPrioridadesMacNicSis=@_cMacNicSis
	WHERE iPrioridadesId=@_iPrioridadesId
	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@_iprioridadesId AS iPrioridadesId
	RETURN @_iPrioridadesId
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0

	-----------------------------------------
	CREATE PROCEDURE [ase].[Sp_INS_finalidades]
	@_iFinalidadesId	 INTEGER,
	@_cFinalidadesNombre varchar(100),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Insertar Nueva Finalidad
* 
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	
	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cFinalidadesNombre=RTRIM(LTRIM(@_cFinalidadesNombre))

	
	IF COALESCE(@_cFinalidadesNombre,'') = ''
		BEGIN
			SET @cMensaje='Debe especificar el nombre de la finalidad.'
			RAISERROR (@cMensaje,18,1,1)
			GOTO ErrorCapturado
		END
	IF @_cFinalidadesNombre IS NOT NULL
		BEGIN
			IF EXISTS(SELECT iFinalidadesId FROM ase.finalidades WHERE UPPER(RTRIM(LTRIM(cFinalidadesNombre))) COLLATE SQL_Latin1_General_Cp1_CI_AI=UPPER(RTRIM(LTRIM(@_cFinalidadesNombre))))
				BEGIN
					SET @cMensaje='La finalidad '+UPPER(RTRIM(LTRIM(@_cFinalidadesNombre)))+' ya se encuentra registrada, verifique por favor...'
					RAISERROR (@cMensaje,18,1,1)
					GOTO ErrorCapturado					
				END
		END

	
	DECLARE @iFinalidadesId INTEGER,@iCodigoError INTEGER
	set @_cFinalidadesNombre = UPPER(@_cFinalidadesNombre)

	INSERT ase.finalidades
	(cFinalidadesNombre, cFinalidadesUsuarioSis, dtFinalidadesFechaSis, cFinalidadesEquipoSis, cFinalidadesIpSis, cFinalidadesOpenUrs,cFinalidadesMAcNicSis)
	VALUES ( @_cFinalidadesNombre,
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	SELECT @iFinalidadesId=@@IDENTITY,@iCodigoError=@@ERROR

	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@iFinalidadesId AS iFinalidadesId
	RETURN @iFinalidadesId

ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0

	----------------------------------------------------------------

ALTER PROCEDURE [ase].[Sp_UPD_finalidades]
	@_iFinalidadesId INTEGER,
	@_cFinalidadesNombre varchar(100),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Actualización de Finalidades
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	IF @@ERROR<>0 GOTO ErrorCapturado

	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cFinalidadesNombre=UPPER(RTRIM(LTRIM(@_cFinalidadesNombre)))

	UPDATE ase.finalidades
	SET cFinalidadesNombre = @_cfinalidadesNombre,

		/*Campos de auditoria*/
		cFinalidadesUsuarioSis=@cUsuarioSis,
		dtFinalidadesFechaSis=GETDATE(),
		cFinalidadesEquipoSis=@_cEquipoSis,
		cFinalidadesIpSis=@_cIpSis,
		cFinalidadesOpenUrs='E',
		cFinalidadesMacNicSis=@_cMacNicSis
	WHERE iFinalidadesId=@_iFinalidadesId
	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@_iFinalidadesId AS iFinalidadesId
	RETURN @_iFinalidadesId
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0