USE UNAM_2020
GO
ALTER PROCEDURE [ase].[Sp_SEL_expedientes]

AS
BEGIN
	SET NOCOUNT ON
	SELECT	[expe].[iExpedientesId] as id,
			[sa].[iSalasId] as salas_id,
			[sa].[cSalasNombre] as nombre_sala,
			[sa].[cSalasDireccion] as direccion_sala,
			[expe].[cExpedientesNombre] as nombre,
			[expe].[cExpedientesDescripcion] as descripcion,
			[expe].[cExpedientesNumero] as numero,
			[expe].[bExpedientesEstado] as estado,
			[expe].[cExpedientesDescripcion] as descripcion,
			[expe].[cExpedienteAgraviado] as agraviado,
			[expe].[cExpedienteDelito] as delito,
			[expe].[iEtapasId] as etapa_actual,
			[expe].[iTiposId] as tipos_id,
			[expe].[cExpedientesIdTramiteDoc] as tramite_doc_id,
			[te].[cTiposNombre] as nombre_tipo_expediente,
			(select top 1 
			ee.iEtapasId as etapa_id, 
			convert(DATE,ee.dtExpedienteFecha) as fecha_etapa,
			ee.cExpedientesMotivo as obs_etapa, 
			ee.iExpedienteEtapasId as id
			from ase.expedientes_etapas where ee.iExpedientesId = expe.iExpedientesId ORDER BY iExpedientesId DESC for JSON AUTO) as etapa,
			(select 
				pers.cPersPaterno + ' '+ pers.cPersMaterno + ', '+pers.cPersNombre as nombre,
				pc.iPersonaAsesoriaId as involucrado_persona_asesoria_id,
				pc.iPersonasCasosId as involucrado_id
				,pers.iPersId as idd
				,cali.cCalidadesNombre as involucrado_calidad_nombre
				,tipo.cTipoNombre as involucrado_tipo_nombre
			 from ase.personas_casos pc 
			LEFT JOIN ase.persona_asesoria pa ON pa.iPersonaAsesoriaId = pc.iPersonaAsesoriaId 
			LEFT JOIN grl.personas pers ON pers.iPersId = pa.iPersonaId
			LEFT JOIN ase.calidades_participacion cali ON pc.iCalidadesId = cali.iCalidadesId
			LEFT JOIN ase.tipo_persona tipo ON tipo.iTipoId = pa.iTipoId
			WHERE pc.iExpedientesId = expe.iExpedientesId /*3*/
			FOR JSON path) as involucrados
  FROM [ase].[expedientes] expe
  LEFT JOIN ase.salas sa ON expe.iSalasId=sa.iSalasId
  LEFT JOIN ase.tipos_expedientes te ON expe.iTiposId=te.itiposId
  LEFT JOIN ase.expedientes_etapas ee ON expe.iExpedientesId=ee.iExpedientesId
  LEFT JOIN ase.etapas e ON ee.iEtapasId=e.iEtapasId
END
GO


ALTER PROCEDURE [ase].[Sp_UPD_expedientes]
	@_iExpedientesId INTEGER,
	@_iSalasId INTEGER,
	@_cExpedientesNombre varchar(250),
	@_cExpedientesNumero varchar(100),
	@_bExpedientesEstado bit,
	@_cExpedientesDescripcion varchar(max),
	@_cExpedientesAgraviado varchar(400),
	@_cExpedienteDelito varchar(200),
	@_iTiposId INTEGER,
	@_cExpedientesIdTramiteDoc varchar(20),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Actualización de Calidades
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	IF @@ERROR<>0 GOTO ErrorCapturado

	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cExpedientesNombre=UPPER(RTRIM(LTRIM(@_cExpedientesNombre)))

	UPDATE ase.expedientes
	SET cExpedientesNombre = @_cExpedientesNombre,
		cExpedientesNumero = @_cExpedientesNumero,
		iSalasId = @_iSalasId,
		bExpedientesEstado= @_bExpedientesEstado,
		cExpedientesDescripcion = @_cExpedientesDescripcion,
		cExpedienteAgraviado = @_cExpedientesAgraviado,
		cExpedienteDelito = @_cExpedienteDelito,
		iTiposId = @_iTiposId,
		cExpedientesIdTramiteDoc = @_cExpedientesIdTramiteDoc,

		/*Campos de auditoria*/
		cExpedientesUsuarioSis=@cUsuarioSis,
		dtExpedientesFechaSis=GETDATE(),
		cExpedientesEquipoSis=@_cEquipoSis,
		cExpedientesIpSis=@_cIpSis,
		cExpedientesOpenUrs='E',
		cExpedientesMacNicSis=@_cMacNicSis
	WHERE iExpedientesId=@_iExpedientesId
	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@_iExpedientesId AS iExpedientesId
	RETURN @_iExpedientesId
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0

delete FROM ase.expedientes_etapas
delete FROM ase.personas_casos
delete FROM ase.expedientes
select iExpedienteEtapasId,iEtapasId, iExpedientesId from ase.expedientes_etapas

CREATE PROCEDURE [ase].[Sp_UPD_expedientes_y_etapas]
	@_iExpedientesId INTEGER,
	@_iSalasId INTEGER,
	@_cExpedientesNombre varchar(250),
	@_cExpedientesNumero varchar(100),
	@_bExpedientesEstado bit,
	@_cExpedientesDescripcion varchar(max),
	@_cExpedientesAgraviado varchar(400),
	@_cExpedienteDelito varchar(200),
	@_iTiposId INTEGER,
	@_cExpedientesIdTramiteDoc varchar(20),

	@_iExpedienteEtapasId INTEGER,
	@_iEtapasId INTEGER,
	@_dtExpedienteFecha DATETIME,
	@_cExpedientesMotivo VARCHAR(MAX),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Actualización de Calidades
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	IF @@ERROR<>0 GOTO ErrorCapturado

	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cExpedientesNombre=UPPER(RTRIM(LTRIM(@_cExpedientesNombre)))

	UPDATE ase.expedientes
	SET cExpedientesNombre = @_cExpedientesNombre,
		cExpedientesNumero = @_cExpedientesNumero,
		iSalasId = @_iSalasId,
		bExpedientesEstado= @_bExpedientesEstado,
		cExpedientesDescripcion = @_cExpedientesDescripcion,
		cExpedienteAgraviado = @_cExpedientesAgraviado,
		cExpedienteDelito = @_cExpedienteDelito,
		iTiposId = @_iTiposId,
		cExpedientesIdTramiteDoc = @_cExpedientesIdTramiteDoc,

		/*Campos de auditoria*/
		cExpedientesUsuarioSis=@cUsuarioSis,
		dtExpedientesFechaSis=GETDATE(),
		cExpedientesEquipoSis=@_cEquipoSis,
		cExpedientesIpSis=@_cIpSis,
		cExpedientesOpenUrs='E',
		cExpedientesMacNicSis=@_cMacNicSis
	WHERE iExpedientesId=@_iExpedientesId

	UPDATE ase.expedientes_etapas
	SET iExpedientesId = @_iExpedientesId,
		iEtapasId = @_iEtapasId,
		dtExpedienteFecha = @_dtExpedienteFecha,
		cExpedientesMotivo= @_cExpedientesMotivo,

		/*Campos de auditoria*/
		cEtapasUsuarioSis=@cUsuarioSis,
		dtEtapasFechaSis=GETDATE(),
		cEtapasEquipoSis=@_cEquipoSis,
		cEtapasIpSis=@_cIpSis,
		cEtapasOpenUrs='E',
		cEtapasMacNicSis=@_cMacNicSis
	WHERE iExpedienteEtapasId=@_iExpedienteEtapasId

	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@_iExpedientesId AS iExpedientesId
	RETURN @_iExpedientesId
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0