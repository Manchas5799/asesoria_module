ALTER PROCEDURE [ase].[Sp_INS_prioridades]
	@_iPrioridadesId	 INTEGER,
	@_cPrioridadesNombre varchar(25),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Insertar Nueva Prioridad
* 
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	
	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cPrioridadesNombre=RTRIM(LTRIM(@_cPrioridadesNombre))

	
	IF COALESCE(@_cPrioridadesNombre,'') = ''
		BEGIN
			SET @cMensaje='Debe especificar el nombre de la prioridad.'
			RAISERROR (@cMensaje,18,1,1)
			GOTO ErrorCapturado
		END
	IF @_cPrioridadesNombre IS NOT NULL
		BEGIN
			IF EXISTS(SELECT iPrioridadesId FROM ase.Prioridades WHERE UPPER(RTRIM(LTRIM(cPrioridadNombre))) COLLATE SQL_Latin1_General_Cp1_CI_AI=UPPER(RTRIM(LTRIM(@_cPrioridadesNombre))))
				BEGIN
					SET @cMensaje='La prioridad '+UPPER(RTRIM(LTRIM(@_cPrioridadesNombre)))+' ya se encuentra registrada, verifique por favor...'
					RAISERROR (@cMensaje,18,1,1)
					GOTO ErrorCapturado					
				END
		END

	
	DECLARE @iPrioridadesId INTEGER,@iCodigoError INTEGER
	set @_cPrioridadesNombre = UPPER(LTRIM(RTRIM(@_cPrioridadesNombre)))

	INSERT ase.prioridades
	(cPrioridadNombre, cPrioridadesUsuarioSis, dtPrioridadesFechaSis, cPrioridadesEquipoSis, cPrioridadesIpSis, cPrioridadesOpenUrs,cPrioridadesMAcNicSis)
	VALUES ( @_cPrioridadesNombre,
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	SELECT @iPrioridadesId=@@IDENTITY,@iCodigoError=@@ERROR

	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@iPrioridadesId AS iPrioridadesId
	RETURN @iPrioridadesId

ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0