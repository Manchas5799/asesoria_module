CREATE PROCEDURE [ase].[Sp_UPD_materias]
	@_iMateriasId INTEGER,
	@_cMateriasNombre varchar(200),
	@_iTiposId INTEGER,
	
	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Actualización de Materias
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	IF @@ERROR<>0 GOTO ErrorCapturado

	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cMateriasNombre=UPPER(RTRIM(LTRIM(@_cMateriasNombre)))

	UPDATE ase.materias
	SET cMateriasNombre = UPPER(LTRIM(RTRIM(@_cMateriasNombre))),
		iTiposId = @_iTiposId,

		/*Campos de auditoria*/
		cMateriasUsuarioSis=@cUsuarioSis,
		dtMateriasFechaSis=GETDATE(),
		cMateriasEquipoSis=@_cEquipoSis,
		cMateriasIpSis=@_cIpSis,
		cMateriasOpenUrs='E',
		cMateriasMacNicSis=@_cMacNicSis
	WHERE iMateriasId=@_iMateriasId
	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@_iMateriasId AS iMateriasId
	RETURN @_iMateriasId
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0