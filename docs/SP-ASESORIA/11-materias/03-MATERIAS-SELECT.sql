CREATE PROCEDURE [ase].[Sp_SEL_materias]

AS
BEGIN
	SET NOCOUNT ON
	SELECT materias.iMateriasId as materia_id,
      materias.cMateriasNombre as materia_nombre,
      materias.iTiposId as tipo_id
	  
  FROM [ase].materias materias
END