
CREATE PROCEDURE [ase].[Sp_INS_materias]
	@_iMateriasId	 INTEGER,
	@_cMateriasNombre varchar(50),
	@_iTiposId INTEGER,

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Insertar Nueva Materia
* 
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	
	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cMateriasNombre=RTRIM(LTRIM(@_cMateriasNombre))

	
	IF COALESCE(@_cMateriasNombre,'') = ''
		BEGIN
			SET @cMensaje='Debe especificar el nombre de la MATERIA.'
			RAISERROR (@cMensaje,18,1,1)
			GOTO ErrorCapturado
		END
	IF @_cMateriasNombre IS NOT NULL
		BEGIN
			IF EXISTS(SELECT iMateriasId FROM ase.materias WHERE UPPER(RTRIM(LTRIM(cMateriasNombre))) COLLATE SQL_Latin1_General_Cp1_CI_AI=UPPER(RTRIM(LTRIM(@_cMateriasNombre))))
				BEGIN
					SET @cMensaje='La materia '+UPPER(RTRIM(LTRIM(@_cMateriasNombre)))+' ya se encuentra registrado, verifique por favor...'
					RAISERROR (@cMensaje,18,1,1)
					GOTO ErrorCapturado					
				END
		END

	
	DECLARE @iMAteriasId INTEGER,@iCodigoError INTEGER
	
	set @_cMateriasNombre = UPPER(LTRIM(RTRIM(@_cMateriasNombre)))

	INSERT ase.materias
	(cMateriasNombre, iTiposId,cMateriasUsuarioSis, dtMateriasFechaSis, cMateriasEquipoSis, cMateriasIpSis, cMateriasOpenUrs,cMateriasMAcNicSis)
	VALUES ( @_cMateriasNombre, @_iTiposId, 
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	SELECT @iMateriasId=@@IDENTITY,@iCodigoError=@@ERROR

	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@iMateriasId AS iMateriasId
	RETURN @iMateriasId

ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0