CREATE PROCEDURE [ase].[Sp_DEL_materias]	
	@_iMateriasId INTEGER
AS
BEGIN TRANSACTION
	SET NOCOUNT ON

	DELETE FROM ase.materias WHERE iMateriasId = @_iMateriasId
	IF @@ERROR<>0 GOTO ErrorCapturado

	COMMIT TRANSACTION
	SELECT 1 AS iResult
	RETURN 1
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0