CREATE PROCEDURE [ase].[Sp_DEL_tipos_expedientes]	
	@_iExpedienteId INTEGER
AS
BEGIN TRANSACTION
	SET NOCOUNT ON

	DELETE FROM ase.tipos_expedientes WHERE iTiposId = @_iExpedienteId
	IF @@ERROR<>0 GOTO ErrorCapturado

	COMMIT TRANSACTION
	SELECT 1 AS iResult
	RETURN 1
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0