
CREATE PROCEDURE [ase].[Sp_SEL_tipos_expedientes]

AS
BEGIN
	SET NOCOUNT ON
	SELECT [iTiposId] as id
      ,[cTiposNombre] as nombre
      
  FROM [ase].[tipos_expedientes]
END
