ALTER PROCEDURE [ase].[Sp_INS_calidades_participacion]
	@_iCalidadesId	 INTEGER,
	@_cCalidadesNombre varchar(50),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Insertar Nueva Calidades
* 
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	
	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cCalidadesNombre=RTRIM(LTRIM(@_cCalidadesNombre))

	
	IF COALESCE(@_cCalidadesNombre,'') = ''
		BEGIN
			SET @cMensaje='Debe especificar el nombre de la calidad.'
			RAISERROR (@cMensaje,18,1,1)
			GOTO ErrorCapturado
		END
	IF @_cCalidadesNombre IS NOT NULL
		BEGIN
			IF EXISTS(SELECT iCalidadesId FROM ase.calidades_participacion WHERE UPPER(RTRIM(LTRIM(cCalidadesNombre))) COLLATE SQL_Latin1_General_Cp1_CI_AI=UPPER(RTRIM(LTRIM(@_cCalidadesNombre))))
				BEGIN
					SET @cMensaje='La calidad '+UPPER(RTRIM(LTRIM(@_cCalidadesNombre)))+' ya se encuentra registrada, verifique por favor...'
					RAISERROR (@cMensaje,18,1,1)
					GOTO ErrorCapturado					
				END
		END

	
	DECLARE @iCalidadesId INTEGER,@iCodigoError INTEGER
	set @_cCalidadesNombre = UPPER(LTRIM(RTRIM(@_cCalidadesNombre)))

	INSERT ase.calidades_participacion
	(cCalidadesNombre, cCalidadesUsuarioSis, dtCalidadesFechaSis, cCalidadesEquipoSis, cCalidadesIpSis, cCalidadesOpenUrs,cCalidadesMAcNicSis)
	VALUES ( @_cCalidadesNombre,
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	SELECT @iCalidadesId=@@IDENTITY,@iCodigoError=@@ERROR

	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@iCalidadesId AS iCalidadesId
	RETURN @iCalidadesId

ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0