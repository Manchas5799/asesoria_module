CREATE PROCEDURE [ase].[Sp_DEL_citaciones]	
	@_iCitacionesId INTEGER
AS
BEGIN TRANSACTION
	SET NOCOUNT ON

	DELETE FROM ase.citaciones WHERE iCitacionesId = @_iCitacionesId
	IF @@ERROR<>0 GOTO ErrorCapturado

	COMMIT TRANSACTION
	SELECT 1 AS iResult
	RETURN 1
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0