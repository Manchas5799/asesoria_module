CREATE PROCEDURE [ase].[Sp_UPD_citaciones]
	@_iCitacionesId INTEGER,
	@_iExpedientesId INTEGER,
	@_iPrioridadesId INTEGER,
	@_iFinalidadesId INTEGER,
	@_cCitacionesNumero varchar(100),
	@_dtCitacionesFechaEmision DATETIME,
	@_dtCitacionesFechaRecibido DATETIME,
	@_dtCitacionesFechaCitacion DATETIME,
	@_cCitacionesPretencionSumilla VARCHAR(max),
	@_cLugarCitacion VARCHAR(200),
	@_cCitacionesReferencia VARCHAR(200),
	@_cCitacionesResolucion VARCHAR(100),
	@_cCitacionesObservacion VARCHAR(max),
	@_dtCitacionesFechaobservacion DATETIME,
	@_iPersonaAsesoriaId INTEGER,
	@_cExpedientesIdTramiteDoc VARCHAR(20),
	@_iEtapasId INTEGER,
	
	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Actualización de Salas
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	IF @@ERROR<>0 GOTO ErrorCapturado

	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cCitacionesNumero=UPPER(RTRIM(LTRIM(@_cCitacionesNumero)))

	UPDATE ase.citaciones
	SET iExpedientesId = UPPER(LTRIM(RTRIM(@_iExpedientesId))),
		iPrioridadesId = UPPER(LTRIM(RTRIM(@_iPrioridadesId))),
		iFinalidadesId = UPPER(LTRIM(RTRIM(@_iFinalidadesId))),
		cCitacionesNumero = UPPER(LTRIM(RTRIM(@_cCitacionesNumero))),
		dtCitacionesFechaEmision = UPPER(LTRIM(RTRIM(@_dtCitacionesFechaEmision))),
		dtCitacionesFechaRecibido = UPPER(LTRIM(RTRIM(@_dtCitacionesFechaRecibido))),
		dtCitacionesFechaCitacion = UPPER(LTRIM(RTRIM(@_dtCitacionesFechaCitacion))),
		cCitacionesPretencionSumilla = UPPER(LTRIM(RTRIM(@_cCitacionesPretencionSumilla))),
		cLugarCitacion = UPPER(LTRIM(RTRIM(@_cLugarCitacion))),
		cCitacionesReferencia = UPPER(LTRIM(RTRIM(@_cCitacionesReferencia))),
		cCitacionesResolucion = UPPER(LTRIM(RTRIM(@_cCitacionesResolucion))),
		cCitacionesObservacion = UPPER(LTRIM(RTRIM(@_cCitacionesObservacion))),
		dtCitacionesFechaObservacion = UPPER(LTRIM(RTRIM(@_dtCitacionesFechaObservacion))),
		iPersonaAsesoriaId = UPPER(LTRIM(RTRIM(@_iPersonaAsesoriaId))),
		cExpedientesIdTramiteDoc = UPPER(LTRIM(RTRIM(@_cExpedientesIdTramiteDoc))),
		iEtapasId = UPPER(LTRIM(RTRIM(@_iEtapasId))),

		/*Campos de auditoria*/
		cCitacionesUsuarioSis=@cUsuarioSis,
		dtCitacionesFechaSis=GETDATE(),
		cCitacionesEquipoSis=@_cEquipoSis,
		cCitacionesIpSis=@_cIpSis,
		cCitacionesOpenUrs='E',
		cCitacionesMacNicSis=@_cMacNicSis
	WHERE iCitacionesId=@_iCitacionesId
	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@_iCitacionesId AS iCitacionesId
	RETURN @_iCitacionesId
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0