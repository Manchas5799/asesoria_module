USE [UNAM_05072020]
GO
/****** Object:  StoredProcedure [ase].[SP_SEL_citaciones]    Script Date: 6/09/2020 8:40:21 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [ase].[SP_SEL_citaciones]
@fecha1 DATE=null, @fecha2 DATE=null
as
begin

	select	ci.iCitacionesid as citacion_id,
			ci.cCitacionesNumero as citacion_numero,
			ci.dtCitacionesFechaEmision as citacion_fecha_emision,
			ci.iExpedientesId as expediente_id,
			ex.cExpedientesNombre as expediente_nombre,
			ci.iPrioridadesId as prioridad_id,
			pri.cPrioridadNombre as prioridad_nombre,
			ci.dtCitacionesFechaRecibido as fecha_recibido,
			ci.dtCitacionesFechaCitacion as fecha_citacion,
			ci.cLugarCitacion as lugar_citacion,
			ci.iFinalidadesId as finalidad_id,
			fin.cFinalidadesNombre as finalidad_nombre,
			ci.cCitacionesReferencia as citacion_referencia,
			ci.cCitacionesObservacion as citacion_observacion,
			ci.dtCitacionesFechaObservacion as fecha_observacion,
			ci.iPersonaAsesoriaId,
			/*SQ-1- Obtenemos la persona asiganada a la citación*/
			(select pers.cPersPaterno + ' '+ pers.cPersMaterno + ', '+pers.cPersNombre as nombre,
					pa.iPersonaAsesoriaId as involucrado_persona_asesoria_id,
					pers.iPersId as idd,
					tipo.cTipoNombre as involucrado_tipo_nombre
				 from ase.persona_asesoria pa 
				LEFT JOIN grl.personas pers ON pers.iPersId = pa.iPersonaId
				LEFT JOIN ase.tipo_persona tipo ON tipo.iTipoId = pa.iTipoId
				WHERE pa.iPersonaAsesoriaId = ci.iPersonaAsesoriaId /*3*/
				FOR JSON path) as involucrado,
			/*End SBq-1*/
			ci.cExpedientesIdTramiteDoc as tramitedoc_id,
			ci.iEtapasId as etapa_id,
			eta.cEtapasNombre as etapa_nombre,
			ci.cCitacionesPretencionSumilla as sumilla,
			ci.cCitacionesResolucion as resolucion

	from ase.citaciones ci
	left join ase.expedientes ex ON ci.iExpedientesId = ex.iExpedientesId
	left join ase.prioridades pri ON ci.iPrioridadesId =pri.iPrioridadesId
	left join ase.finalidades fin ON ci.iFinalidadesId= fin.iFinalidadesId
	left join ase.persona_asesoria ps ON ci.iPersonaAsesoriaId = ps.iPersonaAsesoriaId
	left join ase.etapas eta ON ci.iEtapasId = eta.iEtapasId
	where CAST(ci.dtCitacionesFechaCitacion AS date) BETWEEN @fecha1 AND @fecha2

end