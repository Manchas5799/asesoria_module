ALTER PROCEDURE [ase].[Sp_INS_salas]
	@_iSalasId	 INTEGER,
	@_cSalasNombre varchar(200),
	@_cSalasDireccion varchar(150),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Insertar Nuevo Salas
* 
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	
	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cSalasNombre=RTRIM(LTRIM(@_cSalasNombre))

	
	IF COALESCE(@_cSalasNombre,'') = ''
		BEGIN
			SET @cMensaje='Debe especificar el nombre de la sala.'
			RAISERROR (@cMensaje,18,1,1)
			GOTO ErrorCapturado
		END
	IF @_cSalasNombre IS NOT NULL
		BEGIN
			IF EXISTS(SELECT iSalasId FROM ase.salas WHERE UPPER(RTRIM(LTRIM(cSalasNombre))) COLLATE SQL_Latin1_General_Cp1_CI_AI=UPPER(RTRIM(LTRIM(@_cSalasNombre))))
				BEGIN
					SET @cMensaje='La sala '+UPPER(RTRIM(LTRIM(@_cSalasNombre)))+' ya se encuentra registrado, verifique por favor...'
					RAISERROR (@cMensaje,18,1,1)
					GOTO ErrorCapturado					
				END
		END

	
	DECLARE @iSalasId INTEGER,@iCodigoError INTEGER
	set @_cSalasNombre = UPPER(@_cSalasNombre)

	INSERT ase.salas
	(cSalasNombre, cSalasDireccion, cSalasUsuarioSis, dtSalasFechaSis, cSalasEquipoSis, cSalasIpSis, cSalasOpenUrs,cSalasMAcNicSis)
	VALUES ( UPPER(@_cSalasNombre),UPPER(@_cSalasDireccion),
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	SELECT @iSalasId=@@IDENTITY,@iCodigoError=@@ERROR

	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@iSalasId AS iSalasId
	RETURN @iSalasId

ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0