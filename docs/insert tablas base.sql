--INSERTAR SALAS
insert into ase.salas(cSalasNombre,cSalasDireccion) values('SALA1','DIRECCIÓN SALA1');
insert into ase.salas(cSalasNombre,cSalasDireccion) values('SALA2','DIRECCIÓN SALA2');
insert into ase.salas(cSalasNombre,cSalasDireccion) values('SALA3','DIRECCIÓN SALA3');
insert into ase.salas(cSalasNombre,cSalasDireccion) values('SALA4','DIRECCIÓN SALA4');

--INSERTAR TIPOS EXPEDIENTES
insert into ase.tipos_expedientes(cTiposNombre) values('TIPO EXPEDIENTE1');
insert into ase.tipos_expedientes(cTiposNombre) values('TIPO EXPEDIENTE2');
insert into ase.tipos_expedientes(cTiposNombre) values('TIPO EXPEDIENTE3');
insert into ase.tipos_expedientes(cTiposNombre) values('TIPO EXPEDIENTE4');

--INSERTAR PRIORIDADES
insert into ase.prioridades(cPrioridadNombre) values('PRIORIDAD1');
insert into ase.prioridades(cPrioridadNombre) values('PRIORIDAD2');
insert into ase.prioridades(cPrioridadNombre) values('PRIORIDAD3');
insert into ase.prioridades(cPrioridadNombre) values('PRIORIDAD4');

--INSERTAR FINALIDADES
insert into ase.finalidades(cFinalidadesNombre) values('FINALIDAD1');
insert into ase.finalidades(cFinalidadesNombre) values('FINALIDAD2');
insert into ase.finalidades(cFinalidadesNombre) values('FINALIDAD3');
insert into ase.finalidades(cFinalidadesNombre) values('FINALIDAD4');

--INSERTAR ETAPAS
insert into ase.etapas(cEtapasNombre) values('ETAPA1');
insert into ase.etapas(cEtapasNombre) values('ETAPA2');
insert into ase.etapas(cEtapasNombre) values('ETAPA3');
insert into ase.etapas(cEtapasNombre) values('ETAPA4');

--INSERTAR CALIDADES PARTICIPACI�N 
insert into ase.calidades_participacion(cCalidadesNombre) values('CALIDAD PARTICIAPACIÓN1');
insert into ase.calidades_participacion(cCalidadesNombre) values('CALIDAD PARTICIAPACIÓN2');
insert into ase.calidades_participacion(cCalidadesNombre) values('CALIDAD PARTICIAPACIÓN3');
insert into ase.calidades_participacion(cCalidadesNombre) values('CALIDAD PARTICIAPACIÓN4');

--INSERTAR TIPO PERSONA 
insert into ase.tipo_persona(cTipoNombre) values('TPERSONA1');
insert into ase.tipo_persona(cTipoNombre) values('TPERSONA2');
insert into ase.tipo_persona(cTipoNombre) values('TPERSONA3');
insert into ase.tipo_persona(cTipoNombre) values('TPERSONA4');