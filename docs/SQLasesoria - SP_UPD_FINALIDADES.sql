CREATE PROCEDURE [ase].[Sp_UPD_finalidades]
	@_iFinalidadesId INTEGER,
	@_cFinalidadesNombre varchar(100),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Actualización de Finalidades
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	IF @@ERROR<>0 GOTO ErrorCapturado

	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cFinalidadesNombre=UPPER(RTRIM(LTRIM(@_cFinalidadesNombre)))

	UPDATE ase.finalidades
	SET cFinalidadesNombre = @_cfinalidadesNombre,

		/*Campos de auditoria*/
		cFinalidadesUsuarioSis=@cUsuarioSis,
		dtFinalidadesFechaSis=GETDATE(),
		cFinalidadesEquipoSis=@_cEquipoSis,
		cFinalidadesIpSis=@_cIpSis,
		cFinalidadesOpenUrs='E',
		cFinalidadesMacNicSis=@_cMacNicSis
	WHERE iFinalidadesId=@_iFinalidadesId
	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@_iFinalidadesId AS iFinalidadesId
	RETURN @_iFinalidadesId
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0