USE [UNAM_05072020]
GO
/****** Object:  StoredProcedure [ase].[Sp_INS_expedientes]    Script Date: 5/08/2020 10:58:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [ase].[Sp_INS_expedientes]
	@_iExpedientesId INTEGER,
	@_iSalasId INTEGER,
	@_cExpedientesNombre varchar(250),
	@_cExpedientesNumero varchar(100),
	@_bExpedientesEstado bit,
	@_cExpedienteDescripcion varchar(max),
	@_cExpedientesAgraviado varchar(400),
	@_cExpedienteDelito varchar(200),
	@_iTiposId INTEGER,
	@_cExpedientesIdTramiteDoc varchar(20),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Insertar Nuevo EXPEDIENTE
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	
	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cExpedientesNumero=RTRIM(LTRIM(@_cExpedientesNumero))

	
	IF COALESCE(@_cExpedientesNumero,'') = ''
		BEGIN
			SET @cMensaje='Debe especificar el número de expediente.'
			RAISERROR (@cMensaje,18,1,1)
			GOTO ErrorCapturado
		END
	IF @_cExpedientesNumero IS NOT NULL
		BEGIN
			IF EXISTS(SELECT iExpedientesId FROM ase.expedientes WHERE UPPER(RTRIM(LTRIM(cExpedientesNumero))) COLLATE SQL_Latin1_General_Cp1_CI_AI=UPPER(RTRIM(LTRIM(@_cExpedientesNumero))))
				BEGIN
					SET @cMensaje='El Expediente '+UPPER(RTRIM(LTRIM(@_cExpedientesNumero)))+' ya se encuentra registrado, verifique por favor...'
					RAISERROR (@cMensaje,18,1,1)
					GOTO ErrorCapturado					
				END
		END

	
	DECLARE @iExpedientesId INTEGER,@iCodigoError INTEGER
	set @_cExpedientesNumero = UPPER(@_cExpedientesNumero)

	INSERT ase.expedientes
	(iSalasId, cExpedientesNombre, cExpedientesNumero, bExpedientesEstado, cExpedientesDescripcion, cExpedienteAgraviado, cExpedienteDelito, iTiposId, cExpedientesIdTramiteDoc, cExpedientesUsuarioSis, dtExpedientesFechaSis, cExpedientesEquipoSis, cExpedientesIpSis, cExpedientesOpenUrs,cExpedientesMacNicSis)
	VALUES ( @_iSalasId,@_cExpedientesNombre, @_cExpedientesNumero, @_bExpedientesEstado, @_cExpedienteDescripcion, @_cExpedientesAgraviado, @_cExpedientesAgraviado, @_iTiposId, @_cExpedientesIdTramiteDoc,
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	SELECT @iExpedientesId=@@IDENTITY,@iCodigoError=@@ERROR

	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@iExpedientesId AS iExpedientesId
	RETURN @iExpedientesId

ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0