CREATE PROCEDURE [ase].[Sp_SEL_expedientes]

AS
BEGIN
	SET NOCOUNT ON
	SELECT	[exp].[iExpedientesId] as id,
			[sa].[iSalasId] as salas_id,
			[sa].[cSalasNombre] as Sala,
			[sa].[cSalasDireccion] as direccion_sala,
			[exp].[cExpedientesNombre] as nombre,
			[exp].[cExpedientesDescripcion] as descripcion,
			[exp].[cExpedientesNumero] as numero,
			[exp].[bExpedientesEstado] as estado

  FROM [ase].[expedientes] exp
  LEFT JOIN ase.salas sa ON exp.iSalasId=sa.iSalasId
END