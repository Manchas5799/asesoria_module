USE [UNAM_05072020]
GO
/****** Object:  StoredProcedure [ase].[Sp_SEL_expedientes]    Script Date: 6/08/2020 7:04:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [ase].[Sp_SEL_expedientes]

AS
BEGIN
	SET NOCOUNT ON
	SELECT	[expe].[iExpedientesId] as id,
			[sa].[iSalasId] as salas_id,
			[sa].[cSalasNombre] as Sala,
			[sa].[cSalasDireccion] as direccion_sala,
			[expe].[cExpedientesNombre] as nombre,
			[expe].[cExpedientesDescripcion] as descripcion,
			[expe].[cExpedientesNumero] as numero,
			[expe].[bExpedientesEstado] as estado,
			[expe].[cExpedientesDescripcion] as descripcion,
			[expe].[cExpedienteAgraviado] as agraviado,
			[expe].[cExpedienteDelito] as delito,
			[expe].[iTiposId] as tipos_id,
			[expe].[cExpedientesIdTramiteDoc] as tramite_doc_id

  FROM [ase].[expedientes] expe
  LEFT JOIN ase.salas sa ON expe.iSalasId=sa.iSalasId
  LEFT JOIN ase.tipos_expedientes te ON expe.iTiposId=te.itiposId
END

/***********************************************/