CREATE PROCEDURE [ase].[Sp_UPD_calidades_participacion]
	@_iCalidadesId INTEGER,
	@_cCalidadesNombre varchar(100),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Actualizaci�n de Calidades
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	IF @@ERROR<>0 GOTO ErrorCapturado

	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cCalidadesNombre=UPPER(RTRIM(LTRIM(@_cCalidadesNombre)))

	UPDATE ase.calidades_participacion
	SET cCalidadesNombre = @_cCalidadesNombre,

		/*Campos de auditoria*/
		cCalidadesUsuarioSis=@cUsuarioSis,
		dtCalidadesFechaSis=GETDATE(),
		cCalidadesEquipoSis=@_cEquipoSis,
		cCalidadesIpSis=@_cIpSis,
		cCalidadesOpenUrs='E',
		cCalidadesMacNicSis=@_cMacNicSis
	WHERE iCalidadesId=@_iCalidadesId
	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@_iCalidadesId AS iCalidadesId
	RETURN @_iCalidadesId
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0