USE [UNAM_2020]
GO
/****** Object:  StoredProcedure [ase].[Sp_INS_persona_asesoria]    Script Date: 8/08/2020 9:43:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [ase].[Sp_INS_persona_asesoria]
	@_iPersonaAsesoriaId	 INTEGER,
	@_iTipoId INTEGER,
	@_iPersonaId INTEGER,

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Insertar Nueva Persona Asesoria
* 
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	
	DECLARE @cMensaje VARCHAR(MAX)

	SET @_iPersonaId=RTRIM(LTRIM(@_iPersonaId))

	
	IF COALESCE(@_iPersonaId,'') = ''
		BEGIN
			SET @cMensaje='Debe especificar la persona.'
			RAISERROR (@cMensaje,18,1,1)
			GOTO ErrorCapturado
		END
	IF @_iPersonaId IS NOT NULL
		BEGIN
			IF EXISTS(SELECT iPersonaAsesoriaId FROM ase.persona_asesoria WHERE UPPER(RTRIM(LTRIM(iPersonaId))) COLLATE SQL_Latin1_General_Cp1_CI_AI=UPPER(RTRIM(LTRIM(@_iPersonaId))))
				BEGIN
					SET @cMensaje='La persona '+UPPER(RTRIM(LTRIM(@_iPersonaId)))+' ya se encuentra registrada, verifique por favor...'
					RAISERROR (@cMensaje,18,1,1)
					GOTO ErrorCapturado					
				END
		END

	
	DECLARE @iPersonaAsesoriaId INTEGER,@iCodigoError INTEGER
	set @_iPersonaId = UPPER(@_iPersonaId)

	INSERT ase.persona_asesoria
	(iTipoId, iPersonaId,cPersonaAsesoriaUsuarioSis, dtPersonaAsesoriaFechaSis, cPersonaAsesoriaEquipoSis, cPersonaAsesoriaIpSis, cPersonaAsesoriaOpenUrs,cPersonaAsesoriaMAcNicSis)
	VALUES ( @_iTipoId, @_iPersonaId,
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	SELECT @iPersonaAsesoriaId=@@IDENTITY,@iCodigoError=@@ERROR

	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@iPersonaAsesoriaId AS iPersonaAsesoriaId
	RETURN @iPersonaAsesoriaId

ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0