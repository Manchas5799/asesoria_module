CREATE PROCEDURE [ase].[Sp_SEL_Salas]

AS
BEGIN
	SET NOCOUNT ON
	SELECT salas.[iSalasId] as id,
      salas.[cSalasNombre] as nombre,
      salas.[cSalasDireccion] as direccion
	  
  FROM [ase].[salas] salas
END