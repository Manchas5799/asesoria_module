CREATE PROCEDURE [ase].[Sp_INS_expedientes_etapas]
	@_iExpedientesEtapasId	 INTEGER,
	@_iExpedientesId	 INTEGER,
	@_iEtapasId	 INTEGER,
	@_dtExpedienteFecha DATETIME,
	@_ExpedientesMotivo varchar(MAX),
	@_cExpedientesIdTramiteDoc varchar(20),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Insertar nueva etapa de expediente --> pasar el expediente a otra etapa
* 
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	
	DECLARE @cMensaje VARCHAR(MAX)
	/*
	SET @_cExpedientesIdTramiteDoc=RTRIM(LTRIM(@_cExpedientesIdTramiteDoc))

	IF COALESCE(@_cExpedientesIdTramiteDoc,'') = ''
		BEGIN
			SET @cMensaje='Debe especificar el codigo de tramite documentario.'
			RAISERROR (@cMensaje,18,1,1)
			GOTO ErrorCapturado
		END
	IF @_cExpedientesIdTramiteDoc IS NOT NULL
		BEGIN
			IF EXISTS(SELECT iExpedienteEtapasId FROM ase.expedientes_etapas WHERE UPPER(RTRIM(LTRIM(cExpedientesIdTramiteDoc))) COLLATE SQL_Latin1_General_Cp1_CI_AI=UPPER(RTRIM(LTRIM(@_cExpedientesIdTramiteDoc))))
				BEGIN
					SET @cMensaje='El tramite '+UPPER(RTRIM(LTRIM(@_cExpedientesIdTramiteDoc)))+' ya se encuentra registrado, verifique por favor...'
					RAISERROR (@cMensaje,18,1,1)
					GOTO ErrorCapturado					
				END
		END
	*/
	
	DECLARE @iExpedienteEtapasId INTEGER,@iCodigoError INTEGER
	--set @_cSalasNombre = UPPER(@_cSalasNombre)

	INSERT ase.expedientes_etapas
	(iExpedientesId, iEtapasId, dtExpedienteFecha, cExpedientesMotivo, cExpedientesIdTramiteDoc, cEtapasUsuarioSis, dtEtapasFechaSis, cEtapasEquipoSis, cEtapasIpSis, cEtapasOpenUrs,cEtapasMAcNicSis)
	VALUES ( @_iExpedientesId, @_iEtapasId, @_dtExpedienteFecha, UPPER(@_ExpedientesMotivo), @_cExpedientesIdTramiteDoc,
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	UPDATE ase.expedientes
	SET cExpedientesIdTramiteDoc = @_cExpedientesIdTramiteDoc,
		iEtapasId=@_iEtapasId,
		/*Campos de auditoria*/
		cExpedientesUsuarioSis=@cUsuarioSis,
		dtExpedientesFechaSis=GETDATE(),
		cExpedientesEquipoSis=@_cEquipoSis,
		cExpedientesIpSis=@_cIpSis,
		cExpedientesOpenUrs='E',
		cExpedientesMacNicSis=@_cMacNicSis
	WHERE iExpedientesId=@_iExpedientesId

	SELECT @iExpedienteEtapasId=@@IDENTITY,@iCodigoError=@@ERROR

	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@iExpedienteEtapasId AS iSalasId
	RETURN @iExpedienteEtapasId

ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0