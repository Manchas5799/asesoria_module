USE UNAM_2020
GO
DROP TABLE ase.citaciones
go



DROP TABLE ase.finalidades
go



DROP TABLE ase.prioridades
go



DROP TABLE ase.personas_casos
go



DROP TABLE ase.calidades_participacion
go



DROP TABLE ase.persona_asesoria
go



DROP TABLE ase.tipo_persona
go



DROP TABLE ase.expedientes_etapas
go






DROP TABLE ase.historial_expedientes
go



DROP TABLE ase.expedientes
go
DROP TABLE ase.etapas
go
DROP TABLE ase.tipos_expedientes
go


DROP TABLE ase.salas
go



CREATE TABLE ase.calidades_participacion
(
	iCalidadesId int IDENTITY ( 1,1 ) ,
	cCalidadesNombre varchar(50) NULL ,
	cCalidadesUsuarioSis varchar(50) NULL ,
	dtCalidadesFechaSis datetime NULL ,
	cCalidadesEquipoSis varchar(50) NULL ,
	cCalidadesIpSis varchar(15) NULL ,
	cCalidadesOpenUrs varchar(1) NULL ,
	cCalidadesMacNicSis varchar(35) NULL
)
go



ALTER TABLE ase.calidades_participacion
	ADD  PRIMARY KEY  CLUSTERED (iCalidadesId ASC)
go



CREATE TABLE ase.citaciones
(
	iCitacionesid int IDENTITY ( 1,1 ) ,
	cCitacionesNumero varchar(100) NULL ,
	dtCitacionesFechaEmision datetime NULL ,
	iExpedientesId int NOT NULL ,
	iPrioridadesId int NULL ,
	dtCitacionesFechaRecibido datetime NULL ,
	dtCitacionesFechaCitacion datetime NULL ,
	cLugarCitacion varchar(200) NULL ,
	iFinalidadesId int NULL ,
	cCitacionesReferencia varchar(200) NULL ,
	cCitacionesUsuarioSis varchar(50) NULL ,
	dtCitacionesFechaSis datetime NULL ,
	cCitacionesEquipoSis varchar(50) NULL ,
	cCitacionesIpSis varchar(15) NULL ,
	cCitacionesOpenUrs varchar(1) NULL ,
	cCitacionesMacNicSis varchar(35) NULL ,
	cCitacionesObservacion varchar(max) NULL ,
	dtCitacionesFechaObservacion datetime NULL ,
	iPersonaAsesoriaId int NULL ,
	cExpedientesIdTramiteDoc varchar(20) NULL ,
	iEtapasId int NULL
)
go



ALTER TABLE ase.citaciones
	ADD  PRIMARY KEY  CLUSTERED (iCitacionesid ASC)
go



CREATE TABLE ase.etapas
(
	iEtapasId int IDENTITY ( 1,1 ) ,
	cEtapasNombre varchar(50) NULL ,
	cEtapasUsuarioSis varchar(50) NULL ,
	dtEtapasFechaSis datetime NULL ,
	cEtapasEquipoSis varchar(50) NULL ,
	cEtapasIpSis varchar(15) NULL ,
	cEtapasOpenUrs varchar(1) NULL ,
	cEtapasMacNicSis varchar(35) NULL
)
go



ALTER TABLE ase.etapas
	ADD  PRIMARY KEY  CLUSTERED (iEtapasId ASC)
go



CREATE TABLE ase.expedientes
(
	iExpedientesId int IDENTITY ( 1,1 ) ,
	cExpedientesNombre varchar(250) NULL ,
	cExpedientesNumero varchar(100) NULL ,
	iSalasId int NULL ,
	cExpedientesUsuarioSis varchar(50) NULL ,
	dtExpedientesFechaSis datetime NULL ,
	cExpedientesEquipoSis varchar(50) NULL ,
	cExpedientesIpSis varchar(15) NULL ,
	cExpedientesOpenUrs varchar(1) NULL ,
	cExpedientesMacNicSis varchar(35) NULL ,
	bExpedientesEstado bit NULL ,
	cExpedientesDescripcion varchar(max) NULL ,
	cExpedienteAgraviado varchar(400) NULL ,
	cExpedienteDelito varchar(200) NULL ,
	iTiposId int NULL ,
	cExpedientesIdTramiteDoc varchar(20) NULL ,
	iEtapasId int NULL
)
go



ALTER TABLE ase.expedientes
	ADD  PRIMARY KEY  CLUSTERED (iExpedientesId ASC)
go



CREATE TABLE ase.expedientes_etapas
(
	iExpedienteEtapasId int IDENTITY ( 1,1 ) ,
	dtExpedienteFecha datetime NULL ,
	iEtapasId int NULL ,
	iExpedientesId int NULL ,
	cEtapasUsuarioSis varchar(50) NULL ,
	dtEtapasFechaSis datetime NULL ,
	cEtapasEquipoSis varchar(50) NULL ,
	cEtapasIpSis varchar(15) NULL ,
	cEtapasOpenUrs varchar(1) NULL ,
	cEtapasMacNicSis varchar(35) NULL ,
	cExpedientesMotivo varchar(max) NULL ,
	cExpedientesIdTramiteDoc varchar(20) NULL
)
go



ALTER TABLE ase.expedientes_etapas
	ADD  PRIMARY KEY  CLUSTERED (iExpedienteEtapasId ASC)
go



CREATE TABLE ase.finalidades
(
	iFinalidadesId int IDENTITY ( 1,1 ) ,
	cFinalidadesNombre varchar(100) NULL ,
	cFinalidadesUsuarioSis varchar(50) NULL ,
	dtFinalidadesFechaSis datetime NULL ,
	cFinalidadesEquipoSis varchar(50) NULL ,
	cFinalidadesIpSis varchar(15) NULL ,
	cFinalidadesOpenUrs varchar(1) NULL ,
	cFinalidadesMacNicSis varchar(35) NULL
)
go



ALTER TABLE ase.finalidades
	ADD  PRIMARY KEY  CLUSTERED (iFinalidadesId ASC)
go



CREATE TABLE ase.historial_expedientes
(
	iHistorialId int IDENTITY ( 1,1 ) ,
	dtHistorialFecha datetime NULL ,
	cHistorialPDF varchar(100) NULL ,
	iExpedientesId int NULL ,
	cHistorialUsuarioSis varchar(50) NULL ,
	dtHistorialFechaSis datetime NULL ,
	cHistorialEquipoSis varchar(50) NULL ,
	cHistorialIpSis varchar(15) NULL ,
	cHistorialOpenUrs varchar(1) NULL ,
	cHistorialMacNicSis varchar(35) NULL ,
	cHistorialObservacion varchar(max) NULL
)
go



ALTER TABLE ase.historial_expedientes
	ADD  PRIMARY KEY  CLUSTERED (iHistorialId ASC)
go



CREATE TABLE ase.persona_asesoria
(
	iPersonaAsesoriaId int IDENTITY ( 1,1 ) ,
	cPersonaAsesoriaUsuarioSis varchar(50) NULL ,
	dtPersonaAsesoriaFechaSis datetime NULL ,
	cPersonaAsesoriaEquipoSis varchar(50) NULL ,
	cPersonaAsesoriaIpSis varchar(15) NULL ,
	cPersonaAsesoriaOpenUrs varchar(1) NULL ,
	cPersonaAsesoriaMacNicSis varchar(35) NULL ,
	iTipoId int NULL ,
	iPersonaId int NOT NULL ,
	bPersonaEstado bit NULL
)
go



ALTER TABLE ase.persona_asesoria
	ADD  PRIMARY KEY  CLUSTERED (iPersonaAsesoriaId ASC)
go



CREATE TABLE ase.personas_casos
(
	iPersonasCasosId int IDENTITY ( 1,1 ) ,
	iPersonaAsesoriaId int NULL ,
	iExpedientesId int NULL ,
	iCalidadesId int NULL ,
	cPersonasCasosUsuarioSis varchar(50) NULL ,
	dPersonasCasosFechaSis datetime NULL ,
	cPersonasCasosEquipoSis varchar(50) NULL ,
	cPersonasCasosIpSis varchar(15) NULL ,
	cPersonasCasosOpenUrs varchar(1) NULL ,
	cPersonasCasosMacNicSis varchar(35) NULL ,
	dtPersonaCasoFecha datetime NULL ,
	iEtapasId int NULL ,
	cPersonaCasoObservacion varchar(max) NULL
)
go



ALTER TABLE ase.personas_casos
	ADD  PRIMARY KEY  CLUSTERED (iPersonasCasosId ASC)
go



CREATE TABLE ase.prioridades
(
	iPrioridadesId int IDENTITY ( 1,1 ) ,
	cPrioridadNombre varchar(25) NULL ,
	cPrioridadesUsuarioSis varchar(50) NULL ,
	dtPrioridadesFechaSis datetime NULL ,
	cPrioridadesEquipoSis varchar(50) NULL ,
	cPrioridadesIpSis varchar(15) NULL ,
	cPrioridadesOpenUrs varchar(1) NULL ,
	cPrioridadesMacNicSis varchar(35) NULL
)
go



ALTER TABLE ase.prioridades
	ADD  PRIMARY KEY  CLUSTERED (iPrioridadesId ASC)
go



CREATE TABLE ase.salas
(
	iSalasId int IDENTITY ( 1,1 ) ,
	cSalasNombre varchar(200) NULL ,
	cSalasDireccion varchar(200) NULL ,
	cSalasUsuarioSis varchar(50) NULL ,
	dtSalasFechaSis datetime NULL ,
	cSalasEquipoSis varchar(50) NULL ,
	cSalasIpSis varchar(15) NULL ,
	cSalasOpenUrs varchar(1) NULL ,
	cSalasMacNicSis varchar(35) NULL
)
go



ALTER TABLE ase.salas
	ADD  PRIMARY KEY  CLUSTERED (iSalasId ASC)
go



CREATE TABLE ase.tipo_persona
(
	iTipoId int IDENTITY ( 1,1 ) ,
	cTipoNombre varchar(50) NULL ,
	cTipoUsuarioSis varchar(50) NULL ,
	dTipoFechaSis datetime NULL ,
	cTipoEquipoSis varchar(50) NULL ,
	cTipoIpSis varchar(15) NULL ,
	cTipoOpenUrs varchar(1) NULL ,
	cTipoMacNicSis varchar(35) NULL
)
go



ALTER TABLE ase.tipo_persona
	ADD  PRIMARY KEY  CLUSTERED (iTipoId ASC)
go



CREATE TABLE ase.tipos_expedientes
(
	iTiposId int IDENTITY (1,1) ,
	cTiposNombre varchar(50) NULL ,
	cTiposUsuarioSis varchar(50) NULL ,
	dtTiposFechaSis datetime NULL ,
	cTiposEquipoSis varchar(50) NULL ,
	cTiposIpSis varchar(15) NULL ,
	cTiposOpenUrs varchar(1) NULL ,
	cTiposMacNicSis varchar(35) NULL
)
go



ALTER TABLE ase.tipos_expedientes
	ADD  PRIMARY KEY  CLUSTERED (iTiposId ASC)
go




ALTER TABLE ase.citaciones
	ADD  FOREIGN KEY (iExpedientesId) REFERENCES ase.expedientes(iExpedientesId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.citaciones
	ADD  FOREIGN KEY (iPrioridadesId) REFERENCES ase.prioridades(iPrioridadesId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.citaciones
	ADD  FOREIGN KEY (iFinalidadesId) REFERENCES ase.finalidades(iFinalidadesId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.citaciones
	ADD  FOREIGN KEY (iPersonaAsesoriaId) REFERENCES ase.persona_asesoria(iPersonaAsesoriaId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.citaciones
	ADD  FOREIGN KEY (iEtapasId) REFERENCES ase.etapas(iEtapasId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.expedientes
	ADD  FOREIGN KEY (iSalasId) REFERENCES ase.salas(iSalasId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.expedientes
	ADD  FOREIGN KEY (iTiposId) REFERENCES ase.tipos_expedientes(iTiposId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.expedientes
	ADD  FOREIGN KEY (iEtapasId) REFERENCES ase.etapas(iEtapasId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.expedientes_etapas
	ADD  FOREIGN KEY (iEtapasId) REFERENCES ase.etapas(iEtapasId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.expedientes_etapas
	ADD  FOREIGN KEY (iExpedientesId) REFERENCES ase.expedientes(iExpedientesId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.historial_expedientes
	ADD  FOREIGN KEY (iExpedientesId) REFERENCES ase.expedientes(iExpedientesId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.persona_asesoria
	ADD  FOREIGN KEY (iTipoId) REFERENCES ase.tipo_persona(iTipoId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.personas_casos
	ADD  FOREIGN KEY (iPersonaAsesoriaId) REFERENCES ase.persona_asesoria(iPersonaAsesoriaId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.personas_casos
	ADD  FOREIGN KEY (iExpedientesId) REFERENCES ase.expedientes(iExpedientesId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.personas_casos
	ADD  FOREIGN KEY (iCalidadesId) REFERENCES ase.calidades_participacion(iCalidadesId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.personas_casos
	ADD  FOREIGN KEY (iEtapasId) REFERENCES ase.etapas(iEtapasId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




exec ase.SP_INS_etapas 'Preparatoria', 125, 'equipo', '127.0.0.1', NULL,NULL
go
exec ase.SP_INS_etapas 'Investigación', 125, 'equipo', '127.0.0.1', NULL,NULL
go
exec ase.SP_INS_etapas 'Acusación', 125, 'equipo', '127.0.0.1', NULL,NULL
go







