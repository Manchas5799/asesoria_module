
CREATE PROCEDURE [ase].[Sp_INS_personas_casos]
	@_iPersonasCasosId INTEGER,
	@_iPersonaAsesoriaId INTEGER,
	@_iExpedientesId INTEGER,
	@_iCalidadesId INTEGER,
	@_dtPersonaCasoFecha DATETIME,
	@_iEtapasId INTEGER,

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Insertar Nuevo Persona caso
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	
	DECLARE @cMensaje VARCHAR(MAX)

	SET @_iPersonasCasosId=RTRIM(LTRIM(@_iPersonasCasosId))

	/*
	IF COALESCE(@_iPersonasCasosId,'') = ''
		BEGIN
			SET @cMensaje='Debe especificar el n�mero de expediente.'
			RAISERROR (@cMensaje,18,1,1)
			GOTO ErrorCapturado
		END
	IF @_iPersonasCasosId IS NOT NULL
		BEGIN
			IF EXISTS(SELECT iPersonasCasosId FROM ase.personas_casos WHERE UPPER(RTRIM(LTRIM(iPersonasCasosId))) COLLATE SQL_Latin1_General_Cp1_CI_AI=UPPER(RTRIM(LTRIM(@_iPersonasCasosId))))
				BEGIN
					SET @cMensaje='nnn '+UPPER(RTRIM(LTRIM(@_iPersonasCasosId)))+' nn...'
					RAISERROR (@cMensaje,18,1,1)
					GOTO ErrorCapturado					
				END
		END
	*/
	
	DECLARE @iPersonasCasosId INTEGER,@iCodigoError INTEGER
	set @_iPersonasCasosId = UPPER(@_iPersonasCasosId)

	INSERT ase.personas_casos
	(iPersonaAsesoriaId, iExpedientesId, iCalidadesId, dtPersonaCasoFecha, iEtapasId, cPersonasCasosUsuarioSis, dPersonasCasosFechaSis, cPersonasCasosEquipoSis, cPersonasCasosIpSis, cPersonasCasosOpenUrs,cPersonasCasosMacNicSis)
	VALUES ( @_iPersonaAsesoriaId, @_iExpedientesId,@_iCalidadesId, @_dtPersonaCasoFecha, @_iEtapasId,
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	SELECT @iPersonasCasosId=@@IDENTITY,@iCodigoError=@@ERROR

	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@iPersonasCasosId AS iExpedientesId
	RETURN @iPersonasCasosId

ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0