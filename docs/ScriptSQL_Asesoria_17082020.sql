
CREATE SCHEMA ase
go



CREATE TABLE ase.calidades_participacion
( 
	iCalidadesId         int IDENTITY ( 1,1 ) ,
	cCalidadesNombre     varchar(50)  NULL ,
	cCalidadesUsuarioSis varchar(50)  NULL ,
	dtCalidadesFechaSis  datetime  NULL ,
	cCalidadesEquipoSis  varchar(50)  NULL ,
	cCalidadesIpSis      varchar(15)  NULL ,
	cCalidadesOpenUrs    varchar(1)  NULL ,
	cCalidadesMacNicSis  varchar(35)  NULL 
)
go



ALTER TABLE ase.calidades_participacion
	ADD  PRIMARY KEY  CLUSTERED (iCalidadesId ASC)
go



CREATE TABLE ase.citaciones
( 
	iCitacionesid        int IDENTITY ( 1,1 ) ,
	cCitacionesNumero    varchar(100)  NULL ,
	dtCitacionesFechaEmision datetime  NULL ,
	iExpedientesId       int  NOT NULL ,
	iPrioridadesId       int  NULL ,
	dtCitacionesFechaRecibido datetime  NULL ,
	dtCitacionesFechaCitacion datetime  NULL ,
	cLugarCitacion       varchar(200)  NULL ,
	iFinalidadesId       int  NULL ,
	cCitacionesReferencia varchar(200)  NULL ,
	cCitacionesUsuarioSis varchar(50)  NULL ,
	dtCitacionesFechaSis datetime  NULL ,
	cCitacionesEquipoSis varchar(50)  NULL ,
	cCitacionesIpSis     varchar(15)  NULL ,
	cCitacionesOpenUrs   varchar(1)  NULL ,
	cCitacionesMacNicSis varchar(35)  NULL ,
	cCitacionesObservacion varchar(max)  NULL ,
	dtCitacionesFechaObservacion datetime  NULL ,
	iPersonaAsesoriaId   int  NULL ,
	cExpedientesIdTramiteDoc varchar(20)  NULL ,
	iEtapasId            int  NULL ,
	cCitacionesPretencionSumilla varchar(max)  NULL ,
	cCitacionesResolucion varchar(100)  NULL 
)
go



ALTER TABLE ase.citaciones
	ADD  PRIMARY KEY  CLUSTERED (iCitacionesid ASC)
go



CREATE TABLE ase.etapas
( 
	iEtapasId            int IDENTITY ( 1,1 ) ,
	cEtapasNombre        varchar(50)  NULL ,
	cEtapasUsuarioSis    varchar(50)  NULL ,
	dtEtapasFechaSis     datetime  NULL ,
	cEtapasEquipoSis     varchar(50)  NULL ,
	cEtapasIpSis         varchar(15)  NULL ,
	cEtapasOpenUrs       varchar(1)  NULL ,
	cEtapasMacNicSis     varchar(35)  NULL 
)
go



ALTER TABLE ase.etapas
	ADD  PRIMARY KEY  CLUSTERED (iEtapasId ASC)
go



CREATE TABLE ase.expedientes
( 
	iExpedientesId       int IDENTITY ( 1,1 ) ,
	cExpedientesNombre   varchar(250)  NULL ,
	cExpedientesNumero   varchar(100)  NULL ,
	iSalasId             int  NULL ,
	cExpedientesUsuarioSis varchar(50)  NULL ,
	dtExpedientesFechaSis datetime  NULL ,
	cExpedientesEquipoSis varchar(50)  NULL ,
	cExpedientesIpSis    varchar(15)  NULL ,
	cExpedientesOpenUrs  varchar(1)  NULL ,
	cExpedientesMacNicSis varchar(35)  NULL ,
	bExpedientesEstado   bit  NULL ,
	cExpedientesDescripcion varchar(max)  NULL ,
	cExpedienteDemandante varchar(400)  NULL ,
	cExpedienteMateria   varchar(200)  NULL ,
	iTiposId             int  NULL ,
	cExpedientesIdTramiteDoc varchar(20)  NULL ,
	cExpedienteDemandado varchar(400)  NULL ,
	cExpedienteActoProcesal varchar(400)  NULL 
)
go



ALTER TABLE ase.expedientes
	ADD  PRIMARY KEY  CLUSTERED (iExpedientesId ASC)
go



CREATE TABLE ase.expedientes_etapas
( 
	iExpedienteEtapasId  int IDENTITY ( 1,1 ) ,
	dtExpedienteFecha    datetime  NULL ,
	iEtapasId            int  NULL ,
	iExpedientesId       int  NULL ,
	cEtapasUsuarioSis    varchar(50)  NULL ,
	dtEtapasFechaSis     datetime  NULL ,
	cEtapasEquipoSis     varchar(50)  NULL ,
	cEtapasIpSis         varchar(15)  NULL ,
	cEtapasOpenUrs       varchar(1)  NULL ,
	cEtapasMacNicSis     varchar(35)  NULL ,
	cExpedientesMotivo   varchar(max)  NULL ,
	cExpedientesIdTramiteDoc varchar(20)  NULL 
)
go



ALTER TABLE ase.expedientes_etapas
	ADD  PRIMARY KEY  CLUSTERED (iExpedienteEtapasId ASC)
go



CREATE TABLE ase.finalidades
( 
	iFinalidadesId       int IDENTITY ( 1,1 ) ,
	cFinalidadesNombre   varchar(100)  NULL ,
	cFinalidadesUsuarioSis varchar(50)  NULL ,
	dtFinalidadesFechaSis datetime  NULL ,
	cFinalidadesEquipoSis varchar(50)  NULL ,
	cFinalidadesIpSis    varchar(15)  NULL ,
	cFinalidadesOpenUrs  varchar(1)  NULL ,
	cFinalidadesMacNicSis varchar(35)  NULL 
)
go



ALTER TABLE ase.finalidades
	ADD  PRIMARY KEY  CLUSTERED (iFinalidadesId ASC)
go



CREATE TABLE ase.historial_expedientes
( 
	iHistorialId         int IDENTITY ( 1,1 ) ,
	dtHistorialFecha     datetime  NULL ,
	cHistorialPDF        varchar(100)  NULL ,
	iExpedientesId       int  NULL ,
	cHistorialUsuarioSis varchar(50)  NULL ,
	dtHistorialFechaSis  datetime  NULL ,
	cHistorialEquipoSis  varchar(50)  NULL ,
	cHistorialIpSis      varchar(15)  NULL ,
	cHistorialOpenUrs    varchar(1)  NULL ,
	cHistorialMacNicSis  varchar(35)  NULL ,
	cHistorialObservacion varchar(max)  NULL 
)
go



ALTER TABLE ase.historial_expedientes
	ADD  PRIMARY KEY  CLUSTERED (iHistorialId ASC)
go



CREATE TABLE ase.persona_asesoria
( 
	iPersonaAsesoriaId   int IDENTITY ( 1,1 ) ,
	cPersonaAsesoriaUsuarioSis varchar(50)  NULL ,
	dtPersonaAsesoriaFechaSis datetime  NULL ,
	cPersonaAsesoriaEquipoSis varchar(50)  NULL ,
	cPersonaAsesoriaIpSis varchar(15)  NULL ,
	cPersonaAsesoriaOpenUrs varchar(1)  NULL ,
	cPersonaAsesoriaMacNicSis varchar(35)  NULL ,
	iTipoId              int  NULL ,
	iPersonaId           int  NOT NULL ,
	bPersonaEstado       bit  NULL 
)
go



ALTER TABLE ase.persona_asesoria
	ADD  PRIMARY KEY  CLUSTERED (iPersonaAsesoriaId ASC)
go



CREATE TABLE ase.personas_casos
( 
	iPersonasCasosId     int IDENTITY ( 1,1 ) ,
	iPersonaAsesoriaId   int  NULL ,
	iExpedientesId       int  NULL ,
	iCalidadesId         int  NULL ,
	cPersonasCasosUsuarioSis varchar(50)  NULL ,
	dPersonasCasosFechaSis datetime  NULL ,
	cPersonasCasosEquipoSis varchar(50)  NULL ,
	cPersonasCasosIpSis  varchar(15)  NULL ,
	cPersonasCasosOpenUrs varchar(1)  NULL ,
	cPersonasCasosMacNicSis varchar(35)  NULL ,
	dtPersonaCasoFecha   datetime  NULL ,
	iEtapasId            int  NULL ,
	cPersonaCasoObservacion varchar(max)  NULL 
)
go



ALTER TABLE ase.personas_casos
	ADD  PRIMARY KEY  CLUSTERED (iPersonasCasosId ASC)
go



CREATE TABLE ase.prioridades
( 
	iPrioridadesId       int IDENTITY ( 1,1 ) ,
	cPrioridadNombre     varchar(25)  NULL ,
	cPrioridadesUsuarioSis varchar(50)  NULL ,
	dtPrioridadesFechaSis datetime  NULL ,
	cPrioridadesEquipoSis varchar(50)  NULL ,
	cPrioridadesIpSis    varchar(15)  NULL ,
	cPrioridadesOpenUrs  varchar(1)  NULL ,
	cPrioridadesMacNicSis varchar(35)  NULL 
)
go



ALTER TABLE ase.prioridades
	ADD  PRIMARY KEY  CLUSTERED (iPrioridadesId ASC)
go



CREATE TABLE ase.salas
( 
	iSalasId             int IDENTITY ( 1,1 ) ,
	cSalasNombre         varchar(200)  NULL ,
	cSalasDireccion      varchar(200)  NULL ,
	cSalasUsuarioSis     varchar(50)  NULL ,
	dtSalasFechaSis      datetime  NULL ,
	cSalasEquipoSis      varchar(50)  NULL ,
	cSalasIpSis          varchar(15)  NULL ,
	cSalasOpenUrs        varchar(1)  NULL ,
	cSalasMacNicSis      varchar(35)  NULL 
)
go



ALTER TABLE ase.salas
	ADD  PRIMARY KEY  CLUSTERED (iSalasId ASC)
go



CREATE TABLE ase.tipo_persona
( 
	iTipoId              int IDENTITY ( 1,1 ) ,
	cTipoNombre          varchar(50)  NULL ,
	cTipoUsuarioSis      varchar(50)  NULL ,
	dTipoFechaSis        datetime  NULL ,
	cTipoEquipoSis       varchar(50)  NULL ,
	cTipoIpSis           varchar(15)  NULL ,
	cTipoOpenUrs         varchar(1)  NULL ,
	cTipoMacNicSis       varchar(35)  NULL 
)
go



ALTER TABLE ase.tipo_persona
	ADD  PRIMARY KEY  CLUSTERED (iTipoId ASC)
go



CREATE TABLE ase.tipos_expedientes
( 
	iTiposId             int  NOT NULL ,
	cTiposNombre         varchar(50)  NULL ,
	cTiposUsuarioSis     varchar(50)  NULL ,
	dtTiposFechaSis      datetime  NULL ,
	cTiposEquipoSis      varchar(50)  NULL ,
	cTiposIpSis          varchar(15)  NULL ,
	cTiposOpenUrs        varchar(1)  NULL ,
	cTiposMacNicSis      varchar(35)  NULL 
)
go



ALTER TABLE ase.tipos_expedientes
	ADD  PRIMARY KEY  CLUSTERED (iTiposId ASC)
go




ALTER TABLE ase.citaciones
	ADD  FOREIGN KEY (iExpedientesId) REFERENCES ase.expedientes(iExpedientesId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.citaciones
	ADD  FOREIGN KEY (iPrioridadesId) REFERENCES ase.prioridades(iPrioridadesId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.citaciones
	ADD  FOREIGN KEY (iFinalidadesId) REFERENCES ase.finalidades(iFinalidadesId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.citaciones
	ADD  FOREIGN KEY (iPersonaAsesoriaId) REFERENCES ase.persona_asesoria(iPersonaAsesoriaId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.citaciones
	ADD  FOREIGN KEY (iEtapasId) REFERENCES ase.etapas(iEtapasId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.expedientes
	ADD  FOREIGN KEY (iSalasId) REFERENCES ase.salas(iSalasId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.expedientes
	ADD  FOREIGN KEY (iTiposId) REFERENCES ase.tipos_expedientes(iTiposId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.expedientes_etapas
	ADD  FOREIGN KEY (iEtapasId) REFERENCES ase.etapas(iEtapasId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.expedientes_etapas
	ADD  FOREIGN KEY (iExpedientesId) REFERENCES ase.expedientes(iExpedientesId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.historial_expedientes
	ADD  FOREIGN KEY (iExpedientesId) REFERENCES ase.expedientes(iExpedientesId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.persona_asesoria
	ADD  FOREIGN KEY (iTipoId) REFERENCES ase.tipo_persona(iTipoId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.personas_casos
	ADD  FOREIGN KEY (iPersonaAsesoriaId) REFERENCES ase.persona_asesoria(iPersonaAsesoriaId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.personas_casos
	ADD  FOREIGN KEY (iExpedientesId) REFERENCES ase.expedientes(iExpedientesId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.personas_casos
	ADD  FOREIGN KEY (iCalidadesId) REFERENCES ase.calidades_participacion(iCalidadesId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE ase.personas_casos
	ADD  FOREIGN KEY (iEtapasId) REFERENCES ase.etapas(iEtapasId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




CREATE PROCEDURE sp_cargarDatos 
 AS 
Insert ase.etapas
  (cEtapasNombre, cEtapasUsuarioSis,dtEtapasFechaSis,cEtapasEquipoSis, cEtapasIpSis,cEtapasOpenUrs, cEtapasMacNicSis)
values
('Preparatoria','usuario', getDate(), 'equipo','127.0.0.1','N','')
go
Insert ase.etapas
  (cEtapasNombre, cEtapasUsuarioSis,dtEtapasFechaSis,cEtapasEquipoSis, cEtapasIpSis,cEtapasOpenUrs, cEtapasMacNicSis)
values
('Investigación','usuario', getDate(), 'equipo','127.0.0.1','N','')
go
Insert ase.etapas
  (cEtapasNombre, cEtapasUsuarioSis,dtEtapasFechaSis,cEtapasEquipoSis, cEtapasIpSis,cEtapasOpenUrs, cEtapasMacNicSis)
values
('Acusación','usuario', getDate(), 'equipo','127.0.0.1','N','')

go




CREATE PROCEDURE [ase].[Sp_INS_tipo_persona]

	@_iTipoId	 INTEGER,

	@_cTipoNombre varchar(50),


	@_iCredId INTEGER,

	@_cEquipoSis VARCHAR(50),

	@_cIpSis VARCHAR(15),

	@_cSalasOpenUrs varchar(1),

	@_cMacNicSis VARCHAR(35)

	/*
	*/

AS

/*
* Insertar Nuevo Tipo Persona
* 
*/


BEGIN TRANSACTION

	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	
	DECLARE @cMensaje VARCHAR(MAX)


	SET @_cTipoNombre=RTRIM(LTRIM(@_cTipoNombre))

	
	IF COALESCE(@_cTipoNombre,'') = ''

		BEGIN

			SET @cMensaje='Debe especificar el nombre del tipo de Persona.'

			RAISERROR (@cMensaje,18,1,1)

			GOTO ErrorCapturado
		END
	IF @_cTipoNombre IS NOT NULL
		BEGIN
			IF EXISTS(SELECT iTipoId FROM ase.tipo_persona WHERE UPPER(RTRIM(LTRIM(cTipoNombre))) COLLATE SQL_Latin1_General_Cp1_CI_AI=UPPER(RTRIM(LTRIM(@_cTipoNombre))))
				BEGIN
					SET @cMensaje='El tipo persona '+UPPER(RTRIM(LTRIM(@_cTipoNombre)))+' ya se encuentra registrado, verifique por favor...'
					RAISERROR (@cMensaje,18,1,1)
					GOTO ErrorCapturado					
				END
		END

	
	DECLARE @iTipoId INTEGER,@iCodigoError INTEGER
	set @_cTipoNombre = UPPER(@_cTipoNombre)

	INSERT ase.tipo_persona
	(cTipoNombre, cTipoUsuarioSis, dTipoFechaSis, cTipoEquipoSis, cTipoIpSis, cTipoOpenUrs,cTipoMAcNicSis)
	VALUES ( UPPER(@_cTipoNombre),
				@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	SELECT @iTipoId=@@IDENTITY,@iCodigoError=@@ERROR

	IF @@ERROR<>0 GOTO ErrorCapturado

	
	COMMIT TRANSACTION

	SELECT 1 AS iResult,@iTipoId AS iTipoId
	RETURN @iTipoId

ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0
go


