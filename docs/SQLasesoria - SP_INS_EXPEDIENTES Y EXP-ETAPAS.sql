
ALTER PROCEDURE [ase].[Sp_INS_expedientes_y_exp_etapas]
	@_iExpedientesId INTEGER,
	@_iSalasId INTEGER,
	@_cExpedientesNombre varchar(250),
	@_cExpedientesNumero varchar(100),
	@_bExpedientesEstado bit,
	@_cExpedienteDescripcion varchar(max),
	@_cExpedientesAgraviado varchar(400),
	@_cExpedienteDelito varchar(200),
	@_iTiposId INTEGER,
	@_cExpedientesIdTramiteDoc varchar(20),

	@_iEtapasId INTEGER,
	@_dtExpedienteFecha DATETIME,
	@_cExpedientesMotivo VARCHAR(MAX),

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Insertar Nuevo EXPEDIENTE y ETAPA_EXPEDIENTE
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	
	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cExpedientesNumero=RTRIM(LTRIM(@_cExpedientesNumero))

	
	IF COALESCE(@_cExpedientesNumero,'') = ''
		BEGIN
			SET @cMensaje='Debe especificar el n�mero de expediente.'
			RAISERROR (@cMensaje,18,1,1)
			GOTO ErrorCapturado
		END
	IF @_cExpedientesNumero IS NOT NULL
		BEGIN
			IF EXISTS(SELECT iExpedientesId FROM ase.expedientes WHERE UPPER(RTRIM(LTRIM(cExpedientesNumero))) COLLATE SQL_Latin1_General_Cp1_CI_AI=UPPER(RTRIM(LTRIM(@_cExpedientesNumero))))
				BEGIN
					SET @cMensaje='El Expediente '+UPPER(RTRIM(LTRIM(@_cExpedientesNumero)))+' ya se encuentra registrado, verifique por favor...'
					RAISERROR (@cMensaje,18,1,1)
					GOTO ErrorCapturado					
				END
		END

	
	DECLARE @iExpedientesId INTEGER,@iCodigoError INTEGER
	set @_cExpedientesNumero = UPPER(@_cExpedientesNumero)

	INSERT ase.expedientes
	(iSalasId, cExpedientesNombre, cExpedientesNumero, bExpedientesEstado, cExpedientesDescripcion, cExpedienteAgraviado, cExpedienteDelito, iTiposId, cExpedientesIdTramiteDoc, cExpedientesUsuarioSis, dtExpedientesFechaSis, cExpedientesEquipoSis, cExpedientesIpSis, cExpedientesOpenUrs,cExpedientesMacNicSis)
	VALUES ( @_iSalasId,UPPER(@_cExpedientesNombre), UPPER(@_cExpedientesNumero), @_bExpedientesEstado, UPPER(@_cExpedienteDescripcion), UPPER(@_cExpedientesAgraviado), UPPER(@_cExpedienteDelito), @_iTiposId, UPPER(@_cExpedientesIdTramiteDoc),
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	SELECT @iExpedientesId=@@IDENTITY,@iCodigoError=@@ERROR

	INSERT ase.expedientes_etapas
	(iExpedientesId, iEtapasId, dtExpedienteFecha, cExpedientesMotivo, cEtapasUsuarioSis, dtEtapasFechaSis, cEtapasEquipoSis, cEtapasIpSis, cEtapasOpenUrs,cEtapasMacNicSis)
	VALUES ( @iExpedientesId,@_iEtapasId, @_dtExpedienteFecha, UPPER(@_cExpedientesMotivo),
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@iExpedientesId AS iExpedientesId
	RETURN @iExpedientesId

ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0